import React, { FC } from 'react'
import * as ReactDOM from 'react-dom'
import { CookiesProvider } from 'react-cookie'
import './settings/i18n'
import { Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'ui/theme'
import { createBrowserHistory as createHistory } from 'history'
import { PersistGate } from 'redux-persist/integration/react'
import PreloadProvider from './containers/PreloadProvider'
import { configureStore } from './store/configureStore'
import { RenderRoutes } from './modules/common/components/Routes'
import { routes } from './routes'
import 'assets/main.css'

const history = createHistory()
const { persistor, store } = configureStore({}, history)

const Index: FC = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <PreloadProvider history={history}>
        <CookiesProvider>
          <ThemeProvider>
            <Router history={history}>
              <RenderRoutes routes={routes} />
            </Router>
          </ThemeProvider>
        </CookiesProvider>
      </PreloadProvider>
    </PersistGate>
  </Provider>
)

ReactDOM.render(<Index />, document.getElementById('root'))
