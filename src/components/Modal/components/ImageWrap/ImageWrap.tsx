import styled from 'ui/theme/styled'

const ImageWrap = styled.img`
  border-radius: ${props => props.theme.borderRadius.xs};
  width: 100%;
`

export default ImageWrap
