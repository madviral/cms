import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'

import WorkSchedule from 'src/modules/common/containers/WorkSchedule'

import { Text, Divider, Button } from 'ui'
import { theme } from 'ui/theme'
import { Box } from 'ui/layout'

import { PlacesScheduleProps } from 'src/components/Modal/components/PlacesSchedule/types/interfaces'
import { DayOfWeekArray } from 'src/modules/common/types'

const PlacesSchedule: FC<PlacesScheduleProps> = ({
  triggerModal,
  schedule,
  onChangeIsDayOff,
  onChangeDayFromTime,
  onChangeDayToTime,
}) => {
  const [t] = useTranslation('')

  return (
    <Box
      width={390}
      borderRadius={theme.borderRadius.m}
      backgroundColor={theme.color.white}
      p={30}
    >
      <Box>
        <Text fontSize={16} color={theme.color.secondary}>
          Время работы
        </Text>
      </Box>

      <Box py={20}>
        <Divider />
      </Box>

      <WorkSchedule
        week={Object.keys(schedule) as DayOfWeekArray}
        onChangeIsDayOff={onChangeIsDayOff}
        onChangeDayFromTime={onChangeDayFromTime}
        onChangeDayToTime={onChangeDayToTime}
        schedule={schedule}
      />

      <Box py={20}>
        <Divider />
      </Box>

      <Box display='flex' justifyContent='flex-end' alignItems='center'>
        <Box mr='10px'>
          <Button
            onClick={() => triggerModal('')}
            appearance='outline'
            size='s'
          >
            {t('common:cancel')}
          </Button>
        </Box>

        <Box>
          <Button appearance='primary' size='s'>
            {t('common:save')}
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

export default PlacesSchedule
