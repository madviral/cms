import { DayOfWeekUnion, IWeekSchedule } from 'src/modules/common/types'

export type PlacesScheduleProps = {
  schedule?: IWeekSchedule
  triggerModal: (type: string) => void
  onChangeDayFromTime: (day: DayOfWeekUnion, value: Date) => void
  onChangeDayToTime: (day: DayOfWeekUnion, value: Date) => void
  onChangeIsDayOff: (day: DayOfWeekUnion, value: boolean) => void
}
