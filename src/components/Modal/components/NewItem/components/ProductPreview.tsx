import React, { FC } from 'react'
import { Box } from 'ui/layout'
import styled from 'ui/theme/styled'
import { Description, Caption, Button } from 'ui'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

const ProductPreviewWrap = styled(Box)`
  width: 100%;
  border-radius: ${props => props.theme.borderRadius.xs};
  background: ${props => props.theme.colorTools.getSecondary(0.1)};
  padding: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const Img = styled.img`
  height: 70px;
  width: 70px;
  border-radius: 10px;
  margin-right: 20px;
  flex-shrink: 0;
`

const Column = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
`

interface ProductPreviewProps {
  productInfo: any
  createDraft: (cb: (id: string) => void) => void
}

const ProductPreview: FC<ProductPreviewProps> = ({
  productInfo,
  createDraft,
}) => {
  const [t] = useTranslation('shop')
  const history = useHistory()
  if (!productInfo) {
    return null
  }
  const pushHistory = (id: string) => {
    history.push(`/shop?productID=${id}`)
  }
  const onUseProductClick = () => {
    createDraft(id => pushHistory(id))
  }
  return (
    <Box mb={20}>
      <Description title={t('similarItems')}>
        <ProductPreviewWrap>
          <Column>
            <Img src={productInfo.images[0].origin} />
            <Box>
              <Caption>#{productInfo.vendorCode}</Caption>
              <Caption>{productInfo.name}</Caption>
            </Box>
          </Column>
          <Column style={{ flexShrink: 0 }}>
            <Button appearance='primary' size='s' disabled>
              {t('seeOnWebsite')}
            </Button>
            <Box ml={10}>
              <Button appearance='primary' size='s' onClick={onUseProductClick}>
                {t('use')}
              </Button>
            </Box>
          </Column>
        </ProductPreviewWrap>
      </Description>
    </Box>
  )
}

// const mapDispatchToProps = {
//   createDraft: createDraftAction,
// }
export default ProductPreview
