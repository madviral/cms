import React, { FC, useState, useEffect } from 'react'
import { Box } from 'ui/layout'
import axios from 'axios'
import { Button, Caption, ClockIcon, IncorrectIcon, CorrectIcon } from 'ui'
import { styled } from 'ui/theme'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'
import { triggerModal } from 'src/modules/common/actions/modal'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { load as reloadMerchantList } from 'src/modules/shop/actions/catalog'

interface FileUploadProps {
  file: File
  setFile: React.Dispatch<React.SetStateAction<File | undefined>>
  percent?: number
}

const StyledCaption = styled(Caption)`
  text-align: center;
  font-size: 12px;
`

const Wrap = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
`

const Uploading: FC<FileUploadProps> = ({ file, percent = 0 }) => {
  const [t] = useTranslation('shop')
  return (
    <Wrap>
      <Box display='flex' alignItems='center' flexDirection='column'>
        <ClockIcon strokeWidth={1} height={60} width={60} color='#FD9F28' />
        <Box mt='35px'>
          <Caption style={{ textAlign: 'center' }}>
            {t('fileUpload', { fileName: file.name })}
          </Caption>
        </Box>
      </Box>
      <Box>
        <Box mb='16px'>
          <StyledCaption>{percent}%</StyledCaption>
        </Box>
        <Button appearance='primary' size='l' disabled fullWidth>
          Ok
        </Button>
      </Box>
    </Wrap>
  )
}

interface CompleteProps {
  file: File
  recognized?: number
  unrecognized?: number
}

const Complete: FC<CompleteProps> = ({
  file,
  recognized = 0,
  unrecognized = 0,
}) => {
  const history = useHistory()
  const [t] = useTranslation('shop')
  const dispatch = useDispatch()
  useEffect(() => {
    if (unrecognized !== 0) {
      history.push('/shop/import')
    }
    dispatch(reloadMerchantList())
  }, [])
  const d = new Date()
  const date = `${d.getDay()}-${d.getMonth()}-${d.getFullYear()}`
  return (
    <Wrap>
      <Box display='flex' alignItems='center' flexDirection='column'>
        <CorrectIcon height={60} width={60} />
        <Box mt='35px'>
          <Caption style={{ textAlign: 'center', fontSize: '14px' }}>
            {t('fileUploadComplete', {
              fileName: file.name,
              date,
            })}
          </Caption>
          <Caption
            style={{ textAlign: 'center', fontSize: '14px', marginTop: 8 }}
          >
            {t('fileUploadCompleteTotalItems', {
              total: recognized + unrecognized,
            })}
          </Caption>
        </Box>
      </Box>
      <Box>
        <Box
          mb='16px'
          display='flex'
          flexDirection='row'
          justifyContent='center'
        >
          {unrecognized ? (
            <>
              <Caption style={{ fontSize: '12px' }}>
                {t('recognizedItems', { recognized })}
              </Caption>
              <Caption style={{ fontSize: '12px', marginLeft: 5 }}>
                {t('unrecognizedItems', { unrecognized })}
              </Caption>
            </>
          ) : (
            <Caption style={{ fontSize: '14px' }}>
              {t('itemsArePublished')}
            </Caption>
          )}
        </Box>
        <Button
          appearance='primary'
          size='l'
          fullWidth
          onClick={() => dispatch(triggerModal())}
        >
          Ok
        </Button>
      </Box>
    </Wrap>
  )
}

const Error: FC<FileUploadProps> = ({ setFile, file }) => {
  const [t] = useTranslation('shop')
  return (
    <Wrap>
      <Box display='flex' alignItems='center' flexDirection='column'>
        <IncorrectIcon strokeWidth={1} color='#FC5368' height={60} width={60} />
        <Box mt='35px'>
          <Caption style={{ textAlign: 'center' }}>
            {t('fileUploadFail', { fileName: file.name })}
          </Caption>
        </Box>
      </Box>
      <Box>
        <Box mb='16px'>
          <Caption style={{ textAlign: 'center', fontSize: '12px' }}>
            {t('probableWrongFile')}
          </Caption>
        </Box>
        <Button
          appearance='primary'
          size='l'
          fullWidth
          onClick={() => setFile(undefined)}
        >
          Ok
        </Button>
      </Box>
    </Wrap>
  )
}

const FileUpload: FC<FileUploadProps> = ({ file, setFile }) => {
  const [uploadStatus, setUploadStatus] = useState({
    uploading: false,
    error: false,
    complete: false,
  })
  const [percent, setPercent] = useState(0)
  const [recognizedCount, setRecognizedCount] = useState(0)
  const [unrecognizedCount, setUnrecognizedCount] = useState(0)
  const { accessToken } = useSelector(
    (state: RootState) => state.common.credentials,
  )
  useEffect(() => {
    const xlsFile = new FormData()
    xlsFile.append('file', file)
    setUploadStatus({
      ...uploadStatus,
      uploading: true,
    })
    axios
      .post('https://api-dev.vlife.kz/market/core/v1/imports/excel', xlsFile, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${accessToken}`,
        },
        onUploadProgress: e => {
          setPercent(Math.round((e.loaded / e.total) * 100))
        },
      })
      .then(res => {
        setRecognizedCount(res.data.recognized)
        setUnrecognizedCount(res.data.unrecognized)
        setUploadStatus({
          ...uploadStatus,
          uploading: false,
          complete: true,
        })
      })
      .catch(() => {
        setUploadStatus({
          ...uploadStatus,
          uploading: false,
          error: true,
        })
      })
  }, [])
  return (
    <Box
      borderRadius='16px'
      bg='#fff'
      width='370px'
      height='300px'
      padding='16px'
    >
      {uploadStatus.uploading && (
        <Uploading file={file} percent={percent} setFile={setFile} />
      )}
      {uploadStatus.error && <Error setFile={setFile} file={file} />}
      {uploadStatus.complete && (
        <Complete
          file={file}
          recognized={recognizedCount}
          unrecognized={unrecognizedCount}
        />
      )}
    </Box>
  )
}

export default FileUpload
