import React, { FC, useState, useEffect, useRef } from 'react'
import styled from 'ui/theme/styled'
import { Box } from 'ui/layout'
import { Button, Caption, FormField, EyeIcon } from 'ui'
import { useTranslation } from 'react-i18next'
import { Option } from 'ui/dropdowns/Treeselect/types'
import { ImageWrap } from '../ImageWrap'
import { FileUpload } from './components'
import { downloadExcelTemplate } from 'src/modules/shop/actions/imports'
import { useDispatch } from 'react-redux'

const UL = styled.ul`
  list-style: inside;
  list-style-type: disc;
  color: ${props => props.theme.colorsList.secondary[0]};
  font-size: 12px;
`

const OL = styled.ol`
  list-style: inside;
  list-style-type: decimal;
  color: ${props => props.theme.colorsList.secondary[0]};
  font-size: 12px;
`

const Footer = styled.div`
  border-top: 1px solid ${props => props.theme.colorsList.secondary[2]};
  padding-top: 16px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const InnerWrap = styled.div`
  padding: 16px;
  padding-top: 32px;
  border-radius: 20px;
  box-sizing: border-box;
  width: 650px;
  background: ${props => props.theme.color.white};
  width: 100%;
  height: 100%;
`

interface NewItem {
  loadCategories: () => void
  loadSuggestion: () => void
  items: Option[]
  loading: boolean
  suggestionItem: any
  createDraft: (cb: (id: string) => void) => void
  triggerModal: (name: string) => void
  triggerNewProductDrawer: () => void
}

const NewItem: FC<NewItem> = ({
  loadCategories,
  items,
  loading,
  triggerModal,
  triggerNewProductDrawer,
}) => {
  const ref = useRef(null)
  const [file, setFile] = useState<File | undefined>()
  const [isFileImportOpen, setIsFileImportOpen] = useState(false)
  const [isAutoUploadOpen, setIsAutoUploadOpen] = useState(false)
  const [t] = useTranslation(['shop', 'common'])
  const dispatch = useDispatch()
  useEffect(() => {
    if (!items && !loading) {
      loadCategories()
    }
  }, [])
  const importFromFile = () => {
    if (ref.current) {
      ref.current.click()
    }
  }
  const onFileUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFile(e.target.files[0])
  }
  const renderAutoUpload = () => (
    <Box p='16px' bg='white' borderRadius='16px' width='640px'>
      <Caption style={{ fontSize: 14, fontWeight: 600, textAlign: 'center' }}>
        {t('autoCatalogUpload')}
      </Caption>
      <Box my='16px'>
        <Caption style={{ fontSize: 12, fontWeight: 600 }}>
          {t('autoUploadInstruction')}
        </Caption>
        <Box mt='16px'>
          <OL>
            <li>{t('autoUploadInstruction1')}</li>
            <li>{t('autoUploadInstruction2')}</li>
            <li>{t('autoUploadInstruction3')}</li>
            <li>{t('autoUploadInstruction4')}</li>
          </OL>
        </Box>
      </Box>
      <Box mb='16px'>
        <FormField
          label={`${t('priceListLink')} *`}
          value=''
          autoFocus
          placeholder='https://help.market.forte.kz/?page_id=606'
        />
        <Box my='16px'>
          <Caption style={{ fontSize: 12, fontWeight: 600 }}>
            {t('authorization')} ({t('optional')})
          </Caption>
        </Box>
        <Box display='grid' gridTemplateColumns='1fr 1fr' gridColumnGap='8px'>
          <FormField value='' label={t('login')} />
          <FormField
            value=''
            label={t('password')}
            type='password'
            icon={<EyeIcon width={20} height={20} />}
          />
        </Box>
      </Box>
      <Footer>
        <Box display='flex' flexDirection='row' alignItems='center'>
          <Button appearance='outline' size='m'>
            {t('check')}
          </Button>
          <Box ml='16px'>check status placeholder</Box>
        </Box>
        <Box display='flex' flexDirection='row'>
          <Button
            appearance='secondary'
            size='m'
            onClick={() => setIsAutoUploadOpen(false)}
          >
            {t('cancel')}
          </Button>
          <Box ml='16px'>
            <Button
              appearance='primary'
              size='m'
              onClick={() => setIsAutoUploadOpen(false)}
            >
              {t('save')}
            </Button>
          </Box>
        </Box>
      </Footer>
    </Box>
  )
  const renderMainMenu = () => (
    <InnerWrap>
      <Box mb={16}>
        <Caption style={{ textAlign: 'center', fontSize: '14px' }}>
          {t('addYourCatalog')}
        </Caption>
      </Box>
      <Box mb={16} style={{ textAlign: 'center' }}>
        <ImageWrap
          src={'/images/productPlaceholder.png'}
          alt={'ProductPlacehoder'}
        />
      </Box>
      <Box mb={2}>
        <Button
          fullWidth
          appearance='primary'
          size='xl'
          onClick={() => {
            triggerNewProductDrawer()
            triggerModal('newItem')
          }}
        >
          + {t('addNewProduct')}
        </Button>
      </Box>
      <Box mb={2}>
        <Button
          fullWidth
          appearance='outline'
          size='xl'
          onClick={() => setIsFileImportOpen(true)}
        >
          {t('importFromFile')}
        </Button>
        <Box mt='8px'>
          <Button
            fullWidth
            appearance='outline'
            size='xl'
            onClick={() => setIsAutoUploadOpen(true)}
          >
            {t('automaticFileImport')}
          </Button>
        </Box>
      </Box>
    </InnerWrap>
  )
  const renderFileImportMenu = () => (
    <Box p='16px' width='800px' bg='white' borderRadius='16px'>
      <Box mb='16px'>
        <Caption style={{ textAlign: 'center', fontSize: 14, fontWeight: 600 }}>
          {t('priceListUpload')}
        </Caption>
      </Box>
      <Box mb='16px'>
        <Caption style={{ fontWeight: 600, fontSize: 12 }}>
          {t('formatRequirement')}
        </Caption>
        <Box mt='16px'>
          <UL>
            <li>{t('formatRequirement1')};</li>
            <li>{t('formatRequirement2')};</li>
            <li>{t('formatRequirement3')};</li>
            <li>{t('formatRequirement4')};</li>
          </UL>
        </Box>
      </Box>
      <Box mb='16px'>
        <Caption style={{ fontWeight: 600, fontSize: 12 }}>
          {t('excelRequirement')}
        </Caption>
        <Box mt='16px'>
          <OL>
            <li>{t('excelRequirement1')}</li>
            <li>{t('excelRequirement2')}</li>
            <li>{t('excelRequirement3')}</li>
            <li>{t('excelRequirement4')}</li>
            <li>{t('excelRequirement5')}</li>
            <li>{t('excelRequirement6')}</li>
            <li>{t('excelRequirement7')}</li>
          </OL>
        </Box>
      </Box>
      <Box mb='16px'>
        <Caption style={{ fontWeight: 600, fontSize: 12 }}>
          {t('terms')}
        </Caption>
        <Box mt='16px'>
          <UL>
            <li>{t('terms1')};</li>
            <li>{t('terms2')};</li>
            <li>{t('terms3')};</li>
            <li>{t('terms4')};</li>
          </UL>
        </Box>
      </Box>
      <Footer>
        <Button
          appearance='outline'
          size='m'
          onClick={() => dispatch(downloadExcelTemplate())}
        >
          {t('downloadExcelPriceTemplate')}
        </Button>
        <Box display='flex' flexDirection='row'>
          <Box mr='16px'>
            <Button
              appearance='secondary'
              size='m'
              onClick={() => setIsFileImportOpen(false)}
            >
              {t('cancel')}
            </Button>
          </Box>
          <Button appearance='primary' size='m' onClick={importFromFile}>
            {t('choseFile')}
          </Button>
          <input
            type='file'
            id='file'
            style={{ display: 'none' }}
            ref={ref}
            accept='.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            onChange={onFileUpload}
          />
        </Box>
      </Footer>
    </Box>
  )
  const renderInnerWrap = () => {
    if (isFileImportOpen) {
      return renderFileImportMenu()
    }
    if (isAutoUploadOpen) {
      return renderAutoUpload()
    }
    return renderMainMenu()
  }
  return (
    <Wrap>
      {file ? <FileUpload file={file} setFile={setFile} /> : renderInnerWrap()}
    </Wrap>
  )
}

export default NewItem
