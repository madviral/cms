import { Dispatch } from 'redux'
import { RootState } from 'src/store/rootReducer'
import { connect } from 'react-redux'

import { triggerModal } from 'src/modules/common/actions/modal'

import { PlacesSchedule } from '../../components'
import { change as changeDay } from 'src/modules/common/actions/workSchedule'

const mapStateToProps = (state: RootState) => ({
  schedule: state.common.workSchedule,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  triggerModal: (type: string) => dispatch(triggerModal(type)),
  onChangeIsDayOff: (day, value) => changeDay('isDayOff', day, value),
  onChangeDayFromTime: (day, value) => changeDay('fromTime', day, value),
  onChangeDayToTime: (day, value) => changeDay('toTime', day, value),
})

export default connect(mapStateToProps, mapDispatchToProps)(PlacesSchedule)
