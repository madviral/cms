import { connect } from 'react-redux'
import { NewItem } from '../components'
import { load as loadCategories } from 'src/modules/common/actions/productCategories'
import { load as loadSuggestion } from 'src/modules/shop/actions/newProduct'
import { triggerModal } from 'src/modules/common/actions/modal'
import { triggerNewProductDrawer } from 'src/modules/common/actions/newProductDrawer'
import { RootState } from 'src/store/rootReducer'

const mapStateToProps = (state: RootState) => ({
  items: state.common.general.productCategories.data,
  loading: state.common.general.productCategories.loading,
  error: state.common.general.productCategories.error,
  suggestionItem: state.shop.newProduct.data,
})

const mapDispatchToProps = {
  loadCategories,
  loadSuggestion,
  triggerModal,
  triggerNewProductDrawer,
}

export default connect(mapStateToProps, mapDispatchToProps)(NewItem)
