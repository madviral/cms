import styled from 'ui/theme/styled'
import React, { FC, useRef } from 'react'
import ReactDOM from 'react-dom'
import { useDetectOutsideClick, useDetectKeyboardPress } from 'lib/hooks'
import { NewItem, PlacesSchedule } from './containers'

// import { useHistory } from 'react-router-dom'

interface ModalProps {
  isOpen: boolean
  triggerModal: () => void
  id?: number
  domNode?: string
  type: string
}

interface ModalWrapProps {
  isOpen: boolean
}

const ModalWrap = styled.div<ModalWrapProps>`
  display: ${props => (props.isOpen ? 'block' : 'none')};
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 100;
  &:before {
    content: '';
    z-index: 1;
    display: block;
    height: 100%;
    width: 100%;
    position: absolute;
    background: ${props => props.theme.color.primary};
    background: #000000;
    opacity: 0.15;
  }
`

const ModalBody = styled.div`
  z-index: 1;
  min-width: 400px;
  min-height: 450px;
`

// TODO: add the domNode?
const InnerWrap: FC<ModalProps> = props => {
  const renderBody = {
    newItem: (
      <NewItem
        createDraft={() => {
          return
        }}
      />
    ),
    placesSchedule: <PlacesSchedule />,
  }

  // const history = useHistory()
  // useEffect(() => {
  //   return history.listen(() => {
  //     props.triggerModal()
  //   })
  // }, [history])
  const ref = useRef()
  useDetectOutsideClick(ref, props.triggerModal)
  useDetectKeyboardPress('Escape', props.triggerModal)
  const onClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (e.target === e.currentTarget) {
      props.triggerModal()
    }
  }
  return ReactDOM.createPortal(
    <ModalWrap isOpen={props.isOpen} ref={ref} onClick={onClick}>
      <ModalBody>{renderBody[props.type]}</ModalBody>
    </ModalWrap>,
    document.querySelector(props.domNode || '#root'),
  )
}

const Modal: FC<ModalProps> = props => {
  if (!props.isOpen) {
    return null
  }
  // remove modal from dom
  return <InnerWrap {...props} />
}

export default Modal
