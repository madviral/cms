import React, { useContext, useState } from 'react'
import { Sidebar as BaseSidebar, UserInfo, Rights } from 'src/ui/sidebar'
import { Box } from 'ui/layout'
import SidebarContext from 'src/context/SidebarContext'
import { Condition } from 'ui/condition'
import NavMenu from './NavMenu'
import MainRoutesMenu from './MainRoutesMenu'
import { SidebarProps } from './types'

let delayId
const Sidebar = ({
  fio,
  jobPosition,
  navigationRoutes,
  mainRoutes,
}: SidebarProps) => {
  const [toggled, setToggled] = useContext(SidebarContext)
  const [hovered, setHovered] = useState(false)
  const showed = toggled && !hovered

  return (
    <BaseSidebar
      toggled={toggled}
      hovered={hovered}
      onToggle={() => setToggled(!toggled)}
      onMouseEnter={() => {
        if (toggled) {
          delayId = window.setTimeout(() => {
            setHovered(true)
          }, 300)
        }
      }}
      onMouseLeave={() => {
        clearTimeout(delayId)
        setHovered(false)
      }}
    >
      <div>
        <Box mb='8px' mx='16px'>
          <MainRoutesMenu routes={mainRoutes} toggled={showed} />
        </Box>
        <Condition match={navigationRoutes?.length > 0}>
          <Box mb='8px'>
            <NavMenu toggled={showed} routes={navigationRoutes} />
          </Box>
        </Condition>
        <Box>
          <UserInfo
            toggled={showed}
            name={fio}
            surname=''
            position={jobPosition}
          />
        </Box>
      </div>
      <div>
        <Rights toggled={showed} />
      </div>
    </BaseSidebar>
  )
}

export default Sidebar
