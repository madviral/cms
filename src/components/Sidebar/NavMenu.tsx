import React, { FC } from 'react'
import { MenuLink } from 'ui/sidebar'
import { NavMenuProps } from './types'
import styled from 'ui/theme/styled'

const Wrap = styled.div`
  margin: 0 16px;
  overflow: hidden;
`

const Edge = styled.div`
  height: 8px;
  background: ${props => props.theme.colorsList.secondary[5]};
`

const Top = styled(Edge)`
  border-radius: 8px 8px 0 0;
`

const Bottom = styled(Edge)`
  border-radius: 0 0 8px 8px;
`

const NavMenu: FC<NavMenuProps> = ({ routes, toggled }) => (
  <Wrap>
    <Top />
    {routes
      .filter(route => !route.skip)
      .map(route => (
        <MenuLink
          key={route.key}
          toggled={toggled}
          icon={route.nav.icon}
          name={route.nav.name}
          path={route.path}
          exact={route.exact}
          childs={route.routes}
        />
      ))}
    <Bottom />
  </Wrap>
)

NavMenu.defaultProps = {
  routes: [],
}

export default NavMenu
