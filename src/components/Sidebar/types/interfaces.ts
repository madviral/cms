import { IRoute } from 'src/modules/common/types'

interface ToggledProps {
  toggled?: boolean
}
export interface NavProps {}

export interface UserInfoProps extends ToggledProps {}

export interface RightsProps extends ToggledProps {}

export type SidebarProps = {
  fio?: string
  token?: string
  jobPosition?: string
  navigationRoutes?: IRoute[]
  mainRoutes: any[]
  onLoad?: () => void
}

export interface NavMenuProps extends ToggledProps {
  routes?: IRoute[]
}
