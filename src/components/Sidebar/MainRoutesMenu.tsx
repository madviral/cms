import React, { FC, useState, useRef } from 'react'
import styled from 'ui/theme/styled'
import { CurrentMainRoute, MainRoute } from 'ui'
import { useLocation } from 'react-router-dom'
import { useDetectOutsideClick } from 'lib/hooks'
import { NavMenuProps } from './types'

const Wrap = styled.div`
  position: relative;
  z-index: 2;
`

const RoutesWrap = styled.div`
  margin-top: 8px;
  padding: 8px 0;
  box-shadow: 0px 0px 8px #d9e2ec;
  border-radius: 8px;
  position: absolute;
  background: #fff;
  width: 100%;
`

const MainRoutesMenu: FC<NavMenuProps> = ({ routes, toggled }) => {
  const ref = useRef()
  const [isOpen, setIsOpen] = useState(false)
  const { pathname } = useLocation()
  const currentRoutePath = `/${pathname.split('/')[1]}`
  const notMainRoute = !routes.some(route => route.path === currentRoutePath)
  const currentRoute = notMainRoute
    ? routes[0]
    : routes.filter(r => r.path === currentRoutePath)[0]
  useDetectOutsideClick(ref, () => setIsOpen(false))
  const renderRoutes = () =>
    routes.map(routeProps => (
      <MainRoute
        {...routeProps}
        icon={routeProps.nav.icon}
        onClick={() => setIsOpen(false)}
        toggled={toggled}
        link={routeProps.path}
        name={routeProps.nav.name}
      />
    ))

  return (
    <Wrap ref={ref}>
      <CurrentMainRoute
        route={currentRoute}
        toggled={toggled}
        onClick={() => setIsOpen(!isOpen)}
      />
      {!toggled && isOpen ? <RoutesWrap>{renderRoutes()}</RoutesWrap> : null}
    </Wrap>
  )
}

export default MainRoutesMenu
