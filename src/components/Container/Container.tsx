import React, { FC } from 'react'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'

const Container: FC = ({ children }) => (
  <Box display='grid' width='100%' height='100vh'>
    <Box
      m={20}
      py={64}
      px={100}
      borderRadius={theme.borderRadius.xl}
      backgroundColor={theme.color.paleBlue}
    >
      {children}
    </Box>
  </Box>
)

export default Container
