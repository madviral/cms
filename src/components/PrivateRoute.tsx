import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'

const PrivateRoute = ({ accessToken, component: Component, ...rest }) => (
  <Route
    render={props =>
      accessToken ? (
        <Component {...props} {...rest} />
      ) : (
        <Redirect
          to={{ pathname: '/auth/signin', state: { from: props.location } }}
        />
      )
    }
  />
)

export default connect((state: any) => ({
  accessToken: state.common.credentials.accessToken,
}))(PrivateRoute)
