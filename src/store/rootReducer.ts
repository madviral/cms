import { combineReducers, Action } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { AxiosInstance } from 'axios'
import { signIn, bankList, companyTypeList } from 'src/modules/auth'
import shop from 'src/modules/shop/reducers'
import manager from 'src/modules/manager/reducers/newMerchantDrawer'
import newMerchant from 'src/modules/manager/reducers/newMerchantForm'
import feed from 'src/modules/feed/reducers'
import common from 'src/modules/common/reducers'
import services from 'src/modules/services/reducers'

export const rootReducer = combineReducers({
  common,
  services,
  shop,
  signIn,
  bankList,
  companyTypeList,
  feed,
  manager,
  newMerchant,
})

export type RootState = ReturnType<typeof rootReducer>

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  AxiosInstance,
  Action<string>
>
