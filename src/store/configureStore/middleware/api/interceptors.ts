/* tslint:disable */
import { AxiosInstance } from 'axios'

class AxiosInterceptors {
  private client: AxiosInstance

  constructor(client) {
    this.client = client
  }

  getRequestSuccess(getState) {
    return function(config) {
      const token = getState().common.credentials
      if (config.url.indexOf('refresh') >= 0) {
        return config
      }
      return {
        ...config,
        headers: {
          ...config.headers,
          Authorization: 'Bearer ' + (token as any).accessToken,
        },
      }
    }
  }

  getRequestError() {
    return function(error) {
      return Promise.reject(error)
    }
  }

  getResponseSuccess() {
    return function(response) {
      return response
    }
  }

  getResponseError(getState, onAuthCb, onLogoutCb) {
    const ctx = this
    const refreshUrl = 'auth/user/refresh'
    return function(error) {
      if (error.response.status !== 401 && error.config.url !== refreshUrl) {
        return Promise.reject(error.toJSON())
      }

      // Logout user if token refresh didn't work or user is disabled
      if (error.config.url === refreshUrl) {
        onLogoutCb()

        return Promise.reject(error)
      }

      // Try request again with new token
      const token = getState().common.credentials
      return ctx.client
        .post(refreshUrl, {
          refreshToken: token.refreshToken,
        })
        .then(response => {
          const { data } = response
          onAuthCb(data)
          // New request with new token
          const config = error.config
          config.headers['Authorization'] = `Bearer ${data.token}`

          return new Promise((resolve, reject) => {
            ctx.client
              .request(config)
              .then(res => {
                resolve(res)
              })
              .catch(resError => {
                reject(resError.toJSON)
              })
          })
        })
        .catch(resError => {
          Promise.reject(resError.toJSON())
        })
    }
  }
}

export default AxiosInterceptors
