import { createStore, Store, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk, { ThunkMiddleware } from 'redux-thunk'
import { History } from 'history'
import { persistStore, Persistor } from 'redux-persist'
import { rootReducer } from '../rootReducer'
import apiMiddleware from './middleware/api'

interface ConfigureStore {
  persistor: Persistor
  store: Store<any>
}

const configureStore = (initialState: {}, history: History): ConfigureStore => {
  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(
      applyMiddleware(apiMiddleware(history), thunk as ThunkMiddleware),
    ),
  )

  if ((module as any).hot) {
    // tslint:disable-next-line
    ;(module as any).hot.accept('../rootReducer', () =>
      store.replaceReducer(rootReducer),
    )
  }

  const persistor = persistStore(store)

  return { persistor, store }
}

export default configureStore
