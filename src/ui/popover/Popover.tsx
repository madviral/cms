import React, { FC, useRef, useState } from 'react'
import useDetectOutsideClick from 'lib/hooks/useDetectOutsideClick'
import { PopoverProps, PopoverElemProps } from './types/interfaces'

import styled from 'ui/theme/styled'
import { theme } from 'ui/theme'
import { Text } from 'ui'
import { Box } from 'ui/layout'

const PopoverContainer = styled.div`
  position: relative;
`

const PopoverElem = styled.div<PopoverElemProps>`
  background: #fff;
  box-shadow: 0px 0px 26px -8px rgba(219, 228, 254, 1);
  position: absolute;
  border-radius: 10px;
  z-index: 99;
  width: 200px;
  ${props =>
    props.position === 'top' &&
    `
    bottom: 100%;
    right: 50%;
    transform: translateX(50%);
  `}
  ${props =>
    props.position === 'right' &&
    `
    left: 100%;
    bottom: 50%;
    transform: translateY(50%);
  `}
  ${props =>
    props.position === 'bottom' &&
    `
    top: 100%;
    left: 50%;
    transform: translateX(-50%);
  `}
  ${props =>
    props.position === 'left' &&
    `
    right: 100%;
    bottom: 50%;
    transform: translateY(50%);
  `}

  // &::before {
  //   content: '';
  //   display: block;
  //   position: absolute;
  //   box-shadow: -10px 0px 16px -19px rgba(219, 228, 254, 1);
  //   width: 0;
  //   height: 0;
  //   border-style: solid;
  //   border-width: 14px 18px 14px 0;
  //   right: 100%;
  //   top: 50%;
  //   transform: translateY(-50%);
  //   border-color: transparent #fff transparent transparent;
  // }
`

const PopoverElemItem = styled.div`
  padding: 13px 18px;
  cursor: pointer;
`

const Popover: FC<PopoverProps> = ({
  children,
  position,
  updateRow,
  deleteRow,
}) => {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false)

  const wrapperRef = useRef<HTMLDivElement>()
  useDetectOutsideClick(wrapperRef, () => setIsPopoverOpen(false))

  return (
    <PopoverContainer ref={wrapperRef}>
      <Box
        onClick={e => {
          e.stopPropagation()
          setIsPopoverOpen(!isPopoverOpen)
        }}
      >
        {children}
      </Box>

      {isPopoverOpen && (
        <PopoverElem position={position}>
          <PopoverElemItem onClick={updateRow}>
            <Text fontSize={13} color={theme.color.secondary}>
              Редактировать
            </Text>
          </PopoverElemItem>

          <PopoverElemItem onClick={deleteRow}>
            <Text fontSize={13} color={theme.color.secondary}>
              Удалить клиента
            </Text>
          </PopoverElemItem>
        </PopoverElem>
      )}
    </PopoverContainer>
  )
}

Popover.defaultProps = {
  position: 'bottom',
}

export default Popover
