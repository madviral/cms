type Placement = 'top' | 'left' | 'right' | 'bottom'

export interface PopoverProps {
  isPopoverOpen?: boolean
  position?: Placement
  deleteRow?: () => void
  updateRow?: () => void
}

export interface PopoverElemProps {
  position: Placement
}
