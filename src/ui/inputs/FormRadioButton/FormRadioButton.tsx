import React, { FC } from 'react'
import { FormRadioButtonProps } from './interfaces'
import styled from 'ui/theme/styled'

const RadioButton = styled.div`
  input {
    position: absolute;
    display: none;

    & + label {
      position: relative;
      cursor: pointer;
      padding: 0;
      font-size: 14px;
      line-height: 14px;
      color: ${props => props.theme.color.secondary};
    }

    & + label:before {
      content: '';
      margin-right: 20px;
      display: inline-block;
      vertical-align: text-top;
      width: 20px;
      height: 20px;
      border: 2px solid ${props => props.theme.color.grayishBlue};
      box-sizing: border-box;
      border-radius: ${props => props.theme.borderRadius.r};
      background-color: transparent;
      transform: translateY(-2px);
    }

    &:checked + label:before,
    &:hover + label:before {
      border: 2px solid ${props => props.theme.color.primary};
    }

    &:checked + label:after {
      content: '';
      position: absolute;
      top: 4px;
      left: 4px;
      width: 12px;
      height: 12px;
      background-color: ${props => props.theme.color.primary};
      border-radius: ${props => props.theme.borderRadius.r};
      transition: all 0.2s ease;
      transform: translateY(-2px);
    }
  }
`

const FormRadioButton: FC<FormRadioButtonProps> = ({
  id,
  value,
  name,
  label,
  checked,
  onChange,
}) => {
  return (
    <RadioButton>
      <input
        type='radio'
        id={id}
        value={value}
        name={name}
        checked={checked}
        onChange={event => onChange(event)}
      />
      <label htmlFor={id}>{label}</label>
    </RadioButton>
  )
}

export default FormRadioButton
