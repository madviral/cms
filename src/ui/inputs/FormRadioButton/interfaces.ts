export interface FormRadioButtonProps {
  id: string
  name: string
  label?: string
  value?: string | number
  checked?: boolean
  onChange?: (event) => void
}
