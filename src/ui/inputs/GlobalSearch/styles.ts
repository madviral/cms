import { styled } from 'ui/theme'
import { ifProp } from 'styled-tools'
import { IStyledSearchWrapper } from './types/interfaces'

export const StyledSearchWrapper = styled.div<IStyledSearchWrapper>`
  display: flex;
  height: 32px;
  align-items: center;
  position: relative;

  .__loupeButton {
    outline: none;
    border: none;
    padding: 0;
    height: 40px;
    width: 40px;
    align-items: center;
    justify-content: center;
    background: transparent;
    cursor: pointer;
    position: absolute;
    display: ${({ searchIsOpen }) => (!searchIsOpen ? 'flex' : 'none')};
    right: 0;
    z-index: 1;
  }
`

export const StyledSearchInputWrapper = styled.div<IStyledSearchWrapper>`
  position: relative;
  display: flex;
  height: 32px;
  width: 100%;
  ${ifProp(
    'searchIsOpen',
    `
    min-width: 340px;
    transition: opacity 200ms ease-out;
    opacity: 1;
  `,
    `
    min-width: auto;
    transition: none;
    opacity: 0;
  `,
  )}

  .__loupeIcon {
    position: absolute;
    transform: translateY(-50%);
    top: 50%;
    left: 16px;
  }
`

export const StyledSearchInput = styled.input`
  width: 100%;
  border: none;
  border-radius: 8px;
  outline: none;
  padding: 8px 0 8px 44px;
  font-size: 12px;
  font-family: ${props => props.theme.fontFamily.ms};
`
export const StyledCloseButton = styled.button`
  background: transparent;
  padding: 0;
  border: none;
  outline: none;
  position: absolute;
  transform: translateY(-50%);
  top: 50%;
  right: 16px;
  display: flex;
  align-ttems: center;
  cursor: pointer;
`
