import React, { FC, useState } from 'react'
import { LoupeIcon } from 'ui/icons'
import GlobalSearchInput from './GlobalSearchInput'
import { GlobalSearchProps } from './types'
import { StyledSearchWrapper } from './styles'

const defaultProps = {
  isAbleToHide: true,
  isDefaultOpen: false,
}

const GlobalSearch: FC<GlobalSearchProps> = ({
  isAbleToHide,
  isDefaultOpen,
}) => {
  const [searchIsOpen, setSearchIsOpen] = useState(isDefaultOpen)

  const handleCloseSearch = () => {
    setSearchIsOpen(false)
  }

  return (
    <StyledSearchWrapper searchIsOpen={searchIsOpen}>
      <button className='__loupeButton' onClick={() => setSearchIsOpen(true)}>
        <LoupeIcon width={18} height={18} />
      </button>
      <GlobalSearchInput
        onCloseSearch={handleCloseSearch}
        searchIsOpen={searchIsOpen}
        isAbleToHide={isAbleToHide}
      />
    </StyledSearchWrapper>
  )
}

GlobalSearch.defaultProps = defaultProps

export default GlobalSearch
