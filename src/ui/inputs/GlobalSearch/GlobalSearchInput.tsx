import React, { createRef, FC, useEffect, useState } from 'react'
import { LoupeIcon, CrossIcon } from 'ui/icons'
import { theme } from 'ui/theme'
import { Condition } from 'ui/condition'
import { GlobalSearchInputProps } from './types'
import {
  StyledCloseButton,
  StyledSearchInput,
  StyledSearchInputWrapper,
} from './styles'

const GlobalSearchInput: FC<GlobalSearchInputProps> = ({
  isAbleToHide,
  onCloseSearch,
  searchIsOpen,
}) => {
  const [searchText, setSearchText] = useState('')
  const inputRef = createRef<HTMLInputElement>()

  useEffect(() => {
    if (searchIsOpen && isAbleToHide) {
      inputRef.current.focus()
    }
  }, [searchIsOpen, searchText])

  const handleChange = event => {
    setSearchText(event.target.value)
  }

  const handleClose = () => {
    if (searchText === '' && isAbleToHide) {
      onCloseSearch()
    } else {
      setSearchText('')
    }
  }

  return (
    <StyledSearchInputWrapper searchIsOpen={searchIsOpen}>
      <LoupeIcon
        className='__loupeIcon'
        width={18}
        height={18}
        color={theme.colorsList.secondary[1]}
      />
      <StyledSearchInput
        ref={inputRef}
        type='text'
        value={searchText}
        onChange={handleChange}
        placeholder='Поиск'
      />
      <Condition match={searchText.length > 0}>
        <StyledCloseButton onClick={handleClose}>
          <CrossIcon
            width={20}
            height={20}
            color={theme.colorsList.secondary[1]}
          />
        </StyledCloseButton>
      </Condition>
    </StyledSearchInputWrapper>
  )
}

export default GlobalSearchInput
