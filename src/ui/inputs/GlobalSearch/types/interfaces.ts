export interface GlobalSearchProps {
  isAbleToHide?: boolean
  isDefaultOpen?: boolean
}

export interface GlobalSearchInputProps extends GlobalSearchProps {
  onCloseSearch: () => void
  searchIsOpen: boolean
}

export type IStyledSearchWrapper = {
  searchIsOpen?: boolean
}
