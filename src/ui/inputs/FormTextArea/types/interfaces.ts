export interface FormTextAreaProps {
  disabled?: boolean
  label?: string
  name?: string
  value: string
  height?: string
  placeholder?: string
  onChange?: (event) => void
}

export interface TextAreaProps {
  height: string
}
