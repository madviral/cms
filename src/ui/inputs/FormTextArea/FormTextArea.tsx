import React, { FC } from 'react'

import {
  FormTextAreaProps,
  TextAreaProps,
} from 'ui/inputs/FormTextArea/types/interfaces'
import { Text } from 'ui/text'
import { styled, theme } from 'ui/theme'
import { Box } from 'ui/layout'

const TextArea = styled.textarea<TextAreaProps>`
  color: ${props => props.theme.color.secondary};
  border: 1px solid ${props => props.theme.colorsList.secondary[2]};
  border-radius: ${props => props.theme.borderRadius.xs};
  height: ${props => props.height || '100px'};
  min-height: 58px;
  resize: vertical;
  outline: none;
  width: 100%;
  padding: 12px 16px;
  font-size: 12px;
  box-sizing: border-box;

  &::placeholder {
    font-size: 12px;
    color: ${props => props.theme.color.grayishBlue};
  }
`

const FormTextArea: FC<FormTextAreaProps> = ({
  name,
  height,
  placeholder,
  label,
  value,
  onChange,
  disabled,
}) => (
  <div>
    <Box mb='4px'>
      <Text fontSize={12} color={theme.color.secondary}>
        {label}
      </Text>
    </Box>

    <TextArea
      height={height}
      name={name}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      disabled={disabled}
    />
  </div>
)

export default FormTextArea
