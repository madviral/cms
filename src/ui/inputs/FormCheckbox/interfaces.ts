export interface FormCheckboxProps {
  id: string | number
  elemId?: number
  name: string
  label?: string
  value?: string | number
  outline?: boolean
  indeterminate?: boolean
  checked?: boolean
  defaultChecked?: boolean
  disabled?: boolean
  onChange?: (event, id: any) => void
}

export interface CheckboxStyleProps {
  outline: boolean
  plane?: boolean
  indeterminate?: boolean
}
