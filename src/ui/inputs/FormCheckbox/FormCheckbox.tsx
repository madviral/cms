import React, { FC } from 'react'
import { FormCheckboxProps, CheckboxStyleProps } from './interfaces'
import styled from 'ui/theme/styled'
import { ifProp } from 'styled-tools'

const Checkbox = styled.div<CheckboxStyleProps>`
  input {
    position: absolute;
    display: none;

    & + label {
      position: relative;
      cursor: pointer;
      padding: 0;
      font-size: 14px;
      line-height: 14px;
      top: 2px;
      color: ${props => props.theme.color.secondary};
    }

    & + label:before {
      content: '';
      display: inline-block;
      vertical-align: text-top;
      width: 20px;
      height: 20px;
      border: 1px solid ${props => props.theme.color.grayishBlue};
      box-sizing: border-box;
      border-radius: ${props => props.theme.borderRadius.xxs};
      background-color: transparent;
      ${props =>
        props.outline
          ? `border: 1px solid ${props.theme.color.lightBlue};`
          : ''}
      transform: translateY(-2px);
      transition: background-color ease-in 150ms, border-color ease-in 150ms;
      ${ifProp('plane', ``, `margin-right: 20px;`)}
    }

    &:hover + label:before {
      border: none;
      background-color: ${props => props.theme.color.primary};
      ${props =>
        props.outline
          ? `border: 1px solid ${props.theme.color.lightBlue}; background-color: transparent;`
          : ''};
    }

    &:checked + label:before {
      border: none;
      background-color: ${props => props.theme.color.primary};
      ${props =>
        props.outline
          ? `border: 1px solid ${props.theme.color.lightBlue}; background-color: transparent;`
          : ''};
    }

    &:disabled + label {
      color: ${props => props.theme.color.grayishBlue};
      cursor: auto;
    }

    &:disabled + label:before {
      border: none;
      background-color: ${props => props.theme.color.lightGrayishBlue};
    }

    & + label:after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      display: none;
    }

    ${ifProp(
      'indeterminate',
      props => `
      & + label:before {
        background-color: ${props.theme.color.primary};
        border-color: ${props.theme.color.primary};
      }
      & + label:after {
        display: block;
        width: 8px;
        height: 2px;
        background-color: white;
      }
    `,
    )}

    &:checked + label:after {
      display: block;
      width: 4px;
      height: 10px;
      border-bottom: 2px solid;
      border-right: 2px solid;
      border-color: ${props =>
        props.outline ? props.theme.color.primary : props.theme.color.white};
      transform: translate(-50%, -60%) rotate(45deg);
    }
  }
`

const FormCheckbox: FC<FormCheckboxProps> = ({
  id,
  elemId,
  value,
  name,
  label,
  outline,
  checked,
  disabled,
  indeterminate,
  defaultChecked,
  onChange,
}) => {
  return (
    <Checkbox
      outline={outline}
      plane={!label}
      indeterminate={!checked && indeterminate}
      onClick={e => e.stopPropagation()}
    >
      <input
        type='checkbox'
        id={String(id)}
        value={value}
        name={name}
        defaultChecked={defaultChecked}
        checked={checked}
        disabled={disabled}
        onChange={event => onChange(event, elemId)}
      />
      <label htmlFor={String(id)}>{label}</label>
    </Checkbox>
  )
}

export default FormCheckbox
