```js
import React, { useState, Fragment } from 'react'
import styled from 'ui/theme/styled'
import { FormField } from './FormField'
import { Appearance } from './FormField/types/interfaces'
import { theme } from 'ui/theme'
import { FormCheckbox } from './FormCheckbox'
import { FormRadioButton } from './FormRadioButton'
import { EyeIcon } from 'ui/icons'

const Wrapper = styled.div`
  .formField {
    display: block;
    margin-bottom: 20px;
  }
  .form {
    display: flex;
  }
  .formBox {
    flex: 1;
    display: flex;
    flex-direction: column;
  }
`

const radioItems = [
  { id: 'id1', value: 'red', label: 'Red', name: 'radio' },
  { id: 'id2', value: 'green', label: 'Green', name: 'radio' },
  { id: 'id3', value: 'blue', label: 'Blue', name: 'radio' },
]

const checkboxItems = [
  { id: '1', label: 'First', value: 'first', name: 'checkbox', checked: true },
  { id: '2', label: 'Second', value: 'second', name: 'checkbox' },
  { id: '3', label: 'Third', value: 'third', name: 'checkbox' },
]

const [inputObj, setInputObj] = useState({
  value: '',
  type: 'password',
  iconHide: false,
})

const [miniRoundedInput, setMiniRoundedInput] = useState('')

const [radioChecked, setRadioChecked] = useState('green')
const [checkboxChecked, setCheckboxChecked] = useState(new Map())

const handleInputValueChange = value => {
  const val = value
  setInputObj(prevState => {
    return { ...prevState, value: val }
  })
}

const handleIconClick = () => {
  const hide = !inputObj.iconHide
  const newType = hide ? 'text' : 'password'
  setInputObj(prevState => {
    return { ...prevState, type: newType, iconHide: hide }
  })
}

const handleCheckBoxChange = (event, id) => {
  checkboxChecked.set(id, event.target.value)
  setCheckboxChecked(checkboxChecked)
}

const handleRadioButtonChange = event => {
  setRadioChecked(event.target.value)
}

;<Wrapper>
  <div className='formField'>
    <FormField
      id='1'
      type={inputObj.type}
      value={inputObj.value}
      label='An example label'
      extraLabel='Забыли пароль?'
      placeholder='This is a test input'
      icon={<EyeIcon style={{ width: 20, height: 20 }} />}
      onChange={handleInputValueChange}
      onIconClick={handleIconClick}
    />
  </div>

   <div className='formField'>
     <FormField
       id='2'
       type='text'
       value={miniRoundedInput}
       placeholder='Mini rounded input'
       borderRadius={theme.borderRadius.m}
       appearance={Appearance.mini}
       onChange={value => setMiniRoundedInput(value)}
     />
   </div>  

  <div className='form'>
    <div className='formBox'>
      {radioItems.map(radio => (
        <FormRadioButton
          key={radio.id}
          id={radio.id}
          value={radio.value}
          name={radio.name}
          label={radio.label}
          checked={radioChecked === radio.value}
          onChange={handleRadioButtonChange}
        />
      ))}
    </div>
    <div className='formBox'>
      {checkboxItems.map(checkbox => (
        <FormCheckbox
          key={checkbox.id}
          id={checkbox.id}
          name={checkbox.name}
          label={checkbox.label}
          value={checkbox.value}
          defaultChecked={checkbox.checked || checkboxChecked.get(checkbox.id)}
          outline={checkbox.outline}
          disabled={checkbox.disabled}
          onChange={handleCheckBoxChange}
        />
      ))}
    </div>
  </div>
</Wrapper>
```
