import { styled } from 'ui/theme'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;

  .__formFieldLabel {
    margin-bottom: 8px;
    font-size: 14px;
    line-height: 14px;
    color: #627d98;
  }

  .__inputWrapper {
    display: flex;
  }

  .__counter {
    max-width: 94px;
    border-radius: 0;
    text-align: center;
  }

  input {
    width: 100%;
    display: block;
    padding: 19px;
    font-size: 12px;
    line-height: 14px;
    border: 1px solid #d9e2ec;
    border-radius: 8px;
    color: #627d98;
    box-sizing: border-box;
    outline: none;

    &::placeholder {
      font-size: 12px;
      line-height: 14px;
      color: #627d98;
    }

    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
      webkitappearance: none;
    }

    &[type='number'] {
      webkitappearance: textfield;
    }
  }
`

export const FieldButton = styled.button`
  padding: 16px;
  backgroundcolor: #fff;
  border: 1px solid #d9e2ec;
  outline: none;
  cursor: pointer;

  &.left {
    border-right: none;
    border-radius: 8px 0 0 8px;
  }

  &.right {
    border-left: none;
    border-radius: 0 8px 8px 0;
  }
`
