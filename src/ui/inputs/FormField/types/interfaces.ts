import { ReactNode } from 'react'
import { CSSObject } from '@emotion/serialize'

export enum Appearance {
  large = 'large',
  default = 'default',
  mini = 'mini',
}

export interface GeneralFormFieldProps {
  appearance?: Appearance
  borderRadius?: string
  labelWidth?: string
  isInlineLabel?: boolean
  disabled?: boolean
  labelFontSize?: number
  error?: string
}

export interface FormFieldProps {
  value: string | number
  label?: string
  borderRadius?: string
  labelWidth?: string
  appearance?: Appearance.default | Appearance.mini | Appearance.large
  isInlineLabel?: boolean
  icon?: ReactNode
  inputSx?: CSSObject
  onEnter?: (value?: string) => void
  onIconClick?: (hide?: boolean) => void
  labelFontSize?: number
  error?: string
  shouldDisplayError?: boolean
}

export interface FormFieldNumberProps extends FormFieldProps {
  step: number
  onUp?: (value?: string) => void
  onDown?: (value?: string) => void
}

export interface FormInputProps {
  isInlineLabel: boolean
}
