// import React, { FC } from 'react'
// import clsx from 'clsx'
// import { FormFieldNumberProps } from './types/interfaces'
// import { Wrapper, FieldButton } from './styles'
// import { PlusIcon } from 'ui/icons/Plus'
// import { MinusIcon } from 'ui/icons/Minus'

// const FormFieldNumber: FC<FormFieldNumberProps & React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>> = ({
//   value,
//   label,
//   step,
//   onChange,
//   ...props
// }) => {
//   const count = Number(value)

//   const counterIncrement = () => {
//     return onChange(count + step)
//   }

//   const counterDecrement = () => {
//     if (count > 0 && count >= step) {
//       return onChange(count - step)
//     } else if (count - step < 0) {
//       return onChange(0)
//     }
//   }

//   return (
//     <Wrapper>
//       <label className='__formFieldLabel' htmlFor={props.id}>
//         {label}
//       </label>
//       <div className='__inputWrapper'>
//         <FieldButton
//           type='button'
//           className='left'
//           onClick={() => counterDecrement()}
//         >
//           <MinusIcon />
//         </FieldButton>
//         <input
//           className={clsx('__counter')}
//           value={value}
//           onChange={event => onChange(event)}
//           onKeyDown={e => {
//             if (e.keyCode === 38) {
//               e.preventDefault()
//               counterIncrement()
//             } else if (e.keyCode === 40) {
//               e.preventDefault()
//               counterDecrement()
//             }
//           }}
//         />
//         <FieldButton
//           type='button'
//           className='right'
//           onClick={() => counterIncrement()}
//         >
//           <PlusIcon />
//         </FieldButton>
//       </div>
//     </Wrapper>
//   )
// }

// export default FormFieldNumber
