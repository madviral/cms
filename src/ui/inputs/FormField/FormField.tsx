import React, { forwardRef, FC, useState, useMemo } from 'react'
import { ifProp } from 'styled-tools'
import { styled, theme } from 'ui/theme'
import { sx, WithSXProps } from 'ui/theme/utils'
import {
  Appearance,
  FormFieldProps,
  FormInputProps,
  GeneralFormFieldProps,
} from './types/interfaces'

const GeneralFormField = styled.div<GeneralFormFieldProps & WithSXProps>`
  width: 100%;
  display: flex;
  flex-direction: ${props => (props.isInlineLabel ? 'row' : 'column')};
  ${props =>
    props.isInlineLabel &&
    `
    align-items: center;
  `}

  .formFieldLabel {
    font-size: ${props => (props.labelFontSize ? props.labelFontSize : '14')}px;
    line-height: 14px;
    color: ${theme.color.secondary};
    ${props =>
      props.isInlineLabel &&
      `
      margin-right: 20px;
      width: ${props.labelWidth ? props.labelWidth : 'auto'};
      text-align: right;
    `}
  }

  input {
    width: 100%;
    display: block;
    ${props =>
      props.appearance === Appearance.mini &&
      `
        height: 32px;
        padding: 8px 12px;
        font-size: 12px;
        line-height: 12px;
      `}
    ${props =>
      props.appearance === Appearance.default &&
      `
        height: 40px;
        padding: 12px 15px;
        font-size: 12px;
        line-height: 14px;
      `}
    ${props =>
      props.appearance === Appearance.large &&
      `
      height: 54px;
      padding: 20px;
      font-size: 16px;
      line-height: 16px;
      margin-top: 8px;
    `}
    border: 1px solid;
    ${ifProp(
      'disabled',
      props => `
      border-color: ${props.theme.colorsList.secondary[3]};
      &::placeholder {
        color: ${props.theme.colorsList.secondary[3]};
      }
    `,
      props => `
      color: ${props.theme.color.secondary};
      border-color: ${
        props.error
          ? props.theme.color.alert
          : props.theme.color.lightGrayishBlue
      };
      &::placeholder {
        color: ${props.theme.color.secondary};
      }
    `,
    )}
    border-radius: ${props => props.borderRadius};
    box-sizing: border-box;
    outline: none;

    &::placeholder {
      color: ${props => props.theme.colorsList.secondary[1]};
      font-size: 12px;
      line-height: 14px;
    }
    ${sx};
  }
`

const FormTextBox = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 8px;
`

const FormInput = styled.div<FormInputProps>`
  position: relative;
  ${props =>
    props.isInlineLabel &&
    `
     width: 100%;
  `}
`

const FormInputIcon = styled.button`
  position: absolute;
  top: calc(50% - 10px);
  right: 20px;
  cursor: pointer;
  border: none;
  outline: none;
  background-color: transparent;
`

const FormField: FC<FormFieldProps &
  WithSXProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >> = forwardRef(
  (
    {
      value,
      label,
      borderRadius,
      icon,
      onChange,
      onEnter,
      onIconClick,
      appearance,
      isInlineLabel,
      labelWidth,
      inputSx,
      labelFontSize,
      error,
      shouldDisplayError,
      ...props
    },
    ref,
  ) => {
    const [errorState, setErrorState] = useState('')

    useMemo(() => {
      if (shouldDisplayError) {
        setErrorState(error)
      }
    }, [shouldDisplayError])

    return (
      <GeneralFormField
        appearance={appearance}
        borderRadius={borderRadius}
        isInlineLabel={isInlineLabel}
        labelWidth={labelWidth}
        disabled={props.disabled}
        sx={inputSx}
        labelFontSize={labelFontSize}
        error={errorState}
      >
        {label && (
          <FormTextBox>
            <label className='formFieldLabel' htmlFor={props.id}>
              {label}
            </label>
          </FormTextBox>
        )}
        <FormInput isInlineLabel={isInlineLabel}>
          <input
            value={value}
            ref={ref}
            {...props}
            onChange={event => onChange(event)}
            onBlur={() => (value ? setErrorState('') : setErrorState(error))}
            onKeyPress={e => {
              if (e.key === 'Enter') {
                onEnter((e.target as HTMLInputElement).value)
              }
            }}
          />
          {icon && (
            <FormInputIcon type='button' onClick={() => onIconClick()}>
              {icon}
            </FormInputIcon>
          )}
        </FormInput>
        {(shouldDisplayError || errorState) && (
          <label
            style={{
              fontSize: '10px',
              lineHeight: '10px',
              position: 'relative',
              display: 'inline-block',
              marginTop: '4px',
              color: theme.color.alert,
            }}
          >
            {errorState}
          </label>
        )}
      </GeneralFormField>
    )
  },
)

FormField.defaultProps = {
  appearance: Appearance.default,
  borderRadius: theme.borderRadius.xs,
  onEnter: () => {
    return
  },
}

export default FormField
