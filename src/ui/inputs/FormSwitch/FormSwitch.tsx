import React, { FC } from 'react'
import Switch, { ReactSwitchProps } from 'react-switch'
import { Box } from 'ui/layout'
import styled from 'ui/theme/styled'

interface FormSwitchProps extends ReactSwitchProps {
  label?: string
}

interface LabelProps {
  label: string
}

const LabelWrap = styled(Box)`
  margin-left: 8px;
  font-weight: 500;
  font-size: 14px;
  line-height: 18px;
  color: ${props => props.theme.colorsList.secondary[0]};
`

const Label: FC<LabelProps> = ({ label }) => <LabelWrap>{label}</LabelWrap>

const FormSwitch: FC<FormSwitchProps> = ({
  checked,
  onChange,
  label,
  ...props
}) => (
  <Box display='flex' flexDirection='row'>
    <Switch
      checked={checked}
      onChange={onChange}
      {...props}
      checkedIcon={false}
      uncheckedIcon={false}
      height={20}
      width={36}
      offColor='#D9E2EC'
      onColor='#4D77FB'
    />
    {label ? <Label label={label} /> : null}
  </Box>
)

export default FormSwitch
