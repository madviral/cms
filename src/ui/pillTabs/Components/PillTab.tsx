import React, { FC } from 'react'

import { PillTabProps, TabTitleProps } from 'ui/pillTabs/types/interfaces'
import { styled } from 'ui/theme'

const Tab = styled.div`
  min-height: 36px;
  max-height: 36px;
  border-radius: 18px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1 1 100%;
  cursor: pointer;
  z-index: 1;
`

const TabTitle = styled.p<TabTitleProps>`
  font-size: 12px;
  line-height: 14px;
  transition: all 0.12s ease-in;
  text-align: center;
  color: ${props =>
    props.isActive ? props.theme.color.white : props.theme.color.secondary};
`

const PillTab: FC<PillTabProps> = ({ title, onTabClick, value, isActive }) => {
  const onCurrentTabClick = () => {
    onTabClick(value)
  }
  return (
    <Tab onClick={onCurrentTabClick}>
      <TabTitle isActive={isActive}>{title}</TabTitle>
    </Tab>
  )
}

export default PillTab
