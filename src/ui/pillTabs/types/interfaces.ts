type Tabs = {
  title: string
  value: string | number
}

export interface TabTitleProps {
  isActive: boolean
}

export interface PillTabProps {
  title: string
  isActive: boolean
  value: string | number
  onTabClick: (value: string | number) => void
}

export interface ActiveTabProps {
  length: number
  activeTabIndex: number
}

export interface PillTabsProps {
  tabs: Tabs[]
  onActiveTabChange: (value: any) => void
}
