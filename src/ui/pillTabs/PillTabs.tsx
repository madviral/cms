import React, { FC, useEffect, useState } from 'react'

import { PillTab } from './Components'
import { styled } from 'ui/theme'
import { ActiveTabProps, PillTabsProps } from 'ui/pillTabs/types/interfaces'

const Tabs = styled.div`
  display: flex;
  min-height: 36px;
  max-height: 36px;
  border-radius: 18px;
  background: #f5f5f5;
  position: relative;
`

const ActiveTabs = styled.div<ActiveTabProps>`
  position: absolute;
  background: #4d77fb;
  border-radius: 18px;
  transition: all 80ms ease;
  min-height: 36px;
  max-height: 36px;
  ${props => `
    width: calc(100% / ${props.length});
    left: ${(props.activeTabIndex / props.length) * 100}%;
  `}
`

const PillTabs: FC<PillTabsProps> = ({ tabs, onActiveTabChange }) => {
  const [activeTabValue, setActiveTabValue] = useState(undefined)
  const [activeTabIndex, setActiveTabIndex] = useState(0)

  useEffect(() => {
    onActiveTabChange(activeTabValue)
  }, [activeTabValue])

  useEffect(() => {
    if (tabs.length) {
      setActiveTabValue(tabs[0]?.value)
    }
  }, [tabs.length])

  return (
    <Tabs>
      {tabs.map((tab, index) => (
        <PillTab
          onTabClick={value => {
            setActiveTabValue(value)
            setActiveTabIndex(index)
          }}
          isActive={activeTabValue === tab.value}
          key={`Tab${index}`}
          {...tab}
        />
      ))}

      <ActiveTabs length={tabs.length} activeTabIndex={activeTabIndex} />
    </Tabs>
  )
}

export default PillTabs
