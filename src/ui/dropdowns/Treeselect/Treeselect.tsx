import React, { FC, useState, useRef, useMemo } from 'react'
import { MenuProps } from 'ui/dropdowns/shared/types'
import { Appearance, TreeselectProps } from './types'
import {
  OptionsDisplay,
  ChevronIconWrap,
  CrossIconWrap,
  OptionHeaderInput,
} from './components'
import { useDetectOutsideClick } from 'lib/hooks'
import StyledTreeselect from './StyledTreeselect'
import { Chevron, CrossIcon } from 'ui'
import { styled, theme } from 'ui/theme'

interface AbsoluteWrapperTypes {
  isAbsolute: boolean
  height: string
}

const Wrap = styled.div<AbsoluteWrapperTypes>`
  position: relative;
  height: ${props => (props.isAbsolute ? props.height : 'min-content')};
  width: 100%;
`

const Treeselect: FC<TreeselectProps & MenuProps> = ({
  placeholder,
  options,
  isAbsolute,
  isSingleItemSelector,
  callback,
  appearance,
  error,
  shouldDisplayError,
  value,
}) => {
  const ref = useRef<HTMLDivElement>()
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [bufferOptionArray, setBufferOptionArray] = useState([])
  const [items, setItems] = useState([])
  const [errorState, setErrorState] = useState('')
  useDetectOutsideClick(ref, () => setIsMenuOpen(false))

  const useSetSingleItem = (item: any) => {
    callback(item)
    setBufferOptionArray([])
    setIsMenuOpen(false)
    setItems([item])
    setErrorState('')
  }

  /**
   * Fills an array of options when user starts typing. Can't filter the tree depth > 1
   * @param {string} valueToCompare Value from input
   */
  const inputOptionBufferFill = (valueToCompare: string) => {
    setBufferOptionArray(
      options.filter(option => {
        if (option.name.toLowerCase().includes(valueToCompare.toLowerCase())) {
          return [option, ...bufferOptionArray]
        }
      }),
    )
  }

  useMemo(() => {
    if (shouldDisplayError) {
      setErrorState(error)
      setItems([])
    }
  }, [shouldDisplayError])

  const handleInput = e => {
    e.preventDefault()

    setItems([e.target.value])
    inputOptionBufferFill(e.target.value)
    items[0].name ? setErrorState('') : setErrorState(error)

    if (e.target.value === '') {
      setBufferOptionArray([])
    }
  }

  const selectedIds = items.map(el => el.id)
  return (
    <Wrap
      ref={ref}
      isAbsolute={isAbsolute}
      height={appearance === Appearance.mini ? '32px' : '40px'}
    >
      <StyledTreeselect
        placeholder={placeholder}
        isMenuOpen={isMenuOpen}
        setIsMenuOpen={setIsMenuOpen}
        isAbsolute={isAbsolute}
        height={appearance === Appearance.mini ? '32px' : '40px'}
        hasItems={items[0] !== '' && items.length > 0}
        error={errorState}
      >
        <OptionHeaderInput
          value={
            isSingleItemSelector && items.length ? items[0].name : value || ''
          }
          placeholder={placeholder}
          onChange={handleInput}
          height={appearance === Appearance.mini ? '32px' : '40px'}
        />
        {items[0] !== '' && items.length > 0 && (
          <CrossIconWrap
            onClick={e => {
              e.stopPropagation()
              setItems([])
              setBufferOptionArray([])
              callback(selectedIds, 'deleteItem')
              setErrorState(error)
            }}
          >
            <CrossIcon width={16} height={16} color='#9FB3C8' />
          </CrossIconWrap>
        )}
        <ChevronIconWrap isOpen={isMenuOpen}>
          <Chevron
            color={
              errorState ? theme.color.alert : theme.colorsList.secondary[1]
            }
          />
        </ChevronIconWrap>
        {isMenuOpen && (
          <OptionsDisplay
            options={bufferOptionArray.length ? bufferOptionArray : options}
            handleItemClick={useSetSingleItem}
            selectedIds={selectedIds}
          />
        )}
      </StyledTreeselect>
      {(shouldDisplayError || errorState) && (
        <label
          style={{
            fontSize: '10px',
            lineHeight: '10px',
            display: 'inline-block',
            marginTop: '44px',
            color: theme.color.alert,
            position: 'absolute',
            width: '110%',
          }}
        >
          {errorState}
        </label>
      )}
    </Wrap>
  )
}

Treeselect.defaultProps = {
  isSingleItemSelector: true,
  isAbsolute: false,
  callback: () => {
    return
  },
}
export default Treeselect
