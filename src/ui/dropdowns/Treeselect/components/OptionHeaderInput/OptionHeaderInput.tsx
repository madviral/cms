import React, { FC } from 'react'
import { styled } from 'ui/theme'

interface OptionHeaderInputProps {
  value?: string
  placeholder?: string
  onChange: (e: any) => void
  height: string
}

const Wrap = styled.input`
  height: calc(${props => props.height} - 2px);
  border: none;
  outline: none;
  color: ${props => props.theme.colorsList.secondary[0]};
  margin-left: 16px;
  box-sizing: border-box;
  font-size: 12px;
`

const OptionHeaderInput: FC<OptionHeaderInputProps> = ({
  value,
  placeholder,
  onChange,
  height,
}) => {
  return (
    <Wrap
      type={'text'}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      height={height}
    />
  )
}

export default OptionHeaderInput
