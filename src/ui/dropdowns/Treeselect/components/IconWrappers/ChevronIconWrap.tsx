import styled from 'ui/theme/styled'

interface ChevronIconWrapTypes {
  isOpen: boolean
}

const ChevronIconWrap = styled.div<ChevronIconWrapTypes>`
  height: 100%;
  width: max-content;
  display: flex;
  align-items: center;
  justify-content: center;
  transform: rotate(${props => (props.isOpen ? '180deg' : '0deg')});
  & > svg {
    display: block;
  }
`

export default ChevronIconWrap
