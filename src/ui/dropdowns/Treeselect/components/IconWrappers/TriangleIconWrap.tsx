import styled from 'ui/theme/styled'

interface TriangleIconWrapTypes {
  isOpen: boolean
}

const TriangleIconWrap = styled.div<TriangleIconWrapTypes>`
  height: 40px;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  margin-right: 5px;
  float: left;
  transform: rotate(${props => (props.isOpen ? '0deg' : '-90deg')});
`

export default TriangleIconWrap
