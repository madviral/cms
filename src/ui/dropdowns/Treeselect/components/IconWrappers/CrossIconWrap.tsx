import styled from 'ui/theme/styled'

const CrossIconWrap = styled.div`
  height: 100%;
  width: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  & > svg {
    display: block;
  }
  &:hover > svg > svg > path {
    fill: ${props => props.theme.colorsList.secondary[0]};
  }
`

export default CrossIconWrap
