import React, { FC } from 'react'
import { OptionsDisplayProps } from '../../types'
import styled from 'ui/theme/styled'
import { MenuProps } from 'ui/dropdowns/shared/types'
import { Option } from '../'

const ScrollbarWrap = styled.div`
  overflow: hidden;
  grid-column: 1/-1;
  border-bottom-right-radius: 8px;
  border-bottom-left-radius: 8px;
`

const OptionsDisplayStyledWrapper = styled.div`
  width: auto;
  max-height: 160px;
  height: fit-content;
  overflow: auto;
  border-top: 1px solid ${props => props.theme.colorsList.secondary[0]};
  overflow-x: hidden;
  background: white;
  z-index: 101;
`

const OptionsDisplay: FC<OptionsDisplayProps & MenuProps> = ({
  options,
  handleItemClick,
  selectedIds,
}) => {
  return (
    <ScrollbarWrap>
      <OptionsDisplayStyledWrapper>
        {options.map((option, index) => (
          <Option
            key={option.id}
            option={option}
            handleItemClick={handleItemClick}
            selectedIds={selectedIds}
            showCount={option.children && option.children.length > 0}
          />
        ))}
      </OptionsDisplayStyledWrapper>
    </ScrollbarWrap>
  )
}

export default OptionsDisplay
