import React, { FC, useState } from 'react'
import { styled } from 'ui/theme'
import { Triangle } from 'ui'
import { TriangleIconWrap } from '../'

interface OptionWrapProps {
  selectable: boolean
  paddingMultiplier: number
  selected: boolean
  isOpen: boolean
}

const CaptionWrapper = styled.div`
  line-height: 40px;
  width: max-content;
  display: inline-block;
`

// This 'paddingMultiplier >= 3 contidion' is forced by figma.
// At some point (tree depth is 3 and more) the paddings do not
// obey the multiplication by 16. I had to add 8
const Wrap = styled.div<OptionWrapProps>`
  line-height: 40px;
  width: calc(100% + ${props => props.paddingMultiplier * 16}px);
  display: inline-block;
  cursor: pointer;
  box-sizing: border-box;
  padding: 0 16px 0
    ${props =>
      props.paddingMultiplier >= 3
        ? props.paddingMultiplier * 16 + 8
        : props.paddingMultiplier * 16}px;
  margin: 0 0 0 ${props => (props.paddingMultiplier - 1) * -16}px;
  font-size: 12px;
  color: ${props => props.theme.colorsList.secondary[0]};
  &:hover {
    background: ${props => props.theme.colorsList.secondary[5]};
    ${props => props.isOpen && 'background: white;'}
  }
  ${props =>
    props.selected &&
    `

    font-weight: 600;
    background: ${props.theme.colorTools.getPrimary(0.35)};
    & > div > p {
      font-weight: 600;
    }
  `}
`

interface OptionProps {
  option: {
    id: number
    name: string
    description: string
    isTabbedChildren: boolean
    children?: any[]
  }
  handleItemClick: (item: any) => void
  padding?: number
  selectedIds: number[]
  showCount?: boolean
  paddingMultiplier?: number
  setIsChildLockingParent?: (value: any) => void
  childClick?: boolean
}

const Option: FC<OptionProps> = ({
  option,
  handleItemClick,
  paddingMultiplier = 1,
  selectedIds,
  showCount,
  childClick,
}) => {
  const [isOpen, setIsOpen] = useState(false)
  const [isChildClick, setIsChildClick] = useState(childClick)
  const [branchDepth] = useState(
    option.children && option.children.length > 0
      ? 1 + paddingMultiplier
      : paddingMultiplier,
  )

  const onClick = e => {
    e.stopPropagation()

    if (!option.children || !option.children.length) {
      handleItemClick(option)
    } else {
      setIsOpen(!isOpen)
    }
  }

  return (
    <Wrap
      selected={selectedIds.includes(option.id)}
      paddingMultiplier={paddingMultiplier}
      selectable={!option.children}
      onClick={onClick}
      isOpen={isOpen}
    >
      {option.children && option.children.length > 0 && (
        <TriangleIconWrap isOpen={isOpen}>
          <Triangle />
        </TriangleIconWrap>
      )}
      <CaptionWrapper>
        {option.name +
          ' ' +
          (showCount ? '(' + option.children.length + ')' : '')}
      </CaptionWrapper>
      {(option.children && option.children.length > 0 && isOpen) || isChildClick
        ? option.children.map((item, index) => {
            return (
              <Option
                key={item.id}
                option={item}
                handleItemClick={handleItemClick}
                selectedIds={selectedIds}
                paddingMultiplier={branchDepth + 1}
                showCount={item.children.length > 0}
                setIsChildLockingParent={setIsChildClick}
              />
            )
          })
        : null}
    </Wrap>
  )
}

export default Option
