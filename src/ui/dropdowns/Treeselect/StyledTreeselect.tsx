import React, { FC } from 'react'
import styled from 'ui/theme/styled'
import { theme } from 'ui/theme'
import { HeaderTypes } from '../shared/Header/types'

interface WrapProps {
  isOpen: boolean
  borderColor: string
  hasItems: boolean
  isAbsolute: boolean
  height: string
  error?: string
}

const Wrap = styled.div<WrapProps>`
  z-index: ${props => props.isOpen && 102};
  width: 100%;
  box-sizing: border-box;
  user-select: none;
  cursor: pointer;
  background-color: white;
  display: grid;
  grid-template-columns: calc(100% - 40px) 40px;
  ${props =>
    props.hasItems && 'grid-template-columns: calc(100% - 80px) 40px 40px;'}
  ${props => props.isAbsolute && 'position: absolute;'}

  border: 1px solid ${props =>
    props.error
      ? props.theme.color.alert
      : props.theme.colorsList.secondary[2]};
  border-radius: ${props => props.theme.borderRadius.xs};

  ${props =>
    props.isOpen &&
    `
      grid-template-rows: calc(${props.height} - 2px) auto;
      max-height: 217px;
    `}

  & > p {
    line-height: ${props => props.height};
  }

  &:hover {
    border: 1px solid ${props =>
      props.error
        ? props.theme.color.alert
        : props.theme.colorsList.secondary[0]};

    & > div > svg > path {
      fill: ${props => props.theme.colorsList.secondary[0]};
    }
  }
`

interface StyledTreeselectProps {
  hasItems: boolean
  isAbsolute: boolean
  height: string
  error?: string
}

const StyledTreeselect: FC<HeaderTypes & StyledTreeselectProps> = ({
  setIsMenuOpen,
  isMenuOpen,
  borderColor,
  hasItems,
  children,
  isAbsolute,
  height,
  error,
}) => {
  return (
    <Wrap
      onClick={() => setIsMenuOpen(!isMenuOpen)}
      borderColor={borderColor}
      isOpen={isMenuOpen}
      hasItems={hasItems}
      isAbsolute={isAbsolute}
      height={height}
      error={error}
    >
      {children}
    </Wrap>
  )
}

StyledTreeselect.defaultProps = {
  type: 'default',
  borderColor: theme.color.primary,
}
export default StyledTreeselect
