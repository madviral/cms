export default [
  {
    id: 2002,
    name: 'Телефоны и гаджеты',
    description: 'Телефоны и гаджеты',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/phone.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/phone.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
  {
    id: 20023,
    name: 'Телефоны и приколы',
    description: 'Телефоны и гаджеты',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/phone.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/phone.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
  {
    id: 2013,
    name: 'Парфюмерия и косметика',
    description: 'Парфюмерия и косметика',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/beauty.pdf',
        svg: 'https://static.vlife.kz/icons/beauty.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/beauty.pdf',
        svg: 'https://static.vlife.kz/icons/beauty.svg',
      },
    },
    isTabbedChildren: true,
    children: [
      {
        id: 2104,
        name: 'Женщины',
        description: 'Женщины',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 3406,
            name: 'Ароматы для дома',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3496,
            name: 'Макияж для губ',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3532,
            name: 'Уход за ногтями',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3550,
            name: 'Солнечная линия',
            description: 'Солнечная линия',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3405,
            name: 'Уход за волосами',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 25272,
            name: 'Дезодоранты',
            description: 'Дезодоранты',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 25277,
            name: 'Уход для тела',
            description: 'Уход для тела',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3569,
            name: 'Макияж для глаз',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3408,
            name: 'Аксессуары',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3458,
            name: 'Уход за телом',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 25269,
            name: 'Макияж для лица',
            description: 'Макияж для лица',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 25266,
            name: 'Макияж для бровей',
            description: 'Макияж для бровей',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3563,
            name: 'Уход за лицом',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3499,
            name: 'Парфюмерия',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
      {
        id: 2105,
        name: 'Мужчины',
        description: 'Мужчины',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 13458,
            name: 'Уход за телом',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13550,
            name: 'Солнечная линия',
            description: 'Солнечная линия',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 35272,
            name: 'Дезодоранты',
            description: 'Дезодоранты',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 35277,
            name: 'Уход для тела',
            description: 'Уход для тела',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13408,
            name: 'Аксессуары',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 35269,
            name: 'Макияж для лица',
            description: 'Макияж для лица',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13406,
            name: 'Ароматы для дома',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13569,
            name: 'Макияж для глаз',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13496,
            name: 'Макияж для губ',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13405,
            name: 'Уход за волосами',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 35266,
            name: 'Макияж для бровей',
            description: 'Макияж для бровей',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 35273,
            name: 'Для бритья',
            description: 'Для бритья',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13499,
            name: 'Парфюмерия',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13532,
            name: 'Уход за ногтями',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 13563,
            name: 'Уход за лицом',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
    ],
  },
  {
    id: 2009,
    name: 'Строительство и ремонт',
    description: 'Строительство и ремонт',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/tools.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/tools.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [
      {
        id: 1330,
        name: 'Интерьер',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 24946,
            name: 'Сезонные товары',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 24947,
            name: 'Текстиль',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 24945,
            name: 'Аксессуары для интерьера',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 24948,
            name: 'Домашний декор',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 24942,
            name: 'Столовые приборы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 24943,
            name: 'Постельные и банные принадлежности',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 24944,
            name: 'Посуда',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
    ],
  },
  {
    id: 2006,
    name: 'Супермаркет',
    description: 'Супермаркет',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/market.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/market.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
  {
    id: 2008,
    name: 'Все для дома',
    description: 'Все для дома',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/tea.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/tea.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [
      {
        id: 3412,
        name: 'Одеяла',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3573,
        name: 'Карманные платки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3603,
        name: 'Шали',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3564,
        name: 'Ремни',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3484,
        name: 'Галстуки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3498,
        name: 'Заколки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3523,
        name: 'Игрушки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3606,
        name: 'Галстук-бабочка',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 1311,
        name: 'Сумки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 3436,
            name: 'Чемоданы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3604,
            name: 'Чемоданы на колесиках',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3365,
            name: 'Аксессуары для гаджетов',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3602,
            name: 'Портмоне',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3611,
            name: 'Другие сумки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3521,
            name: 'Портфели',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3359,
            name: 'Поясные сумки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3372,
            name: 'Сумки через плечо ',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3464,
            name: 'Визитницы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3452,
            name: 'Брелок',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3619,
            name: 'Дорожные сумки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3610,
            name: 'Сумки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3455,
            name: 'Рюкзаки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
      {
        id: 3595,
        name: 'Пледы',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3423,
        name: 'Ободки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3380,
        name: 'Шляпы',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3507,
        name: 'Перчатки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3447,
        name: 'Шарфы',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3612,
        name: 'Палантины',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3376,
        name: 'Чехлы для паспорта',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3420,
        name: 'Повязки на голову ',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3583,
        name: 'Кепки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3577,
        name: 'Подтяжки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3370,
        name: 'Другие аксессуары для волос',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3361,
        name: 'Чехлы для телефона',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3386,
        name: 'Другие аксессуары',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3393,
        name: 'Other stationary',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3480,
        name: 'Конверты',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3524,
        name: 'Шапки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3579,
        name: 'Солнцезащитные очки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3409,
        name: 'Платки на шею',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3435,
        name: 'Cover-Ups',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3438,
        name: 'ручки',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3502,
        name: 'Другие аксессуары для техники',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 3501,
        name: 'Зонты',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
    ],
  },
  {
    id: 2000,
    name: 'Мода',
    description: 'Мода',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/shoes.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/shoes.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: true,
    children: [
      {
        id: 2102,
        name: 'Детская мода',
        description: 'Детская мода',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 3456,
            name: 'Комбинезоны',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3465,
            name: 'Платья',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3463,
            name: 'Блузки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/ball.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/ball.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
      {
        id: 2100,
        name: 'Женская мода',
        description: 'Женская мода',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 3394,
            name: 'Кожаные куртки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3403,
            name: 'Зимние комбинезоны',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 1324,
            name: 'Обувь',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [
              {
                id: 3430,
                name: 'Кроссовки ',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3493,
                name: 'Ботинки',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3445,
                name: 'Слипоны',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3622,
                name: 'Сандалии ',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3538,
                name: 'Тапочки',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3443,
                name: 'Балетки',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3511,
                name: 'Ботфорты',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3618,
                name: 'Мокасины ',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3373,
                name: 'Кеды',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3396,
                name: 'Шлепанцы',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [
                  {
                    id: 25273,
                    name: 'Для бритья',
                    description: 'Для бритья',
                    image: {
                      dark: {
                        pdf: 'https://static.vlife.kz/icons/car.pdf',
                        svg: 'https://static.vlife.kz/icons/car.svg',
                      },
                      light: {
                        pdf: 'https://static.vlife.kz/icons/car.pdf',
                        svg: 'https://static.vlife.kz/icons/car.svg',
                      },
                    },
                    isTabbedChildren: false,
                    children: [],
                  },
                ],
              },
              {
                id: 3541,
                name: 'Ботильоны',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3558,
                name: 'Туфли',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3424,
                name: 'Байкеры ',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3402,
                name: 'Сапоги',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3486,
                name: 'Формальные туфли',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3578,
                name: 'Лоферы ',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3364,
                name: 'Резиновые сапоги',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3427,
                name: 'Мюли',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3431,
                name: 'Эспадрильи',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
              {
                id: 3590,
                name: 'Угги',
                description: '',
                image: {
                  dark: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                  light: {
                    pdf: 'https://static.vlife.kz/icons/car.pdf',
                    svg: 'https://static.vlife.kz/icons/car.svg',
                  },
                },
                isTabbedChildren: false,
                children: [],
              },
            ],
          },
          {
            id: 3581,
            name: 'Короткие шубы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3599,
            name: 'Корсеты',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3566,
            name: 'Юбки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3596,
            name: 'Спортивные кофты',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3574,
            name: 'Смокинги',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3488,
            name: 'Куртки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3491,
            name: 'Брюки Слакс и чинос',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3533,
            name: 'Дубленки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3547,
            name: 'Джинсы скинни',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3522,
            name: 'Боди',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3545,
            name: 'Леггинсы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3605,
            name: 'Джинсовые куртки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3608,
            name: 'Свитеры',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3621,
            name: 'Боди',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3378,
            name: 'Накидки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3360,
            name: 'Кофты-поло',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3369,
            name: 'Нагрудники',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3593,
            name: 'Болеро',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3561,
            name: 'Носки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3519,
            name: 'Чулочные изделия',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3387,
            name: 'Кардиганы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3416,
            name: 'Туники',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3410,
            name: 'Брюки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3401,
            name: 'Формальные костюмы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3415,
            name: 'Джинсы буткат',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3375,
            name: 'Пуховики',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3576,
            name: 'Свитшоты и худи',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3571,
            name: 'Шорты',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3557,
            name: 'Пальто',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3565,
            name: 'Трусы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3575,
            name: 'Танки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3421,
            name: 'Пеньюары',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3432,
            name: 'Бомберы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3404,
            name: 'Коктейльные платья',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3485,
            name: 'Парки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3515,
            name: 'Пончо',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3539,
            name: 'Плавки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3516,
            name: 'Колготки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3586,
            name: 'Купальники',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3615,
            name: 'Одежда для дома',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3584,
            name: 'Наборы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3407,
            name: 'Жилеты ',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3433,
            name: 'Рубашки с короткими рукавами',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3475,
            name: 'Джинсы клеш',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3490,
            name: 'Вечерние платья',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3512,
            name: 'Платья-халат',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3554,
            name: 'Пижамы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3570,
            name: 'Спортивные костюмы ',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3598,
            name: 'Гольфы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3390,
            name: 'Джинсы фэшн',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3540,
            name: 'Ночные сорочки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3543,
            name: 'Футболки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3520,
            name: 'Топы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3549,
            name: 'Джинсы бойфрэнд',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3544,
            name: 'Трусы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3367,
            name: 'Плавки-бикини',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3422,
            name: 'Шубы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3392,
            name: 'Джинсы прямые',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3474,
            name: 'Чулки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3508,
            name: 'Плащи',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3513,
            name: 'Рубашки с длинными рукавами',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3481,
            name: 'Футболки-поло',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3510,
            name: 'Пиджаки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3509,
            name: 'Жилеты',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3592,
            name: 'Спорт',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
      {
        id: 2101,
        name: 'Мужская мода',
        description: 'Мужская мода',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 3434,
            name: 'Джинсы капри',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3448,
            name: 'Водолазки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3446,
            name: 'Джинсы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
      {
        id: 2103,
        name: 'Мода для дроидов',
        description: 'Мода для дроидов',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 3472,
            name: 'Пляжная одежда',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3467,
            name: 'Халаты',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3466,
            name: 'Майки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
    ],
  },
  {
    id: 2010,
    name: 'Красота и здоровье',
    description: 'Красота и здоровье',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/cosmetics.pdf',
        svg: 'https://static.vlife.kz/icons/cosmetics.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/cosmetics.pdf',
        svg: 'https://static.vlife.kz/icons/cosmetics.svg',
      },
    },
    isTabbedChildren: false,
    children: [
      {
        id: 1310,
        name: 'Красота',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [],
      },
      {
        id: 1322,
        name: 'Ювелирные украшения',
        description: '',
        image: {
          dark: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
          light: {
            pdf: 'https://static.vlife.kz/icons/car.pdf',
            svg: 'https://static.vlife.kz/icons/car.svg',
          },
        },
        isTabbedChildren: false,
        children: [
          {
            id: 3398,
            name: 'Ювелирные аксессуары',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3379,
            name: 'Гвоздики',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3374,
            name: 'Солитер',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3594,
            name: 'Футляры для ювелирных изделий',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3459,
            name: ' Амулеты',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3589,
            name: 'Цепочки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3441,
            name: 'Серьги',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3505,
            name: 'Запонки',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3451,
            name: 'Часы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3613,
            name: 'Сейф',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3542,
            name: 'Настольные часы',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3489,
            name: 'Кольца',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3620,
            name: 'Ожерелья',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3616,
            name: 'Дорожный чемодан',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3400,
            name: 'Другие бижутерии',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3429,
            name: 'Подвески',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3536,
            name: 'Колье ',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3362,
            name: 'Броши',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3368,
            name: 'Сотуар',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3585,
            name: 'Браслеты',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
          {
            id: 3460,
            name: 'Шкатулки для часов',
            description: '',
            image: {
              dark: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
              light: {
                pdf: 'https://static.vlife.kz/icons/car.pdf',
                svg: 'https://static.vlife.kz/icons/car.svg',
              },
            },
            isTabbedChildren: false,
            children: [],
          },
        ],
      },
    ],
  },
  {
    id: 2007,
    name: 'Хобби и развлечения',
    description: 'Хобби и развлечения',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/joystick.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/joystick.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
  {
    id: 2011,
    name: 'Товары для детей',
    description: 'Товары для детей',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/teddy.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/teddy.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
  {
    id: 2003,
    name: 'Компьютеры и аксессуары',
    description: 'Компьютеры и аксессуары',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/PC.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/PC.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
  {
    id: 2004,
    name: 'Техника для дома',
    description: 'Техника для дома',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/washing_machine.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/washing_machine.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
  {
    id: 2005,
    name: 'Спорт и туризм',
    description: 'Спорт и туризм',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/ball.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/ball.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
  {
    id: 2001,
    name: 'Автотовары',
    description: 'Автотовары',
    image: {
      dark: {
        pdf: 'https://static.vlife.kz/icons/car.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
      light: {
        pdf: 'https://static.vlife.kz/icons/car.pdf',
        svg: 'https://static.vlife.kz/icons/car.svg',
      },
    },
    isTabbedChildren: false,
    children: [],
  },
]
