export type Option = {
  id: number
  name: string
  description: string
  isTabbedChildren: boolean
  children: Option[]
}

export enum Appearance {
  default = 'default',
  mini = 'mini',
}

export interface OptionsDisplayProps {
  options: Option[]
  handleItemClick: (item: any) => void
  selectedIds: number[]
}

export interface TreeselectProps {
  placeholder: string
  options: any[]
  isSingleItemSelector?: boolean
  isAbsolute?: boolean
  callback?: (item: any, flag?: string) => void
  error?: string
  value?: string
  shouldDisplayError?: boolean
}
