```js
import options from './mock-data';
import React from 'react';

const [state, setState] = React.useState('cb not called');

const treeCallback = item => {
    console.log(item);
    setState('cb called');
};

<div>
    <p>{state}</p>
    <Treeselect placeholder="Example treeselect" options={options} callback={treeCallback} isSingleItemSelector />
    <br/>
    <p>isAbsolute</p>
    <Treeselect placeholder="Example treeselect" options={options} callback={treeCallback} isSingleItemSelector isAbsolute />
    <br/>
    <p>appearance = mini</p>
    <Treeselect placeholder="Example treeselect" options={options} callback={treeCallback} isSingleItemSelector appearance={'mini'} />
</div>
```
