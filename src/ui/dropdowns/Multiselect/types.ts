import React from 'react'

export enum Appearance {
  mini = 'mini',
  default = 'default',
}

export interface MultiselectProps {
  options: Array<{ value: string; label: string; data?: any }>
  appearance?: Appearance
  placeholder?: string
  LastItem?: React.FC
  onChange?: (data: any) => void
  defaultFirst?: boolean
  isInlineLabel?: boolean
  disabled?: boolean
  label?: string
  labelWidth?: string
  active?: string
}
