import React from 'react'
import { Appearance } from 'ui/dropdowns/Multiselect/types'

export interface OptionsProps {
  options: Array<{ value: string; label: string; data?: any }>
  LastItem?: React.FC
  activeId: number | null
  appearance?: Appearance
  changeActiveId: (
    id: number,
    shouldMakeMenuVisible?: boolean,
    data?: any,
  ) => void
}
