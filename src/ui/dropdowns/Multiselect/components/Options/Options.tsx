import React, { FC, useEffect } from 'react'
import styled from 'ui/theme/styled'
import { Caption } from 'ui'
import { OptionsProps } from './types'
import { FlexWrap, Menu } from 'ui/dropdowns/shared'
import { MenuProps } from 'ui/dropdowns/shared/types'
import { closeButtons, getNewId } from './utils'
import { Appearance } from 'ui/dropdowns/Multiselect/types'

interface ItemProps {
  isActive: boolean
  appearance: Appearance
}

const Item = styled.div<ItemProps>`
  padding: 0 20px;
  box-sizing: border-box;
  height: 48px;
  cursor: pointer;

  &:hover {
    background: ${props => props.theme.colorTools.getPrimary(0.5)};
  }

  ${props =>
    props.appearance === Appearance.mini &&
    `
    font-size: 12px;
    height: 40px;
  `}

  ${props =>
    props.isActive &&
    `
    background: ${props.theme.colorTools.getPrimary(0.5)}
  `}
`

const Options: FC<OptionsProps & MenuProps> = ({
  options,
  LastItem,
  changeActiveId,
  activeId,
  isAbsolute,
  appearance,
}) => {
  const handleKeyDown = (e: KeyboardEvent) => {
    e.preventDefault()
    if (closeButtons.includes(e.code)) {
      const itemData = options[activeId]
      changeActiveId(activeId, false, itemData)
    } else {
      const newId = getNewId(activeId, options.length, e.code)
      const itemData = options[newId]
      changeActiveId(newId, true, itemData)
    }
  }
  useEffect(() => {
    window.addEventListener('keydown', handleKeyDown)
    return () => window.removeEventListener('keydown', handleKeyDown)
  })
  return (
    <Menu isAbsolute={isAbsolute} appearance={appearance}>
      {options.map((el, id) => (
        <Item
          onClick={() => changeActiveId(id, false, el)}
          key={`option.key.${el.value}`}
          isActive={activeId === id}
          appearance={appearance}
        >
          <FlexWrap>
            <Caption>{el.label}</Caption>
          </FlexWrap>
        </Item>
      ))}
      {<LastItem />}
    </Menu>
  )
}

Options.defaultProps = {
  LastItem: () => null,
}
export default Options
