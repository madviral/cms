export const closeButtons = ['Escape', 'Enter', 'Space']

export const getNewId = (
  activeId = 0,
  optionsLength: number,
  keyAction: string,
) => {
  if (keyAction === 'ArrowUp' && activeId > 0) {
    return activeId - 1
  }
  if (keyAction === 'ArrowDown' && activeId < optionsLength - 1) {
    return activeId + 1
  }

  return activeId
}
