Default multiSelect Dropdown component

```js
const options = [
  {
    label: 'label 1',
    value: 'val1'
  },
  {
    label: 'label 2',
    value: 'val2'
  },
  {
    label: 'label 3',
    value: 'val3'
  },
];

<Multiselect options={options} placeholder="Выберите склад" />
```

isAbsolute = true

```js
const options = [
  {
    label: 'label 1',
    value: 'val1'
  },
  {
    label: 'label 2',
    value: 'val2'
  },
  {
    label: 'label 3',
    value: 'val3'
  },
];

<Multiselect options={options} placeholder="Выберите склад" isAbsolute />
```

MultiSelect Dropdown component with custom bottom button component

```js
const options = [
  {
    label: 'label 1',
    value: 'val1'
  },
  {
    label: 'label 2',
    value: 'val2'
  },
  {
    label: 'label 3',
    value: 'val3'
  },
];

const CustomButton = () => {
  const onClick = () => alert('test');
  return (
    <div onClick={onClick} style={{ padding: 10 }}>
      🍺click me
    </div>
  )
};

<Multiselect options={options} placeholder="Выберите склад" LastItem={CustomButton} />
```