import React, { FC, useRef, useState } from 'react'
import { Appearance, MultiselectProps } from './types'
import { Options } from './components'
import { Header, Wrap, Label } from 'ui/dropdowns/shared'
import { MenuProps } from 'ui/dropdowns/shared/types'
import { useDetectOutsideClick } from 'lib/hooks'
import { Box } from 'ui/layout'

const Multiselect: FC<MultiselectProps & MenuProps> = ({
  options,
  placeholder,
  LastItem,
  onChange,
  defaultFirst,
  isAbsolute,
  appearance,
  label,
  isInlineLabel,
  labelWidth,
  active,
  disabled,
}) => {
  const ref = useRef<HTMLDivElement>()
  const [activeId, setActiveId] = useState(defaultFirst ? 0 : null)
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const changeActiveId = (
    id: number,
    shouldMakeMenuOpen: boolean = false,
    data: any,
  ) => {
    setActiveId(id)
    setIsMenuOpen(shouldMakeMenuOpen)
    onChange(data)
  }
  useDetectOutsideClick(ref, () => setIsMenuOpen(false))

  const defaultLabel = options?.find(
    option => option.value || option.label === active,
  )?.label

  return (
    <Box
      display={isInlineLabel && 'flex'}
      alignItems={isInlineLabel && 'center'}
    >
      <Label
        labelWidth={labelWidth}
        isInlineLabel={isInlineLabel}
        label={label}
      />
      <Wrap ref={ref}>
        <Header
          disabled={disabled}
          placeholder={placeholder}
          label={options[activeId]?.label || defaultLabel}
          setIsMenuOpen={setIsMenuOpen}
          isMenuOpen={isMenuOpen}
          appearance={appearance}
        />
        {isMenuOpen && (
          <Options
            isAbsolute={isAbsolute}
            activeId={activeId}
            changeActiveId={changeActiveId}
            options={options}
            LastItem={LastItem}
            appearance={appearance}
          />
        )}
      </Wrap>
    </Box>
  )
}

Multiselect.defaultProps = {
  onChange: () => null,
  defaultFirst: false,
  appearance: Appearance.default,
}

export default Multiselect
