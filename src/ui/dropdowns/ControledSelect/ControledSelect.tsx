import React from 'react'
import Select, { Option, SelectProps } from 'rc-select'
import { Global, css } from '@emotion/core'
import { theme } from 'ui/theme'
import { Chevron } from 'ui/icons'
import {
  StyledSelect,
  getDropdownStyles,
  selectPrefixCls,
  StyledSelectProps,
} from './styles'

const defaultProps = {
  allowClear: false,
  id: `controled-select-${Math.random() * 100}`,
  withoutPortal: false,
  width: '100px',
}

interface InternalSelectProps<DataType> extends Omit<SelectProps, 'onChange'> {
  onChange: (value: DataType) => void
}

const ControledSelect = <DataType extends {}>({
  allowClear,
  id,
  width,
  options,
  onChange,
  withoutPortal,
  ...props
}: InternalSelectProps<DataType> & typeof defaultProps & StyledSelectProps) => (
  <StyledSelect width={width}>
    <Global
      styles={css`
        ${getDropdownStyles(theme)}
      `}
    />
    <Select
      id={id}
      prefixCls={selectPrefixCls}
      optionFilterProp='label'
      menuItemSelectedIcon={null}
      allowClear={allowClear}
      inputIcon={<Chevron width={20} color={theme.colorsList.secondary[1]} />}
      filterOption={(input, option) =>
        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
      onChange={(_, value) => onChange((value as any).data)}
      getPopupContainer={withoutPortal ? node => node : undefined}
      {...props}
    >
      {options.map(item => (
        <Option
          key={item.value}
          value={item.value}
          disabled={item.disabled}
          data={item}
          data-key={item.label}
        >
          {item.label}
        </Option>
      ))}
    </Select>
  </StyledSelect>
)

ControledSelect.defaultProps = defaultProps

export default ControledSelect
