import { styled } from 'ui/theme'

export const selectPrefixCls = 'controled-select'

export type StyledSelectProps = {
  width?: string
}

export const StyledSelect = styled.div<StyledSelectProps>`
  .controled-select {
    display: inline-block;
    font-size: 12px;
    line-height: 16px;
    width: ${props => props.width};
    position: relative;
    font-family: ${({ theme }) => theme.fontFamily.ms};
  }
  .controled-select-disabled,
  .controled-select-disabled input {
    cursor: not-allowed;
  }
  .controled-select-disabled .controled-select-selector {
    opacity: 0.3;
  }
  .controled-select-show-arrow.controled-select-loading
    .controled-select-arrow-icon::after {
    box-sizing: border-box;
    width: 12px;
    height: 12px;
    border-radius: 100%;
    border: 2px solid #999;
    border-top-color: transparent;
    border-bottom-color: transparent;
    transform: none;
    margin-top: 4px;
    animation: rcSelectLoadingIcon 0.5s infinite;
  }
  .controled-select .controled-select-selection-placeholder {
    opacity: 0.4;
  }
  .controled-select-single .controled-select-selector {
    display: flex;
    position: relative;
  }
  .controled-select-single
    .controled-select-selector
    .controled-select-selection-search {
    width: 100%;
  }
  .controled-select-single
    .controled-select-selector
    .controled-select-selection-search-input {
    width: 100%;
  }
  .controled-select-single
    .controled-select-selector
    .controled-select-selection-item,
  .controled-select-single
    .controled-select-selector
    .controled-select-selection-placeholder {
    position: absolute;
    top: 50%;
    left: 16px;
    pointer-events: none;
    transform: translateY(-50%);
  }
  .controled-select-single:not(.controled-select-customize-input)
    .controled-select-selector {
    border: 1px solid ${props => props.theme.colorsList.secondary[2]};
    font-family: ${props => props.theme.fontFamily.ms};
    border-radius: 8px;
  }
  .controled-select-single:not(.controled-select-customize-input)
    .controled-select-selector
    .controled-select-selection-search-input {
    border: none;
    outline: none;
    width: 100%;
    padding: 13px 44px 14px 16px;
    box-sizing: border-box;
    border-radius: 8px;
  }
  .controled-select-multiple .controled-select-selector {
    display: flex;
    flex-wrap: wrap;
    padding: 1px;
    border: 1px solid #000;
  }
  .controled-select-multiple
    .controled-select-selector
    .controled-select-selection-item {
    flex: none;
    background: #bbb;
    border-radius: 4px;
    margin-right: 2px;
    padding: 0 8px;
  }
  .controled-select-multiple
    .controled-select-selector
    .controled-select-selection-item-disabled {
    cursor: not-allowed;
    opacity: 0.5;
  }
  .controled-select-multiple
    .controled-select-selector
    .controled-select-selection-search {
    position: relative;
  }
  .controled-select-multiple
    .controled-select-selector
    .controled-select-selection-search-input,
  .controled-select-multiple
    .controled-select-selector
    .controled-select-selection-search-mirror {
    padding: 1px;
    font-family: system-ui;
  }
  .controled-select-multiple
    .controled-select-selector
    .controled-select-selection-search-mirror {
    position: absolute;
    z-index: 999;
    white-space: nowrap;
    position: none;
    left: 0;
    top: 0;
    visibility: hidden;
  }
  .controled-select-multiple
    .controled-select-selector
    .controled-select-selection-search-input {
    border: none;
    outline: none;
    width: 100%;
  }
  .controled-select-allow-clear.controled-select-multiple
    .controled-select-selector {
    padding-right: 20px;
  }
  .controled-select-allow-clear .controled-select-clear {
    position: absolute;
    right: 28px;
    top: calc(50% - 1px);
    transform: translateY(-50%);
    line-height: 1;
  }
  .controled-select-clear-icon {
    font-size: 20px;
    line-height: 1;
    color: ${props => props.theme.colorsList.secondary[1]};
  }
  .controled-select-show-arrow.controled-select-multiple
    .controled-select-selector {
    padding-right: 20px;
  }
  .controled-select-show-arrow .controled-select-arrow {
    position: absolute;
    right: 8px;
    top: calc(50% + 2px);
    transform: translateY(-50%);
    line-height: 1;
    pointer-events: none;
  }
  .controled-select-show-arrow .controled-select-arrow-icon::after {
    content: '';
    border: 5px solid transparent;
    width: 0;
    height: 0;
    display: inline-block;
    border-top-color: #999;
    transform: translateY(5px);
  }
  .controled-select-focused .controled-select-selector {
    border-color: ${props => props.theme.colorsList.secondary[1]} !important;
  }
  .controled-select-selection-item {
    color: ${props => props.theme.colorsList.secondary[1]};
  }
  .controled-select-open {
    z-index: 2;
  }
  .controled-select-open .controled-select-selection-item {
    opacity: 0.4;
  }
`

export const getDropdownStyles = theme => `
  .controled-select-dropdown {
    position: absolute;
    padding-top: 8px;
    padding-bottom: 8px;
    background: #fff;
    min-height: 40px;
    border-radius: 8px;
    box-shadow: 0px 0px 8px rgba(37, 40, 43, 0.12);
    font-family: ${theme.fontFamily.ms}
  }
  .controled-select-dropdown-hidden {
    display: none;
  }
  .controled-select-item {
    padding: 12px 16px;
    cursor: pointer;
    color: ${theme.colorsList.secondary[0]};
    font-size: 12px;
    line-height: 16px;

    :hover {
      background: ${theme.colorsList.primary[1]};
      color: #fff;
    }
  }
  .controled-select-item-group {
    color: ${theme.colorsList.secondary[0]};
    font-weight: bold;
    font-size: 12px;
  }
  .controled-select-item-option {
    position: relative;
  }
  .controled-select-item-option-grouped {
    padding-left: 24px;
  }
  .controled-select-item-option .controled-select-item-option-state {
    position: absolute;
    right: 0;
    top: 4px;
    pointer-events: none;
  }
  .controled-select-item-option-active {
    background: ${theme.colorsList.primary[1]};
    color: #fff;
  }
  .controled-select-item-option-disabled {
    color: ${theme.colorsList.secondary[2]};
    cursor: not-allowed;
    :hover {
      color: ${theme.colorsList.secondary[2]};
      background: #fff;
    }
  }
  .controled-select-item-empty {
    text-align: center;
    color: ${theme.colorsList.secondary[2]};
  }
  .controled-select-selection__choice-zoom {
    transition: all 0.3s;
  }
  .controled-select-selection__choice-zoom-appear {
    opacity: 0;
    transform: scale(0.5);
  }
  .controled-select-selection__choice-zoom-appear.controled-select-selection__choice-zoom-appear-active {
    opacity: 1;
    transform: scale(1);
  }
  .controled-select-selection__choice-zoom-leave {
    opacity: 1;
    transform: scale(1);
  }
  .controled-select-selection__choice-zoom-leave.controled-select-selection__choice-zoom-leave-active {
    opacity: 0;
    transform: scale(0.5);
  }
  @keyframes rcSelectLoadingIcon {
    0% {
      transform: rotate(0);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`
