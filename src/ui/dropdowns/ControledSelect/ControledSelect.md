Default ControledSelect component

[Documentation](https://github.com/react-component/select)

```js
const options = [
  {
    label: 'Склад один',
    value: 'stock1'
  },
  {
    label: 'Склад два',
    value: 'stock2'
  },
  {
    label: 'Склад три',
    value: 'stock3'
  },
];

<ControledSelect 
  options={options}
  style={{ width: '100%' }}
  showSearch
  placeholder="Выберите склад"
  onChange={console.info}
/>