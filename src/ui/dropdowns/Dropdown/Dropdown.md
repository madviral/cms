```jsx
<div>
  <div style={{ width: '300px' }}>
    <Dropdown width='100%'>
      <Dropdown.OutlinedButton>Опубликованные</Dropdown.OutlinedButton>
      <Dropdown.Menu width='100%'>
        <Dropdown.Item>123</Dropdown.Item>
        <Dropdown.Item>234</Dropdown.Item>
        <Dropdown.Item>345</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  </div>
  <div style={{ width: '200px' }}>
    <Dropdown width='100%'>
      <Dropdown.SimpleButton>Опубликованные</Dropdown.SimpleButton>
      <Dropdown.Menu width='100%'>
        <Dropdown.Item>123</Dropdown.Item>
        <Dropdown.Item>234</Dropdown.Item>
        <Dropdown.Item>345</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  </div>
</div>
```
