import React, { FC, ReactNode } from 'react'
import { styled } from 'ui/theme'
import Details, { DetailsProps } from './Details'
import { OutlinedButton, SimpleButton } from './Button'
import Menu, { MenuProps, MenuItem } from './Menu'

type DropdownProps = {
  className?: string
  children: ReactNode
} & MenuProps &
  DetailsProps

interface DropdownInterface extends FC<DropdownProps> {
  OutlinedButton: typeof OutlinedButton
  SimpleButton: typeof SimpleButton
  Menu: typeof Menu
  Item: typeof MenuItem
}

const StyledDetails = styled(Details)<DetailsProps>`
  position: relative;
  display: inline-block;
`

const Dropdown: DropdownInterface = ({ children, className, ...rest }) => {
  return (
    <StyledDetails overlay className={className} {...rest}>
      {children}
    </StyledDetails>
  )
}

Dropdown.OutlinedButton = OutlinedButton
Dropdown.OutlinedButton.displayName = 'Dropdown.OutlinedButton'

Dropdown.SimpleButton = SimpleButton
Dropdown.SimpleButton.displayName = 'Dropdown.SimpleButton'

Dropdown.Menu = Menu
Dropdown.Menu.displayName = 'Dropdown.Menu'

Dropdown.Item = MenuItem
Dropdown.Item.displayName = 'Dropdown.Item'

export default Dropdown
