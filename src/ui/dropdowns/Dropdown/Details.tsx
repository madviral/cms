import React, {
  useState,
  useEffect,
  useCallback,
  useRef,
  ReactNode,
} from 'react'
import { styled } from 'ui/theme'
import { sx } from 'ui/theme/utils'
import {
  compose,
  display,
  layout,
  space,
  DisplayProps,
  LayoutProps,
  SpaceProps,
} from 'styled-system'

// The <details> element is not yet supported in Edge so we have to use a polyfill.
// We have to check if window is defined before importing the polyfill
// so the code doesn’t run while pages build
// uses require because of primer/components issue #638
if (typeof window !== 'undefined') {
  // tslint:disable-next-line
  require('details-element-polyfill')
}

type RenderProps = {
  open: boolean
  onToggle: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
}
type Render = ({ open, onToggle }: RenderProps) => ReactNode

export type DetailsProps = {
  overlay?: boolean
  children: ReactNode
  disabled?: boolean
  onClickOutside?: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
  onToggle?: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
  defaultOpen?: boolean
  open?: boolean
  render?: Render
  className?: string
} & DisplayProps &
  LayoutProps &
  SpaceProps

const StyledDetails = styled('details')`
  & > summary {
    list-style: none;
    outline: none;
  }
  & > summary::-webkit-details-marker {
    display: none;
  }
  ${compose(display, layout, space)};
  ${sx};
`
function getRenderer(children) {
  return typeof children === 'function' ? children : () => children
}

function Details({
  children,
  disabled,
  overlay,
  render = getRenderer(children),
  open: userOpen,
  onClickOutside,
  onToggle,
  defaultOpen = false,
  ...rest
}: DetailsProps) {
  const [internalOpen, setInternalOpen] = useState(defaultOpen)
  const ref = useRef(null)
  const open = typeof userOpen !== 'undefined' ? userOpen : internalOpen

  const onClickOutsideInternal = useCallback(
    event => {
      if (event.target.closest('details') !== ref.current) {
        if (onClickOutside) {
          onClickOutside(event)
        }
        if (!event.defaultPrevented) {
          setInternalOpen(false)
        }
      }
    },
    [ref, onClickOutside, setInternalOpen],
  )

  useEffect(() => {
    if (open && overlay) {
      document.addEventListener('click', onClickOutsideInternal)
      return () => {
        document.removeEventListener('click', onClickOutsideInternal)
      }
    }
  }, [open, overlay, onClickOutsideInternal])

  function handleToggle(e) {
    if (onToggle) {
      onToggle(e)
    }

    if (!e.defaultPrevented) {
      setInternalOpen(e.target.open)
    }
  }

  return (
    <StyledDetails
      {...rest}
      ref={ref}
      open={open}
      onToggle={handleToggle}
      overlay={overlay}
      onClick={disabled ? e => e.preventDefault() : e => e}
    >
      {render({ open, onToggle: handleToggle })}
    </StyledDetails>
  )
}

Details.defaultProps = {
  overlay: false,
}

export default Details
