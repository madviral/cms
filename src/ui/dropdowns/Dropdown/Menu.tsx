import { styled } from 'ui/theme'
import { sx } from 'ui/theme/utils'
import {
  compose,
  display,
  space,
  layout,
  DisplayProps,
  LayoutProps,
  SpaceProps,
} from 'styled-system'
import { getDirectionStyles } from './styles'

export type MenuProps = {
  direction?: 'tl' | 'l' | 'br' | 'b' | 'bl' | 'r'
}

export const MenuItem = styled.li<DisplayProps & LayoutProps & SpaceProps>`
  display: block;
  padding: 12px 16px;
  overflow: hidden;
  color: ${({ theme }) => theme.colorsList.secondary[0]};
  text-overflow: ellipsis;
  white-space: nowrap;
  cursor: default;
  font-weight: 500;
  font-size: 12px;
  line-height: 1.25;
  a {
    color: ${({ theme }) => theme.colorsList.secondary[0]};
    text-decoration: none;
    display: block;
    padding: 12px 16px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    font-weight: 500;
    font-size: 12px;
    line-height: 1.25;
  }
  &:focus,
  a:focus {
    color: ${({ theme }) => theme.colorsList.primary[0]};
    text-decoration: none;
    background-color: ${({ theme }) => theme.colorsList.primary[4]};
  }
  &:hover,
  &:hover a {
    color: ${({ theme }) => theme.colorsList.primary[0]};
    text-decoration: none;
    background-color: ${({ theme }) => theme.colorsList.primary[4]};
    outline: none;
  }
  ${compose(display, layout, space)};
  ${sx};
`

const Menu = styled.ul<MenuProps & DisplayProps & LayoutProps & SpaceProps>`
  background-clip: padding-box;
  background-color: #fff;
  border: 1px solid;
  border-color: ${({ theme }) => theme.colorsList.primary[0]};
  border-radius: ${props => props.theme.borderRadius.xs};
  box-shadow: 0px 0px 8px ${({ theme }) => theme.colorsList.secondary[2]};
  left: 0;
  list-style: none;
  margin-top: 4px;
  padding: 8px 0 8px 0 !important; //TODO: fix this override on our markdown styles
  position: absolute;
  top: 100%;
  width: 160px;
  z-index: 100;
  > ul {
    list-style: none;
  }
  ${props =>
    props.direction ? getDirectionStyles(props.theme, props.direction) : ''};
  ${compose(display, layout, space)};
  ${sx};
`

Menu.defaultProps = {
  direction: 'br',
}

export default Menu
