export const getDirectionStyles = (theme, direction) => {
  const map = {
    l: `
      top: 0;
      right: 100%;
      left: auto;
      width: auto;
      margin-top: 0;
      margin-right: 10px;
    `,
    r: `
      top: 0;
      left: 100%;
      width: auto;
      margin-top: 0;
      margin-left: 10px;
    `,
    tl: `
      top: auto;
      bottom: 100%;
      left: 0;
      margin-bottom: 3px;
    `,
    b: `
      right: 50%;
      left: auto;
      transform: translateX(50%);
    `,
    br: `
      right: 0;
      left: auto;
    `,
    bl: ``,
  }
  return map[direction]
}
