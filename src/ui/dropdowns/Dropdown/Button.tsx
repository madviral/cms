import React, { ReactNode, FC } from 'react'
import { Button, ButtonTransparent } from 'ui/buttons'
import { Text } from 'ui/text'
import { Box } from 'ui/layout'
import { Chevron } from 'ui/icons'
import { useHover } from 'lib/hooks'
import { theme } from 'ui/theme'
import { Condition } from 'ui/condition'

type ButtonProps = {
  children: string | ReactNode
  disabled?: boolean
  noCarret?: boolean
}

export const OutlinedButton: FC<ButtonProps> = ({
  children,
  disabled,
  ...props
}) => {
  const { hover, onMouseEnter, onMouseLeave } = useHover()

  return (
    <summary>
      <Button
        as='div'
        appearance='outline'
        size='m'
        fullWidth
        aria-haspopup='true'
        justifyContent='flex-start'
        onMouseEnter={!disabled ? onMouseEnter : undefined}
        onMouseLeave={!disabled ? onMouseLeave : undefined}
        disabled={disabled}
        {...props}
      >
        <Box display='inline-flex' mr='4px'>
          <Text fontWeight='500'>{children}</Text>
        </Box>
        <Box display='inline-flex' ml='auto' pt='4px'>
          <Chevron
            color={
              !disabled
                ? hover
                  ? theme.color.white
                  : theme.colorsList.primary[0]
                : theme.colorsList.secondary[4]
            }
          />
        </Box>
      </Button>
    </summary>
  )
}

export const SimpleButton: FC<ButtonProps> = ({
  noCarret,
  children,
  ...props
}) => {
  const isText = typeof children === 'string'
  return (
    <summary>
      <ButtonTransparent
        as='button'
        aria-haspopup='true'
        display='inline-flex'
        justifyContent='flex-start'
        alignItems='center'
        p='0'
        {...props}
      >
        <Condition match={isText}>
          <Text fontSize='12px' fontWeight='500' color={'secondary.0'}>
            {children}
          </Text>
        </Condition>
        <Condition match={!isText}>{children}</Condition>
        <Condition match={!noCarret}>
          <Box display='inline-flex' ml='4px' pt='6px'>
            <Chevron color={theme.colorsList.secondary[1]} />
          </Box>
        </Condition>
      </ButtonTransparent>
    </summary>
  )
}
