```js padded
import data from './mock-data';

const [d, sd] = React.useState({
  value: ''
});

const callback = data => {
  console.log(data)
  sd(data)
};

<div>
  <p>{d.value || 'nada'}</p>
  <Expander {...data} callback={callback} />
</div>
```
