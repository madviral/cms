import React, { FC, useState } from 'react'
import { Menu } from 'ui/dropdowns/shared'
import { Caption } from 'ui'
import styled from 'ui/theme/styled'
import { MenuProps } from 'ui/dropdowns/shared/types'
import { Attribute, CallbackValues } from '../../Expander'
import { useTranslation } from 'react-i18next'

interface AttributesProps extends MenuProps {
  attributes: Attribute[]
  callback: (values: CallbackValues) => void
  isOpen: boolean
}

const InnerWrap = styled.div`
  padding: 0 20px 20px 20px;
`

const Item = styled.div`
  height: 56px;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0 20px;
  &:nth-of-type(odd) {
    background: ${props => props.theme.colorTools.getSecondary(0.1)};
  }
`

const StyledLabel = styled(Caption)`
  flex: 5;
`

const StyledToggle = styled.div`
  flex: 1;
  &:before {
    content: '•';
  }
`

interface AttributesMenuProps {
  isOpen: boolean
}

const AttributesMenu = styled(Menu)<AttributesMenuProps>`
  display: ${props => (props.isOpen ? 'block' : 'none')};
`

interface ItemWrapProps {
  attr: Attribute
  callback: (values: CallbackValues | undefined) => void
}

const ItemWrap: FC<ItemWrapProps> = ({ attr, callback }) => {
  const [showPlaceholder, setShowPlaceholder] = useState(true)
  const onChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setShowPlaceholder(false)
    if (e.target.value !== '') {
      callback({
        attributeId: attr.id,
        attributeValueId: attr.values[Number(e.target.value)].id,
        value: attr.values[Number(e.target.value)].value,
      })
    } else {
      callback(undefined)
    }
  }
  const [t] = useTranslation('shop')
  return (
    <Item>
      <StyledLabel>{attr.name}</StyledLabel>
      <StyledToggle />
      <select style={{ flex: 10 }} onChange={onChange}>
        {showPlaceholder ? <option value=''>{t('chooseValue')}</option> : null}
        {attr.values.map((v, idx) => (
          <option key={v.id} value={idx}>
            {v.value}
          </option>
        ))}
      </select>
    </Item>
  )
}

const Options: FC<AttributesProps> = ({
  attributes,
  isAbsolute,
  callback,
  isOpen,
}) => {
  return (
    <AttributesMenu isAbsolute={isAbsolute} isOpen={isOpen}>
      <InnerWrap>
        {attributes.map(attr => (
          <ItemWrap key={attr.id} attr={attr} callback={callback} />
        ))}
      </InnerWrap>
    </AttributesMenu>
  )
}

export default Options
