import React, { FC, useState, useRef } from 'react'
import { Header, Wrap } from 'ui/dropdowns/shared'
import { MenuProps } from 'ui/dropdowns/shared/types'
// import { useDetectOutsideClick } from 'lib/hooks'
import { Attributes } from './components'

export type Value = {
  id: number
  value: string
  status: number
  isSelected: boolean
}

export type Attribute = {
  id: number
  name: string
  status: number
  isRequired: boolean
  values: Value[]
}

export type CallbackValues = {
  attributeId: number
  attributeValueId?: number
  value: string
}

export interface ExpanderProps extends MenuProps {
  id: number
  name: string
  attributes: Attribute[]
  callback: (values: CallbackValues) => void
}

const Expander: FC<ExpanderProps> = ({
  attributes,
  isAbsolute,
  name,
  callback,
}) => {
  const ref = useRef<HTMLDivElement>()
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  // useDetectOutsideClick(ref, () => setIsMenuOpen(false))
  return (
    <Wrap ref={ref}>
      <Header
        isMenuOpen={isMenuOpen}
        setIsMenuOpen={setIsMenuOpen}
        placeholder={name}
      />
      <Attributes
        callback={callback}
        isAbsolute={isAbsolute}
        attributes={attributes}
        isOpen={isMenuOpen}
      />
    </Wrap>
  )
}

export default Expander
