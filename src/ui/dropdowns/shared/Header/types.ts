import { Appearance } from 'ui/dropdowns/Multiselect/types'
import React from 'react'

export interface HeaderTypes {
  label?: string
  placeholder: string
  setIsMenuOpen: React.Dispatch<React.SetStateAction<boolean>>
  isMenuOpen: boolean
  disabled?: boolean
  items?: any[]
  type?: 'treeselect' | 'default'
  crossCallback?: () => void
  borderColor?: string
  appearance?: Appearance
}
