import React, { FC } from 'react'
import { HeaderTypes } from './types'
import styled from 'ui/theme/styled'
import { Caption, Chevron, CrossIcon } from 'ui'
import { FlexWrap } from '..'
import { theme } from 'ui/theme'
import { Appearance } from 'ui/dropdowns/Multiselect/types'

interface WrapProps {
  isOpen: boolean
  disabled: boolean
  borderColor: string
  appearance: Appearance
}

// find a better way to style maybe
const Wrap = styled.div<WrapProps>`
  height: 54px;
  box-sizing: border-box;
  width: 100%;
  padding: 0 20px;
  cursor: pointer;
  user-select: none;

  ${props =>
    props.disabled &&
    `
    background-color: #FAFAFA;
    border-color: ${props.theme.colorsList.secondary[3]};
    p {
      color: ${props.theme.colorsList.secondary[3]};
    }
  `}

  ${props =>
    props.appearance === Appearance.mini &&
    `
    height: 40px;
    font-size: 12px;
    line-height: 14px;
  `}

  ${props =>
    props.isOpen
      ? `
    border-top: 1px solid ${props.borderColor};
    border-right: 1px solid ${props.borderColor};
    border-left: 1px solid ${props.borderColor};
    border-bottom: 1px solid transparent;
    border-top-left-radius: ${props.theme.borderRadius.xs};
    border-top-right-radius: ${props.theme.borderRadius.xs};
    `
      : `
    border: 1px solid ${props.borderColor};
    border: 1px solid ${props.theme.colorsList.secondary[2]};
    border-radius: ${props.theme.borderRadius.xs};
  `}
`

interface IconWrapProps {
  isOpen: boolean
}

const IconWrap = styled.div<IconWrapProps>`
  transform: rotate(${props => (props.isOpen ? '180deg' : '0deg')});
  & > svg {
    display: block;
  }
`

const CrossWrap = styled.div`
  & > svg {
    display: block;
  }
  &:hover {
    & > svg {
      & > svg {
        & > path {
          fill: red;
        }
      }
    }
  }
  margin-right: 24px;
`

// name or label are temp, should be either or
const Header: FC<HeaderTypes> = ({
  label,
  placeholder,
  setIsMenuOpen,
  isMenuOpen,
  items,
  type,
  crossCallback,
  borderColor,
  appearance,
  disabled,
}) => {
  const onCrossClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.stopPropagation()
    crossCallback()
  }
  return (
    <Wrap
      onClick={() => !disabled && setIsMenuOpen(!isMenuOpen)}
      borderColor={borderColor}
      disabled={disabled}
      isOpen={isMenuOpen}
      appearance={appearance}
    >
      {type === 'treeselect' ? (
        <FlexWrap>
          <FlexWrap>
            {items.length ? (
              items.map(i => (
                <FlexWrap key={i.id}>
                  <Caption>{i.name}</Caption>
                  <CrossWrap onClick={onCrossClick}>
                    <CrossIcon width={14} height={14} color='#9FB3C8' />
                  </CrossWrap>
                </FlexWrap>
              ))
            ) : (
              <Caption>{placeholder}</Caption>
            )}
          </FlexWrap>
          <IconWrap isOpen={isMenuOpen}>
            <Chevron color={'#9FB3C8'} />
          </IconWrap>
        </FlexWrap>
      ) : (
        <FlexWrap>
          <Caption>{label || placeholder}</Caption>
          <IconWrap isOpen={isMenuOpen}>
            <Chevron color={'#9FB3C8'} />
          </IconWrap>
        </FlexWrap>
      )}
    </Wrap>
  )
}

Header.defaultProps = {
  items: [],
  type: 'default',
  borderColor: theme.color.primary,
}
export default Header
