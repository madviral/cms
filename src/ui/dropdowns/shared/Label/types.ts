export interface LabelProps {
  label: string
  labelWidth?: string
  isInlineLabel?: boolean
}

export interface LabelTextProps {
  labelWidth?: string
  isInlineLabel?: boolean
}
