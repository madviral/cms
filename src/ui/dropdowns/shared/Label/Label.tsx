import React, { FC } from 'react'
import { styled, theme } from 'ui/theme'

import { LabelProps, LabelTextProps } from 'ui/dropdowns/shared/Label/types'

const LabelBox = styled.div<LabelTextProps>`
  min-width: ${props => props.labelWidth || 'initial'};
  margin-bottom: 8px;

  ${props =>
    props.isInlineLabel &&
    `
    margin-right: 20px;
    margin-bottom: 0;
    text-align: right;
  `}
`

const LabelText = styled.label`
  font-size: 14px;
  line-height: 14px;
  color: ${theme.color.secondary};
`

const Label: FC<LabelProps> = ({ labelWidth, label, isInlineLabel }) => (
  <LabelBox labelWidth={labelWidth} isInlineLabel={isInlineLabel}>
    <LabelText>{label}</LabelText>
  </LabelBox>
)

export default Label
