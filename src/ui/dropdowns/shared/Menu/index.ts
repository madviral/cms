import styled from 'ui/theme/styled'
import { variant } from 'styled-system'
import { MenuProps } from '../types'

const variants = variant({
  prop: 'appearance',
  variants: {
    mini: {
      maxHeight: '160px',
      overflow: 'auto',
    },
  },
})

const Menu = styled.div<MenuProps>`
  display: flex;
  flex-direction: column;
  ${props =>
    props.isAbsolute
      ? `
    position: absolute;
    left: ${props.labelWidth ? '159px' : '0'};
    right: 0;
  `
      : null}
  z-index: 1;
  background: ${props => props.theme.color.white};
  border-left: 1px solid ${props => props.theme.color.primary};
  border-bottom: 1px solid ${props => props.theme.color.primary};
  border-right: 1px solid ${props => props.theme.color.primary};
  border-bottom-left-radius: ${props => props.theme.borderRadius.xs};
  border-bottom-right-radius: ${props => props.theme.borderRadius.xs};
  margin-top: -1px;
  ${variants}
  ${props =>
    props.isTreeselect &&
    `
    overflow: auto;
    max-height: 160px;
  `}
`

Menu.defaultProps = {
  isAbsolute: false,
  isTreeselect: false,
}
export default Menu
