import { Appearance } from 'ui/dropdowns/Multiselect/types'

export interface MenuProps {
  isAbsolute?: boolean
  isTreeselect?: boolean
  labelWidth?: string
  appearance?: Appearance
}
