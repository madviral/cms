import styled from 'ui/theme/styled'

interface BadgeProps {
  fill?: boolean
  borderColor?: string
  color?: string
}

const Badge = styled('span', {
  shouldForwardProp: prop => !['fill', 'borderColor'].includes(prop),
})<BadgeProps>(({ borderColor, color, fill, theme }) => ({
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: fill ? '100%' : 'auto',
  borderRadius: theme.borderRadius.m,
  padding: '4px 8px',
  border: '1px solid',
  fontSize: 10,
  lineHeight: 1,
  borderColor: theme.color[color ? color : borderColor],
  color: theme.color[color],
  boxSizing: 'border-box',
}))

export default Badge
