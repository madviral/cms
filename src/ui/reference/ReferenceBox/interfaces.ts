export interface ReferenceBoxProps {
  text: string
  link: string
  className?: string
}
