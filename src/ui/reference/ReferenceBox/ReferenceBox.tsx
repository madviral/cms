import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { Box } from '../../layout'
import { theme, styled } from '../../theme'
import { Text } from '../../text'
import { ReferenceBoxProps } from './interfaces'

const BoxWrapper = styled.div`
  width: 100%;
  max-width: 160px;
`

const BoxLogo = styled.div`
  margin-bottom: 30px;
`

const StyledLink = styled(Link)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  text-align: center;
  height: 100%;
  width: 100%;
`

const ReferenceBox: FC<ReferenceBoxProps> = ({ text, link, children }) => (
  <BoxWrapper>
    <Box
      height={180}
      borderRadius={theme.borderRadius.xs}
      border='1px solid'
      borderColor={theme.color.primary}
    >
      <StyledLink to={link}>
        <BoxLogo>{children}</BoxLogo>
        <Text as='p' color={theme.color.primary} fontSize={12}>
          {text}
        </Text>
      </StyledLink>
    </Box>
  </BoxWrapper>
)

export default ReferenceBox
