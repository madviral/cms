```js
import { ReferenceBox } from './ReferenceBox'
import { NoteIcon } from 'ui/icons/Note'
import { BrowserRouter as Router } from 'react-router-dom'
;<div>
  <Router>
    <ReferenceBox text='Создать аккаунт' link='/'>
      <NoteIcon width={60} className='icon' />
    </ReferenceBox>
  </Router>
</div>
```
