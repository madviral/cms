import { styled } from 'ui/theme'
import { ifNotProp } from 'styled-tools'
import { Link, LinkProps } from 'react-router-dom'
import { sx, WithSXProps } from 'ui/theme/utils'

type StyledLinkProps = {
  href?: string
  likeButton?: boolean
} & LinkProps &
  WithSXProps

const StyledLink = styled(Link, {
  shouldForwardProp: prop => !['likeButton', 'sx'].includes(prop),
})<StyledLinkProps>`
  text-decoration: none;
  color: ${props => props.theme.color.primary};

  ${ifNotProp(
    'likeButton',
    `
    &:hover {
      text-decoration: underline;
    }
  `,
  )}
  ${sx};
`

export default StyledLink
