import { HTMLProps } from 'react'
import { styled } from 'ui/theme'
import { ifNotProp } from 'styled-tools'
import { sx, WithSXProps } from 'ui/theme/utils'

type StyledLinkProps = {
  href?: string
  likeButton?: boolean
} & HTMLProps<HTMLAnchorElement> &
  WithSXProps

const StyledLink = styled('a', {
  shouldForwardProp: prop => !['likeButton'].includes(prop),
})<StyledLinkProps>`
  text-decoration: none;
  color: ${props => props.theme.color.primary};

  ${ifNotProp(
    'likeButton',
    `
    &:hover {
      text-decoration: underline;
    }
  `,
  )}
  ${sx};
`

export default StyledLink
