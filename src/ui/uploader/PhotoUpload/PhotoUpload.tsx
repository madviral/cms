import React, { FC, useState } from 'react'

import { Box } from 'ui/layout'
import { Grid, Preview } from './components'
import { PhotoUplaodProps } from './types'

const PhotoUpload: FC<PhotoUplaodProps> = ({ size }) => {
  // make grid dynamic
  const [images, setImages] = useState<ArrayBuffer[]>([])
  return (
    <Box display='grid' gridTemplateColumns='180px 155px' gridGap='20px'>
      <Preview previewImage={images[0]} />
      <Grid size={size} images={images} setImages={setImages} />
    </Box>
  )
}

PhotoUpload.defaultProps = {
  size: 6,
}
export default PhotoUpload
