import React, { FC } from 'react'
import { Wrap } from '../shared'
import styled from 'ui/theme/styled'
import { useTranslation } from 'react-i18next'
import { PictureIcon, Text } from 'ui'

interface PreviewProps {
  previewImage: ArrayBuffer | undefined
}
// @ts-ignore
const PreviewBox = styled.div<PreviewProps>`
  height: 100%;
  width: 100%;
  background-image: url(${props => props.previewImage});
  border-radius: ${props => props.theme.borderRadius.s};
  background-size: contain;
`

const NoImageWrap = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const NoImagePlaceholder = () => {
  const [t] = useTranslation()
  return (
    <NoImageWrap>
      <PictureIcon />
      <Text
        fontSize={12}
        mt={15}
        color='secondary.0'
        width='min-content'
        textAlign='center'
      >
        {t('shop:addProductPhoto')}
      </Text>
    </NoImageWrap>
  )
}

const Preview: FC<PreviewProps> = ({ previewImage }) => {
  return (
    <Wrap>
      <PreviewBox previewImage={previewImage}>
        {!previewImage && <NoImagePlaceholder />}
      </PreviewBox>
    </Wrap>
  )
}

export default Preview
