import styled from 'ui/theme/styled'

export default styled.div`
  border: 1px solid ${props => props.theme.colorTools.getSecondary(0.5)};
  border-radius: ${props => props.theme.borderRadius.s};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`
