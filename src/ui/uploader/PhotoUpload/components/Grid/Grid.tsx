import React, { FC, useRef, useState } from 'react'
import { Wrap } from '../shared'
import { Box } from 'ui/layout'
import styled from 'ui/theme/styled'
import { PictureIcon } from 'ui'
import { theme } from 'ui/theme'

interface ImgWrapProps {
  img: string | null
}

const ImgWrap = styled(Wrap)<ImgWrapProps>`
  background-image: url(${props => props.img});
  background-size: contain;
  color: ${props => props.theme.color.grayishBlue};
  font-weight: 500;
  height: 70px;
  width: 70px;
`

interface GridProps {
  size: number
  images: Array<string | ArrayBuffer>
  setImages: React.Dispatch<React.SetStateAction<Array<string | ArrayBuffer>>>
}

interface PicProps extends GridProps {
  active: boolean
}

const Pic: FC<PicProps> = ({ active, setImages, images }) => {
  const [img, setImg] = useState(null)
  const inputUploadRef = useRef(null)
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files[0]
    if (file) {
      const reader = new FileReader()
      reader.onloadend = () => {
        setImg(reader.result)
        setImages([...images, reader.result])
      }
      reader.readAsDataURL(file)
    }
  }
  return (
    <div>
      <input
        type='file'
        accept='image/*'
        onChange={onChange}
        ref={inputUploadRef}
        style={{ display: 'none', position: 'absolute' }}
      />
      <ImgWrap
        onClick={() => active && inputUploadRef.current.click()}
        img={img}
      >
        {active ? '+' : <PictureIcon color={theme.color.grayishBlue} />}
      </ImgWrap>
    </div>
  )
}

const Grid: FC<GridProps> = ({ size, images, setImages }) => {
  return (
    <Box display='grid' gridGap='15px' gridTemplateColumns='1fr 1fr'>
      {Array.from(Array(size)).map((pic, idx) => (
        <Pic
          size={size}
          key={idx}
          active={idx === images.length}
          images={images}
          setImages={setImages}
        />
      ))}
    </Box>
  )
}

export default Grid
