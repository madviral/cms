import React from 'react'
import styled from 'ui/theme/styled'
import { ifProp } from 'styled-tools'
import { layout } from 'styled-system'
import { Condition } from 'ui/condition'
import { color } from 'ui/theme/utils'

type StyledImageProps = {
  width?: number | string
  height?: number | string
  isBg?: boolean
  src?: string
  borderRadius?: string
}

type ImageProps = StyledImageProps & {
  alt?: string
}

const StyledImage = styled('div', {
  shouldForwardProp: prop =>
    !['width', 'height', 'isBg', 'borderRadius'].includes(prop),
})<StyledImageProps>`
  border-radius: ${props => props.theme.borderRadius[props.borderRadius]};
  background-color: ${color('secondary.2')};
  overflow: hidden;
  ${layout};

  ${ifProp(
    'isBg',
    props => `
    background-image: url(${props.src});
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
  `,
  )}

  img {
    width: 100%;
    max-height: 100%;
    object-fit: cover;
  }
`

const Image: React.FC<ImageProps> = ({
  width,
  height,
  isBg,
  alt,
  src,
  borderRadius,
}) => {
  return (
    <StyledImage
      width={width}
      height={height}
      isBg={isBg}
      src={isBg && src}
      borderRadius={borderRadius}
    >
      <Condition match={!isBg && src}>
        <img src={src} alt={alt} width={width} height={height} />
      </Condition>
    </StyledImage>
  )
}

Image.defaultProps = {
  width: 32,
  height: 32,
  borderRadius: 'xs',
}

export default Image
