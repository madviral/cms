import React, { useState, useCallback } from 'react'
import { ifProp } from 'styled-tools'
import { styled } from 'ui/theme'
import { Box } from 'ui/layout'
import { Text } from 'ui/text'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { StyledSwitcherProps, SwitcherProps } from './types'

const defaultProps = {
  customLabel: false,
  defaultToggled: false,
  onChange: () => null,
}

const StyledWrapper = styled.div<StyledSwitcherProps>`
  display: flex;
  align-items: center;
  ${ifProp(
    'disabled',
    `
    cursor: not-allowed;
  `,
    `
    cursor: pointer;
  `,
  )}
`

const StyledSwitcher = styled.div<StyledSwitcherProps>`
  position: relative;
  width: 38px;
  height: 20px;
  padding: 2px;
  border-radius: 16px;
  box-sizing: border-box;
  z-index: 1;

  &::after {
    content: '';
    position: absolute;
    left: 2px;
    top: 2px;
    width: 16px;
    height: 16px;
    border-radius: 50%;
    background-color: white;
    transition: transform ease-out 100ms;
    z-index: 2;
  }
  ${ifProp('disabled', `opacity: .4;`)}
  ${ifProp(
    'toggled',
    ({ theme }) => `
      background-color: ${theme.colorsList.primary[1]};
      &::after {
        transform: translateX(calc(100% + 2px));
      }
    `,
    ({ theme }) => `
      background-color: ${theme.colorsList.secondary[1]};
    `,
  )}
`

const Switcher = ({
  id,
  customLabel,
  disabled,
  defaultToggled,
  label,
  onChange,
}: SwitcherProps & StyledSwitcherProps) => {
  const [toggled, setToggled] = useState(defaultToggled)

  const change = useCallback(() => {
    if (!disabled) {
      onChange(!toggled)
      setToggled(!toggled)
    }
  }, [toggled])

  return (
    <StyledWrapper disabled={disabled} data-type='switcher' data-id={id}>
      <StyledSwitcher onClick={change} disabled={disabled} toggled={toggled} />
      <Condition match={customLabel}>{label}</Condition>
      <Condition match={!customLabel}>
        <Box ml='12px' onClick={change}>
          <Text fontSize='12px' lineHeight='1' color='secondary.1'>
            {label}
          </Text>
        </Box>
      </Condition>
    </StyledWrapper>
  )
}

Switcher.defaultProps = defaultProps

export default Switcher
