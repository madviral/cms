import { ReactNode } from 'react'

export type StyledSwitcherProps = {
  toggled?: boolean
  disabled?: boolean
}

export type SwitcherProps = {
  id?: number | string
  defaultToggled?: boolean
  customLabel?: boolean
  label?: ReactNode | string
  onChange?: (value: boolean) => void
}
