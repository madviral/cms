import { ifProp } from 'styled-tools'
import styled from '@emotion/styled'
import { theme } from '../theme'

interface DividerProps {
  vertical?: boolean
  transparent?: boolean
  color?: string
}

const Divider = styled('div', {
  shouldForwardProp: prop =>
    !['vertical', 'transparent', 'color'].includes(prop),
})<DividerProps>(
  ({ color }) => ({
    backgroundColor: color,
  }),
  ifProp(
    'vertical',
    {
      width: '1px',
      height: 'auto',
    },
    {
      width: '100%',
      height: '1px',
    },
  ),
  ifProp('transparent', {
    opacity: '0.2',
  }),
)

Divider.defaultProps = {
  color: theme.color.lightGrayishBlue,
}

export default Divider
