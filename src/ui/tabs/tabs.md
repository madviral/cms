```js
const tabs = [
  {
    id: 1,
    name: 'tab1',
    component: () => <div>im tab #1</div>,
    color: 'primary',
  },
  {
    id: 2,
    name: 'tab2',
    component: () => <div>im tab #2</div>,
    color: 'secondary',
    length: 420,
  },
  {
    id: 3,
    name: 'tab3',
    component: () => <div>im tab #3</div>,
    color: 'primary',
    length: 69,
  },
]
;<div style={{ height: 400, background: '#eee' }}>
  <Tabs tabs={tabs} />
</div>
```
