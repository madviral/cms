import React, { FC, useState, useEffect } from 'react'
import styled from 'ui/theme/styled'
import { Box } from 'ui/layout'
import { useTranslation } from 'react-i18next'

export interface Tab {
  id: number
  name: string
  component: FC
  color: string
  length?: number
}

export interface TabsProps {
  tabs: Tab[]
  passActiveTabIndex?: (value: number) => void
}

const Head = styled.ul`
  display: flex;
  flex-direction: row;
  list-style: none;
`

interface TabProps {
  active: boolean
  color: string
}

const Tab = styled.li<TabProps>`
  cursor: pointer;
  color: ${props =>
    props.active
      ? props.theme.color[props.color]
      : props.theme.color.secondary};

  border-bottom: ${props =>
    props.active ? 'none' : `1px solid ${props.theme.colorsList.secondary[2]}`};

  box-shadow: inset 0 -4px ${props => (props.active ? props.theme.color[props.color] : 'transparent')};
  padding: 0 15px;
  min-width: 240px;
  box-sizing: border-box;
  height: 48px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  font-size: 14px;
`

const Length = styled(Box)`
  height: 20px;
  padding: 0 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`

interface ChildProps {
  active: boolean
}

const ChildWrap = styled.div<ChildProps>`
  display: ${props => (props.active ? 'block' : 'none')};
  overflow: auto;
  height: 100%;
`

const Tabs: FC<TabsProps> = ({ tabs, passActiveTabIndex }) => {
  const [active, setActive] = useState(0)
  const [t] = useTranslation()

  useEffect(() => {
    passActiveTabIndex(active)
  }, [active])

  return (
    <Box display='flex' flexDirection='column' height='100%'>
      <Head>
        {tabs.map((tab, idx) => (
          <Tab
            onClick={() => setActive(idx)}
            active={idx === active}
            key={tab.id}
            color={tab.color}
          >
            <p>{t(tab.name)}</p>
            {tab.length ? (
              <Box
                ml='8px'
                bg={idx === active ? `${tab.color}.2` : 'secondary.3'}
                borderRadius='12px'
              >
                <Length
                  color={idx === active ? `${tab.color}.0` : 'secondary.0'}
                >
                  {tab.length}
                </Length>
              </Box>
            ) : null}
          </Tab>
        ))}
      </Head>
      {tabs.map((tab, idx) => (
        <ChildWrap key={tab.id} active={idx === active}>
          <tab.component />
        </ChildWrap>
      ))}
    </Box>
  )
}

export default Tabs
