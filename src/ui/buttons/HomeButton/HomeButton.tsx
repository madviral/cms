import React, { FC } from 'react'
import styled from 'ui/theme/styled'
import { HomeIcon } from '../../icons'
import { HomeButtonProps } from './types/interfaces'

const Button = styled.button(({ theme }) => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  width: 40,
  height: 40,
  padding: 0,
  border: 'none',
  outline: 'none',
  background: theme.colorTools.getPrimary(0.75),
  borderRadius: '50%',
  cursor: 'pointer',
}))

const HomeButton: FC<HomeButtonProps> = ({ onClick, className }) => (
  <Button className={className} onClick={onClick}>
    <HomeIcon />
  </Button>
)

export default HomeButton
