export interface ButtonInterface {
  className?: string
  onClick?: (event?) => void
}
