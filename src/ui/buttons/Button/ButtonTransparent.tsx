import { styled, theme } from 'ui/theme'
import { sx, WithSXProps } from 'ui/theme/utils'
import { ifProp } from 'styled-tools'
import ButtonBase from './ButtonBase'
import { ButtonBaseProps } from './interfaces'

const ButtonTransparent = styled(ButtonBase)<ButtonBaseProps & WithSXProps>`
  :hover {
    opacity: 0.8;
  }
  ${ifProp(
    'disabled',
    `
    cursor: not-allowed;
    opacity: 0.6;
    :hover {
      opacity: 0.6;
    }
  `,
  )}
  ${sx}
`

ButtonTransparent.defaultProps = {
  theme: {
    ...theme,
    colors: theme.colorsList,
  },
}

export default ButtonTransparent
