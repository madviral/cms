import React, { FC } from 'react'
import styled from 'ui/theme/styled'
import {
  compose,
  space,
  display,
  flexbox,
  SpaceProps,
  LayoutProps,
  DisplayProps,
  FlexboxProps,
} from 'styled-system'
import { ifProp } from 'styled-tools'
import { sizes, appearance } from './buttonStyles'

export interface ButtonProps
  extends React.DetailedHTMLProps<
      React.ButtonHTMLAttributes<HTMLButtonElement>,
      HTMLButtonElement
    >,
    SpaceProps,
    LayoutProps,
    DisplayProps,
    FlexboxProps {
  as?: string
  size: 's' | 'm' | 'l' | 'xl'
  appearance: 'primary' | 'secondary' | 'outline'
  fullWidth?: boolean
}

const StyledButton = styled.button<ButtonProps>`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  white-space: nowrap;
  user-select: none;
  cursor: pointer;
  background: transparent;
  border: 0;
  font-size: 12px;
  box-sizing: border-box;
  width: ${props => (props.fullWidth ? '100%' : 'fit-content')};
  ${sizes};
  ${props => appearance[props.appearance]};
  &:disabled {
    cursor: not-allowed;
  }
  ${ifProp('disabled', 'cursor: not-allowed;')};
  ${compose(space, display, flexbox)}
`

const Button: FC<ButtonProps> = ({ children, ...props }) => {
  return <StyledButton {...props}>{children}</StyledButton>
}

Button.defaultProps = {
  fullWidth: false,
}
export default Button
