```js
import styled from 'ui/theme/styled';

const Wrap = styled.div`
  display: flex;
  flex-direction: row;
`;

const Child = styled.div`
  flex: 1;
  border: 1px solid #eee;
  padding: 10px;
`;

const ButtonWrap = styled.div`
  &:not(:last-of-type) {
    margin-bottom: 10px;
  }
  &:first-of-type {
    margin-top: 10px;
  }
  display: flex;
  justify-content: center;
`;

<Wrap>
  <Child>
    <p>Size S</p>
    <ButtonWrap>
      <Button size='s' appearance='primary'>S primary</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='s' appearance='primary' fullWidth>S primary fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='s' appearance='primary' disabled>S primary disabled</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='s' appearance='secondary'>S secondary</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='s' appearance='secondary' fullWidth>S secondary fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='s' appearance='secondary' disabled>S secondary disabled</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='s' appearance='outline'>S outline</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='s' appearance='outline' fullWidth>S outline fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='s' appearance='outline' disabled>S outline disabled</Button>
    </ButtonWrap>
  </Child>
  <Child>
    <p>Size M</p>
    <ButtonWrap>
      <Button size='m' appearance='primary'>M primary</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='m' appearance='primary' fullWidth>M primary fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='m' appearance='primary' disabled>M primary disabled</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='m' appearance='secondary'>M secondary</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='m' appearance='secondary' fullWidth>M secondary fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='m' appearance='secondary' disabled>M secondary disabled</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='m' appearance='outline'>M outline</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='m' appearance='outline' fullWidth>M outline fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='m' appearance='outline' disabled>M outline disabled</Button>
    </ButtonWrap>
  </Child>
  <Child>
    <p>Size L</p>
    <ButtonWrap>
      <Button size='l' appearance='primary'>L primary</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='l' appearance='primary' fullWidth>L primary fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='l' appearance='primary' disabled>L primary disabled</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='l' appearance='secondary'>L secondary</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='l' appearance='secondary' fullWidth>L secondary fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='l' appearance='secondary' disabled>L secondary disabled</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='l' appearance='outline'>L outline</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='l' appearance='outline' fullWidth>L outline fullWidth</Button>
    </ButtonWrap>
    <ButtonWrap>
      <Button size='l' appearance='outline' disabled>L outline disabled</Button>
    </ButtonWrap>
  </Child>
</Wrap>
```