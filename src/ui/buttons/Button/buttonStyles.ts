import { css } from '@emotion/core'

const s = css`
  height: 20px;
  padding: 0 8px;
  border-radius: 4px;
`

const m = css`
  height: 32px;
  padding: 0 16px;
  border-radius: 8px;
`

const l = css`
  height: 40px;
  padding: 0 32px;
  border-radius: 8px;
`

const xl = css`
  height: 48px;
  padding: 0 32px;
  border-radius: 8px;
`

export const sizes = ({ size }) => {
  const table = {
    s,
    m,
    l,
    xl,
  }
  return table[size]
}
// temp
const primary = (props: any) => css`
  color: ${props.theme.color.white};
  background: ${props.theme.colorsList.primary[0]};
  &:hover {
    background: ${props.theme.colorsList.primary[1]};
  }
  &:disabled {
    background: ${props.theme.colorsList.secondary[5]};
    color: ${props.theme.colorsList.secondary[2]};
  }
  ${props.disabled &&
    `
      background: ${props.theme.colorsList.secondary[5]};
      color: ${props.theme.colorsList.secondary[2]};
      &:hover {
        background: ${props.theme.colorsList.secondary[5]};
        color: ${props.theme.colorsList.secondary[2]};
      }
  `}
`

const secondary = (props: any) => css`
  color: ${props.theme.color.white};
  background: ${props.theme.colorsList.secondary[1]};
  &:hover {
    background: ${props.theme.colorsList.secondary[2]};
  }
  &:disabled {
    background: ${props.theme.colorsList.secondary[5]};
    color: ${props.theme.colorsList.secondary[1]};
  }
  ${props.disabled &&
    `
      background: ${props.theme.colorsList.secondary[5]};
      color: ${props.theme.colorsList.secondary[1]};
      &:hover {
        background: ${props.theme.colorsList.secondary[5]};
        color: ${props.theme.colorsList.secondary[1]};
      }
  `}
`

const outline = (props: any) => css`
  color: ${props.theme.colorsList.primary[0]};
  border: 1px solid ${props.theme.colorsList.primary[0]};
  &:hover {
    color: ${props.theme.color.white};
    background: ${props.theme.colorsList.primary[0]};
  }
  &:disabled {
    border: 1px solid ${props.theme.colorsList.secondary[2]};
    color: ${props.theme.colorsList.secondary[2]};
    background: ${props.theme.color.white};
  }
  ${props.disabled &&
    `
      border: 1px solid ${props.theme.colorsList.secondary[2]};
      color: ${props.theme.colorsList.secondary[2]};
      background: ${props.theme.color.white};
      &:hover {
        border: 1px solid ${props.theme.colorsList.secondary[2]};
        color: ${props.theme.colorsList.secondary[2]};
        background: ${props.theme.color.white};
      }
  `}
`

export const appearance = {
  primary,
  secondary,
  outline,
}
