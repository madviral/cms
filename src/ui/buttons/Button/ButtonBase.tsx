import React from 'react'
import { styled, theme } from 'ui/theme'
import { css } from '@emotion/core'
import {
  compose,
  fontSize,
  space,
  color,
  display,
  flexbox,
  layout,
  SpaceProps,
  LayoutProps,
  DisplayProps,
  FlexboxProps,
} from 'styled-system'
import { ButtonBaseProps } from './interfaces'

export const baseStyles = css`
  position: relative;
  display: inline-block;
  white-space: nowrap;
  vertical-align: middle;
  cursor: pointer;
  user-select: none;
  appearance: none;
  text-decoration: none;
  text-align: center;
  background-color: transparent;
  border: none;
  &:hover {
    text-decoration: none;
  }
  &:focus {
    outline: none;
  }
  &:disabled {
    cursor: default;
  }
  &:disabled svg {
    opacity: 0.6;
  }
`

const Button = ({
  className,
  disabled,
  onClick,
  children,
}: ButtonBaseProps) => (
  <button className={className} onClick={disabled ? undefined : onClick}>
    {children}
  </button>
)

const ButtonBase = styled(Button)<
  ButtonBaseProps & SpaceProps & LayoutProps & DisplayProps & FlexboxProps
>`
  ${baseStyles};
  ${compose(fontSize, space, color, display, flexbox, layout)}
`

ButtonBase.defaultProps = {
  theme: {
    ...theme,
    colors: theme.colorsList,
  },
}

export default ButtonBase
