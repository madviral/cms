import { ReactNode } from 'react'
import { ButtonInterface } from '../commonTypes'

export enum Appearance {
  default,
  mini,
  outline,
  miniOutlined,
}

export type ButtonBaseProps = {
  as?: string
  disabled?: boolean
  className?: string
  onClick?: VoidFunction
  children?: ReactNode
}
export interface ButtonProps extends ButtonInterface {
  fullWidth?: boolean
  appearance?: Appearance
  borderRadius?: string
  secondaryStyle?: boolean
  bgColor?: string
}
