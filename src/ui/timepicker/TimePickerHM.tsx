import React from 'react'
import { Box } from 'ui/layout'
import { Space, Text } from 'ui/text'
import TimePicker from './TimePicker'
import { TimePickerHMProps } from './types'

const TimePickerHM = ({
  dataTimeDirection,
  disabled,
  disabledHours,
  value,
  onChange,
}: TimePickerHMProps) => {
  const [date, setDate] = React.useState(value)
  const change = React.useCallback(inputValue => {
    if (onChange) {
      onChange(inputValue)
    }
    setDate(inputValue)
  }, [])

  return (
    <Box
      display='inline-flex'
      alignItems='center'
      data-time-direction={dataTimeDirection}
    >
      <TimePicker
        value={date}
        showMinute={false}
        showSecond={false}
        disabled={disabled}
        disabledHours={disabledHours}
        onChange={change}
      />
      <Space />
      <Text color='secondary.1'>{':'}</Text>
      <Space />
      <TimePicker
        value={date}
        showHour={false}
        showSecond={false}
        minuteStep={15}
        disabled={disabled}
        onChange={change}
      />
    </Box>
  )
}

export default TimePickerHM
