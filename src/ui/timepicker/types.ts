type DisabledHours = () => number[]

export type TimePickerProps = {
  value?: Date
  showHour?: boolean
  showMinute?: boolean
  showSecond?: boolean
  minuteStep?: number
  disabled?: boolean
  disabledHours?: DisabledHours
  onChange: (value: Date) => void
}

export type TimePickerHMProps = {
  dataTimeDirection?: 'from' | 'to'
  value: Date
  disabled?: boolean
  disabledHours?: DisabledHours
  onChange: (value: Date) => void
}

type FromToTime = {
  fromTime: Date
  toTime: Date
}

export type TimePickerFromToTimeProps = {
  disabled?: boolean
  onChange: (date: FromToTime) => void
}
