import React from 'react'
import RcTimePicker from 'rc-time-picker-date-fns'
import 'rc-time-picker-date-fns/assets/index.css'
import { theme } from 'ui/theme'
import { ClassNames } from '@emotion/core'
import { TimePickerProps } from './types'

const input = `
  width: 60px;
  height: 32px;

  > input {
    height: 100%;
  }
  input {
    font-size: 12px;
    line-height: 1;
    text-align: center;
    color: ${theme.colorsList.secondary[0]};
    border-color: ${theme.colorsList.secondary[2]};
    border-radius: ${theme.borderRadius.xs};
    font-family: ${theme.fontFamily.ms}
  }
`

const popup = `
  width: 60px;
  font-family: ${theme.fontFamily.ms};
  color: ${theme.colorsList.secondary[0]};

  input {
    font-size: 12px;
    line-height: 1;
    text-align: center;
  }

  .rc-time-picker-panel-inner {
    border-color: ${theme.colorsList.secondary[2]};
    border-radius: ${theme.borderRadius.xs};
  }

  .rc-time-picker-panel-clear-btn {
    display: none;
  }
  .rc-time-picker-panel-combobox {
    display: flex;
  }
  .rc-time-picker-panel-select {
    flex-grow: 1;
  }
`

const TimePicker = ({
  value,
  showHour,
  showMinute,
  showSecond,
  minuteStep,
  disabled,
  disabledHours,
  onChange,
}: TimePickerProps) => {
  const timepicker = React.useRef(null)
  const change = React.useCallback(inputValue => {
    timepicker.current.panelInstance.close()
    onChange(inputValue)
  }, [])

  return (
    <ClassNames>
      {({ css }) => (
        <RcTimePicker
          value={value}
          ref={timepicker}
          showHour={showHour}
          showMinute={showMinute}
          showSecond={showSecond}
          minuteStep={minuteStep}
          hideDisabledOptions
          disabledHours={disabledHours}
          disabled={disabled}
          getPopupContainer={node => node}
          className={css`
            ${input}
          `}
          onChange={change}
          popupClassName={css`
            ${popup}
          `}
        />
      )}
    </ClassNames>
  )
}

export default TimePicker
