import React from 'react'
import { Box } from 'ui/layout'
import { getHours, set } from 'date-fns'
import { DaySchedule } from 'src/modules/common/types/enums'
import { range } from './utils'
import TimePickerHM from './TimePickerHM'
import { TimePickerFromToTimeProps } from './types'

const defaultProps = {
  minHours: 8,
  maxHours: 20,
  maxMinutes: 0,
  minMinutes: 0,
}

const TimePickerFromToTime = ({
  disabled,
  minHours,
  maxHours,
  maxMinutes,
  minMinutes,
  onChange,
}: TimePickerFromToTimeProps & typeof defaultProps) => {
  const dateNow = new Date()

  const [date, setDate] = React.useState({
    fromTime: set(dateNow, { hours: minHours, minutes: minMinutes }),
    toTime: set(dateNow, { hours: maxHours, minutes: maxMinutes }),
  })

  const onSetFromTime = React.useCallback(value => {
    setDate(prevState => {
      const newState = { ...prevState, fromTime: value }
      onChange(newState)
      return newState
    })
  }, [])
  const onSetToTime = React.useCallback(value => {
    setDate(prevState => {
      const newState = { ...prevState, toTime: value }
      onChange(newState)
      return newState
    })
  }, [])

  const getMinDisabled = React.useCallback(
    () => [
      ...range(0, DaySchedule.MIN_HOURS - 1),
      ...range(getHours(date.toTime), 23),
    ],
    [date.toTime],
  )
  const getMaxDisabled = React.useCallback(
    () => [...range(0, getHours(date.fromTime))],
    [date.fromTime],
  )

  return (
    <Box display='flex' alignItems='center'>
      <TimePickerHM
        disabled={disabled}
        value={date.fromTime}
        dataTimeDirection='from'
        disabledHours={getMinDisabled}
        onChange={onSetFromTime}
      />
      <Box flexBasis='30px' flixShrink='1' />
      <TimePickerHM
        disabled={disabled}
        value={date.toTime}
        dataTimeDirection='to'
        disabledHours={getMaxDisabled}
        onChange={onSetToTime}
      />
    </Box>
  )
}

TimePickerFromToTime.defaultProps = defaultProps

export default TimePickerFromToTime
