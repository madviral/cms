import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

interface FilterProps extends IconProps {}

const Filter: FC<FilterProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M2 0a1 1 0 011 1v6a1 1 0 01-2 0V1a1 1 0 011-1zm7 0a1 1 0 011 1v2a1 1 0 01-2 0V1a1 1 0 011-1zM3 17a1 1 0 10-2 0v2a1 1 0 102 0v-2zm-1-3a2 2 0 100-4 2 2 0 000 4zm9-6a2 2 0 11-4 0 2 2 0 014 0zm5-4a2 2 0 100-4 2 2 0 000 4zm1 3a1 1 0 10-2 0v12a1 1 0 102 0V7zm-8 5a1 1 0 011 1v6a1 1 0 11-2 0v-6a1 1 0 011-1z'
      fill={color}
    />
  </Icon>
)

Filter.defaultProps = {
  originalWidth: 18,
  originalHeight: 20,
  width: 18,
  height: 20,
  color: '#9FB3C8',
}

export default Filter
