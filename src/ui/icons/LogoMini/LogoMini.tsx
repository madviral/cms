import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

const LogoMini: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    height={originalHeight}
    width={originalWidth}
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M5 7.28027H29.7902C25.6563 15.2211 21.5458 23.1127 17.4197 31.0393L5 7.28027Z'
      fill='#4C7CFF'
    />
    <path
      d='M29.6289 29.6501H23.792L26.7066 24.0596L29.6289 29.6501Z'
      fill='black'
    />
  </Icon>
)

LogoMini.defaultProps = {
  originalWidth: 34,
  originalHeight: 34,
  color: '#4C7CFF',
}

export default LogoMini
