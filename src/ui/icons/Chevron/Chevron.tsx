import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { rotate, Rotate } from './utils'

const Chevron: FC<IconProps & Rotate> = ({
  originalWidth,
  originalHeight,
  color,
  up,
  right,
  down,
  left,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill={color}
    {...props}
  >
    <g transform={rotate({ up, right, down, left }, props.width, props.height)}>
      <path d='M5.83893 7.26651L10.0054 10.8018L14.1718 7.26651C14.5906 6.91116 15.2671 6.91116 15.6859 7.26651C16.1047 7.62187 16.1047 8.1959 15.6859 8.55125L10.757 12.7335C10.3383 13.0888 9.66174 13.0888 9.24295 12.7335L4.31409 8.55125C3.8953 8.1959 3.8953 7.62187 4.31409 7.26651C4.73289 6.92027 5.42013 6.91116 5.83893 7.26651Z' />
      <path d='M5.83893 7.26651L10.0054 10.8018L14.1718 7.26651C14.5906 6.91116 15.2671 6.91116 15.6859 7.26651C16.1047 7.62187 16.1047 8.1959 15.6859 8.55125L10.757 12.7335C10.3383 13.0888 9.66174 13.0888 9.24295 12.7335L4.31409 8.55125C3.8953 8.1959 3.8953 7.62187 4.31409 7.26651C4.73289 6.92027 5.42013 6.91116 5.83893 7.26651Z' />
    </g>
  </Icon>
)

Chevron.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#000',
}

export default Chevron
