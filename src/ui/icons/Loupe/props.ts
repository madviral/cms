import { IconProps } from '@vlife-grand-era/vlife-storybook'

export interface LoupeProps extends IconProps {
  color?: string
}
