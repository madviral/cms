import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const FileText: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M12 1H4.8A1.8 1.8 0 003 2.8v14.4A1.8 1.8 0 004.8 19h10.8a1.8 1.8 0 001.8-1.8V6.4M12 1l5.4 5.4M12 1v5.4h5.4m-3.6 4.5H6.6m7.2 3.6H6.6m1.8-7.2H6.6'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

FileText.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default FileText
