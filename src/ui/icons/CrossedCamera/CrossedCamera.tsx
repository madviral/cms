import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

const CrossedCamera: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => {
  return (
    <Icon
      originalHeight={originalHeight}
      originalWidth={originalWidth}
      {...props}
    >
      <svg viewBox='0 0 36 36' fill='none' xmlns='http://www.w3.org/2000/svg'>
        <path
          d='M1.5 1.5L34.5 34.5M13.5 4.5H22.5L25.5 9H31.5C32.2957 9 33.0587 9.31607 33.6213 9.87868C34.1839 10.4413 34.5 11.2044 34.5 12V26.01M22.92 22.92C22.4222 23.6476 21.7701 24.2565 21.0102 24.7035C20.2503 25.1504 19.4012 25.4244 18.5234 25.506C17.6456 25.5876 16.7606 25.4747 15.9314 25.1754C15.1021 24.8761 14.349 24.3978 13.7256 23.7744C13.1022 23.151 12.6239 22.3979 12.3246 21.5686C12.0253 20.7394 11.9124 19.8544 11.994 18.9766C12.0756 18.0988 12.3496 17.2497 12.7965 16.4898C13.2435 15.7299 13.8524 15.0779 14.58 14.58M31.5 31.5H4.5C3.70435 31.5 2.94129 31.1839 2.37868 30.6213C1.81607 30.0587 1.5 29.2957 1.5 28.5V12C1.5 11.2044 1.81607 10.4413 2.37868 9.87868C2.94129 9.31607 3.70435 9 4.5 9H9L31.5 31.5Z'
          stroke={color}
          strokeWidth='3'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </svg>
    </Icon>
  )
}

CrossedCamera.defaultProps = {
  originalWidth: 36,
  originalHeight: 36,
  width: 36,
  height: 36,
  color: '#9FB3C8',
}

export default CrossedCamera
