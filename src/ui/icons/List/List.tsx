import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

interface ListProps extends IconProps {}

const List: FC<ListProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <g clipPath='url(#clipList0)'>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M0 1a1 1 0 011-1h1a1 1 0 010 2H1a1 1 0 01-1-1zm19 5a1 1 0 110 2H6a1 1 0 010-2h13zM0 7a1 1 0 001 1h1a1 1 0 000-2H1a1 1 0 00-1 1zm5 6a1 1 0 001 1h13a1 1 0 100-2H6a1 1 0 00-1 1zm-5 0a1 1 0 001 1h1a1 1 0 100-2H1a1 1 0 00-1 1zm5 6a1 1 0 001 1h13a1 1 0 100-2H6a1 1 0 00-1 1zm-5 0a1 1 0 001 1h1a1 1 0 100-2H1a1 1 0 00-1 1zM6 0a1 1 0 000 2h13a1 1 0 100-2H6z'
        fill={color}
      />
    </g>
    <defs>
      <clipPath id='clipList0'>
        <path d='M0 0h20v20H0V0z' fill='#fff' />
      </clipPath>
    </defs>
  </Icon>
)

List.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#9FB3C8',
}

export default List
