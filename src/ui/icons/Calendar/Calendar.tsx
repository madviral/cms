import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const Calendar: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M13.556 1v3.6M6.444 1v3.6M2 8.2h16M3.778 2.8h12.444c.982 0 1.778.806 1.778 1.8v12.6c0 .994-.796 1.8-1.778 1.8H3.778A1.789 1.789 0 012 17.2V4.6c0-.994.796-1.8 1.778-1.8z'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

Calendar.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default Calendar
