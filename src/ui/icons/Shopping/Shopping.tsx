import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

interface ShoppingProps extends IconProps {}

const Shopping: FC<ShoppingProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M19 1H15.7273L13.5345 11.9555C13.4597 12.3321 13.2548 12.6705 12.9556 12.9113C12.6565 13.1522 12.2822 13.2801 11.8982 13.2727H3.94545C3.56148 13.2801 3.18715 13.1522 2.88799 12.9113C2.58883 12.6705 2.38391 12.3321 2.30909 11.9555L1 5.09091H14.9091M11 18C11 18.5523 11.4477 19 12 19C12.5523 19 13 18.5523 13 18C13 17.4477 12.5523 17 12 17C11.4477 17 11 17.4477 11 18ZM3 18C3 18.5523 3.44772 19 4 19C4.55228 19 5 18.5523 5 18C5 17.4477 4.55228 17 4 17C3.44772 17 3 17.4477 3 18Z'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

Shopping.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#627D98',
}

export default Shopping
