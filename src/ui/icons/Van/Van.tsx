import React from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

interface VanProps extends IconProps {}

const Van = ({ originalWidth, originalHeight, color, ...props }: VanProps) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M13.2727 13.3333V2.5H1V13.3333H13.2727ZM13.2727 13.3333H19V9.16667L16.5455 6.66667H13.2727V13.3333ZM6.72727 15.4167C6.72727 16.5673 5.81149 17.5 4.68182 17.5C3.55214 17.5 2.63636 16.5673 2.63636 15.4167C2.63636 14.2661 3.55214 13.3333 4.68182 13.3333C5.81149 13.3333 6.72727 14.2661 6.72727 15.4167ZM17.3636 15.4167C17.3636 16.5673 16.4479 17.5 15.3182 17.5C14.1885 17.5 13.2727 16.5673 13.2727 15.4167C13.2727 14.2661 14.1885 13.3333 15.3182 13.3333C16.4479 13.3333 17.3636 14.2661 17.3636 15.4167Z'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

Van.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default Van
