import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

const Expander: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  ...props
}) => {
  return (
    <Icon
      originalHeight={originalHeight}
      originalWidth={originalWidth}
      {...props}
    >
      <svg
        width='6'
        height='10'
        viewBox='0 0 6 10'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M5.51716 3H0.482843C0.304662 3 0.215428 2.78457 0.341421 2.65858L2.85858 0.141421C2.93668 0.0633165 3.06332 0.0633162 3.14142 0.141421L5.65858 2.65858C5.78457 2.78457 5.69534 3 5.51716 3Z'
          fill='#C4C4C4'
        />
        <path
          d='M0.482843 7L5.51716 7C5.69534 7 5.78457 7.21543 5.65858 7.34142L3.14142 9.85858C3.06332 9.93668 2.93668 9.93668 2.85858 9.85858L0.341422 7.34142C0.215429 7.21543 0.304662 7 0.482843 7Z'
          fill='#C4C4C4'
        />
      </svg>
    </Icon>
  )
}

Expander.defaultProps = {
  originalWidth: 6,
  originalHeight: 10,
  width: 6,
  height: 10,
}

export default Expander
