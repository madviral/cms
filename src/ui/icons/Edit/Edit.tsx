import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

interface EditProps extends IconProps {}

const Edit: FC<EditProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill={color}
    {...props}
  >
    <path d='M19.344 5.346a1 1 0 00-.025-1.388L16.047.682a1 1 0 00-1.388-.027l-9.43 8.74a1 1 0 00-.32.733v3.977a1 1 0 001 1h3.925a1 1 0 00.733-.319l8.777-9.44zm-3.87-2.664l1.728 1.723-1.345 1.38-1.797-1.79 1.414-1.313zM6.873 13.147v-2.633l5.596-5.169L14.433 7.5l-4.998 5.648H6.873zm12.148 4.895a.979.979 0 110 1.958H.979a.979.979 0 010-1.958h18.042z' />
  </Icon>
)

Edit.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#9FB3C8',
}

export default Edit
