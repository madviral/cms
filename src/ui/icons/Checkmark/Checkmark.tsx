import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

export interface CheckmarkProps extends IconProps {}

const Checkmark: FC<CheckmarkProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill={color}
    {...props}
  >
    <path d='M10.6924 16.3355C10.3055 16.7068 9.69452 16.7068 9.30758 16.3355L5.73448 12.9064C5.33382 12.5219 5.33382 11.8812 5.73448 11.4967C6.11212 11.1342 6.70828 11.1338 7.08642 11.4957L9.30762 13.6215C9.69468 13.9919 10.3049 13.9915 10.6915 13.6205L16.9092 7.65337C17.2893 7.28862 17.8897 7.28968 18.2685 7.65577C18.668 8.04186 18.6668 8.6825 18.266 9.06717L10.6924 16.3355Z' />
  </Icon>
)

Checkmark.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#627D98',
}

export default Checkmark
