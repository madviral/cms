import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const Package: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M14 7.66L6.035 3.007m0 0l3.076-1.767a1.76 1.76 0 011.778 0l6.222 3.6c.27.159.494.386.65.659.156.273.239.583.239.9v7.2c0 .316-.083.626-.239.9-.156.273-.38.5-.65.658l-6.222 3.6A1.76 1.76 0 0110 19M6.035 3.008L2.89 4.842a1.79 1.79 0 00-.65.658A1.818 1.818 0 002 6.4v7.2c0 .316.083.626.239.9.156.273.38.5.65.658l6.222 3.6c.27.159.577.242.889.242m0 0v-9M2.24 5.463L10 10.009l7.76-4.546'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

Package.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default Package
