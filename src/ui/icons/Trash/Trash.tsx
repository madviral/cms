import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const Trash: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M1 4.6H2.77778M2.77778 4.6H17M2.77778 4.6V17.2C2.77778 17.6774 2.96508 18.1352 3.29848 18.4728C3.63187 18.8104 4.08406 19 4.55556 19H13.4444C13.9159 19 14.3681 18.8104 14.7015 18.4728C15.0349 18.1352 15.2222 17.6774 15.2222 17.2V4.6H2.77778ZM5.44444 4.6V2.8C5.44444 2.32261 5.63175 1.86477 5.96514 1.52721C6.29854 1.18964 6.75073 1 7.22222 1H10.7778C11.2493 1 11.7015 1.18964 12.0349 1.52721C12.3683 1.86477 12.5556 2.32261 12.5556 2.8V4.6M7.22222 9.1V14.5M10.7778 9.1V14.5'
      stroke={theme.colorsList.secondary[1]}
      fill={theme.color.white}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

Trash.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#627D98',
}

export default Trash
