import { IconProps } from '@vlife-grand-era/vlife-storybook'

export interface PlusProps extends IconProps {}
