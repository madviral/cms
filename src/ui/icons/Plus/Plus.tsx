import React, { FC } from 'react'
import { Icon } from '@vlife-grand-era/vlife-storybook'
import { PlusProps } from './props'

const Plus: FC<PlusProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M13 9C13.5523 9 14 9.44772 14 10C14 10.5523 13.5523 11 13 11H11V13C11 13.5523 10.5523 14 10 14C9.44772 14 9 13.5523 9 13V11H7C6.44772 11 6 10.5523 6 10C6 9.44772 6.44772 9 7 9H9V7C9 6.44772 9.44772 6 10 6C10.5523 6 11 6.44772 11 7V9H13Z'
      fill={color}
    />
  </Icon>
)

Plus.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#627D98',
}

export default Plus
