import { IconProps } from '@vlife-grand-era/vlife-storybook/build'

export interface SettingsProps extends IconProps {
  color?: string
}
