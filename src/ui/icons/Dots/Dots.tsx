import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const Dots: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill={color}
    {...props}
  >
    <path
      d='M11 10a1 1 0 10-2 0 1 1 0 002 0zM11 3a1 1 0 10-2 0 1 1 0 002 0zM11 17a1 1 0 10-2 0 1 1 0 002 0z'
      stroke={theme.colorsList.secondary[1]}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

Dots.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#000',
}

export default Dots
