import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const Printer: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M4.6 7.3V1h10.8v6.3M4.6 15.4H2.8A1.8 1.8 0 011 13.6V9.1a1.8 1.8 0 011.8-1.8h14.4A1.8 1.8 0 0119 9.1v4.5a1.8 1.8 0 01-1.8 1.8h-1.8M4.6 11.8h10.8V19H4.6v-7.2z'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

Printer.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default Printer
