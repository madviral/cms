import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

const Plus: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M7.13504 7C6.2872 7 5.82405 7.98886 6.36682 8.64018L9.23178 12.0781C9.63157 12.5579 10.3684 12.5579 10.7682 12.0781L13.6332 8.64018C14.176 7.98886 13.7128 7 12.865 7H7.13504Z'
      fill={color}
    />
  </Icon>
)

Plus.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#627D98',
}

export default Plus
