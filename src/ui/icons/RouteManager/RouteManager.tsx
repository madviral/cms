import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

export interface RouteIconProps {
  stroke?: string
}

const Users: FC<IconProps & RouteIconProps> = ({
  originalWidth,
  originalHeight,
  color,
  stroke,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <svg
      width='20'
      height='20'
      viewBox='0 0 20 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M18 19v-2a4 4 0 00-4-4H6a4 4 0 00-4 4v2M14 5a4 4 0 11-8 0 4 4 0 018 0z'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
        stroke={stroke}
      />
    </svg>
  </Icon>
)

Users.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[0],
  stroke: '#fff',
}

export default Users
