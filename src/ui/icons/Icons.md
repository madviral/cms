```js
import {
  HomeIcon,
  ArrowLeftIcon,
  SettingsIcon,
  ContentIcon,
  EyeIcon,
  FilterIcon,
  ListIcon,
  LogoMini,
  MailIcon,
  MinusIcon,
  NoteIcon,
  PlusIcon,
  ShoppingIcon,
  SortingIcon,
  LoupeIcon,
  PhoneIcon,
  Chevron,
  ExpanderIcon,
  PictureIcon,
  Triangle,
  CrossIcon,
  RouteMarketIcon,
  RouteServicesIcon,
} from './';

const style = {
  display: 'flex',
  flexDirection: 'row',
  cursor: 'pointer',
  flexWrap: 'wrap'
};

const iconStyles = {
  margin: '0 5px 5px 0',
  background: '#eee',
  padding: 10,
  height: 30,
  width: 30,
  flexShrink: 0,
};

<div style={style}>
  <HomeIcon  style={iconStyles} />
  <LoupeIcon style={iconStyles} />
  <PhoneIcon style={iconStyles} />
  <Chevron style={iconStyles} />
  <ExpanderIcon style={iconStyles} />
  <PictureIcon style={iconStyles} />
  <Triangle style={iconStyles} />
  <CrossIcon style={iconStyles} />
  <ArrowLeftIcon style={iconStyles} />
  <ContentIcon style={iconStyles} />
  <EyeIcon style={iconStyles} />
  <FilterIcon style={iconStyles} />
  <ListIcon style={iconStyles} />
  <LogoMini style={iconStyles} />
  <MailIcon style={iconStyles} />
  <MinusIcon style={iconStyles} />
  <NoteIcon style={iconStyles} />
  <PlusIcon style={iconStyles} />
  <ShoppingIcon style={iconStyles} />
  <SortingIcon style={iconStyles} />
  <SettingsIcon style={iconStyles} />
  <RouteMarketIcon style={iconStyles} />
  <RouteServicesIcon style={iconStyles} />
</div>
```
