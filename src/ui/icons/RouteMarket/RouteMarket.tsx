import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

export interface RouteIconProps {
  stroke?: string
}

const RouteMarket: FC<IconProps & RouteIconProps> = ({
  originalWidth,
  originalHeight,
  color,
  stroke,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <svg
      width='20'
      height='20'
      viewBox='0 0 20 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M17.2 3.90041V1.6C17.2 1.26863 16.9314 1 16.6 1H3.4C3.06863 1 2.8 1.26863 2.8 1.6V3.90041M17.2 3.90041H2.8M17.2 3.90041L18.6861 8.27614C18.8783 8.84205 18.7324 9.46801 18.3098 9.89063L18.1 10.1004C17.6029 10.5975 16.7971 10.5975 16.3 10.1004V10.1004C15.8029 9.60335 14.9971 9.60335 14.5 10.1004V10.1004C14.0029 10.5975 13.1971 10.5975 12.7 10.1004V10.1004C12.2029 9.60335 11.3971 9.60335 10.9 10.1004V10.1004C10.4029 10.5975 9.59706 10.5975 9.1 10.1004V10.1004C8.60294 9.60335 7.79706 9.60335 7.3 10.1004V10.1004C6.80294 10.5975 5.99706 10.5975 5.5 10.1004V10.1004C5.00294 9.60335 4.19706 9.60335 3.7 10.1004V10.1004C3.20294 10.5975 2.39706 10.5975 1.9 10.1004L1.69022 9.89063C1.26761 9.46801 1.1217 8.84205 1.3139 8.27614L2.8 3.90041M2.8 13.2004V18.0004C2.8 18.5527 3.24769 19.0004 3.79998 19.0004C3.85877 19.0004 3.92466 19.0004 4 19.0004H11.4C11.9523 19.0004 12.4 18.5527 12.4 18.0004V13.2004M17.2 13.2004V19.0004M10 15.4004C8.5 13.9004 6.5 13.9004 5.2 15.4004'
        stroke={stroke}
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  </Icon>
)

RouteMarket.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  stroke: '#fff',
  color: '#627D98',
}

export default RouteMarket
