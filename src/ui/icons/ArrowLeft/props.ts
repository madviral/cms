import { IconProps } from '@vlife-grand-era/vlife-storybook'

export interface ArrowLeftProps extends IconProps {}
