import React, { FC } from 'react'
import { Icon } from '@vlife-grand-era/vlife-storybook'
import { ArrowLeftProps } from './props'

const ArrowLeft: FC<ArrowLeftProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M9.71698 0.300001C9.33962 -0.0999992 8.77358 -0.0999992 8.39623 0.300001L0.566037 8.6C-0.18868 9.4 -0.18868 10.6 0.566037 11.4L8.39622 19.7C8.77358 20.1 9.33962 20.1 9.71698 19.7C10.0943 19.3 10.0943 18.7 9.71698 18.3L2.54717 10.7C2.16981 10.3 2.16981 9.7 2.54717 9.3L9.71698 1.7C10.0943 1.3 10.0943 0.700001 9.71698 0.300001Z'
      fill={color}
    />
  </Icon>
)

ArrowLeft.defaultProps = {
  originalWidth: 10,
  originalHeight: 20,
  width: 10,
  height: 20,
  color: '#9FB3C8',
}

export default ArrowLeft
