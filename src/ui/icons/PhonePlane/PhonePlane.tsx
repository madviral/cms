import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const PhonePlane: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M19 14.476v2.71a1.803 1.803 0 01-1.23 1.719c-.24.08-.492.11-.744.088a17.933 17.933 0 01-7.81-2.773 17.63 17.63 0 01-5.43-5.42 17.853 17.853 0 01-2.779-7.83A1.803 1.803 0 012.08 1.153c.23-.101.478-.154.73-.154h2.714a1.812 1.812 0 011.81 1.554c.115.867.328 1.718.634 2.538a1.803 1.803 0 01-.407 1.906L6.41 8.145a14.466 14.466 0 005.43 5.42l1.15-1.148a1.811 1.811 0 011.91-.406c.82.305 1.674.518 2.543.632a1.81 1.81 0 011.556 1.833z'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

PhonePlane.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default PhonePlane
