import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const User: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M18 19v-2a4 4 0 00-4-4H6a4 4 0 00-4 4v2M14 5a4 4 0 11-8 0 4 4 0 018 0z'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

User.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default User
