import React, { FC } from 'react'
import { Icon } from '@vlife-grand-era/vlife-storybook'
import { MinusProps } from './props'

const Minus: FC<MinusProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <rect x='6' y='9' width='8' height='2' rx='1' fill={color} />
  </Icon>
)

Minus.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#9FB3C8',
}

export default Minus
