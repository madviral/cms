import { IconProps } from '@vlife-grand-era/vlife-storybook'

export interface MinusProps extends IconProps {}
