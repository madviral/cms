import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

export interface RouteIconProps {
  stroke?: string
}

const RouteServices: FC<IconProps & RouteIconProps> = ({
  originalWidth,
  originalHeight,
  color,
  stroke,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <svg
      width='20'
      height='20'
      viewBox='0 0 20 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      style={{ display: 'block' }}
    >
      <path
        d='M19 7.14286V16C19 17.1046 18.1046 18 17 18H3C1.89543 18 1 17.1046 1 16V7.14286M19 7.14286C19 6.03829 18.1046 5.14286 17 5.14286H3C1.89543 5.14286 1 6.03829 1 7.14286M19 7.14286L19 9.28571C19 10.9426 17.6569 12.2857 16 12.2857H4C2.34315 12.2857 1 10.9426 1 9.28571V7.14286M12.5714 4.57143V3C12.5714 2.44772 12.1237 2 11.5714 2H8.42857C7.87629 2 7.42857 2.44772 7.42857 3V4.57143H12.5714ZM8.71429 12.2857H11.2857V13.5714C11.2857 14.2815 10.7101 14.8571 10 14.8571C9.28992 14.8571 8.71429 14.2815 8.71429 13.5714V12.2857Z'
        stroke={stroke}
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  </Icon>
)

RouteServices.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  stroke: '#fff',
  color: '#627D98',
}

export default RouteServices
