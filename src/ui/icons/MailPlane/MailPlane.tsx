import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const MailPlane: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M19 4.3c0-.99-.81-1.8-1.8-1.8H2.8c-.99 0-1.8.81-1.8 1.8m18 0v11.4c0 .99-.81 1.8-1.8 1.8H2.8c-.99 0-1.8-.81-1.8-1.8V4.3m18 0l-9 6.3-9-6.3'
      stroke={color}
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

MailPlane.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default MailPlane
