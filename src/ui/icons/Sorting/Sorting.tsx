import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

interface SortingProps extends IconProps {}

const Sorting: FC<SortingProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M1 0a1 1 0 100 2h18a1 1 0 100-2H1zm0 6a1 1 0 000 2h14a1 1 0 100-2H1zm-1 7a1 1 0 011-1h10a1 1 0 110 2H1a1 1 0 01-1-1zm1 5a1 1 0 100 2h6a1 1 0 100-2H1z'
      fill={color}
    />
  </Icon>
)

Sorting.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#9FB3C8',
}

export default Sorting
