import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

export interface CorrectProps extends IconProps {
  strokeWidth?: number
}

const Correct: FC<CorrectProps> = ({
  originalWidth,
  originalHeight,
  color,
  strokeWidth,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill={color}
    {...props}
  >
    <svg
      width='20'
      height='20'
      viewBox='0 0 20 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M6.5 10.3766L8.83333 12.7532L13.5 8.00008M19 10C19 14.9706 14.9706 19 10 19C5.02944 19 1 14.9706 1 10C1 5.02944 5.02944 1 10 1C14.9706 1 19 5.02944 19 10Z'
        stroke={color}
        stroke-width={strokeWidth}
        stroke-linecap='round'
        stroke-linejoin='round'
      />
    </svg>
  </Icon>
)

Correct.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  strokeWidth: 1,
  color: '#4D77FB',
}

export default Correct
