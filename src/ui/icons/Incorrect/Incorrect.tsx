import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

export interface IncorrectProps extends IconProps {
  strokeWidth?: number
}

const Incorrect: FC<IncorrectProps> = ({
  originalWidth,
  originalHeight,
  color,
  strokeWidth,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill={color}
    {...props}
  >
    <svg
      width='20'
      height='20'
      viewBox='0 0 20 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M13 7.00008L10 10.0001M10 10.0001L7 13.0001M10 10.0001L7 7.00008M10 10.0001L13 13.0001M19 10C19 14.9706 14.9706 19 10 19C5.02944 19 1 14.9706 1 10C1 5.02944 5.02944 1 10 1C14.9706 1 19 5.02944 19 10Z'
        stroke={color}
        strokeWidth={strokeWidth}
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  </Icon>
)

Incorrect.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  strokeWidth: 1,
  color: '#627D98',
}

export default Incorrect
