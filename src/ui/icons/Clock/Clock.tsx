import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

export interface ClockProps extends IconProps {
  strokeWidth?: number
}

const Clock: FC<ClockProps> = ({
  originalWidth,
  originalHeight,
  color,
  strokeWidth,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    {...props}
  >
    <path
      d='M10 5V10L13.6 11.8M19 10C19 14.9706 14.9706 19 10 19C5.02944 19 1 14.9706 1 10C1 5.02944 5.02944 1 10 1C14.9706 1 19 5.02944 19 10Z'
      stroke={color}
      strokeWidth={strokeWidth}
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Icon>
)

Clock.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  strokeWidth: 2,
  color: '#627D98',
}

export default Clock
