import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'
import { theme } from 'ui/theme'

const DoubleArrows: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill={color}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M7.7773 11.7275C7.11953 11.7275 6.76021 12.4947 7.1813 13L9.404 15.6673C9.71417 16.0395 10.2858 16.0395 10.596 15.6673L12.8187 13C13.2398 12.4947 12.8805 11.7275 12.2227 11.7275H7.7773Z'
      fill={color}
    />
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M7.7773 8.21888C7.11953 8.21888 6.76021 7.4517 7.1813 6.94639L9.404 4.27915C9.71417 3.90695 10.2858 3.90695 10.596 4.27915L12.8187 6.94639C13.2398 7.4517 12.8805 8.21888 12.2227 8.21888H7.7773Z'
      fill={color}
    />
  </Icon>
)

DoubleArrows.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: theme.colorsList.secondary[1],
}

export default DoubleArrows
