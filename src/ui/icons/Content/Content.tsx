import React, { FC } from 'react'
import { Icon, IconProps } from '@vlife-grand-era/vlife-storybook'

interface ContentProps extends IconProps {}

const Content: FC<ContentProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    fill='none'
    width={20}
    height={20}
    {...props}
  >
    <svg
      width='20'
      height='20'
      viewBox='0 0 20 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M8 2C8 1.44772 7.55228 1 7 1H2C1.44772 1 1 1.44772 1 2V10C1 10.5523 1.44772 11 2 11H7C7.55228 11 8 10.5523 8 10V2Z'
        stroke={color}
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M19 2C19 1.44772 18.5523 1 18 1H13C12.4477 1 12 1.44772 12 2V4C12 4.55228 12.4477 5 13 5H18C18.5523 5 19 4.55228 19 4V2Z'
        stroke={color}
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M19 10C19 9.44772 18.5523 9 18 9H13C12.4477 9 12 9.44772 12 10V18C12 18.5523 12.4477 19 13 19H18C18.5523 19 19 18.5523 19 18V10Z'
        stroke={color}
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M8 16C8 15.4477 7.55228 15 7 15H2C1.44772 15 1 15.4477 1 16V18C1 18.5523 1.44772 19 2 19H7C7.55228 19 8 18.5523 8 18V16Z'
        stroke={color}
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  </Icon>
)

Content.defaultProps = {
  originalWidth: 20,
  originalHeight: 20,
  width: 20,
  height: 20,
  color: '#627D98',
}

export default Content
