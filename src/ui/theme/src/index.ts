import fontWeight from './fontWeight'
import fontFamily from './fontFamily'
import lineHeight from './lineHeight'
import borderRadius from './borderRadius'
import fontSize from './fontSize'
import color, { colorsList, colorTools } from './color'

export const theme = {
  fontSize,
  fontWeight,
  fontFamily,
  lineHeight,
  borderRadius,
  color,
  colorsList,
  colorTools,
}

export * from './types'
