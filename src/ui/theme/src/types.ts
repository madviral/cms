export interface ThemeFontSize {
  xs: string
  s: string
  m: string
  l: string
  xl: string
}

export interface ThemeBorderRadius {
  xxs: string
  xs: string
  s: string
  n: string
  m: string
  l: string
  xl: string
  r: string
}

export interface ThemeFontFamily {
  ms: string
}

export interface ThemeLineHeight {
  xs: number
  s: number
  m: number
  l: number
  xl: number
}

export interface ThemeFontWeight {
  light: number
  regular: number
  medium: number
  semibold: number
  bold: number
  black: number
}

export interface ThemeColor {
  [color: string]: string
}
export interface ThemeColors {
  [color: string]: string | string[]
}

export interface ThemeColorTools {
  getColor: (name: string, alpha: number) => string
  getPrimary: (alpha: number) => string
  getSecondary: (alpha: number) => string
}

export interface Theme {
  fontWeight: ThemeFontWeight
  fontFamily: ThemeFontFamily
  lineHeight: ThemeLineHeight
  borderRadius: ThemeBorderRadius
  color: ThemeColor
  colors?: ThemeColors
  colorsList?: ThemeColors
  colorTools: ThemeColorTools
}
