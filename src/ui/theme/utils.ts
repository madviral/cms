import { CSSObject } from '@emotion/serialize'
import { css } from '@emotion/core'
import { color as styleColor } from 'styled-system'

export type WithSXProps = {
  sx?: CSSObject
}

export const sx = props => css(props.sx)

export const borderRadius = value => ({ theme }) => theme.borderRadius[value]

export const color = value => ({ theme }) =>
  styleColor({ color: value, theme: { colors: theme.colorsList } }).color
