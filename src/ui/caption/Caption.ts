import styled from 'ui/theme/styled'

interface CaptionProps {
  fontSize?: string
  color?: string
}

export default styled.p<CaptionProps>`
  margin: 0;
  font-family: ${props => props.theme.fontFamily.ms};
  color: ${props => props.color || props.theme.color.secondary};
  font-size: ${props => props.fontSize};
`
