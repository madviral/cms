import React, { FC } from 'react'
import { getYear } from 'date-fns'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { styled, theme as mainTheme } from '../theme'
import { Box } from '../layout'
import { PhoneIcon } from '../icons'
import { Text } from '../text'
import { RightsProps, ToggledProps } from './types'
import { useTranslation } from 'react-i18next'

const Wrap = styled.div`
  margin: 0 16px;
`

const InfoWrap = styled.div<ToggledProps>`
  background: ${props => props.theme.colorsList.primary[1]};
  border-radius: 8px;
  height: 48px;
  display: flex;
  align-items: center;
  flex-direction: row;
  overflow: hidden;
  ${props =>
    props.toggled
      ? `
    justify-content: flex-start;
    padding: 0 8px;
  `
      : `padding: 0 16px;`}
`

const PhoneIconWrap = styled.div`
  background: #fff;
  height: 32px;
  width: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 16px;
  flex-shrink: 0;
`

const TextWrap = styled.div<ToggledProps>`
  width: 140px;
  margin-left: 10px;
  flex-shrink: 0;
  // display: ${props => (props.toggled ? 'none' : 'block')};
`

const Rights: FC<RightsProps> = ({ toggled }) => {
  const [t] = useTranslation()
  return (
    <Wrap>
      <InfoWrap toggled={toggled}>
        <PhoneIconWrap>
          <PhoneIcon color={mainTheme.colorsList.primary[1]} />
        </PhoneIconWrap>
        <TextWrap toggled={toggled}>
          <Text
            as='p'
            color='white'
            fontSize='10px'
            lineHeight='12px'
            fontWeight={500}
          >
            58 58
          </Text>
          <Text
            as='p'
            color='white'
            fontSize='10px'
            lineHeight='12px'
            fontWeight={500}
          >
            {t('support')}
          </Text>
        </TextWrap>
      </InfoWrap>
      <Box
        display='flex'
        justifyContent='center'
        width={1}
        mt='16px'
        height='22px'
        overflow='hidden'
      >
        <Text as='p' fontSize='14px' lineHeight='1.6'>
          <Condition match={!toggled}>
            © CMS.VLife. {getYear(new Date())}
          </Condition>
          <Condition match={toggled}>CMS</Condition>
        </Text>
      </Box>
    </Wrap>
  )
}

export default Rights
