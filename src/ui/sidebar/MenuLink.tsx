import React, { FC, createElement } from 'react'
import { styled, theme } from 'ui/theme'
import { NavLink, useLocation } from 'react-router-dom'
import { withTranslation } from 'react-i18next'
import { Text } from 'ui/text'
import { Caption } from 'ui'
import { ifProp, ifNotProp } from 'styled-tools'
import { Condition } from '../condition'
import { StyledLinkProps } from './types'

const IconWrap = styled.span`
  & > svg {
    display: block;
  }
`

const Parent = styled.div`
  background: ${props => props.theme.colorsList.secondary[5]};
`

const StyledLink = styled(NavLink, {
  shouldForwardProp: prop => !['toggled', 'child'].includes(prop),
})<StyledLinkProps>`
  position: relative;
  display: flex;
  height: 40px;
  flex-direction: row;
  align-items: center;
  text-decoration: none;
  justify-content: space-between;
  z-index: 1;
  :hover {
    background: ${props => props.theme.colorsList.secondary[3]};
  }
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 2px;
    height: 100%;
    background-color: ${props => props.theme.colorsList.primary[0]};
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    display: none;
  }
  &.--active {
    color: ${props => props.theme.colorsList.primary[0]};
    ${ifNotProp(
      'child',
      `
      &::before {
        display: block;
      }
    `,
    )}
  }
  &:not(.--active) {
    color: ${props => props.theme.colorsList.secondary[0]};
  }
  ${ifProp('toggled', 'padding: 0 12px;', 'padding: 0 24px;')}
  ${ifProp('child', 'padding-left: 44px;')}
`

interface StyledCaptionProps {
  toggled?: boolean
}

const StyledCaption = styled(Text)<StyledCaptionProps>`
  min-width: 180px;
  font-size: 12px;
  margin-left: 16px;
  font-weight: 500;
  display: ${props => (props.toggled ? 'none' : 'block')};
`

const NumberWrap = styled.div`
  border-radius: 12px;
  height: 20px;
  background: ${props => props.theme.colorsList.secondary[4]};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 12px;
`

const StyledCaptionNumber = styled(Caption)`
  font-size: 10px;
`

const Left = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

const Right = styled.div``

const MenuLink: FC<any> = ({
  childs,
  icon,
  name,
  t,
  toggled,
  exact,
  path,
  count = 0,
}) => {
  const location = useLocation()
  const active = path.split('/')[2] === location.pathname.split('/')[2]

  return (
    <Parent>
      <StyledLink
        toggled={toggled}
        to={path}
        activeClassName='--active'
        exact={exact}
      >
        <Left>
          <IconWrap>
            {createElement(icon, {
              color: active
                ? theme.colorsList.primary[0]
                : theme.colorsList.secondary[0],
            })}
          </IconWrap>
          <StyledCaption toggled={toggled}>{t(name)}</StyledCaption>
        </Left>
        <Right>
          {count ? (
            <NumberWrap>
              <StyledCaptionNumber>{count}</StyledCaptionNumber>
            </NumberWrap>
          ) : null}
        </Right>
      </StyledLink>
      <Condition match={!toggled && childs && active}>
        {childs?.map(route => (
          <StyledLink
            to={route.path}
            key={route.key}
            exact={route.exact}
            activeClassName='--active'
            child
          >
            <StyledCaption>{t(route.nav.name)}</StyledCaption>
          </StyledLink>
        ))}
      </Condition>
    </Parent>
  )
}

export default withTranslation('shop')(MenuLink)
