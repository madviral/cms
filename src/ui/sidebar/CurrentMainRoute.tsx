import React, { FC } from 'react'
import styled from 'ui/theme/styled'
import { Caption, Chevron } from 'ui'
import { useTranslation } from 'react-i18next'

interface ToggleProps {
  toggled: boolean
}

const Wrap = styled.div<ToggleProps>`
  cursor: pointer;
  height: 48px;
  display: flex;
  flex-direction: row;
  background: ${props => props.theme.colorsList.primary[0]};
  border-radius: 8px;
  align-items: center;
  justify-content: space-between;
  & > div > svg {
    display: block;
  }
  ${props => (props.toggled ? 'padding: 0 14px;' : 'padding: 0 24px;')}
`

const StyledCaption = styled(Caption)`
  font-size: 14px;
  font-weight: 600;
  color: ${props => props.theme.color.white};
  margin-left: 16px;
`

const Left = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

const Right = styled.div``

interface CurrentMainRouteProps
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > {
  toggled: boolean
  route: any
}

const CurrentMainRoute: FC<CurrentMainRouteProps> = ({
  route,
  toggled,
  ...props
}) => {
  const [t] = useTranslation()
  return (
    <Wrap toggled={toggled} {...props}>
      <Left>
        {route?.nav ? <route.nav.icon /> : null}
        {toggled ? null : <StyledCaption>{t(route?.nav?.name)}</StyledCaption>}
      </Left>
      {toggled ? null : (
        <Right>
          <Chevron color='#fff' />
        </Right>
      )}
    </Wrap>
  )
}

export default CurrentMainRoute
