import styled from 'ui/theme/styled'
import React, { FC } from 'react'
import { ifProp } from 'styled-tools'
import { Box } from '../layout'
import { SidebarProps, ToggledProps } from './types'
import LogoWrapper from './LogoWrapper'
import TriggerButton from './Trigger'

const StyledSidebar = styled.aside<ToggledProps>(
  {
    gridArea: 'aside',
    userSelect: 'none',
    maxHeight: '100vh',
    '& .__wrapper': {
      position: 'fixed',
      left: 0,
      top: 0,
      bottom: 0,
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      padding: '32px 0 16px 0',
      boxSizing: 'border-box',
      backgroundColor: '#fff',
      zIndex: 100,
    },
    '& .__spacer, & .__wrapper': {
      willChange: 'width',
      transition: 'width 200ms cubic-bezier(0.2, 0, 0, 1) 0s',
    },
  },
  ifProp(
    'toggled',
    {
      '& .__spacer': {
        width: 80,
      },
      '& .__wrapper': {
        width: 80,
      },
    },
    {
      '& .__spacer': {
        width: 300,
      },
      '& .__wrapper': {
        width: 300,
      },
    },
  ),
  ifProp('hovered', {
    '& .__wrapper': {
      width: 300,
    },
  }),
)

const ChildrenWrap = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

const Sidebar: FC<SidebarProps> = ({
  hovered,
  toggled,
  onToggle,
  children,
  onMouseEnter,
  onMouseLeave,
}) => (
  <StyledSidebar
    hovered={hovered}
    toggled={toggled}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
  >
    <div className='__spacer' />
    <div className='__wrapper'>
      <LogoWrapper toggled={toggled && !hovered} />
      <Box ml='auto' mb={16}>
        <TriggerButton toggled={toggled} onToggle={onToggle} />
      </Box>
      <ChildrenWrap>{children}</ChildrenWrap>
    </div>
  </StyledSidebar>
)

export default Sidebar
