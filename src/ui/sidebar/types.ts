import { NavLinkProps } from 'react-router-dom'
import { IRoute } from 'src/modules/common/types'
import { TFunction } from 'i18next'

export interface ToggledProps {
  toggled?: boolean
  hovered?: boolean
}

export interface TriggerProps {
  hovered?: boolean
  toggled?: boolean
  onToggle?: () => void
}
export interface SidebarProps extends TriggerProps {
  onMouseEnter?: VoidFunction
  onMouseLeave?: VoidFunction
}

export interface NavProps {}

export interface UserInfoProps extends ToggledProps {
  name?: string
  surname?: string
  position?: string
  img?: string
}

export interface RightsProps extends ToggledProps {}

export interface NavMenuProps {}

export interface StyledLinkProps extends NavLinkProps {
  active?: boolean
  child?: boolean
  toggled?: boolean
}

export interface MenuLinkProps {
  t?: TFunction
  path?: string
  childs?: IRoute[]
  count?: number
  toggled?: boolean
  name?: string
  icon?: any
  exact?: boolean
}
