import React, { FC } from 'react'
import styled from 'ui/theme/styled'
import { ifProp } from 'styled-tools'
import { ArrowLeftIcon } from '../icons'
import { TriggerProps, ToggledProps } from './types'

const StyledTrigger = styled.button<ToggledProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  height: 40px;
  border-radius: 0 10px 10px 0;
  border: none;
  cursor: pointer;
  background-color: ${props => props.theme.color.paleBlue};
  outline: none;
  padding: 0;
  ${ifProp(
    'toggled',
    `
  transform: rotate(180deg);
  border-radius: 0 10px 10px 0;
`,
    `
  border-radius: 10px 0 0 10px;
`,
  )}
`

const TriggerButton: FC<TriggerProps> = ({ toggled, onToggle }) => (
  <StyledTrigger toggled={toggled} onClick={onToggle}>
    <ArrowLeftIcon />
  </StyledTrigger>
)

export default TriggerButton
