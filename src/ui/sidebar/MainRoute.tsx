import React, { FC } from 'react'
import styled from 'ui/theme/styled'
import { Caption } from 'ui'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

interface ToggleProps {
  isToggled: boolean
}

const Wrap = styled(Link, {
  shouldForwardProp: prop => !['isToggled'].includes(prop),
})<ToggleProps>`
  height: 40px;
  background: #fff;
  text-decoration: none;
  &:hover {
    background: ${props => props.theme.colorsList.primary[4]};
  }
  display: flex;
  flex-direction: row;
  align-items: center;
  & > div > svg > svg > path {
    stroke: ${props => props.theme.colorsList.secondary[0]};
  }
  &:hover > div > svg > svg > path {
    stroke: ${props => props.theme.colorsList.primary[0]};
  }
  ${props =>
    props.isToggled
      ? `
    padding: 0 12px;
  `
      : `padding: 0 24px;`}
`

const Left = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

const StyledCaption = styled(Caption)`
  margin-left: 16px;
  font-size: 14px;
  &:hover {
    color: ${props => props.theme.colorsList.primary[0]};
  }
`

const Right = styled.div``

const MainRoute: FC<any> = ({ toggled, icon, name, link, onClick }) => {
  const [t] = useTranslation()
  return (
    <Wrap onClick={onClick} isToggled={toggled} to={link}>
      <Left>
        {React.createElement(icon)}
        {toggled ? null : <StyledCaption>{t(name)}</StyledCaption>}
      </Left>
      <Right></Right>
    </Wrap>
  )
}

export default MainRoute
