import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { LogoMini, LogoBig } from '../icons'
import { Box } from '../layout'

interface ToggledProps {
  toggled: boolean
}

const LogoWrapper: FC<ToggledProps> = ({ toggled }) => {
  const homeUrl = '/'
  return (
    <Box>
      <Condition match={!toggled}>
        <Box display='flex' justifyContent='center' height={38} mb={16}>
          <Link to={homeUrl}>
            <LogoBig />
          </Link>
        </Box>
      </Condition>
      <Condition match={toggled}>
        <Box display='flex' justifyContent='center' height={38} mb={16}>
          <Link to={homeUrl}>
            <LogoMini />
          </Link>
        </Box>
      </Condition>
    </Box>
  )
}

export default LogoWrapper
