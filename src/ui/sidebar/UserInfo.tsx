import React, { FC } from 'react'
import { styled } from '../theme'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { ToggledProps, UserInfoProps } from './types'
import { Caption } from 'ui'
import SignOutButton from '../../modules/common/containers/SignOutButton'

const Wrap = styled.div<ToggledProps>`
  margin: 0 16px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  background: ${props => props.theme.colorsList.primary[2]};
  border-radius: 8px;
  height: 48px;
  overflow: hidden;
  ${props => (props.toggled ? `padding: 0 8px;` : `padding: 0 16px;`)}
`

const PhotoWrap = styled.div<ToggledProps>`
  height: 32px;
  width: 32px;
  border-radius: 16px;
  overflow: hidden;
  flex-shrink: 0;
`

const PhotoPlaceholder = styled.div`
  background: red;
  height: 100%;
  width: 100%;
`

const UserWrap = styled.div<ToggledProps>`
  margin: 0 12px;
  flex-shrink: 0;
  ${props =>
    props.toggled &&
    `
    display: none;
  `}
`

const FullnameCaption = styled(Caption)`
  font-size: 12px;
  color: black;
`

const PositionCaption = styled(Caption)`
  font-size: 10px;
`

const UserInfo: FC<UserInfoProps> = ({
  name,
  surname,
  position,
  img,
  toggled,
}) => {
  const fullname = `${name} ${surname}`
  return (
    <Wrap toggled={toggled}>
      <PhotoWrap toggled={toggled}>
        <Condition match={img}>
          <img src={img} alt={fullname} />
        </Condition>
        <Condition match={!img}>
          <PhotoPlaceholder />
        </Condition>
      </PhotoWrap>
      <UserWrap toggled={toggled}>
        <FullnameCaption>{fullname}</FullnameCaption>
        <PositionCaption>{position}</PositionCaption>
      </UserWrap>
      <SignOutButton />
    </Wrap>
  )
}

UserInfo.defaultProps = {
  name: '',
  surname: '',
  position: '',
}

export default UserInfo
