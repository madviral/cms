import React, { FC } from 'react'
import { Box } from 'ui/layout'
import { styled } from 'ui/theme'
import { borderRadius, color } from 'ui/theme/utils'
import Tooltip, { TooltipWrapProps } from './Tooltip'

const StyledWrapper = styled(Box)`
  border-radius: ${borderRadius('n')};
  border: 2px solid #fff;
  box-shadow: 0px 4px 8px ${color('secondary.2')};
`

const Shadowed: FC<TooltipWrapProps> = ({ children, ...props }) => {
  return (
    <Tooltip {...props}>
      <StyledWrapper>{children}</StyledWrapper>
    </Tooltip>
  )
}

export default Shadowed
