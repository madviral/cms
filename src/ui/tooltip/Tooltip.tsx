import React, { FC } from 'react'
import ReactTooltip, { TooltipProps } from 'react-tooltip'
import { styled } from 'ui/theme'

interface WrapperProps {
  notFade?: boolean
}
export interface TooltipWrapProps extends TooltipProps, WrapperProps {
  id: string
}

const Wrapper = styled.div<WrapperProps>`
  ${props =>
    props.notFade &&
    `.show {
    opacity: 1;
  }`}
`

const Tooltip: FC<TooltipWrapProps> = ({
  id,
  children,
  notFade,
  place,
  ...props
}) => {
  return (
    <Wrapper notFade={notFade}>
      <ReactTooltip
        {...props}
        id={id}
        place={place}
        arrowColor='transparent'
        backgroundColor='transparent'
      >
        {children}
      </ReactTooltip>
    </Wrapper>
  )
}

Tooltip.defaultProps = {
  place: 'right',
  effect: 'float',
}
export default Tooltip
