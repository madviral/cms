```js
import Tooltip from './Tooltip'
import Shadowed from './Shadowed'
const divStyle = {
  background: '#e2e2e2',
  padding: 20,
  width: 200,
}

const tooltipStyle = {
  width: 200,
  height: 200,
  background: 'pink',
  borderRadius: 4,
}

;<div>
  <div
    data-tip
    data-for='tooltip_float'
    style={{ ...divStyle, marginBottom: 30 }}
  >
    floating tooltip
  </div>
  <Tooltip id='tooltip_float'>
    <div style={tooltipStyle}>
      <p>effect: 'float'</p>
    </div>
  </Tooltip>
  <div data-tip data-for='tooltip_solid' style={divStyle}>
    solid tooltip
  </div>
  <Tooltip id='tooltip_solid' effect='solid'>
    <div style={tooltipStyle}>
      <p>effect: 'solid'</p>
    </div>
  </Tooltip>
  <div data-tip data-for='tooltip_solid_shadow' style={divStyle}>
    solid tooltip
  </div>
  <Shadowed effect='solid' id='tooltip_solid_shadow'>
    <div style={tooltipStyle}>
      <p>effect: 'solid', type: shadowed</p>
    </div>
  </Shadowed>
</div>
```
