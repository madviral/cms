import React from 'react'
import RcDrawer from 'rc-drawer'
import 'rc-drawer/assets/index.css'

const Drawer = ({ children, ...props }) => {
  return <RcDrawer {...props}>{children}</RcDrawer>
}

export default Drawer
