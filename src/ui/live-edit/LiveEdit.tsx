import React, { FocusEvent, FormEvent, useState, memo } from 'react'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { Text } from 'ui/text'
import { Box } from 'ui/layout'
import { EditIcon } from 'ui/icons'
import { styled } from 'ui/theme'
import { ButtonTransparent } from 'ui/buttons/Button'

type LiveEditProps = {
  defaultValue: string
  value?: string
  onChange?: (
    e: FormEvent<HTMLInputElement> | FocusEvent<HTMLInputElement>,
  ) => void
}

const StyledInput = styled.input`
  background-color: transparent;
  border: none;
  outline: none;
  font-family: ${({ theme }) => theme.fontFamily.ms};
  font-size: 18px;
  height: 28px;
  padding-left: 0px;
  padding-top: 0;
  padding-bottom: 0;
  line-height: 1;
  font-weight: 500;
  color: ${({ theme }) => theme.colorsList.secondary[0]};
`

const LiveEdit = ({ defaultValue, value, onChange }: LiveEditProps) => {
  const [toggleEdit, setToggleEdit] = useState(false)
  const [init, setInit] = useState(false)

  return (
    <>
      <Condition match={toggleEdit}>
        <StyledInput
          type='text'
          value={init ? value : defaultValue}
          onChange={e => {
            if (!init) {
              setInit(true)
            }
            onChange(e)
          }}
          autoFocus
          onBlur={e => {
            if (value.length === 0) {
              onChange({ ...e, target: { ...e.target, value: defaultValue } })
            }
            setToggleEdit(false)
          }}
        />
      </Condition>
      <Condition match={!toggleEdit}>
        <Text
          fontWeight='500'
          fontSize='18px'
          color='secondary.0'
          lineHeight={1.75}
        >
          {value || defaultValue}
        </Text>
        <Box ml='12px' display='flex' alignItems='center'>
          <ButtonTransparent onClick={() => setToggleEdit(!toggleEdit)}>
            <EditIcon />
          </ButtonTransparent>
        </Box>
      </Condition>
    </>
  )
}

export default memo(LiveEdit)
