```js
  const style = {
    width: 80,
    height: 30,
    background: 'gray',
    borderRadius: 2
  };
  const lstyle = {
    width: 120,
    height: 30,
    background: 'lightgray',
    margin: '5px 0',
    borderRadius: 2
  };

  <Description title='Some title' footer='some footer'>
    <div style={style} />
    <div style={lstyle} />
    <div style={style} />
  </Description>
```