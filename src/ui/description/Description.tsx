import React, { FC } from 'react'
import { Box } from 'ui/layout'
import styled from 'ui/theme/styled'
import { useTranslation } from 'react-i18next'

interface DesciptionProps {
  title: string
  footer?: string
}

const Title = styled.p`
  line-height: 14px;
  font-size: 14px;
  color: ${props => props.theme.color.secondary};
  margin-bottom: 8px;
`

const Footer = styled.p`
  font-size: 12px;
  line-height: 14px;
  color: ${props => props.theme.colorTools.getSecondary(0.5)};
  margin-top: 8px;
`

const Description: FC<DesciptionProps> = ({ children, title, footer }) => {
  const [t] = useTranslation()
  return (
    <Box>
      <Title>{t(title)}</Title>
      {children}
      <Footer>{t(footer)}</Footer>
    </Box>
  )
}

export default Description
