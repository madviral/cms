import styled from 'ui/theme/styled'
import { AvatarProps, RowProps } from 'ui/table/types/interfaces'
import { Appearance } from 'ui/table/types/enum'

export const THead = styled.div`
  padding-top: 15px;
`

export const TBody = styled.div`
  padding-bottom: 15px;

  .table-row {
    cursor: pointer;
    transition: all 0.3s;

    &:hover {
      background: ${props => props.theme.color.paleBlue};
    }
  }
`

export const Row = styled.div<RowProps>`
  padding: 0px 30px;

  ${props =>
    props.appearance === Appearance.mini &&
    `
    border-bottom: 1px solid ${props.theme.colorsList.secondary[2]};
  `}

  .table-inner-row {
    display: flex;
    justify-content: space-between;
    align-items: center;

    ${props =>
      props.appearance === Appearance.mini &&
      `
      padding: 12px 0;
    `}

    ${props =>
      props.appearance === Appearance.default &&
      `
      padding: 28px 0;
      border-bottom: 1px solid ${props.theme.colorsList.secondary[2]};
    `}
  }

  ${props =>
    props.checked &&
    `
    background: ${props.theme.colorsList.secondary[5]};
  `}

  &:last-child {
    .body-inner-row {
      border-bottom: none;
    }
  }
`

export const Avatar = styled.div<AvatarProps>`
  background: ${props => props.theme.color.lightGrayishBlue};
  border-radius: ${props => (props.rounded ? '50%' : '8px')};
  overflow: hidden;
  margin-right: 15px;
  min-width: 40px;
  min-height: 40px;

  img {
    width: 100%;
    height: 100%;
  }
`

export const HeadSettingsColumn = styled.div`
  margin-right: 30px;
`

export const SettingsColumns = styled.div`
  margin-right: 30px;
  cursor: pointer;
`
