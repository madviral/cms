import React, { FC } from 'react'

import { Box } from 'ui/layout'

import { Appearance } from 'ui/table/types/enum'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { TableProps } from 'ui/table/types/interfaces'
import { FilterIcon, Popover, Text } from 'ui'
import { theme } from 'ui/theme'

import {
  Avatar,
  HeadSettingsColumn,
  Row,
  SettingsColumns,
  TBody,
  THead,
} from './styles'

const RenderRows = (item, column) => (
  <Box display='flex' alignItems='center'>
    {column.avatar?.isExist && (
      <Avatar>
        {item.avatar && <img src={item.avatar} alt={item[column.key]} />}
      </Avatar>
    )}
    <Box>
      <Text fontSize={14} color={theme.color.secondary}>
        {item[column.key]}
      </Text>

      {column.extraLabelKey && (
        <Text as='p' fontSize={13} color={theme.color.secondary} mt='5px'>
          {item[column.extraLabelKey]}
        </Text>
      )}
    </Box>
  </Box>
)

const Table: FC<TableProps> = ({
  data,
  columns,
  rowSelection,
  rowSettings,
  onRowClick,
  appearance,
}) => {
  const getWidth = column => column.width || `${100 / columns.length}%`
  const getCheckStatus = item =>
    rowSelection &&
    rowSelection.selectedRows.filter(row => row === item.id).length

  const handleCheckbox = (event, id) => {
    const selectedRows = rowSelection.selectedRows
    const exists = selectedRows.includes(id)

    exists
      ? rowSelection.onChange(selectedRows.filter(rowId => rowId !== id))
      : rowSelection.onChange([...selectedRows, id])
  }

  const selectAll = () => {
    const rows = data.map(item => item.id)

    rowSelection.selectedRows.length === rows.length
      ? rowSelection.onChange([])
      : rowSelection.onChange(rows)
  }

  return (
    <Box>
      <THead>
        <Row appearance={appearance}>
          <div className='table-inner-row'>
            {rowSelection && (
              <Box mr='20px'>
                <FormCheckbox
                  id='all'
                  name='check'
                  // outline
                  indeterminate={rowSelection.selectedRows.length !== 0}
                  checked={
                    rowSelection.selectedRows.length > 0 &&
                    data.length === rowSelection.selectedRows.length
                  }
                  onChange={selectAll}
                />
              </Box>
            )}

            {rowSettings && (
              <HeadSettingsColumn>
                <FilterIcon />
              </HeadSettingsColumn>
            )}

            {columns.map((column, idx) => (
              <Box width={getWidth(column)} key={idx}>
                <Text
                  fontSize={14}
                  color={theme.color.secondary}
                  fontWeight={theme.fontWeight.semibold}
                >
                  {column.title}
                </Text>
              </Box>
            ))}
          </div>
        </Row>
      </THead>
      <TBody>
        {data.map((item, idx) => (
          <Row
            className={onRowClick && 'table-row'}
            onClick={() => onRowClick(item, idx)}
            appearance={appearance}
            checked={!!getCheckStatus(item)}
            key={idx}
          >
            <div className='table-inner-row body-inner-row'>
              {rowSelection && (
                <Box mr='20px'>
                  <FormCheckbox
                    id={`${Math.random() * 100}`}
                    name='checkbox'
                    // outline
                    checked={
                      rowSelection.selectedRows.filter(
                        rowId => rowId === item.id,
                      ).length !== 0
                    }
                    onChange={handleCheckbox}
                    elemId={item.id}
                  />
                </Box>
              )}

              {rowSettings && (
                <Popover
                  position='right'
                  deleteRow={() => rowSettings.delete(item.id)}
                  updateRow={() => rowSettings.update(item.id)}
                >
                  <SettingsColumns>
                    <FilterIcon />
                  </SettingsColumns>
                </Popover>
              )}

              {columns.map((column, id) => (
                <Box width={getWidth(column)} key={id}>
                  {RenderRows(item, column)}
                </Box>
              ))}
            </div>
          </Row>
        ))}
      </TBody>
    </Box>
  )
}

Table.defaultProps = {
  appearance: Appearance.default,
}

export default Table
