import { Appearance } from 'ui/table/types/enum'

interface Columns {
  title: string
  key: string
  width?: string
  extraLabelKey?: string
  avatar?: {
    isExist: boolean
    rounded?: boolean
  }
}

export interface TableProps {
  columns: Columns[]
  data: any[]
  rowSettings?: any
  appearance?: Appearance
  onRowClick?: (record, idx) => void
  rowSelection?: {
    selectedRows: any[]
    onChange: (selectedRows: number[]) => void
  }
}

export interface RowProps {
  appearance?: Appearance
  checked?: boolean
}

export interface AvatarProps {
  rounded?: boolean
}
