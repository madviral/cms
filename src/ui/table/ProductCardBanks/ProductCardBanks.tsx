import React, { FC } from 'react'
import { Box } from 'ui/layout'
import { Text } from 'ui'
import styled from 'ui/theme/styled'

interface Bank {
  id: number
  name: string
  bic: string
}
interface ProductCardBanksProps {
  banks: Bank[]
}

interface StyledBoxProps {
  withBorder?: boolean
}
const Row = styled(Box)<StyledBoxProps>`
  height: 56px;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
  border-bottom: ${props =>
    props.withBorder
      ? `1px solid ${props.theme.colorTools.getSecondary(0.75)}`
      : '0'};
`

const Cell = styled(Box)<StyledBoxProps>`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border-bottom: ${props =>
    props.withBorder
      ? `1px solid ${props.theme.colorTools.getSecondary(0.5)}`
      : ''};
`

const Img = styled.img`
  height: 40px;
  width: 40px;
  border-radius: ${props => props.theme.borderRadius.xs};
  border: 1px solid ${props => props.theme.colorTools.getSecondary(0.5)};
  margin-right: 20px;
  flex-shrink: 0;
  box-sizing: border-box;
`

const ProductCardBanks: FC<ProductCardBanksProps> = ({ banks }) => {
  return (
    <Box>
      <Row withBorder>
        <Cell>
          {/* <Tabs
            type='switcher'
            tabs={[
              { label: 'Кредит', id: 1, component: null },
              { label: 'Рассрочка', id: 2, component: null },
            ]}
          /> */}
        </Cell>
        <Cell>
          <p>3 mon</p>
        </Cell>
        <Cell>
          <p>6 mon</p>
        </Cell>
        <Cell>
          <p>12 mon</p>
        </Cell>
      </Row>
      {banks.map((bank, idx) => (
        <Row key={bank.id}>
          <Cell withBorder={!(idx === banks.length - 1)}>
            <Img />
            <Text fontSize={12}>{bank.name}</Text>
          </Cell>
          <Cell withBorder={!(idx === banks.length - 1)}>
            <Text fontSize={12}>5% 300000</Text>
          </Cell>
          <Cell withBorder={!(idx === banks.length - 1)}>
            <Text fontSize={12}>10% 600000</Text>
          </Cell>
          <Cell withBorder={!(idx === banks.length - 1)}>
            <Text fontSize={12}>15% 900000</Text>
          </Cell>
        </Row>
      ))}
    </Box>
  )
}

export default ProductCardBanks
