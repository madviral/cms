import React, { FC } from 'react'
import { Box } from 'ui/layout'
import { Condition } from 'ui/condition'
import { Divider } from 'ui/divider'

const defaultProps = {
  bordered: true,
}

type RowProps = {
  bordered?: boolean
}

const Row: FC<RowProps> = ({ bordered, children }) => {
  return (
    <Box>
      <Box display='flex' alignItems='center' height='48px' position='relative'>
        {children}
      </Box>
      <Box px='16px'>
        <Condition match={bordered}>
          <Divider />
        </Condition>
      </Box>
    </Box>
  )
}

Row.defaultProps = defaultProps

export default Row
