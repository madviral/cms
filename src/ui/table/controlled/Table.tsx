import React, { FC, useEffect, useRef, useState } from 'react'
import RcTable from 'rc-table'
import { TableProps as RcTableProps } from 'rc-table/lib/Table'
import { theme } from 'ui/theme'
import { ColumnType } from 'rc-table/lib/interface'
import { Box } from 'ui/layout'
import { RouteMarketIcon } from 'ui'
import { Caption } from 'src/ui/caption'
import { Global, css } from '@emotion/core'
import { getTableStyles } from './styles'

export { ColumnType }

type TableProps = {
  clickable?: boolean
  isPlaceholder?: boolean
  placeholderText?: string
} & RcTableProps

const tableClassName = 'controled-custom-table'
const Table: FC<TableProps> = ({
  clickable,
  data,
  isPlaceholder,
  placeholderText,
  columns,
  ...props
}) => {
  const wrapperRef = useRef(null)
  const [tableHeight, setTableHeight] = useState(0)

  useEffect(() => {
    setTableHeight(wrapperRef.current.clientHeight)
  }, [wrapperRef.current])

  return (
    <Box
      height='100%'
      ref={wrapperRef}
      backgroundColor='#fff'
      px='16px'
      borderRadius='16px'
      position={isPlaceholder && 'relative'}
    >
      <Global
        styles={css`
          ${getTableStyles({ clickable, theme }, tableClassName)}
        `}
      />
      <RcTable
        columns={columns}
        data={data}
        {...props}
        className={tableClassName}
        tableLayout='fixed'
        scroll={{
          x: 1000,
          y: tableHeight - 64,
        }}
      />
      {isPlaceholder && (
        <Box
          position='absolute'
          top='50px'
          width='100%'
          height='calc(100% - 50px)'
          borderRadius={theme.borderRadius.n}
          backgroundColor='white'
          textAlign='center'
          left='0'
        >
          <Box
            width='100%'
            height='calc(50% - 18px)'
            display='grid'
            alignItems='end'
            justifyItems='center'
            marginBottom='18px'
          >
            <RouteMarketIcon
              height={36}
              width={36}
              stroke={theme.colorsList.secondary[1]}
            />
          </Box>
          <Box
            width='100%'
            height='50%'
            display='grid'
            alignItems='baseline'
            justifyItems='center'
          >
            <Caption>{placeholderText}</Caption>
          </Box>
        </Box>
      )}
    </Box>
  )
}

export default Table
