export const getTableStyles = ({ clickable, theme }, tableClassName) => `
  .${tableClassName} {
    height: 100%;

    .rc-table-container,
    .rc-table-body {
      height: 100%;
    }

    .rc-table-body {
      overflow: auto !important;
    }

    table {
      border-spacing: 0px;
      width: 100%;
    }
    th,
    td {
      text-align: left;
      font-size: 12px;
      line-height: 1.25;
      font-weight: 600;
      font-family: ${theme.fontFamily.ms};
      color: ${theme.colorsList.secondary[0]};
      height: 48px;
      padding: 0;
    }

    th,
    tr:not(:last-child) td {
      border-bottom: 1px solid ${theme.colorsList.secondary[2]};
    }
 
    th {
      font-weight: 600;
    }
    td {
      font-weight: 500;
      cursor: ${clickable ? 'pointer' : 'default'};
    }

    tr.active td {
      background-color: ${theme.colorsList.primary[3]};
    }
  }
`
