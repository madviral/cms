import * as actions from '../constants/isOpenNewProductDrawer'
import { AppThunk } from 'src/store/rootReducer'
import { ActionCreator } from 'redux'

export const triggerNewProductDrawer: ActionCreator<any> = (): AppThunk => async dispatch => {
  dispatch({
    type: actions.TRIGGER,
  })
}
