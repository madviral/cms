import * as actions from '../constants/productCategories'

export const load = () => async (dispatch, getState, client) => {
  dispatch({
    type: [actions.LOADING],
  })
  const { data, error } = await client.get('market/catalog/v1/categories')
  if (!error) {
    dispatch({
      type: [actions.FINISH],
      payload: data.list,
    })
  } else {
    dispatch({
      type: [actions.ERROR],
      payload: error,
    })
  }
}
