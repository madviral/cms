import { ActionCreator } from 'redux'
import * as actions from '../constants/user'

export const load: ActionCreator<any> = () => async (
  dispatch,
  getState,
  client,
) => {
  const { data, error } = await client.get('user/v1/profiles/me')

  if (!error) {
    dispatch({
      type: actions.LOAD,
      data,
    })
  }
}

export const getAllMerchants: ActionCreator<any> = () => async (
  dispatch,
  getState,
  client,
) => {
  const { data, error } = await client.get('user/v1/profiles/all')

  if (!error) {
    dispatch({
      type: actions.LOAD_ALL_MERCHANTS,
      data,
    })
  }
}
