import * as actions from '../constants/modal'
import { AppThunk } from 'src/store/rootReducer'
import { ActionCreator } from 'redux'

export const triggerModal: ActionCreator<any> = (
  type: string = '',
  id?: number,
  domNode?: string,
): AppThunk => async dispatch => {
  dispatch({
    type: actions.TRIGGER,
    payload: {
      type,
      id,
      domNode,
    },
  })
}
