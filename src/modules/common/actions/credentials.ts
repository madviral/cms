import { ActionCreator } from 'redux'
import * as actions from '../constants/credentials'

export const auth: ActionCreator<any> = token => ({
  type: actions.AUTH,
  ...token,
})

export const logout: ActionCreator<any> = () => (
  dispatch,
  getState,
  client,
  history,
) => {
  dispatch({ type: actions.LOGOUT })
}
