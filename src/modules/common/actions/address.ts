import { AppThunk } from 'src/store/rootReducer'
import * as actions from '../constants/address'
import { IAdress } from '../types'

export const loadCities = (): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  dispatch({
    type: actions.LOADING,
  })
  const { data, error } = await client.get('market/catalog/v1/address/city')

  if (!error) {
    dispatch({
      type: actions.LOAD_CITIES,
      payload: {
        cities: data,
      },
    })
  } else {
    dispatch({
      type: actions.ERROR,
      payload: error,
    })
  }
}

export const suggest = (address: IAdress): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  dispatch({
    type: actions.LOADING,
  })
  const { data, error } = await client.post('market/location/v1/check', {
    ...address,
  })

  if (!error) {
    dispatch({
      type: actions.SUGGEST,
      payload: data,
    })
  } else {
    // TODO: handle errors
    throw error
  }
}
