import { Action } from 'redux'
import * as actions from '../constants/workSchedule'
import { DayOfWeekUnion } from '../types'

export const change = (
  field: string,
  day: DayOfWeekUnion,
  value: string | boolean,
) => ({
  type: actions.CHANGE,
  payload: {
    field,
    day,
    value,
  },
})

export const clear = (): Action<typeof actions.CLEAR> => ({
  type: actions.CLEAR,
})
