import React, { useState } from 'react'
import { getHours, format, getMinutes } from 'date-fns'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { styled, theme } from 'ui/theme'
import { ifProp } from 'styled-tools'
import { Divider } from 'ui'
import { Space, Text } from 'ui/text'
import { TimePickerFromToTime } from 'ui/timepicker'
import { Chevron as ChevronIcon } from 'ui/icons'
import { Box } from 'ui/layout'
import { TFunction } from 'i18next'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Switcher } from 'ui/switcher'
import { DayOfWeekUnion } from '../types'

type WorkingHourhItemProps = {
  day: DayOfWeekUnion
  fromTime: Date
  toTime: Date
  isDayOff?: boolean
  onChangeIsDayOff?: (value: boolean) => void
  onChangeDayFromTime?: (value: Date) => void
  onChangeDayToTime?: (value: Date) => void
  t?: TFunction
}

type StyledWrapperProps = {
  opened: boolean
}

const StyledWrapper = styled.div<StyledWrapperProps>`
  border: 1px solid;
  border-color: ${props => props.theme.colorsList.secondary[2]};
  border-radius: ${props => props.theme.borderRadius.xs};
  ${ifProp(
    'opened',
    `
    .__icon-wrap svg {
      transform: rotate(180deg);
    }
  `,
  )}
`

const WeekDay = ({
  day,
  fromTime,
  toTime,
  isDayOff,
  onChangeIsDayOff,
  onChangeDayFromTime,
  onChangeDayToTime,
  t,
}: WorkingHourhItemProps & WithTranslation) => {
  const [opened, setOpened] = useState(false)
  const minHours = getHours(fromTime)
  const maxHours = getHours(toTime)
  const minMinutes = getMinutes(fromTime)
  const maxMinutes = getMinutes(toTime)
  return (
    <StyledWrapper opened={opened} data-weekday={day}>
      <Box
        display='flex'
        alignItems='center'
        style={{ cursor: 'pointer' }}
        py='8px'
        px='20px'
        onClick={() => setOpened(!opened)}
      >
        <Box display='inline-flex' width='120px' mr='20px'>
          <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
            {t(String(day).toLowerCase())}
          </Text>
        </Box>

        <Box
          display='inline-flex'
          alignItems='center'
          justifyContent='center'
          width='100px'
          ml='auto'
          mr='auto'
        >
          <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
            <Condition match={!isDayOff}>
              {format(fromTime, 'HH:mm')}
              <Space />
              {'-'}
              <Space />
              {format(toTime, 'HH:mm')}
            </Condition>
            <Condition match={isDayOff}>{t('closed')}</Condition>
          </Text>
        </Box>

        <Box ml='auto'>
          <div className='__icon-wrap'>
            <ChevronIcon color={theme.colorsList.secondary[1]} />
          </div>
        </Box>
      </Box>
      <Condition match={opened}>
        <Box px='20px' pb='20px'>
          <Box mb='20px'>
            <Divider />
          </Box>
          <Box mb='20px'>
            <TimePickerFromToTime
              disabled={isDayOff}
              minHours={minHours}
              maxHours={maxHours}
              minMinutes={minMinutes}
              maxMinutes={maxMinutes}
              onChange={date => {
                onChangeDayFromTime(date.fromTime)
                onChangeDayToTime(date.toTime)
              }}
            />
          </Box>
          <Box display='flex' alignItems='center'>
            <Switcher
              defaultToggled={isDayOff}
              onChange={value => onChangeIsDayOff(value)}
              label={t('makeDayOff')}
            />
          </Box>
        </Box>
      </Condition>
    </StyledWrapper>
  )
}

export default withTranslation('common')(WeekDay)
