import React, { FC } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { Text } from 'ui/text'
import { NativeLink } from 'ui/links'
import { Button } from 'ui/buttons'
import ErrorPageLayout from './ErrorPageLayout'
import Icon from './NotFoundIcon'

const NotFound: FC<WithTranslation> = ({ t }) => {
  return (
    <ErrorPageLayout>
      <Box mt='15vh'>
        <Icon />
      </Box>
      <Box mb='32px'>
        <Text
          fontSize='48px'
          lineHeight='1'
          fontWeight='600'
          color='secondary.0'
        >
          {t('pageNotFound')}
        </Text>
      </Box>
      <Box width='200px'>
        <NativeLink href='/' sx={{ display: 'block' }} likeButton>
          <Button appearance='primary' size='xl' fullWidth>
            {t('toHome')}
          </Button>
        </NativeLink>
      </Box>
    </ErrorPageLayout>
  )
}

export default withTranslation()(NotFound)
