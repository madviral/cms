import React from 'react'

const Icon = () => {
  return (
    <svg
      width='1023'
      height='428'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <g filter='url(#filter0_d)' fill='#fff'>
        <path d='M363.96 290.546h-40.724v52.459H222.957v-52.459H64v-65.573L194.056 36.995h105.971L184.422 210.546h41.6V163.77h97.214v46.776h40.724v80zM517.187 350c-27.15 0-51.234-6.266-72.253-18.798-20.727-12.823-37.076-31.184-49.045-55.082-11.677-24.189-17.516-52.896-17.516-86.12 0-33.224 5.839-61.785 17.516-85.683 11.969-24.19 28.318-42.55 49.045-55.082C465.953 36.412 490.037 30 517.187 30s51.088 6.412 71.815 19.235c21.019 12.532 37.368 30.892 49.045 55.082 11.969 23.898 17.954 52.459 17.954 85.683 0 33.224-5.985 61.931-17.954 86.12-11.677 23.898-28.026 42.259-49.045 55.082C568.275 343.734 544.337 350 517.187 350zm0-82.623c23.355 0 35.032-25.792 35.032-77.377 0-51.585-11.677-77.377-35.032-77.377-23.355 0-35.032 25.792-35.032 77.377 0 51.585 11.677 77.377 35.032 77.377zM978.899 290.546h-40.724v52.459H837.896v-52.459H678.939v-65.573L808.995 36.995h105.971L799.361 210.546h41.6V163.77h97.214v46.776h40.724v80z' />
      </g>
      <defs>
        <filter
          id='filter0_d'
          x='0'
          y='0'
          width='1022.9'
          height='428'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
          />
          <feOffset dx='-10' dy='24' />
          <feGaussianBlur stdDeviation='27' />
          <feColorMatrix values='0 0 0 0 0.85098 0 0 0 0 0.886275 0 0 0 0 0.92549 0 0 0 0.8 0' />
          <feBlend in2='BackgroundImageFix' result='effect1_dropShadow' />
          <feBlend in='SourceGraphic' in2='effect1_dropShadow' result='shape' />
        </filter>
      </defs>
    </svg>
  )
}

export default Icon
