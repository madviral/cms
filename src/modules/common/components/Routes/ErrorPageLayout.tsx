import React from 'react'
import { Box } from 'ui/layout'

const ErrorPageLayout = ({ children }) => {
  return (
    <Box
      position='fixed'
      top='0'
      left='0'
      zIndex='1000'
      display='flex'
      flexDirection='column'
      alignItems='center'
      width='100%'
      height='100%'
      backgroundColor='primary.4'
    >
      {children}
    </Box>
  )
}

export default ErrorPageLayout
