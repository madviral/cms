import React from 'react'
import { YMaps as BasedYMaps } from 'react-yandex-maps'

const YMaps = ({ children }) => (
  <BasedYMaps query={{ apikey: process.env.MAP_API_KEY }}>
    {children}
  </BasedYMaps>
)

export default YMaps
