import { Change, Select, Update } from './types'

export const update: Update = (ymaps, setItems) => async value => {
  const options = {
    results: 10,
  }

  if (!ymaps.suggest) {
    return false
  }

  const items = await ymaps.suggest(value, options)
  setItems(items)

  return true
}

export const change: Change = (value, prefix, onUpdate, onChange) => {
  if (value.length >= 2) {
    onUpdate(`${prefix}, ${value}`)
  }
  onChange(value)
}

export const select: Select = async (ymaps, value, onSuggest, onUpdate) => {
  const geocode = await ymaps.geocode(value)
  const coordinates = geocode.geoObjects.get(0).geometry.getCoordinates()
  const addressData = geocode.geoObjects.get(0).properties.getAll()
    .metaDataProperty.GeocoderMetaData.Address.Components

  onSuggest({ address: addressData, coordinates })
  onUpdate(value)
}
