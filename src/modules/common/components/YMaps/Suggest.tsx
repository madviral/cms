import React, { useRef, useState } from 'react'
import { withYMaps } from 'react-yandex-maps'
import { FormField } from 'ui'
import { styled } from 'ui/theme'
import 'rc-dropdown/assets/index.css'
import Dropdown from 'rc-dropdown'
import { DebounceInput } from 'react-debounce-input'
import { OnOutsideClick } from 'ui/layout'
import { update, select } from './handlers'
import { SuggestProps } from './types'

const Menu = styled.div`
  padding-top: 8px;
  padding-bottom: 8px;
  background: white;
  border-radius: 8px;
  box-shadow: 0px 0px 8px rgba(37, 40, 43, 0.12);
  max-height: 216px;
  overflow-y: auto;
`

const MenuItem = styled.div`
  padding: 12px 16px;
  cursor: pointer;
  color: ${props => props.theme.colorsList.secondary[0]};

  :hover {
    background: ${props => props.theme.colorsList.primary[1]};
    color: #fff;
  }
`

const Suggest = ({
  id,
  prefix,
  value,
  placeholder,
  appearance,
  disabled,
  required,
  ymaps,
  onChange,
  onSuggest,
}: SuggestProps) => {
  const [items, setItems] = useState([])
  const [toggle, setToggle] = useState(false)
  const onUpdate = update(ymaps, setItems)
  const inputRef = useRef()
  const menuRef = useRef()

  const menu = (
    <OnOutsideClick
      onOutsideClick={() => {
        setToggle(false)
      }}
      target={inputRef.current}
    >
      <Menu ref={menuRef}>
        {items.map(item => (
          <MenuItem
            key={item.displayName}
            onClick={() => {
              select(ymaps, item.displayName, onSuggest, onUpdate)
              setToggle(false)
              setItems([])
            }}
          >
            {item.displayName}
          </MenuItem>
        ))}
      </Menu>
    </OnOutsideClick>
  )

  return (
    <Dropdown
      overlay={menu}
      trigger={['click']}
      visible={toggle && items.length > 0}
      getPopupContainer={node => node}
    >
      <DebounceInput
        element={FormField as any}
        id={id}
        autoComplete='off'
        autoCorrect='off'
        value={value}
        placeholder={placeholder}
        appearance={appearance}
        inputRef={inputRef}
        onChange={
          disabled
            ? null
            : ({ target }) => {
                onUpdate(`${prefix}, ${target.value}`)
                onChange(target.value)
                setToggle(true)
              }
        }
        onClick={() => {
          if (!toggle && value.length > 1) {
            onUpdate(`${prefix}, ${value}`)
            setToggle(true)
          }
        }}
        disabled={disabled}
        required={required}
        debounceTimeout={300}
      />
    </Dropdown>
  )
}

export default withYMaps(Suggest)
