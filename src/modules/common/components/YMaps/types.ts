import { YMapsApi } from 'react-yandex-maps'
import { Appearance } from 'ui/inputs/FormField/types/interfaces'

type OnChange = (value: string) => void

type OnUpdateCallback = (value: string) => Promise<boolean>

export type IMapState = {
  center: number[]
  zoom?: number
  controls?: string[]
}

export type Update = (
  ymaps: YMapsApi,
  setItems: (items: any[]) => void,
) => OnUpdateCallback

export type Change = (
  value: string,
  prefix: string,
  onUpdate: OnUpdateCallback,
  onChange: OnChange,
) => void

export type IAddressData = {
  king: 'country' | 'province' | 'area' | 'locality' | 'street' | 'house'
  name: string
}

export type ISuggestArgs = {
  address: IAddressData[]
  coordinates: number[]
}

export type Select = (
  ymaps: YMapsApi,
  value: string,
  onSuggest: (value: ISuggestArgs) => void,
  onUpdate: OnUpdateCallback,
) => void

export type SuggestProps = {
  id: string
  prefix?: string
  value: string
  placeholder?: string
  appearance?: Appearance
  disabled?: boolean
  required?: boolean
  ymaps?: YMapsApi
  onChange: (value: string) => void
  onSuggest: (value: ISuggestArgs) => void
}
