import React, { useEffect, memo } from 'react'
import { Box } from 'ui/layout'
import isEqual from 'lodash.isequal'
import { IWeekSchedule, DayOfWeekUnion, DayOfWeekArray } from '../types'
import WeekDay from './WeekDay'

type WorkScheduleProps = {
  week: DayOfWeekArray
  schedule: IWeekSchedule
  onClear?: VoidFunction
  onChangeDayFromTime: (day: DayOfWeekUnion, value: Date) => void
  onChangeDayToTime: (day: DayOfWeekUnion, value: Date) => void
  onChangeIsDayOff: (day: DayOfWeekUnion, value: boolean) => void
}

const WorkSchedule = ({
  week,
  schedule,
  onClear,
  onChangeDayFromTime,
  onChangeDayToTime,
  onChangeIsDayOff,
}: WorkScheduleProps) => {
  useEffect(() => {
    onClear()
  }, [])

  return (
    <Box display='flex' flexDirection='column' minWidth={0}>
      {week.map(day => (
        <Box key={day} mb='10px'>
          <WeekDay
            day={day}
            fromTime={schedule[day].fromTime}
            toTime={schedule[day].toTime}
            isDayOff={schedule[day].isDayOff}
            onChangeIsDayOff={value => onChangeIsDayOff(day, value)}
            onChangeDayFromTime={value => onChangeDayFromTime(day, value)}
            onChangeDayToTime={value => onChangeDayToTime(day, value)}
          />
        </Box>
      ))}
    </Box>
  )
}

export default memo(WorkSchedule, (prevProps, nextProps) =>
  isEqual(prevProps.schedule, nextProps.schedule),
)
