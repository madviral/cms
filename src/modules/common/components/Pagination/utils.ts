export const getAvailabilityState = (num, pageCount) => ({
  canNextPage: num < pageCount,
  canPreviousPage: num > 1,
})
