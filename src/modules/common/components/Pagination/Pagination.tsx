import React, { FC, useCallback, useEffect, useState, memo } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { Space, Text } from 'ui/text'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { FormField } from 'ui/inputs'
import { Condition } from 'ui/condition'
import { ButtonTransparent } from 'ui/buttons'
import { Appearance as FormFieldAppearance } from 'ui/inputs/FormField/types/interfaces'
import { Chevron } from 'ui/icons'
import { theme } from 'ui/theme'
import { getAvailabilityState } from './utils'

const defaultProps = {
  canChangePageDirectly: true,
  pageIndex: 1,
  goToPage: () => ({}),
}

type PaginationProps = {
  canChangePageDirectly?: boolean
  pageCount: number
  defaultPageSize?: number
  goToPage?: (page: number) => void
  setPageSize?: (size: number) => void
} & Omit<Partial<typeof defaultProps>, 'goToPage'> &
  WithTranslation

export const pageSizes = [25, 50, 75]

const Pagination: FC<PaginationProps> = ({
  canChangePageDirectly,
  defaultPageSize,
  pageIndex,
  pageCount,
  goToPage,
  setPageSize,
  t,
}) => {
  const pageSize = defaultPageSize || pageSizes[0]
  const [pageSizeState, setPageSizeState] = useState(pageSize)
  const [page, setPage] = useState({
    index: pageIndex,
    ...getAvailabilityState(pageIndex, pageCount),
  })

  useEffect(() => {
    setPage({
      index: pageIndex,
      ...getAvailabilityState(pageIndex, pageCount),
    })
  }, [pageIndex, pageCount])

  const onChangePage = useCallback(
    e => {
      const parsedNum = parseInt(e.target.value, 10)
      const num = Math.min(parsedNum > 0 ? parsedNum : 0, pageCount)
      if (e.target.validity.valid) {
        setPage({
          index: num,
          ...getAvailabilityState(num, pageCount),
        })
        goToPage(num)
      }
    },
    [pageIndex],
  )
  const onBlur = useCallback(() => {
    if (page.index === 0) {
      setPage({
        index: 1,
        ...getAvailabilityState(1, pageCount),
      })
    }
  }, [pageIndex, page.index])

  const onNextPage = useCallback(() => {
    const num = page.index + 1
    setPage({
      index: num,
      ...getAvailabilityState(num, pageCount),
    })
    if (goToPage) {
      goToPage(num)
    }
  }, [pageIndex, page.index])

  const onPrevPage = useCallback(() => {
    const num = page.index - 1
    setPage({
      index: num,
      ...getAvailabilityState(num, pageCount),
    })
    if (goToPage) {
      goToPage(num)
    }
  }, [pageIndex, page.index])

  return (
    <Box
      display='flex'
      alignItems='center'
      justifyContent='flex-end'
      width='360px'
    >
      <Box mr='8px'>
        <Text fontSize='12px' color='secondary.0'>
          {t('showed')}
        </Text>
      </Box>
      <Dropdown width='50px'>
        {({ onToggle }) => {
          return (
            <>
              <Dropdown.SimpleButton>
                {String(pageSizeState)}
              </Dropdown.SimpleButton>
              <Dropdown.Menu direction='b' width='100%'>
                {pageSizes.map(sizeItem => (
                  <Dropdown.Item
                    key={sizeItem}
                    onClick={useCallback(e => {
                      if (setPageSize) {
                        setPageSize(sizeItem)
                      }
                      setPageSizeState(sizeItem)
                      onToggle(e)
                    }, [])}
                  >
                    {sizeItem}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </>
          )
        }}
      </Dropdown>
      <Condition match={canChangePageDirectly}>
        <Box width='34px' ml='20px'>
          <FormField
            id='tablePagination'
            value={page.index}
            appearance={FormFieldAppearance.mini}
            pattern='[0-9]*'
            onChange={onChangePage}
            onBlur={onBlur}
            inputSx={{ padding: '9px 5px', textAlign: 'center' }}
          />
        </Box>
      </Condition>
      <Condition match={!canChangePageDirectly}>
        <Text fontSize='12px' color='secondary.0'>
          {page.index}
        </Text>
      </Condition>
      <Space />
      <Text fontSize='12px' color='secondary.0'>
        {t('of')}
      </Text>
      <Space />
      <Text fontSize='12px' color='secondary.0' fontWeight='600'>
        {pageCount}
      </Text>

      <Box ml='24px' pt='4px'>
        <ButtonTransparent
          disabled={!page.canPreviousPage}
          onClick={onPrevPage}
        >
          <Chevron left color={theme.colorsList.secondary[0]} />
        </ButtonTransparent>
      </Box>
      <Box ml='12px' pt='4px'>
        <ButtonTransparent disabled={!page.canNextPage} onClick={onNextPage}>
          <Chevron right color={theme.colorsList.secondary[0]} />
        </ButtonTransparent>
      </Box>
    </Box>
  )
}

Pagination.defaultProps = defaultProps

export default withTranslation('common')(memo(Pagination))
