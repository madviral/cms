import { DayOfWeek } from './enums'

export type WeekDay = {
  isDayOff: boolean
  fromTime: Date
  toTime: Date
}

export type DayOfWeekUnion =
  | DayOfWeek.MONDAY
  | DayOfWeek.TUESDAY
  | DayOfWeek.WEDNESDAY
  | DayOfWeek.THURSDAY
  | DayOfWeek.FRIDAY
  | DayOfWeek.SATURDAY
  | DayOfWeek.SUNDAY

export type DayOfWeekArray = [
  DayOfWeek.MONDAY,
  DayOfWeek.TUESDAY,
  DayOfWeek.WEDNESDAY,
  DayOfWeek.THURSDAY,
  DayOfWeek.FRIDAY,
  DayOfWeek.SATURDAY,
  DayOfWeek.SUNDAY,
]

export type IWeekSchedule = {
  [DayOfWeek.MONDAY]: WeekDay
  [DayOfWeek.TUESDAY]: WeekDay
  [DayOfWeek.WEDNESDAY]: WeekDay
  [DayOfWeek.THURSDAY]: WeekDay
  [DayOfWeek.FRIDAY]: WeekDay
  [DayOfWeek.SATURDAY]: WeekDay
  [DayOfWeek.SUNDAY]: WeekDay
}

export type ICity = {
  id: number
  code: string
  name: string
  label?: string
  map?: {
    latitude: number
    longitude: number
  }
}

export type IChangeAction<ActionType> = {
  type: ActionType
  payload: {
    field: string
    value: string
  }
}

export type IStringCb = (value: string) => void

export type IAdress = {
  country: string
  cityId: number
  city?: string
  street: string
  house: string
  flat: string
}

export type IStock = {
  id: number
  name: string
  address: IAdress
}
