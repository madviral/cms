import { ReactNode } from 'react'
import { RouteProps } from 'react-router-dom'

export interface INavigationRoute {
  icon: ReactNode
  name: string
  childs?: any[]
}

export interface IRoute {
  path: string
  key: string
  exact?: boolean
  nav?: INavigationRoute
  skip?: boolean
  routes?: IRoute[]
}

export interface IMapRoute extends RouteProps {
  key: string
}

export interface IPageRoute extends IRoute {
  link: string
  component: ReactNode
  childrenRoutes: IPageRoute[]
  // navigationRoutes: INavigationRoute[]
}

export interface IPrice {
  value: number
  currency: string
}

export interface IImage {
  id: number
  status: number
  small: string
  medium: string
  origin: string
  sort: number
}

export interface IToken {
  accessToken: string
  expiresIn: number
  refreshToken: string
}
