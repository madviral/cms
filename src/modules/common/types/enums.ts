export enum DayOfWeek {
  MONDAY = 'MONDAY',
  TUESDAY = 'TUESDAY',
  WEDNESDAY = 'WEDNESDAY',
  THURSDAY = 'THURSDAY',
  FRIDAY = 'FRIDAY',
  SATURDAY = 'SATURDAY',
  SUNDAY = 'SUNDAY',
}

export enum DaySchedule {
  MIN_HOURS = 8,
  MAX_HOURS = 20,
}

export enum Access {
  MERCHANT = 'merchant',
  SERVICES = 'services',
  FEED = 'feed',
  MANAGER = 'market_contract_manager',
}
