import { connect } from 'react-redux'
import { clear } from '../actions/workSchedule'
import WorkSchedule from '../components/WorkSchedule'

export default connect(() => ({}), {
  onClear: clear,
})(WorkSchedule)
