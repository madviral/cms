import * as React from 'react'
import { connect } from 'react-redux'

import { logout } from '../../actions/credentials'
import { useTranslation } from 'react-i18next'
import { styled } from 'ui/theme'

type SignOutButtonProps = {
  logout: () => void
}

const StyledSignOutButton = styled.button`
  border: none;
  cursor: pointer;
  font-size: 12px;
  font-family: ${props => props.theme.fontFamily.ms};
  font-weight: ${props => props.theme.fontWeight.semibold};
  color: ${props => props.theme.colorsList.primary[0]};
  background: ${props => props.theme.colorsList.primary[2]};
`

const SignOutButton: React.FC<SignOutButtonProps> = props => {
  const [t] = useTranslation('common')
  return (
    <StyledSignOutButton onClick={props.logout}>
      {t('logout')}
    </StyledSignOutButton>
  )
}

export default connect(null, { logout })(SignOutButton)
