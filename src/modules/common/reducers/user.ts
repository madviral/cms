import { createReducer } from 'lib/utils'
import * as actions from '../constants/user'

const initialState = {
  id: 0,
  email: '',
  fio: '',
  phoneNumber: '',
  jobPosition: '',
  roles: [],
  merchants: [],
}

export default createReducer(initialState, {
  [actions.LOAD]: (state, { data }: any) => ({
    ...state,
    ...data,
    id: data.merchantId,
  }),
  [actions.LOAD_ALL_MERCHANTS]: (state, { data }: any) => ({
    ...state,
    merchants: data,
  }),
})
