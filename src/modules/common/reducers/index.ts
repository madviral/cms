import { combineReducers } from 'redux'
import address from './address'
import credentials from './credentials'
import user from './user'
import modal from './modal'
import workSchedule from './workSchedule'
import newProductDrawer from './newProductDrawer'
import productCategories from './productCategories'

export default combineReducers({
  address,
  credentials,
  user,
  modal,
  workSchedule,
  newProductDrawer,
  general: combineReducers({
    productCategories,
  }),
})
