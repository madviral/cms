import { createReducer } from 'lib/utils'
import * as actions from '../constants/address'

const initialState = {
  loading: false,
  error: null,
  cities: [],
  suggest: [],
}

// Initial setup
export default createReducer(initialState, {
  [actions.LOADING]: state => ({
    ...state,
    loading: true,
    error: false,
  }),
  [actions.LOAD_CITIES]: (state, { payload }: any) => ({
    ...state,
    loading: false,
    error: null,
    cities: payload.cities,
  }),
  [actions.SUGGEST]: (state, { payload }: any) => ({
    ...state,
    suggest: payload,
  }),
  [actions.ERROR]: (state, { payload }: any) => ({
    ...initialState,
    error: payload.data,
  }),
  [actions.CLEAR]: () => initialState,
})
