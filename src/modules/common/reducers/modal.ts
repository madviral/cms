import { createReducer } from 'lib/utils'
import * as actions from '../constants/modal'

const initialState = {
  isOpen: false,
  type: '',
  id: null,
  domNode: null,
}

export default createReducer(initialState, {
  [actions.TRIGGER]: (state, { payload }: any) => ({
    isOpen: !state.isOpen,
    type: payload.type,
    id: payload.id,
    domNode: payload.domNode,
  }),
})
