import { createReducer } from 'lib/utils'
import * as actions from '../constants/productCategories'

const initialState = {
  loading: false,
  error: null,
  data: null,
}
// Initial setup
export default createReducer(initialState, {
  [actions.LOADING]: (state, { payload }: any) => ({
    ...state,
    loading: true,
    error: false,
  }),
  [actions.FINISH]: (state, { payload }: any) => ({
    loading: false,
    error: null,
    data: payload,
  }),
  [actions.ERROR]: (state, { payload }: any) => ({
    loading: false,
    error: payload.data,
    data: null,
  }),
})
