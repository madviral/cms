import { createReducer } from 'lib/utils'
import * as actions from '../constants/newProductDrawer'

const initialState = {
  isOpenNewProductDrawer: false,
}

export default createReducer(initialState, {
  [actions.TRIGGER]: state => ({
    isOpenNewProductDrawer: !state.isOpenNewProductDrawer,
  }),
})
