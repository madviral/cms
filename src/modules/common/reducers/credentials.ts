import storage from 'redux-persist/lib/storage'
import { persistReducer } from 'redux-persist'
import { createReducer } from 'lib/utils'
import * as actions from '../constants/credentials'

const initialState = {
  accessToken: '',
  expiresIn: '',
  refreshToken: '',
}

const reducer = createReducer(initialState, {
  [actions.AUTH]: (state, payload: any) => ({
    ...state,
    ...payload,
  }),
  [actions.LOGOUT]: () => initialState,
})

export default persistReducer(
  {
    storage,
    key: 'credentials',
    keyPrefix: 'cms',
    version: 1,
  },
  reducer,
)
