import { createReducer } from 'lib/utils'
import { set } from 'date-fns'
import * as actions from '../constants/workSchedule'
import { DayOfWeek, DaySchedule } from '../types/enums'

const dateNow = new Date()
const date = {
  fromTime: set(dateNow, { hours: DaySchedule.MIN_HOURS, minutes: 0 }),
  toTime: set(dateNow, { hours: DaySchedule.MAX_HOURS, minutes: 0 }),
}

const initialState = Object.values(DayOfWeek).reduce((acc, day) => {
  return {
    ...acc,
    [day]: {
      day,
      isDayOff: false,
      isAllDay: false,
      fromTime: date.fromTime,
      toTime: date.toTime,
    },
  }
}, {})

export default createReducer(initialState, {
  [actions.CHANGE]: (state, { payload }: any) => {
    const { day, field, value } = payload

    return {
      ...state,
      [day]: {
        ...state[day],
        [field]: value,
      },
    }
  },
  [actions.CLEAR]: () => initialState,
})
