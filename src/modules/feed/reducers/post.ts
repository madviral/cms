import { createReducer } from 'lib/utils'
import { Post } from '../types'
import * as postActions from '../constants/feed'

export interface PostItemState {
  loaded: boolean
  item: Post
  error?: null
}

const initialState: PostItemState = {
  loaded: false,
  item: null,
}

export default createReducer(initialState, {
  [postActions.LOAD_POST_REQUEST]: state => ({
    ...initialState,
  }),
  [postActions.LOAD_POST_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    item: payload,
  }),
  [postActions.LOAD_POST_FAILURE]: (state, { error }: any) => ({
    ...state,
    error,
  }),
})
