import { createReducer } from 'lib/utils'
import { Post } from '../types'
import * as postActions from '../constants/feed'

export interface PostState {
  loaded: boolean
  list: Post[]
  error?: null
}

const initialState: PostState = {
  loaded: false,
  list: [],
}

export default createReducer(initialState, {
  [postActions.LOAD_POSTS_REQUEST]: state => ({
    ...state,
  }),
  [postActions.LOAD_POSTS_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    list: payload,
  }),
  [postActions.LOAD_POSTS_FAILURE]: (state, { error }: any) => ({
    ...state,
    error,
  }),
})
