import { connect } from 'react-redux'
import { Unaccepted } from '../pages'
import {
  loadPost,
  loadPosts,
  deletePost,
  approvePost,
} from 'src/modules/feed/actions/feed'

const mapDispatchToProps = {
  loadPost,
  loadPosts,
  deletePost,
  approvePost,
}

const mapStateToProps = state => ({
  selectedPost: state.feed.post,
  posts: state.feed.posts,
})

export default connect(mapStateToProps, mapDispatchToProps)(Unaccepted)
