import { connect } from 'react-redux'
import { Accepted } from '../pages'
import {
  loadPost,
  loadPosts,
  deletePost,
  makeTopPost,
} from 'src/modules/feed/actions/feed'

const mapDispatchToProps = {
  loadPost,
  loadPosts,
  deletePost,
  makeTopPost,
}

const mapStateToProps = state => ({
  selectedPost: state.feed.post,
  posts: state.feed.posts,
})

export default connect(mapStateToProps, mapDispatchToProps)(Accepted)
