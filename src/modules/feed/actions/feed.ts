import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'
import * as postActions from '../constants/feed'

export const loadPost: ActionCreator<any> = (
  postId: string,
): AppThunk => async (dispatch, getState, client) => {
  await client
    .get(`entertainment/feed/v1/posts/${postId}`)
    .then(resp => {
      dispatch({
        type: postActions.LOAD_POST_SUCCESS,
        payload: resp.data,
      })
    })
    .catch(er => {
      dispatch({
        type: postActions.LOAD_POST_FAILURE,
        error: er,
      })
    })
}

export const loadPosts: ActionCreator<any> = (
  status: string,
): AppThunk => async (dispatch, getState, client) => {
  dispatch({ type: postActions.LOAD_POST_REQUEST })

  await client
    .get(`entertainment/feed/v1/posts`, {
      params: { status },
    })
    .then(resp => {
      dispatch({
        type: postActions.LOAD_POSTS_SUCCESS,
        payload: resp.data.list,
      })
    })
    .catch(er => {
      dispatch({
        type: postActions.LOAD_POSTS_FAILURE,
        error: er,
      })
    })
}

export const deletePost: ActionCreator<any> = (id: number): AppThunk => (
  dispatch,
  getState,
  client,
) => {
  client
    .delete(`entertainment/feed/v1/posts/${id}`)
    .then(resp => {
      window.location.reload()
    })
    .catch(er => {
      // console.log(er)
    })
}

export const approvePost: ActionCreator<any> = (id: number): AppThunk => (
  dispatch,
  getState,
  client,
) => {
  client
    .put(`entertainment/feed/v1/posts/approve`, { id })
    .then(resp => {
      window.location.reload()
    })
    .catch(er => {
      // console.log(er)
    })
}

export const makeTopPost: ActionCreator<any> = (id: number): AppThunk => (
  dispatch,
  getState,
  client,
) => {
  client
    .put(`entertainment/feed/v1/posts/makeTop`, { id })
    .then(resp => {
      window.location.reload()
    })
    .catch(er => {
      // console.log(er)
    })
}
