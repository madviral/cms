import { ClockIcon, CheckmarkIcon } from 'ui/icons'
import { getDashedStr } from 'lib/utils'
import Unaccepted from './containers/Unaccepted'
import Accepted from './containers/Accepted'

enum KEYS {
  UNACCEPTED = 'UNACCEPTED',
  ACCEPTED = 'ACCEPTED',
}

export const getRoutes = rootKey => [
  {
    path: '/feed',
    key: getDashedStr(rootKey, KEYS.UNACCEPTED),
    exact: true,
    component: Unaccepted,
    nav: {
      name: 'feed:unAccepted',
      icon: ClockIcon,
      childs: [],
    },
  },
  {
    path: '/feed/accepted',
    key: getDashedStr(rootKey, KEYS.ACCEPTED),
    exact: true,
    component: Accepted,
    nav: {
      name: 'feed:accepted',
      icon: CheckmarkIcon,
      childs: [],
    },
  },
]
