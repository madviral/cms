export interface Post {
  id: number
  title: string
  body: string
  url: string
  image: string
  resource: string
  icon: string
  tags: string[]
  locale: string
  dateCreated: string
  sort: number
}

export interface PostList {
  list: Post[]
  pageNumber: number
  total: number
  totalPages: number
}

export interface PostItemProps {
  item: Post
  name: string
  onChange?: (id: string) => void
}
