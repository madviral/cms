import React from 'react'
import { styled } from 'ui/theme'

const Img = styled.img`
  width: 480px;
  height: 320px;
  object-fit: cover;
  border-radius: ${props => props.theme.borderRadius.m};
`

const PostImage = ({ src }) => <Img src={src} />

export default PostImage
