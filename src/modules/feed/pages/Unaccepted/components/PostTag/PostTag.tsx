import React from 'react'
import { styled } from 'ui/theme'

const TagItem = styled.span`
  display: inline-block;
  padding: 4px 14px;
  font-size: 10px;
  line-height: 10px;
  background-color: ${props => props.theme.colorsList.primary[2]};
  border-radius: ${props => props.theme.borderRadius.xl};
  color: ${props => props.theme.colorsList.secondary[0]};

  &:not(:last-child) {
    margin-right: 20px;
  }
`

const PostTag = ({ tag }) => <TagItem>{tag}</TagItem>

export default PostTag
