import React from 'react'
import { styled } from 'ui/theme'

const Wrapper = styled.div`
  padding-bottom: 24px;
  margin-bottom: 30px;
  border-bottom: 1px solid #d9e2ec;
`

const FilterWrapper = ({ children }) => <Wrapper>{children}</Wrapper>

export default FilterWrapper
