import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Text, Button, Badge } from 'ui'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { FilterWrapper } from '../shared'
import { PostItem } from '../../components'
import { Post } from '../../types'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { PostTag, PostImage } from '../Unaccepted/components'

const Accepted = ({
  selectedPost,
  posts,
  loadPost,
  loadPosts,
  deletePost,
  makeTopPost,
}) => {
  useEffect(() => {
    loadPosts('1')
  }, [])

  const [t] = useTranslation()

  const handleItemSelect = (value: string) => loadPost(value)

  return (
    <Box
      display='grid'
      gridTemplateColumns='360px auto'
      gridGap='0 30px'
      height='100%'
    >
      <Box display='flex' flexDirection='column' overflow='hidden'>
        <Text
          mb={30}
          display='block'
          fontWeight={theme.fontWeight.medium}
          fontSize='18px'
          lineHeight='18px'
          color={theme.colorsList.secondary[0]}
        >
          {t('feed:accepted')}{' '}
        </Text>
        <Box
          flex={1}
          p={30}
          backgroundColor={theme.color.white}
          borderRadius={theme.borderRadius.m}
          overflow='auto'
        >
          <FilterWrapper>
            <Text
              display='block'
              fontWeight={theme.fontWeight.medium}
              fontSize='18px'
              lineHeight='18px'
              color={theme.colorsList.secondary[0]}
            >
              {t('feed:accepted')}{' '}
            </Text>
          </FilterWrapper>
          {posts.list.map((post: Post) => (
            <PostItem
              key={post.id}
              item={post}
              name='post'
              onChange={handleItemSelect}
            />
          ))}
        </Box>
      </Box>
      <Box display='flex' flexDirection='column' overflow='hidden'>
        <Condition match={selectedPost.item !== null}>
          <Box display='flex' mb={24}>
            <Button
              fullWidth
              appearance='outline'
              size='s'
              onClick={() => deletePost(selectedPost.item?.id)}
            >
              {t('common:delete')}
            </Button>
            <Button
              fullWidth
              appearance='outline'
              size='s'
              onClick={() => makeTopPost(selectedPost.item?.id)}
            >
              Отправить в топ
            </Button>
          </Box>
        </Condition>
        <Box
          flex={1}
          p={30}
          backgroundColor={theme.color.white}
          borderRadius={theme.borderRadius.m}
          overflow='auto'
        >
          <Condition match={selectedPost.item !== null}>
            <Badge borderColor='grayishBlue' color='grayishBlue'>
              Природа
            </Badge>
            <Text
              as='p'
              mb={10}
              pt={10}
              fontWeight={theme.fontWeight.semibold}
              fontSize='24px'
              lineHeight='26px'
              color={theme.colorsList.secondary[0]}
            >
              {selectedPost.item?.title}
            </Text>
            <Condition match={selectedPost.item?.tags !== null}>
              <Box mb={20}>
                {selectedPost.item?.tags.map((tag: string) => (
                  <PostTag key={tag} tag={tag} />
                ))}
              </Box>
            </Condition>
            <Condition match={selectedPost.item?.image !== null}>
              <Box mb={20}>
                <PostImage src={selectedPost.item?.image} />
              </Box>
            </Condition>
            <Text
              as='p'
              fontWeight={theme.fontWeight.medium}
              fontSize='14px'
              lineHeight='16px'
              color={theme.colorsList.secondary[0]}
            >
              {selectedPost.item?.body}
            </Text>
          </Condition>
        </Box>
      </Box>
    </Box>
  )
}

export default Accepted
