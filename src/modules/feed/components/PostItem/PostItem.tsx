import React, { FC } from 'react'
import { Text, CrossedCameraIcon } from 'ui'
import { Box } from 'ui/layout'
import { theme, styled } from 'ui/theme'
import { textSlice } from '../../utils.ts'
import { PostItemProps } from '../../types'

const SelectContainer = styled.div`
  position: relative;

  input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    visibility: hidden;
  }

  input + label {
    display: flex;
    position: relative;
    padding: 10px;
    border: 1px solid;
    background-color: ${props => props.theme.colorsList.secondary[5]};
    border-color: ${props => props.theme.colorsList.secondary[5]};
    border-radius: ${props => props.theme.borderRadius.xs};
    cursor: pointer;
  }

  input + label:hover {
    border-color: ${props => props.theme.colorsList.primary[0]};
  }

  input:checked + label {
    border-color: ${props => props.theme.colorsList.primary[0]};
  }

  &:not(:last-child) {
    margin-bottom: 10px;
  }
`

const Img = styled.img`
  display: block;
  height: 66px;
  width: 66px;
  border-radius: ${props => props.theme.borderRadius.xs};
`

const ImgSkeleton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 66px;
  min-width: 66px;
  background-color: ${props => props.theme.color.secondary[4]};
  border-radius: ${props => props.theme.borderRadius.xs};
`

const Icon = styled.img`
  position: absolute;
  bottom: 10px;
  right: 10px;
`

const PostItem: FC<PostItemProps> = ({ item, name, onChange }) => (
  <SelectContainer>
    <input
      type='radio'
      id={item.id.toString()}
      value={item.id}
      name={name}
      onChange={event => onChange(event.target.value)}
    />
    <label htmlFor={item.id.toString()}>
      {item.image !== null ? (
        <Img src={item.image} alt='' />
      ) : (
        <ImgSkeleton>
          <CrossedCameraIcon />
        </ImgSkeleton>
      )}
      <Box ml='10px'>
        <Text
          as='p'
          fontWeight={theme.fontWeight.medium}
          fontSize='12px'
          lineHeight='14px'
          color={theme.colorsList.secondary[0]}
        >
          {item.title ? textSlice(item.title, 52) : ''}
        </Text>
      </Box>
      <Icon src={item.icon} />
    </label>
  </SelectContainer>
)

export default PostItem
