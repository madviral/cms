export const LOAD_JOB_POSITIONS_REQUEST =
  'services/jobPositions/LOAD_JOB_POSITIONS_REQUEST'
export const LOAD_JOB_POSITIONS_SUCCESS =
  'services/jobPositions/LOAD_JOB_POSITIONS_SUCCESS'
export const LOAD_JOB_POSITIONS_FAILURE =
  'services/jobPositions/LOAD_JOB_POSITIONS_FAILURE'

export const LOAD_JOB_POSITION_DETAILS_REQUEST =
  'services/jobPositions/LOAD_JOB_POSITION_DETAILS_REQUEST'
export const LOAD_JOB_POSITION_DETAILS_SUCCESS =
  'services/jobPositions/LOAD_JOB_POSITION_DETAILS_SUCCESS'
export const LOAD_JOB_POSITION_DETAILS_FAILURE =
  'services/jobPositions/LOAD_JOB_POSITION_DETAILS_FAILURE'

export const CREATE_JOB_POSITION_REQUEST =
  'services/jobPositions/CREATE_JOB_POSITION_REQUEST'
export const CREATE_JOB_POSITION_SUCCESS =
  'services/jobPositions/CREATE_JOB_POSITION_SUCCESS'
export const CREATE_JOB_POSITION_FAILURE =
  'services/jobPositions/CREATE_JOB_POSITION_FAILURE'

export const UPDATE_JOB_POSITION_REQUEST =
  'services/jobPositions/UPDATE_JOB_POSITION_REQUEST'
export const UPDATE_JOB_POSITION_SUCCESS =
  'services/jobPositions/UPDATE_JOB_POSITION_SUCCESS'
export const UPDATE_JOB_POSITION_FAILURE =
  'services/jobPositions/UPDATE_JOB_POSITION_FAILURE'

export const DELETE_JOB_POSITION_REQUEST =
  'services/jobPositions/DELETE_JOB_POSITION_REQUEST'
export const DELETE_JOB_POSITION_SUCCESS =
  'services/jobPositions/DELETE_JOB_POSITION_SUCCESS'
export const DELETE_JOB_POSITION_FAILURE =
  'services/jobPositions/DELETE_JOB_POSITION_FAILURE'

export const CHANGE_PAGINATION = 'services/jobPositions/CHANGE_PAGINATION'
export const CHANGE = 'services/jobPositions/CHANGE'
export const CLEAR = 'services/jobPositions/CLEAR'
