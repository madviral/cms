export const LOAD_SPECIALISTS_REQUEST =
  'services/specialists/LOAD_SPECIALISTS_REQUEST'
export const LOAD_SPECIALISTS_SUCCESS =
  'services/specialists/LOAD_SPECIALISTS_SUCCESS'
export const LOAD_SPECIALISTS_FAILURE =
  'services/specialists/LOAD_SPECIALISTS_FAILURE'
