export const LOAD_SERVICE_REQUEST = 'services/servicesPage/LOAD_SERVICE_REQUEST'
export const LOAD_SERVICE_SUCCESS = 'services/servicesPage/LOAD_SERVICE_SUCCESS'
export const LOAD_SERVICE_FAILURE = 'services/servicesPage/LOAD_SERVICE_FAILURE'

export const CHANGE_PAGINATION = 'services/servicesPage/CHANGE_PAGINATION'
