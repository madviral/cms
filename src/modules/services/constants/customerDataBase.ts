export const CHANGE_PAGINATION = 'services/customers/CHANGE_PAGINATION'

export const LOAD_CUSTOMERS_REQUEST =
  'services/customers/LOAD_CUSTOMERS_REQUEST'
export const LOAD_CUSTOMERS_SUCCESS =
  'services/customers/LOAD_CUSTOMERS_SUCCESS'
export const LOAD_CUSTOMERS_FAILURE =
  'services/customers/LOAD_CUSTOMERS_FAILURE'

export const CREATE_CUSTOMER_REQUEST =
  'services/customers/CREATE_CUSTOMER_REQUEST'
export const CREATE_CUSTOMER_SUCCESS =
  'services/customers/CREATE_CUSTOMER_SUCCESS'
export const CREATE_CUSTOMER_FAILURE =
  'services/customers/CREATE_CUSTOMER_FAILURE'

export const DELETE_CUSTOMER_REQUEST =
  'services/customers/DELETE_CUSTOMER_REQUEST'
export const DELETE_CUSTOMER_SUCCESS =
  'services/customers/DELETE_CUSTOMER_SUCCESS'
export const DELETE_CUSTOMER_FAILURE =
  'services/customers/DELETE_CUSTOMER_FAILURE'

export const SEARCH_CUSTOMER_REQUEST =
  'services/customers/SEARCH_CUSTOMER_REQUEST'
export const SEARCH_CUSTOMER_SUCCESS =
  'services/customers/SEARCH_CUSTOMER_SUCCESS'
export const SEARCH_CUSTOMER_FAILURE =
  'services/customers/SEARCH_CUSTOMER_FAILURE'

export const GET_CUSTOMER_DETAIL_REQUEST =
  'services/customers/GET_CUSTOMER_DETAIL_REQUEST'
export const GET_CUSTOMER_DETAIL_SUCCESS =
  'services/customers/GET_CUSTOMER_DETAIL_SUCCESS'
export const GET_CUSTOMER_DETAIL_FAILURE =
  'services/customers/GET_CUSTOMER_DETAIL_FAILURE'

export const UPDATE_CUSTOMER_DETAIL_REQUEST =
  'services/customers/UPDATE_CUSTOMER_DETAIL_REQUEST'
export const UPDATE_CUSTOMER_DETAIL_SUCCESS =
  'services/customers/UPDATE_CUSTOMER_DETAIL_SUCCESS'
export const UPDATE_CUSTOMER_DETAIL_FAILURE =
  'services/customers/UPDATE_CUSTOMER_DETAIL_FAILURE'
