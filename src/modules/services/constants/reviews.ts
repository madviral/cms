export const LOAD_REVIEWS_REQUEST = 'services/reviews/LOAD_REVIEWS_REQUEST'
export const LOAD_REVIEWS_SUCCESS = 'services/reviews/LOAD_REVIEWS_SUCCESS'
export const LOAD_REVIEWS_FAILURE = 'services/reviews/LOAD_REVIEWS_FAILURE'

export const CHANGE_PAGINATION = 'services/reviews/CHANGE_PAGINATION'
