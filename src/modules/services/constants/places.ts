export const LOAD_PLACES_REQUEST = 'services/places/LOAD_PLACES_REQUEST'
export const LOAD_PLACES_SUCCESS = 'services/places/LOAD_PLACES_SUCCESS'
export const LOAD_PLACES_FAILURE = 'services/places/LOAD_PLACES_FAILURE'

export const LOAD_PLACE_DETAILS_REQUEST =
  'services/places/LOAD_PLACE_DETAILS_REQUEST'
export const LOAD_PLACE_DETAILS_SUCCESS =
  'services/places/LOAD_PLACE_DETAILS_SUCCESS'
export const LOAD_PLACE_DETAILS_FAILURE =
  'services/places/LOAD_PLACE_DETAILS_FAILURE'

export const CREATE_PLACES_REQUEST = 'services/places/CREATE_PLACES_REQUEST'
export const CREATE_PLACES_SUCCESS = 'services/places/CREATE_PLACES_SUCCESS'
export const CREATE_PLACES_FAILURE = 'services/places/CREATE_PLACES_FAILURE'

export const DELETE_PLACES_REQUEST = 'services/places/DELETE_PLACES_REQUEST'
export const DELETE_PLACES_SUCCESS = 'services/places/DELETE_PLACES_SUCCESS'
export const DELETE_PLACES_FAILURE = 'services/places/DELETE_PLACES_FAILURE'

export const ACTIVATE_PLACE_REQUEST = 'services/places/ACTIVATE_PLACE_REQUEST'
export const ACTIVATE_PLACE_SUCCESS = 'services/places/ACTIVATE_PLACE_SUCCESS'
export const ACTIVATE_PLACE_FAILURE = 'services/places/ACTIVATE_PLACE_FAILURE'

export const DEACTIVATE_PLACE_REQUEST =
  'services/places/DEACTIVATE_PLACE_REQUEST'
export const DEACTIVATE_PLACE_SUCCESS =
  'services/places/DEACTIVATE_PLACE_SUCCESS'
export const DEACTIVATE_PLACE_FAILURE =
  'services/places/DEACTIVATE_PLACE_FAILURE'

export const CHANGE_PLACE = 'services/places/CHANGE_PLACE'
