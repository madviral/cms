export const LOAD_CATEGORIES_REQUEST =
  'services/categories/LOAD_CATEGORIES_REQUEST'
export const LOAD_CATEGORIES_SUCCESS =
  'services/categories/LOAD_CATEGORIES_SUCCESS'
export const LOAD_CATEGORIES_FAILURE =
  'services/categories/LOAD_CATEGORIES_FAILURE'

export const LOAD_CATEGORY_TYPES_REQUEST =
  'services/categories/LOAD_CATEGORY_TYPES_REQUEST'
export const LOAD_CATEGORY_TYPES_SUCCESS =
  'services/categories/LOAD_CATEGORY_TYPES_SUCCESS'
export const LOAD_CATEGORY_TYPES_FAILURE =
  'services/categories/LOAD_CATEGORY_TYPES_FAILURE'
