import React, { FC } from 'react'

import { Box } from 'ui/layout'
import { Text } from 'ui'

import { Pagination } from 'src/modules/common/components/Pagination'
import { Rating } from 'src/modules/services/components'
import { Table } from './components/Table'

import { ReviewType } from 'src/modules/services/types'
import { theme } from 'ui/theme'

interface ReviewsTableProps {
  list: ReviewType[]
  rating: number
  pageCount: number
  pageIndex: number
  pageSize: number
  loadingOfList: boolean
  onChangePageSize: (size: number) => void
  onChangePageIndex: (index: number) => void
}

const ReviewsTable: FC<ReviewsTableProps> = ({
  rating,
  list,
  pageCount,
  pageIndex,
  pageSize,
  loadingOfList,
  onChangePageSize,
  onChangePageIndex,
}) => (
  <Box display='flex' flexDirection='column' height='100%'>
    <Box display='flex' alignItems='center' marginBottom='16px' pl='16px'>
      <Text fontSize={14} color={'primary.0'}>
        Правила расчета рейтинга
      </Text>

      <Box ml='auto'>
        <Pagination
          pageIndex={pageIndex}
          defaultPageSize={pageSize}
          setPageSize={onChangePageSize}
          goToPage={onChangePageIndex}
          pageCount={pageCount}
        />
      </Box>
    </Box>

    <Box
      backgroundColor={theme.color.white}
      borderRadius={theme.borderRadius.n}
      mb='16px'
      p='16px'
    >
      <Rating rating={rating} />
    </Box>

    <Table loadingOfList={loadingOfList} items={list} />
  </Box>
)

export default ReviewsTable
