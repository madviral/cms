import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import StarRatings from 'react-star-ratings'

import { Text } from 'ui'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'

interface RatingProps {
  rating: number
}

const Rating: FC<RatingProps> = ({ rating }) => {
  const [t] = useTranslation('services')

  return (
    <Box
      border={`1px solid ${theme.color.success}`}
      borderRadius={theme.borderRadius.xs}
      p='16px'
    >
      <div style={{ textAlign: 'center' }}>
        <Box>
          <Text fontSize={16} color={'secondary.0'}>
            {t('yourRating')}
          </Text>
        </Box>

        <Box my='24px'>
          <Box mb='8px'>
            <Text
              color={theme.color.success}
              fontWeight={theme.fontWeight.medium}
              lineHeight={'48px'}
              fontSize={48}
            >
              {rating}
            </Text>
          </Box>

          <StarRatings
            rating={rating}
            starRatedColor={theme.color.success}
            starDimension='18px'
            starSpacing='0px'
          />
        </Box>

        <Box>
          <Text fontSize={16} color={'secondary.0'}>
            {t('minRating')}
          </Text>
        </Box>
      </div>
    </Box>
  )
}

export default Rating
