import React, { FC, useMemo } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import StarRatings from 'react-star-ratings'

import BaseTable from 'ui/table/controlled/Table'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { theme } from 'ui/theme'
import { Box } from 'ui/layout'
import { Text } from 'ui'

interface TableProps {
  items: any[]
  loadingOfList: boolean
}

const Table: FC<TableProps & WithTranslation> = ({
  loadingOfList,
  items,
  t,
}) => {
  const data = useMemo(
    () =>
      items.map(item => ({
        ...item,
        key: item.id,
      })),
    [items.length],
  )

  const columns = [
    {
      title: 'Рейтинг',
      dataIndex: 'stars',
      key: 'stars',
      render: stars => (
        <StarRatings
          rating={stars}
          starRatedColor={theme.colorsList.primary[0]}
          starDimension='18px'
          starSpacing='0px'
        />
      ),
    },
    {
      title: 'Дата',
      dataIndex: 'createdDate',
      key: 'createdDate',
      render: createdDate => (
        <Text
          color='secondary.0'
          fontSize='12px'
          lineHeight={1}
          fontWeight='500'
        >
          {createdDate}
        </Text>
      ),
    },
    {
      title: 'Клиент',
      dataIndex: 'clientName',
      key: 'clientName',
      render: clientName => (
        <Text
          color='secondary.0'
          fontSize='12px'
          lineHeight={1}
          fontWeight='500'
        >
          {clientName}
        </Text>
      ),
    },
    {
      title: 'Отзыв',
      dataIndex: 'text',
      key: 'text',
      width: '50%',
      render: text => (
        <Text
          color='secondary.0'
          fontSize='12px'
          lineHeight={1}
          fontWeight='500'
        >
          {text}
        </Text>
      ),
    },
  ]

  return (
    <Box
      backgroundColor={theme.color.white}
      borderRadius={theme.borderRadius.n}
      height='64%'
      py={16}
    >
      <Box
        borderBottom={`1px solid ${theme.colorsList.secondary[2]}`}
        display='flex'
        alignItems='center'
        pb={'8px'}
        mx={16}
      >
        <Text
          fontWeight={theme.fontWeight.semibold}
          fontSize={14}
          color={'secondary.0'}
        >
          {t('services:clientsReview')}
        </Text>

        <Box ml='auto' display='flex' alignItems='center'>
          <Box mr='8px'>
            <Text
              fontSize='14px'
              color='secondary.0'
              fontWeight={theme.fontWeight.medium}
            >
              Данные за:
            </Text>
          </Box>
          <Box width='200px'>
            <Dropdown width='100%' disabled={loadingOfList}>
              <Dropdown.OutlinedButton disabled={loadingOfList}>
                Все филиалы
              </Dropdown.OutlinedButton>
              <Dropdown.Menu width='100%'>
                <Dropdown.Item>Все филиалы</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Box>

          <Box mx='8px'>
            <Text
              fontSize='14px'
              color='secondary.0'
              fontWeight={theme.fontWeight.medium}
            >
              По:
            </Text>
          </Box>
          <Box width='130px'>
            <Dropdown width='100%' disabled={loadingOfList}>
              <Dropdown.OutlinedButton disabled={loadingOfList}>
                Все
              </Dropdown.OutlinedButton>
              <Dropdown.Menu width='100%'>
                <Dropdown.Item>Все</Dropdown.Item>
                <Dropdown.Item>1 звезда</Dropdown.Item>
                <Dropdown.Item>2 звезды</Dropdown.Item>
                <Dropdown.Item>3 звезды</Dropdown.Item>
                <Dropdown.Item>4 звезды</Dropdown.Item>
                <Dropdown.Item>5 звезд</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        </Box>
      </Box>

      <BaseTable id='reviews-table' data={data} columns={columns} />
    </Box>
  )
}

export default withTranslation(['shop', 'common'])(Table)
