import React, { FC } from 'react'

import { Drawer } from 'ui/drawer'
import { CustomerDetail } from 'src/modules/services/components'
import { ServicesDrawerProps } from 'src/modules/services/types/interfaces'

const CustomerDetailDrawer: FC<ServicesDrawerProps> = ({
  isOpen,
  id,
  onClose,
  places,
}) => (
  <Drawer
    width='632px'
    handler={false}
    open={isOpen}
    level={null}
    onClose={onClose}
    className='drawer1'
    placement='right'
    fr
  >
    <CustomerDetail onClose={onClose} isOpen={isOpen} places={places} id={id} />
  </Drawer>
)

export default CustomerDetailDrawer
