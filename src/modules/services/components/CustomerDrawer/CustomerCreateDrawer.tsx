import React, { FC } from 'react'

import { Drawer } from 'ui/drawer'
import { CustomerCreate } from 'src/modules/services/components'
import { ServicesDrawerProps } from 'src/modules/services/types/interfaces'

const CustomerCreateDrawer: FC<ServicesDrawerProps> = ({
  isOpen,
  onClose,
  places,
}) => (
  <Drawer
    width='632px'
    handler={false}
    open={isOpen}
    level={null}
    onClose={onClose}
    className='drawer1'
    placement='right'
  >
    <CustomerCreate places={places} onClose={onClose} />
  </Drawer>
)

export default CustomerCreateDrawer
