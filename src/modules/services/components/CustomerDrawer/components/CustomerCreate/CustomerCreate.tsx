import React, { FC, useEffect, useState } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import { Box } from 'ui/layout'
import { styled, theme } from 'ui/theme'
import { Button, FormField, FormTextArea, Multiselect, Text } from 'ui'
import { Appearance } from 'ui/dropdowns/Multiselect/types'

import { createCustomer } from 'src/modules/services/actions/customerDataBase'
import { CreateCustomerBodyType, PlaceType } from 'src/modules/services/types'
// import { RootState } from 'src/store/rootReducer'

const Field = styled.div`
  margin-bottom: 20px;
`

const Upload = styled.div`
  border-radius: 8px;
  border: 1px solid ${props => props.theme.colorsList.secondary[3]};
  width: 100%;
  height: 100%;
  display: flex;
  text-align: center;
  justify-content: center;
  align-items: center;

  img {
    width: 100%;
    height: 100%;
  }
`

interface CustomerCreateProps {
  places: PlaceType[]
  onClose: VoidFunction
  onCreateCustomer: (
    params: CreateCustomerBodyType,
    closeCb: VoidFunction,
    onClose: VoidFunction,
  ) => void
}

const categoryOptions = [
  {
    value: 'none',
    label: 'Категория',
  },
  {
    value: 'loyal',
    label: 'Лояльный',
  },
  {
    value: 'vip',
    label: 'VIP',
  },
]

const sexOptions = [
  {
    value: 'MALE',
    label: 'Мужчина',
  },
  {
    value: 'FEMALE',
    label: 'Женщина',
  },
]

const CustomerCreate: FC<CustomerCreateProps> = ({
  onClose,
  onCreateCustomer,
  places,
}) => {
  const [sexValue, setSexValue] = useState(null)
  const [categoryValue, setCategoryValue] = useState(null)
  const [placeValue, setPlaceValue] = useState(null)
  const [fields, setFields] = useState({
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    discount: '',
    comment: '',
    dateOfBirth: '',
  })
  const [t] = useTranslation()

  const updateFields = event => {
    setFields({
      ...fields,
      [event.target.name]: event.target.value,
    })
  }

  const onClearCb = () => {
    setSexValue(null)
    setCategoryValue(null)
    setPlaceValue(null)
    setFields({
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
      comment: '',
      discount: '',
      dateOfBirth: '',
    })
  }

  const placeOptions = places.map(place => ({
    value: String(place.id),
    label: place.name,
  }))

  useEffect(() => {
    if (placeOptions.length !== 0) {
      setPlaceValue(String(places[0].id))
    }
  }, [placeOptions])

  return (
    <Box
      px='16px'
      display='flex'
      flexDirection='column'
      height='100%'
      backgroundColor='#fff'
    >
      <Box flex='1 1 auto' pt='32px' pb='20px' overflowY='auto'>
        <Box
          mb='16px'
          pb='16px'
          borderBottom={`1px solid ${theme.colorsList.secondary[2]}`}
        >
          <Text
            fontSize='13px'
            lineHeight='1.25'
            fontWeight='600'
            color='secondary.0'
          >
            Добавить клиента
          </Text>
        </Box>

        <Box display='flex' justifyContent='space-between'>
          <Box width='72%'>
            <Box>
              <Field>
                <FormField
                  id={'lastName'}
                  name={'lastName'}
                  type={'text'}
                  value={fields.lastName}
                  label={'Фамилия *'}
                  placeholder={'Шикарян'}
                  onChange={updateFields}
                />
              </Field>
              <Field>
                <FormField
                  id={'firstName'}
                  name={'firstName'}
                  type={'text'}
                  value={fields.firstName}
                  label={t('common:name') + ' *'}
                  placeholder={'Гарик'}
                  onChange={updateFields}
                />
              </Field>
            </Box>
          </Box>

          <Box width='25%' mr='2px'>
            <Upload>
              <Text fontSize={12} color={'secondary.1'}>
                Добавьте <br /> логотип <br /> филиала
              </Text>
            </Upload>
          </Box>
        </Box>

        <Box>
          <Field>
            <FormField
              id={'phone'}
              name={'phone'}
              type={'text'}
              value={fields.phone}
              label={t('common:phone') + ' *'}
              placeholder={'+7 (---) --- -- --'}
              onChange={updateFields}
              required
            />
          </Field>

          <Box mt={18}>
            <Text color={'primary.0'} fontSize={12} fontWeight={600}>
              + Добавить телефон
            </Text>
          </Box>
        </Box>

        <Box mt='18px'>
          <Field>
            <FormField
              id={'1'}
              name={'email'}
              label={'Email'}
              placeholder={'garik.mkhitaryan@gmail.com'}
              value={fields.email}
              onChange={updateFields}
            />
          </Field>

          <Field>
            <Box
              display='flex'
              justifyContent='space-between'
              alignItems='flex-end'
            >
              <Box width='57%'>
                <FormField
                  id={'3'}
                  name={'dateOfBirth'}
                  label={t('common:dateOfBirth')}
                  placeholder={t('common:dateFormat')}
                  value={fields.dateOfBirth}
                  onChange={updateFields}
                />
              </Box>

              <Box width='40%'>
                <Multiselect
                  isAbsolute
                  active={sexValue}
                  appearance={Appearance.mini}
                  label={t('common:chooseGender')}
                  placeholder={t('common:chooseGender')}
                  onChange={data => setSexValue(data.value)}
                  options={sexOptions}
                />
              </Box>
            </Box>
          </Field>

          <Field>
            <Box
              display='flex'
              justifyContent='space-between'
              alignItems='flex-end'
            >
              <Box width='45%'>
                <Multiselect
                  isAbsolute
                  active={categoryValue}
                  appearance={Appearance.mini}
                  label={t('services:places')}
                  placeholder={'Выбрать'}
                  onChange={data => setPlaceValue(data.value)}
                  options={placeOptions}
                />
              </Box>

              <Box width='40%'>
                <Multiselect
                  isAbsolute
                  active={categoryValue}
                  appearance={Appearance.mini}
                  label={t('services:category')}
                  placeholder={t('services:category')}
                  onChange={data => setCategoryValue(data.value)}
                  options={categoryOptions}
                />
              </Box>

              <Box width='10%'>
                <FormField
                  id={'2'}
                  name={'discount'}
                  label={'Скидка'}
                  placeholder={'10%'}
                  value={fields.discount}
                  onChange={updateFields}
                />
              </Box>
            </Box>
          </Field>
        </Box>

        <Box mt={18}>
          <Text color={'primary.0'} fontSize={12} fontWeight={600}>
            + Добавить филиал
          </Text>
        </Box>

        <Box mt='18px'>
          <FormTextArea
            height={'160px'}
            label={'Комментарий'}
            name={'comment'}
            value={fields.comment}
            onChange={updateFields}
            placeholder={t('common:textAreaDefaultPlaceholder')}
          />
        </Box>
      </Box>

      <Box
        display='flex'
        alignItems='center'
        px='10px'
        py='18px'
        justifyContent='flex-end'
        borderTop={`1px solid ${theme.colorsList.secondary[2]}`}
      >
        <Box mr='16px'>
          <Button onClick={onClose} appearance='secondary' size='m'>
            {t('common:cancel')}
          </Button>
        </Box>
        <Box>
          <Button
            appearance='primary'
            disabled={
              fields.firstName.length === 0 &&
              fields.lastName.length === 0 &&
              fields.phone.length === 0
            }
            size='m'
            onClick={() => {
              if (
                fields.firstName.length !== 0 &&
                fields.lastName.length !== 0 &&
                fields.phone.length !== 0
              ) {
                onCreateCustomer(
                  {
                    firstName: fields.firstName,
                    lastName: fields.lastName,
                    email: fields.email,
                    phoneNumber: fields.phone,
                    birthday: fields.dateOfBirth,
                    comment: fields.comment,
                    sex: sexValue,
                    customerPlaces: [
                      {
                        categoryCode: categoryValue,
                        discount:
                          fields.discount.length === 0
                            ? 0
                            : fields.discount.length,
                        placeId: placeValue,
                      },
                    ],
                  },
                  onClearCb,
                  onClose,
                )
              }
            }}
          >
            {t('common:save')}
          </Button>
        </Box>
      </Box>
    </Box>
  )
}
//
// const mapStateToProps = (state: RootState) => ({
//
// })

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onCreateCustomer: (params: CreateCustomerBodyType, onClearCb, onClose) =>
    dispatch(createCustomer(params, onClearCb, onClose)),
})

export default connect(null, mapDispatchToProps)(CustomerCreate)
