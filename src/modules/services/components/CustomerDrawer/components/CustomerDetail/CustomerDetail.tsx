import React, { FC, useEffect, useState } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { useTranslation } from 'react-i18next'

import { Box } from 'ui/layout'
import { styled, theme } from 'ui/theme'
import { Button, FormField, FormTextArea, Multiselect, Text } from 'ui'
import { Appearance } from 'ui/dropdowns/Multiselect/types'

import {
  getCustomerDetail,
  updateCustomerDetail,
} from 'src/modules/services/actions/customerDataBase'
import {
  GetCustomerDetailType,
  UpdateCustomerDetailType,
  PlaceType,
} from 'src/modules/services/types'

const BorderBox = styled.div`
  padding: 16px;
  box-sizing: border-box;
  border-radius: ${props => props.theme.borderRadius.xs};
  border: 1px solid ${props => props.theme.colorsList.secondary[1]};
`

const Field = styled.div`
  margin-bottom: 20px;
`

const Upload = styled.div`
  border-radius: 8px;
  border: 1px solid ${props => props.theme.colorsList.secondary[3]};
  width: 100%;
  height: 100%;
  display: flex;
  text-align: center;
  justify-content: center;
  align-items: center;

  img {
    width: 100%;
    height: 100%;
  }
`

interface CustomerCreateProps {
  onUpdateCustomerDetail: (
    params: UpdateCustomerDetailType,
    closeCb: VoidFunction,
  ) => void
  onLoadCustomerDetail: (params: GetCustomerDetailType) => void
  isOpen: boolean
  customerDetail: any
  onClose: VoidFunction
  places: PlaceType[]
  id: number
}

const categoryOptions = [
  {
    value: 'none',
    label: 'Категория',
  },
  {
    value: 'LOYAL',
    label: 'Лояльный',
  },
  {
    value: 'VIP',
    label: 'VIP',
  },
]

const genderOptions = [
  {
    value: 'MALE',
    label: 'Мужчина',
  },
  {
    value: 'FEMALE',
    label: 'Мужчина',
  },
]

const CustomerDetail: FC<CustomerCreateProps> = ({
  onClose,
  onLoadCustomerDetail,
  isOpen,
  onUpdateCustomerDetail,
  customerDetail,
  id,
  places,
}) => {
  const [sexValue, setSexValue] = useState(null)
  const [categoryValue, setCategoryValue] = useState(null)
  const [placeValue, setPlaceValue] = useState(null)
  const [fields, setFields] = useState({
    name: '',
    phone: '',
    email: '',
    discount: '',
    comment: '',
    dateOfBirth: '',
  })

  const updateFields = event => {
    setFields({
      ...fields,
      [event.target.name]: event.target.value,
    })
  }

  useEffect(() => {
    if (isOpen) {
      onClearCb()
      onLoadCustomerDetail({ id })
    }
  }, [isOpen])

  useEffect(() => {
    if (Object.keys(customerDetail).length !== 0) {
      setSexValue(customerDetail.sex)
      setCategoryValue(customerDetail.placeCustomers[0].category?.code)
      setPlaceValue(customerDetail.placeCustomers[0].place?.id)
      setFields({
        name: customerDetail.name,
        phone: customerDetail.phoneNumber,
        email: customerDetail.email,
        comment: customerDetail.comment,
        discount: customerDetail.placeCustomers[0].discount,
        dateOfBirth: customerDetail.birthday,
      })
    }
  }, [customerDetail])

  const [t] = useTranslation()

  const onClearCb = () => {
    setSexValue(null)
    setCategoryValue(null)
    setPlaceValue(null)
    setFields({
      name: '',
      phone: '',
      email: '',
      comment: '',
      discount: '',
      dateOfBirth: '',
    })
  }

  const placeOptions = places.map(place => ({
    value: String(place.id),
    label: place.name,
  }))

  return (
    <Box
      px='16px'
      display='flex'
      flexDirection='column'
      height='100%'
      backgroundColor='#fff'
    >
      <Box flex='1 1 auto' pt='32px' pb='20px' overflowY='auto'>
        <Box mb='24px'>
          <Box
            mb='16px'
            pb='16px'
            borderBottom={`1px solid ${theme.colorsList.secondary[2]}`}
          >
            <Text
              fontSize='13px'
              lineHeight='1.25'
              fontWeight='600'
              color='secondary.0'
            >
              Карточка клиента
            </Text>
          </Box>

          <Box mb='8px'>
            <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
              Основные данные
            </Text>
          </Box>

          <BorderBox>
            <Box display='flex' justifyContent='space-between'>
              <Box width='72%'>
                <Box>
                  <Field>
                    <FormField
                      id={'name'}
                      name={'name'}
                      type={'text'}
                      value={fields.name}
                      label={t('common:name')}
                      placeholder={'Гарик шикарян'}
                      onChange={updateFields}
                      required
                    />
                  </Field>
                  <Field>
                    <FormField
                      id={'phone'}
                      name={'phone'}
                      type={'text'}
                      value={fields.phone}
                      label={t('common:phone')}
                      placeholder={'+7 (---) --- -- --'}
                      onChange={updateFields}
                      required
                    />
                  </Field>
                </Box>

                <Box mt={18}>
                  <Text color={'primary.0'} fontSize={12} fontWeight={600}>
                    + Добавить телефон
                  </Text>
                </Box>
              </Box>

              <Box width='25%' mr='2px'>
                <Upload>
                  <Text fontSize={12} color={'secondary.1'}>
                    Добавьте <br /> логотип <br /> филиала
                  </Text>
                </Upload>
              </Box>
            </Box>

            <Box mt='18px'>
              <Field>
                <FormField
                  id={'1'}
                  name={'email'}
                  label={'Email'}
                  placeholder={'garik.mkhitaryan@gmail.com'}
                  value={fields.email}
                  onChange={updateFields}
                />
              </Field>

              <Field>
                <Box
                  display='flex'
                  justifyContent='space-between'
                  alignItems='flex-end'
                >
                  <Box width='57%'>
                    <FormField
                      id={'3'}
                      name={'dateOfBirth'}
                      label={t('common:dateOfBirth')}
                      placeholder={t('common:dateFormat')}
                      value={fields.dateOfBirth}
                      onChange={updateFields}
                    />
                  </Box>

                  <Box width='40%'>
                    <Multiselect
                      isAbsolute
                      active={sexValue}
                      appearance={Appearance.mini}
                      label={t('common:chooseGender')}
                      placeholder={t('common:chooseGender')}
                      onChange={data => setSexValue(data.value)}
                      options={genderOptions}
                    />
                  </Box>
                </Box>
              </Field>

              <Field>
                <Box
                  display='flex'
                  justifyContent='space-between'
                  alignItems='flex-end'
                >
                  <Box width='45%'>
                    <Multiselect
                      isAbsolute
                      active={placeValue}
                      appearance={Appearance.mini}
                      label={t('services:places')}
                      placeholder={'Выбрать'}
                      onChange={data => setPlaceValue(data.value)}
                      options={placeOptions}
                    />
                  </Box>

                  <Box width='40%'>
                    <Multiselect
                      isAbsolute
                      active={categoryValue}
                      appearance={Appearance.mini}
                      label={t('services:category')}
                      placeholder={t('services:category')}
                      onChange={data => setCategoryValue(data.value)}
                      options={categoryOptions}
                    />
                  </Box>

                  <Box width='10%'>
                    <FormField
                      id={'2'}
                      name={'discount'}
                      label={'Скидка'}
                      placeholder={'10%'}
                      value={fields.discount}
                      onChange={updateFields}
                    />
                  </Box>
                </Box>
              </Field>

              <Box mt='18px'>
                <Text color={'primary.0'} fontSize={12} fontWeight={600}>
                  + Добавить филиал
                </Text>
              </Box>
            </Box>

            <Box mt='18px'>
              <FormTextArea
                height={'160px'}
                label={'Комментарий'}
                value={fields.comment}
                placeholder={t('common:textAreaDefaultPlaceholder')}
                onChange={updateFields}
              />
            </Box>
          </BorderBox>
        </Box>

        <Box mb='16px'>
          <Box
            mb='16px'
            pb='16px'
            borderBottom={`1px solid ${theme.colorsList.secondary[2]}`}
          >
            <Text
              fontSize='13px'
              lineHeight='1.25'
              fontWeight='600'
              color='secondary.0'
            >
              История посещения
            </Text>
          </Box>

          <BorderBox />
        </Box>
      </Box>

      <Box
        flex='1 0 66px'
        display='flex'
        alignItems='center'
        px='10px'
        justifyContent='flex-end'
        borderTop={`1px solid ${theme.colorsList.secondary[2]}`}
      >
        <Box mr='16px'>
          <Button onClick={onClose} appearance='secondary' size='m'>
            {t('common:cancel')}
          </Button>
        </Box>
        <Box>
          <Button
            appearance='primary'
            size='m'
            onClick={() => {
              if (
                fields.name.length !== 0 &&
                fields.email.length !== 0 &&
                fields.phone.length !== 0 &&
                fields.dateOfBirth.length !== 0 &&
                sexValue &&
                categoryValue
              ) {
                onUpdateCustomerDetail(
                  {
                    id,
                    body: {
                      name: fields.name,
                      email: fields.email,
                      phoneNumber: fields.phone,
                      birthday: fields.dateOfBirth,
                      comment: fields.comment,
                      sex: sexValue,
                      customerPlaces: [
                        {
                          categoryCode: categoryValue,
                          discount:
                            fields.discount.length === 0
                              ? 0
                              : fields.discount.length,
                          placeId: placeValue,
                        },
                      ],
                    },
                  },
                  onClose,
                )
              }
            }}
          >
            {t('common:save')}
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

const mapStateToProps = (state: RootState) => ({
  customerDetail: state.services.customers.customerDetail,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onLoadCustomerDetail: (params: GetCustomerDetailType) =>
    dispatch(getCustomerDetail(params)),
  onUpdateCustomerDetail: (
    params: GetCustomerDetailType,
    closeCb: VoidFunction,
  ) => dispatch(updateCustomerDetail(params, closeCb)),
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerDetail)
