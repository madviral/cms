import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { useTableCbx } from 'lib/hooks'

import { Text } from 'ui'
import { Box } from 'ui/layout'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'

import { Table } from 'src/modules/services/components/SpecialistsTable/components'

import { SpecialistsTableProps } from 'src/modules/services/types/interfaces'

const checkboxName = 'service_specialists'

const SpecialistsTable: FC<SpecialistsTableProps> = ({ list }) => {
  const [t] = useTranslation('services')
  const { cbxState, onChangeCbx, onCheckAllCbxChange } = useTableCbx(list)
  const disabledActions = cbxState.checkedList.length === 0

  return (
    <Box display='flex' flexDirection='column' height='100%'>
      <Box display='flex' alignItems='center' marginBottom='16px' pl='16px'>
        <Box flex='0 0 20px' mr='16px'>
          <FormCheckbox
            id='selectAllProducts'
            indeterminate={cbxState.indeterminate}
            name={checkboxName}
            value={checkboxName}
            checked={cbxState.checkAll}
            disabled={list.length === 0}
            onChange={onCheckAllCbxChange}
          />
        </Box>

        <Box width='200px' mr='32px'>
          <Dropdown width='100%' disabled={disabledActions}>
            <Dropdown.OutlinedButton disabled={disabledActions}>
              {t('shop:chooseAnAction')}
            </Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item>Активировать</Dropdown.Item>
              <Dropdown.Item>Деактивировать</Dropdown.Item>
              <Dropdown.Item>Удалить</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Box>
        <Box mr='8px'>
          <Text fontSize='14px' color='secondary.0'>
            {t('filter')}:
          </Text>
        </Box>
        <Box width='200px'>
          <Dropdown width='100%'>
            <Dropdown.OutlinedButton>Все филиалы</Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item>Все филиалы</Dropdown.Item>
              <Dropdown.Item>Все филиалы</Dropdown.Item>
              <Dropdown.Item>Все филиалы</Dropdown.Item>
              <Dropdown.Item>Все филиалы</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Box>
      </Box>

      <Table
        items={list}
        checkboxName={checkboxName}
        checkedList={cbxState.checkedList}
        onChangeCbx={onChangeCbx}
      />
    </Box>
  )
}

export default SpecialistsTable
