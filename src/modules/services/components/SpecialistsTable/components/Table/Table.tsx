import React, { FC } from 'react'
import { WithTranslation, withTranslation } from 'react-i18next'

import { SpecialistsTableColumnsProps } from 'src/modules/services/types/interfaces'

const Table: FC<WithTranslation & SpecialistsTableColumnsProps> = () => {
  return <div />
}

export default withTranslation(['shop', 'common'])(Table)
