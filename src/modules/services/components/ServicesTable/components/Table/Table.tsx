import React, { FC, useCallback, useMemo } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'

import { ServiceType } from 'src/modules/services/types'
import BaseTable, { ColumnType } from 'ui/table/controlled/Table'

import { SettingsIcon, ShadowedTooltip, Text } from 'ui'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { theme } from 'ui/theme'
import { Box } from 'ui/layout'
import { Image } from 'ui/image'
import { Condition } from 'ui/condition'

export interface TableProps {
  checkboxName?: string
  checkedList: string[]
  onChangeCbx: (id: string | number) => void
  items: ServiceType[]
}

const Table: FC<TableProps & WithTranslation> = ({
  checkedList,
  checkboxName,
  onChangeCbx,
  items,
}) => {
  const data = useMemo(
    () =>
      items.map(item => ({
        ...item,
        price: `${item.minPrice.value} ${item.minPrice.currency} - ${item.maxPrice.value} ${item.maxPrice.currency}`,
      })),
    [items.length],
  )

  const columns = useMemo(
    (): Array<ColumnType<ServiceType>> => [
      {
        title: <SettingsIcon color={theme.colorsList.secondary[1]} />,
        dataIndex: 'id',
        key: 'checkbox',
        width: 46,
        render: id => {
          const onChangeCbxCached = useCallback(() => {
            onChangeCbx(id)
          }, [id])
          return (
            <Box>
              <FormCheckbox
                id={id}
                name={checkboxName}
                value={id}
                checked={checkedList.some(itemId => itemId === id)}
                onChange={onChangeCbxCached}
              />
            </Box>
          )
        },
      },
      {
        title: 'Статус',
        dataIndex: 'status',
        key: 'status',
        width: 250,
        render: status => (
          <Text
            color={
              (status === 'ONLINE' && '#1AB759') ||
              (status === 'OFFLINE' && 'secondary.1')
            }
            fontWeight='500'
            fontSize='12px'
            lineHeight={1}
          >
            {status === 'ONLINE' && 'Онлайн'}
            {status === 'OFFLINE' && 'Оффлайн'}
          </Text>
        ),
      },
      {
        title: 'Название',
        dataIndex: 'name',
        key: 'name',
        width: 350,
        render: name => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {name}
          </Text>
        ),
      },
      {
        title: 'Стоимость',
        dataIndex: 'price',
        key: 'price',
        width: 250,
        render: price => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {price}
          </Text>
        ),
      },
      {
        title: 'Сотрудники',
        dataIndex: 'specialists',
        key: 'specialists',
        render: (specialists, row) => {
          const imgId = `serviceSpecialist${row.id}`

          return (
            <Box display='flex'>
              {specialists.map((specialist, idx) => (
                <Box>
                  <Box mr='8px' data-tip data-for={imgId} key={idx}>
                    <Image
                      src={specialist.photoUrl}
                      alt={specialist.name}
                      borderRadius={'r'}
                      height={32}
                      width={32}
                      isBg
                    />
                  </Box>
                  <Condition match={!!specialist.photoUrl}>
                    <ShadowedTooltip id={imgId} effect='solid' notFade>
                      <Box mb='8px' backgroundColor={'#fff'}>
                        <Image
                          borderRadius={'n'}
                          src={'https://i.ibb.co/4j3F2Q1/Mask.png'}
                          alt={specialist.name}
                          width={200}
                          height={200}
                        />
                      </Box>

                      <Box
                        borderRadius={theme.borderRadius.n}
                        backgroundColor={'#fff'}
                      >
                        <div style={{ textAlign: 'center' }}>
                          <Box mb='8px'>
                            <Text
                              color='secondary.0'
                              fontSize='14px'
                              lineHeight={1}
                              fontWeight='500'
                            >
                              {specialist.name}
                            </Text>
                          </Box>

                          <Box>
                            <Text
                              color='secondary.0'
                              fontSize='12px'
                              lineHeight={1}
                              fontWeight='500'
                            >
                              {specialist.jobPosition}
                            </Text>
                          </Box>
                        </div>
                      </Box>
                    </ShadowedTooltip>
                  </Condition>
                </Box>
              ))}
            </Box>
          )
        },
      },
    ],
    [checkedList.length, onChangeCbx],
  )

  return <BaseTable data={data} columns={columns} />
}

export default withTranslation(['shop', 'common'])(Table)
