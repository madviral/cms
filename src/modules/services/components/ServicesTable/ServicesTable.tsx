import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { useTableCbx } from 'lib/hooks'

import { Pagination } from 'src/modules/common/components/Pagination'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { Box } from 'ui/layout'
import { Text } from 'ui'

import { ServiceType } from 'src/modules/services/types'
import { Table } from 'src/modules/services/components/ServicesTable/components'

interface ServicesTableProps {
  data: ServiceType[]
  pageCount: number
  pageIndex: number
  pageSize: number
  onChangePageSize?: (size: number) => void
  onChangePageIndex?: (index: number) => void
}

const checkboxName = 'service'

const ServicesTable: FC<ServicesTableProps> = ({
  data,
  pageSize,
  pageIndex,
  pageCount,
  onChangePageSize,
  onChangePageIndex,
}) => {
  const [t] = useTranslation('')
  const { cbxState, onChangeCbx, onCheckAllCbxChange } = useTableCbx(data)
  const disabledActions = cbxState.checkedList.length === 0

  return (
    <Box display='flex' flexDirection='column' height='100%'>
      <Box display='flex' alignItems='center' marginBottom='16px' pl='16px'>
        <Box flex='0 0 20px' mr='16px'>
          <FormCheckbox
            id='selectAllProducts'
            indeterminate={cbxState.indeterminate}
            name={checkboxName}
            value={checkboxName}
            checked={cbxState.checkAll}
            disabled={data.length === 0}
            onChange={onCheckAllCbxChange}
          />
        </Box>

        <Box width='200px' mr='32px'>
          <Dropdown width='100%' disabled={disabledActions}>
            <Dropdown.OutlinedButton disabled={disabledActions}>
              {t('shop:chooseAnAction')}
            </Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item>Активировать</Dropdown.Item>
              <Dropdown.Item>Деактивировать</Dropdown.Item>
              <Dropdown.Item>Удалить</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Box>
        <Box mr='8px'>
          <Text fontSize='14px' color='secondary.0'>
            {t('filter')}:
          </Text>
        </Box>
        <Box width='200px'>
          <Dropdown width='100%'>
            <Dropdown.OutlinedButton>Все категорий</Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item>Все филиалы</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Box>

        <Box ml='auto'>
          <Pagination
            pageIndex={pageIndex}
            defaultPageSize={pageSize}
            setPageSize={onChangePageSize}
            goToPage={onChangePageIndex}
            pageCount={pageCount}
          />
        </Box>
      </Box>

      <Table
        items={data}
        checkboxName={checkboxName}
        checkedList={cbxState.checkedList}
        onChangeCbx={onChangeCbx}
      />
    </Box>
  )
}

export default ServicesTable
