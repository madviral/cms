import React, { FC } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'

import { useTranslation } from 'react-i18next'

import { createPlace } from 'src/modules/services/actions/places'

import { theme } from 'ui/theme'
import { Box } from 'ui/layout'
import { Button } from 'ui'

interface FooterProps {
  id: number
  onClose: VoidFunction
  activeTabIndex: number
  onCreatePlace: (cb: VoidFunction, id: number) => void
}

const Footer: FC<FooterProps> = ({
  id,
  onClose,
  onCreatePlace,
  activeTabIndex,
}) => {
  const [t] = useTranslation('common')

  return (
    <Box
      display='flex'
      alignItems='center'
      px='10px'
      justifyContent='flex-end'
      borderTop={`1px solid ${theme.colorsList.secondary[2]}`}
    >
      <Box pt='20px' display='flex'>
        <Box mr='16px'>
          <Button onClick={onClose} appearance='primary' size='m'>
            {t('cancel')}
          </Button>
        </Box>

        <Button
          onClick={() => onCreatePlace(onClose, id)}
          disabled={activeTabIndex !== 2}
          appearance='outline'
          size='m'
        >
          {t('save')}
        </Button>
      </Box>
    </Box>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onCreatePlace: (closeCb: VoidFunction, id: number) =>
    dispatch(createPlace(closeCb, id)),
})

export default connect(null, mapDispatchToProps)(Footer)
