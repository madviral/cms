import React, { FC } from 'react'
import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'

import { Address, Schedule, Wrap } from 'src/modules/services/components'
import { AddressProps } from 'src/modules/services/components/PlaceDrawer'
import { ContactsTabProps } from 'src/modules/services/types/interfaces'
import { loadCities } from 'src/modules/common/actions/address'
import { change as changeDay } from 'src/modules/common/actions/workSchedule'
import {
  suggest,
  changeCity,
  change,
  clear,
} from 'src/modules/services/actions/placeDetail'

import { Box } from 'ui/layout'

const Contacts: FC<ContactsTabProps & AddressProps> = ({
  cities,
  city,
  house,
  street,
  flat,
  office,
  mapState,
  onChangeCity,
  onChangeStreet,
  onChangeFlat,
  onChangeOffice,
  onChangeHouse,
  onSuggest,
  schedule,
  onChangeIsDayOff,
  onChangeDayFromTime,
  onChangeDayToTime,
}) => (
  <Wrap>
    <Box mb='16px'>
      <Address
        office={office}
        cities={cities}
        city={city}
        house={house}
        mapState={mapState}
        street={street}
        flat={flat}
        onChangeFlat={onChangeFlat}
        onChangeOffice={onChangeOffice}
        onChangeCity={onChangeCity}
        onChangeStreet={onChangeStreet}
        onChangeHouse={onChangeHouse}
        onSuggest={onSuggest}
      />
    </Box>

    <Schedule
      schedule={schedule}
      onChangeIsDayOff={onChangeIsDayOff}
      onChangeDayFromTime={onChangeDayFromTime}
      onChangeDayToTime={onChangeDayToTime}
    />
  </Wrap>
)

const mapStateToProps = (state: RootState) => ({
  cities: state.common.address.cities,
  city: state.services.placeDetail.city,
  street: state.services.placeDetail.street,
  office: state.services.placeDetail.office,
  house: state.services.placeDetail.house,
  flat: state.services.placeDetail.flat,
  schedule: state.common.workSchedule,
  mapState: state.services.placeDetail.mapState,
})

const mapDispatchToProps = dispatch => ({
  onChangeDayFromTime: (day, value) =>
    dispatch(changeDay('fromTime', day, value)),
  onChangeIsDayOff: (day, value) => dispatch(changeDay('isDayOff', day, value)),
  onChangeDayToTime: (day, value) => dispatch(changeDay('toTime', day, value)),
  onChangeStreet: value => dispatch(change('street', value)),
  onChangeHouse: value => dispatch(change('house', value)),
  onChangeFlat: value => dispatch(change('flat', value)),
  onChangeOffice: value => dispatch(change('office', value)),
  onChangeCity: value => dispatch(changeCity(value)),
  onSuggest: value => dispatch(suggest(value)),
  onLoadCities: () => dispatch(loadCities()),
  onClear: () => dispatch(clear()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Contacts)
