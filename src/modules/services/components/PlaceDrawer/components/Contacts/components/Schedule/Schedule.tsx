import React from 'react'
import { useTranslation } from 'react-i18next'

import { Text } from 'ui'
import { Box } from 'ui/layout'

import { DayOfWeekArray } from 'src/modules/common/types'
import WorkSchedule from 'src/modules/common/containers/WorkSchedule'

const Schedule = ({
  schedule,
  onChangeIsDayOff,
  onChangeDayFromTime,
  onChangeDayToTime,
}) => {
  const [t] = useTranslation('common')

  return (
    <Box>
      <Box mb='4px'>
        <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
          {t('workingHours')}
        </Text>
      </Box>
      <WorkSchedule
        week={Object.keys(schedule) as DayOfWeekArray}
        onChangeIsDayOff={onChangeIsDayOff}
        onChangeDayFromTime={onChangeDayFromTime}
        onChangeDayToTime={onChangeDayToTime}
        schedule={schedule}
      />
    </Box>
  )
}

export default Schedule
