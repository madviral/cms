import React, { FC, memo } from 'react'
import { useTranslation } from 'react-i18next'
import { Map, MapState } from 'react-yandex-maps'

import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { FormField, Text } from 'ui'
import { ControledSelect } from 'ui/dropdowns/ControledSelect'

import { ICity, IStringCb } from 'src/modules/common/types'
import { YMaps, Suggest } from 'src/modules/common/components/YMaps'
import { ISuggestArgs } from 'src/modules/common/components/YMaps/types'
import { Appearance as FormFieldAppearance } from 'ui/inputs/FormField/types/interfaces'

export interface AddressProps {
  cities: ICity[]
  city?: ICity
  street?: string
  house?: string
  mapState?: MapState
  flat?: string
  office?: string
  onChangeCity: (city: ICity) => void
  onChangeStreet: IStringCb
  onChangeHouse: IStringCb
  onChangeOffice: IStringCb
  onChangeFlat: IStringCb
  onSuggest: (value: ISuggestArgs) => void
}

const Address: FC<AddressProps> = ({
  cities,
  city,
  house,
  flat,
  mapState,
  street,
  office,
  onChangeStreet,
  onChangeHouse,
  onChangeOffice,
  onChangeFlat,
  onChangeCity,
  onSuggest,
}) => {
  const [t] = useTranslation()
  const mapedCities = cities.map(cityItem => ({
    ...cityItem,
    label: cityItem.name,
    value: String(cityItem.id),
  }))

  return (
    <YMaps>
      <Box mb='16px'>
        <Box mb='4px'>
          <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
            {t('shop:address')}
          </Text>
        </Box>

        <Box mb='16px'>
          <ControledSelect<ICity>
            showSearch
            value={city?.label}
            style={{ width: '100%' }}
            placeholder={t('city')}
            options={mapedCities}
            withoutPortal
            onChange={value => onChangeCity(value)}
          />
        </Box>
        <Box mb='16px'>
          <Suggest
            id='stockSuggest'
            value={street}
            prefix={`Казахстан, ${city ? city.label : ''}`}
            placeholder={t('street')}
            appearance={FormFieldAppearance.default}
            onChange={value => onChangeStreet(value)}
            disabled={!city}
            onSuggest={onSuggest}
            required
          />
        </Box>
        <Box
          display='grid'
          gridTemplateColumns='100px 100px 100px 100px'
          gridColumnGap='16px'
        >
          <FormField
            value={house}
            placeholder={t('house')}
            onChange={e => onChangeHouse(e.target.value)}
            disabled={!city}
            required
          />
          <FormField
            value={office}
            onChange={e => onChangeOffice(e.target.value)}
            placeholder={t('office')}
            disabled={!city}
          />
          <FormField
            value={flat}
            onChange={e => onChangeFlat(e.target.value)}
            placeholder={t('flat')}
            disabled={!city}
          />
        </Box>
      </Box>

      <Box mb='4px'>
        <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
          {t('shop:location')}
        </Text>
      </Box>

      <Box
        borderRadius='8px'
        border='1px solid'
        borderColor={theme.colorsList.secondary[2]}
        overflow='hidden'
      >
        <Map
          width='100%'
          height={300}
          state={mapState}
          onLoad={ymaps => ((window as any).ymaps = ymaps)}
          modules={['geocode', 'suggest']}
        />
      </Box>
    </YMaps>
  )
}

export default memo(Address)
