import React, { FC } from 'react'

import { Wrap } from 'src/modules/services/components'
import { PhotoUpload } from 'ui'

const Gallery: FC = () => {
  return (
    <Wrap>
      <PhotoUpload size={6} />
    </Wrap>
  )
}

export default Gallery
