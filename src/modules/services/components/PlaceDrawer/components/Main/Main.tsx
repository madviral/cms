import React, { FC, useEffect } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { useTranslation } from 'react-i18next'

import { Wrap } from 'src/modules/services/components'
import { MainTabProps } from 'src/modules/services/types/interfaces'
import {
  loadCategories,
  loadCategoryTypes,
} from 'src/modules/services/actions/categories'

import { Box } from 'ui/layout'
import { styled } from 'ui/theme'
import { FormField, Multiselect, Text } from 'ui'

import { Appearance } from 'ui/dropdowns/Multiselect/types'
import { change } from 'src/modules/services/actions/placeDetail'

const Field = styled.div`
  margin-bottom: 20px;
`

const Upload = styled.div`
  border-radius: 8px;
  border: 1px solid ${props => props.theme.colorsList.secondary[3]};
  width: 100%;
  height: 100%;
  display: flex;
  text-align: center;
  justify-content: center;
  align-items: center;

  img {
    width: 100%;
    height: 100%;
  }
`

const Main: FC<MainTabProps> = ({
  list,
  typeList,
  onLoadCategories,
  onLoadCategoryTypes,
  businessAreaValue,
  businessTypeValue,
  INSTAGRAM,
  FACEBOOK,
  WHATSAPP,
  VK,
  TIKTOK,
  placeName,
  placePhone,
  webSiteUrl,
  onChange,
  onSelectChange,
  loadingOfPLaceDetail,
}) => {
  const [t] = useTranslation()

  useEffect(() => {
    onLoadCategories()
  }, [])

  useEffect(() => {
    if (businessAreaValue) {
      onLoadCategoryTypes(Number(businessAreaValue))
    }
  }, [businessAreaValue])

  const areaOptions = list.map(item => ({
    value: item.id,
    label: item.name,
  }))

  const typeOptions = typeList.map(item => ({
    value: item.id,
    label: item.name,
  }))

  return (
    <Wrap>
      <Box display='flex' justifyContent='space-between'>
        <Box width='72%'>
          <Box>
            <Field>
              <FormField
                id={'placeName'}
                name={'placeName'}
                type={'text'}
                value={placeName}
                label={t('services:placeName')}
                placeholder={t('services:placeName')}
                onChange={onChange}
                error={'Вы не ввели имя!'}
                disabled={loadingOfPLaceDetail}
              />
            </Field>
            <Field>
              <FormField
                id={'placePhone'}
                name={'placePhone'}
                type={'text'}
                value={placePhone}
                label={t('common:phone')}
                placeholder={'+7 (---) --- -- --'}
                onChange={onChange}
                disabled={loadingOfPLaceDetail}
                required
              />
            </Field>
          </Box>

          <Box mt={18}>
            <Text color={'primary.0'} fontSize={12} fontWeight={600}>
              + Добавить телефон
            </Text>
          </Box>
        </Box>

        <Box width='25%' mr='2px'>
          <Upload>
            <Text fontSize={12} color={'secondary.1'}>
              Добавьте <br /> логотип <br /> филиала
            </Text>
          </Upload>
        </Box>
      </Box>

      <Box display='flex' justifyContent='space-between' mt='19px'>
        <Box width='48%'>
          <Field>
            <Multiselect
              isAbsolute
              active={businessAreaValue}
              appearance={Appearance.mini}
              label={t('services:businessArea')}
              placeholder={t('services:businessArea')}
              onChange={data => onSelectChange('businessAreaValue', data.value)}
              options={areaOptions}
              disabled={loadingOfPLaceDetail}
            />
          </Field>

          <Field>
            <FormField
              id={'1'}
              name={'webSiteUrl'}
              label={'Ваш сайт'}
              placeholder={'Введите ссылку'}
              value={webSiteUrl}
              onChange={onChange}
              disabled={loadingOfPLaceDetail}
            />
          </Field>

          <Field>
            <FormField
              id={'2'}
              value={INSTAGRAM}
              name={'INSTAGRAM'}
              label={'Instagram'}
              placeholder={'Введите ссылку'}
              onChange={onChange}
              disabled={loadingOfPLaceDetail}
            />
          </Field>

          <Field>
            <FormField
              id={'3'}
              value={FACEBOOK}
              name={'FACEBOOK'}
              label={'Facebook'}
              placeholder={'Введите ссылку'}
              onChange={onChange}
              disabled={loadingOfPLaceDetail}
            />
          </Field>
        </Box>

        <Box width='48%'>
          <Field>
            <Multiselect
              isAbsolute
              active={businessTypeValue}
              disabled={typeList.length === 0 || loadingOfPLaceDetail}
              appearance={Appearance.mini}
              label={t('services:businessType')}
              placeholder={t('services:businessType')}
              onChange={data => onSelectChange('businessTypeValue', data.value)}
              options={typeOptions}
            />
          </Field>

          <Field>
            <FormField
              id={'4'}
              type={'text'}
              value={WHATSAPP}
              name={'WHATSAPP'}
              label={'WhatsApp'}
              placeholder={'Введите номер'}
              onChange={onChange}
              disabled={loadingOfPLaceDetail}
            />
          </Field>

          <Field>
            <FormField
              id={'5'}
              type={'text'}
              label={'Vk'}
              name={'VK'}
              value={VK}
              placeholder={'Введите ссылку'}
              onChange={onChange}
              disabled={loadingOfPLaceDetail}
            />
          </Field>

          <Field>
            <FormField
              id={'6'}
              type={'text'}
              label={'TikTok'}
              value={TIKTOK}
              name={'TIKTOK'}
              placeholder={'Введите ссылку'}
              onChange={onChange}
              disabled={loadingOfPLaceDetail}
            />
          </Field>
        </Box>
      </Box>
    </Wrap>
  )
}

const mapStateToProps = (state: RootState) => ({
  list: state.services.categories.list,
  loadingOfList: state.services.categories.loadingOfList,
  errorOfList: state.services.categories.errorOfList,

  typeList: state.services.categories.typeList,
  loadingOfTypeList: state.services.categories.loadingOfTypeList,
  errorOfTypeList: state.services.categories.errorOfTypeList,

  loadingOfPLaceDetail: state.services.places.loadingOfPLaceDetail,

  businessAreaValue: state.services.placeDetail.businessAreaValue,
  businessTypeValue: state.services.placeDetail.businessTypeValue,
  placeName: state.services.placeDetail.placeName,
  placePhone: state.services.placeDetail.placePhone,
  webSiteUrl: state.services.placeDetail.webSiteUrl,
  INSTAGRAM: state.services.placeDetail.INSTAGRAM,
  FACEBOOK: state.services.placeDetail.FACEBOOK,
  WHATSAPP: state.services.placeDetail.WHATSAPP,
  VK: state.services.placeDetail.VK,
  TIKTOK: state.services.placeDetail.TIKTOK,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onLoadCategories: () => dispatch(loadCategories()),
  onLoadCategoryTypes: (catId: number) => dispatch(loadCategoryTypes(catId)),
  onChange: event => dispatch(change(event.target.name, event.target.value)),
  onSelectChange: (name, value) => dispatch(change(name, value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Main)
