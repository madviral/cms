import React, { FC, useState } from 'react'

import { Tabs } from 'ui'
import { Drawer } from 'ui/drawer'

import { ServicesDrawerProps } from 'src/modules/services/types/interfaces'

import { Main } from './components/Main'
import { Contacts } from './components/Contacts'
import { Gallery } from './components/Gallery'
import { Footer } from './components/shared/Footer'
import { Box } from 'ui/layout'

interface PlaceDrawerProps {
  id: number
}

const PlaceDrawer: FC<ServicesDrawerProps & PlaceDrawerProps> = ({
  id,
  isOpen,
  onClose,
}) => {
  const [activeTabIndex, setActiveTabIndex] = useState(0)

  const tabs = [
    {
      id: 0,
      name: 'shop:main',
      component: Main,
      color: 'primary',
    },
    {
      id: 1,
      name: 'common:contacts',
      component: Contacts,
      color: 'primary',
    },
    {
      id: 2,
      name: 'common:gallery',
      component: Gallery,
      color: 'primary',
    },
  ]

  return (
    <Drawer
      width='754px'
      handler={false}
      open={isOpen}
      level={null}
      onClose={onClose}
      className='drawer1'
      placement='right'
    >
      <Box
        p='16px'
        display='flex'
        flexDirection='column'
        height='100%'
        backgroundColor='#fff'
      >
        <Box flex='1 1 auto' pb='20px' overflowY='auto'>
          <Tabs tabs={tabs} passActiveTabIndex={setActiveTabIndex} />
        </Box>
        <Footer
          id={id}
          activeTabIndex={activeTabIndex}
          onClose={() => onClose()}
        />
      </Box>
    </Drawer>
  )
}

export default PlaceDrawer
