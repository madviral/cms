import React, { FC, useCallback, useMemo } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'

import BaseTable, { ColumnType } from 'ui/table/controlled/Table'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { Text } from 'ui/text'
import { Box } from 'ui/layout'
import { DotsIcon, SettingsIcon } from 'ui/icons'
import { theme } from 'ui/theme'

interface TableProps {
  items: any[]
  checkboxName?: string
  checkedList: string[]
  handleUpdate: (n: number) => void
  onChangeCbx: (id: string | number) => void
}

const Table: FC<WithTranslation & TableProps> = ({
  checkedList,
  checkboxName,
  items,
  t,
  onChangeCbx,
  handleUpdate,
}) => {
  const data = useMemo(
    () =>
      items.map(item => ({
        ...item,
        key: item.id,
      })),
    [items.length],
  )

  const columns = useMemo(
    (): Array<ColumnType<any>> => [
      {
        title: <SettingsIcon color={theme.colorsList.secondary[1]} />,
        dataIndex: 'id',
        key: 'checkbox',
        width: 46,
        render: id => {
          const onChangeCbxCached = useCallback(() => {
            onChangeCbx(id)
          }, [id])
          return (
            <Box>
              <FormCheckbox
                id={id}
                name={checkboxName}
                value={id}
                checked={checkedList.some(itemId => itemId === id)}
                onChange={onChangeCbxCached}
              />
            </Box>
          )
        },
      },
      {
        title: 'Название',
        dataIndex: 'name',
        key: 'name',
        width: 480,
        render: name => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {name}
          </Text>
        ),
      },
      {
        title: 'Описание',
        dataIndex: 'description',
        key: 'description',
        render: phone => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {phone}
          </Text>
        ),
      },
      {
        title: '',
        dataIndex: 'id',
        key: 'actions',
        width: 20,
        fixed: 'right',
        render: () => (
          <Box
            display='flex'
            alignItems='center'
            width='100%'
            height='100%'
            backgroundColor='white'
          >
            <Dropdown>
              <Dropdown.SimpleButton noCarret>
                <DotsIcon />
              </Dropdown.SimpleButton>
              <Dropdown.Menu direction='l'>
                <Dropdown.Item>{t('edit')}</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        ),
      },
    ],
    [checkedList.length, onChangeCbx],
  )

  return (
    <BaseTable
      id='job-position-table'
      data={data}
      columns={columns}
      onRow={(row: any) => ({
        onClick: () => handleUpdate(row.id),
      })}
    />
  )
}

export default withTranslation(['shop', 'common'])(Table)
