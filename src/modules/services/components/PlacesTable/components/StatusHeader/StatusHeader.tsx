import React from 'react'

import { Box } from 'ui/layout'
import { Text, DoubleArrowsIcon } from 'ui'

const StatusHeader = ({ status }) => (
  <div style={{ cursor: 'pointer' }}>
    <Box display='flex' alignItems='center'>
      <Box mr='4px'>
        <Text>{status}</Text>
      </Box>

      <DoubleArrowsIcon />
    </Box>
  </div>
)

export default StatusHeader
