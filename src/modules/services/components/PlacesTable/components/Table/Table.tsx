import React, { FC, useCallback, useMemo } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'

import BaseTable, { ColumnType } from 'ui/table/controlled/Table'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { Text, Truncate } from 'ui/text'
import { DotsIcon, SettingsIcon } from 'ui/icons'
import { Box } from 'ui/layout'

import { StatusHeader } from 'src/modules/services/components/PlacesTable/components'
import { PlacesTableProps } from 'src/modules/services/types/interfaces'
import { PlaceType } from 'src/modules/services/types'
import { theme } from 'ui/theme'

const Table: FC<WithTranslation & PlacesTableProps> = ({
  checkedList,
  checkboxName,
  items,
  t,
  onChangeCbx,
  setDrawerStatus,
}) => {
  const data = useMemo(
    () =>
      items.map(item => {
        return {
          ...item,
          key: item.id,
          address: `г. ${item.address.city || 'Алматы'}, ${
            item.address.street
          }, дом ${item.address.house}`,
          workingTime: item.workingTime
            .map(time => [time.day + ' ' + time.time])
            .join(' '),
        }
      }),
    [items.length],
  )

  const columns = useMemo(
    (): Array<ColumnType<PlaceType>> => [
      {
        title: <SettingsIcon color={theme.colorsList.secondary[1]} />,
        dataIndex: 'id',
        key: 'checkbox',
        width: 46,
        render: id => {
          const onChangeCbxCached = useCallback(() => {
            onChangeCbx(id)
          }, [id])
          return (
            <Box>
              <FormCheckbox
                id={id}
                name={checkboxName}
                value={id}
                checked={checkedList.some(itemId => itemId === id)}
                onChange={onChangeCbxCached}
              />
            </Box>
          )
        },
      },
      {
        title: 'Название',
        dataIndex: 'name',
        key: 'name',
        width: 150,
        render: name => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            <Truncate limit={52}>{name}</Truncate>
          </Text>
        ),
      },
      {
        title: 'Адрес филиала',
        dataIndex: 'address',
        key: 'address',
        width: 450,
        render: address => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {address}
          </Text>
        ),
      },
      {
        title: 'Время работы',
        dataIndex: 'workingTime',
        key: 'workingTime',
        width: 240,
        render: workingTime => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {workingTime}
          </Text>
        ),
      },
      {
        title: <StatusHeader status='Статус' />,
        dataIndex: 'status',
        key: 'status',
        width: 90,
        render: status => (
          <Text
            color={
              (status === 'ACTIVE' && '#1AB759') ||
              (status === 'INACTIVE' && 'secondary.1')
            }
            fontWeight='500'
            fontSize='12px'
            lineHeight={1}
          >
            {status === 'ACTIVE' && 'Активный'}
            {status === 'INACTIVE' && 'Деактивирован'}
          </Text>
        ),
      },
      {
        title: '',
        dataIndex: 'id',
        key: 'actions',
        width: 20,
        fixed: 'right',
        render: () => (
          <Box
            display='flex'
            alignItems='center'
            width='100%'
            height='100%'
            backgroundColor='white'
          >
            <Dropdown>
              <Dropdown.SimpleButton noCarret>
                <DotsIcon />
              </Dropdown.SimpleButton>
              <Dropdown.Menu direction='l'>
                <Dropdown.Item>{t('edit')}</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        ),
      },
    ],
    [checkedList.length, onChangeCbx],
  )

  return (
    <BaseTable
      id='place-table'
      data={data}
      columns={columns}
      onRow={(row: PlaceType) => ({
        onClick: () =>
          setDrawerStatus({
            open: true,
            id: row.id,
          }),
      })}
    />
  )
}

export default withTranslation(['shop', 'common'])(Table)
