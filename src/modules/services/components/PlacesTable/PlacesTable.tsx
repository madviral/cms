import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'

import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { Box } from 'ui/layout'

import { IPlacesData } from 'src/modules/services/types/interfaces'
import { Table } from 'src/modules/services/components/PlacesTable/components'
import { useTableCbx } from 'lib/hooks'

const checkboxName = 'service_place'

const PlacesTable: FC<IPlacesData> = ({
  places,
  deletePlace,
  activatePlace,
  deactivatePlace,
  setDrawerStatus,
}) => {
  const [t] = useTranslation('services')
  const { cbxState, onChangeCbx, onCheckAllCbxChange } = useTableCbx(
    places.list,
  )
  const disabledActions = cbxState.checkedList.length === 0

  return (
    <Box display='flex' flexDirection='column' height='100%'>
      <Box display='flex' alignItems='center' marginBottom='16px' pl='16px'>
        <Box flex='0 0 20px' mr='16px'>
          <FormCheckbox
            id='selectAllProducts'
            indeterminate={cbxState.indeterminate}
            name={checkboxName}
            value={checkboxName}
            checked={cbxState.checkAll}
            disabled={places.list.length === 0}
            onChange={onCheckAllCbxChange}
          />
        </Box>

        <Box width='200px' mr='32px'>
          <Dropdown width='100%' disabled={disabledActions}>
            <Dropdown.OutlinedButton disabled={disabledActions}>
              {t('shop:chooseAnAction')}
            </Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item
                onClick={() => activatePlace(cbxState.checkedList)}
              >
                Активировать
              </Dropdown.Item>
              <Dropdown.Item
                onClick={() => deactivatePlace(cbxState.checkedList)}
              >
                Деактивировать
              </Dropdown.Item>
              <Dropdown.Item onClick={() => deletePlace(cbxState.checkedList)}>
                Удалить
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Box>
      </Box>

      <Table
        checkboxName={checkboxName}
        checkedList={cbxState.checkedList}
        setDrawerStatus={setDrawerStatus}
        onChangeCbx={onChangeCbx}
        items={places.list}
      />
    </Box>
  )
}

export default PlacesTable
