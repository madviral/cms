import React, { FC, useCallback, useMemo } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'

import BaseTable, { ColumnType } from 'ui/table/controlled/Table'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { Text, Truncate } from 'ui/text'
import { DotsIcon, SettingsIcon } from 'ui/icons'
import { Box } from 'ui/layout'

import { CustomersTableProps } from 'src/modules/services/types/interfaces'
import { Image } from 'ui/image'
import { theme } from 'ui/theme'

const Table: FC<WithTranslation & CustomersTableProps> = ({
  checkedList,
  checkboxName,
  items,
  t,
  onChangeCbx,
  handleUpdate,
}) => {
  const data = useMemo(
    () =>
      items.map(item => ({
        ...item,
        key: item.id,
      })),
    [items.length],
  )

  const columns = useMemo(
    (): Array<ColumnType<any>> => [
      {
        title: <SettingsIcon color={theme.colorsList.secondary[1]} />,
        dataIndex: 'id',
        key: 'checkbox',
        width: 46,
        render: id => {
          const onChangeCbxCached = useCallback(() => {
            onChangeCbx(id)
          }, [id])
          return (
            <Box>
              <FormCheckbox
                id={id}
                name={checkboxName}
                value={id}
                checked={checkedList.some(itemId => itemId === id)}
                onChange={onChangeCbxCached}
              />
            </Box>
          )
        },
      },
      {
        title: 'Название',
        dataIndex: 'name',
        key: 'name',
        width: 210,
        render: (name, row) => (
          <Box display='flex' alignItems='center'>
            <Box pr='8px'>
              <Image
                src={'https://i.ibb.co/JRL5hSP/40px.png'}
                borderRadius={'r'}
                alt={name}
                width={32}
                height={32}
              />
            </Box>

            <Box flex='1' pr='16px'>
              <Truncate limit={53}>{name}</Truncate>
            </Box>

            {row.isVlifeUser && (
              <Box pr='16px'>
                <Image
                  borderRadius={'xxs'}
                  src={'https://i.ibb.co/LdP1bTN/Layer-2.png'}
                  alt={name}
                  width={19}
                  height={19}
                  isBg
                />
              </Box>
            )}
          </Box>
        ),
      },
      {
        title: 'Телефон',
        dataIndex: 'phoneNumber',
        key: 'phoneNumber',
        width: 240,
        render: phoneNumber => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {phoneNumber}
          </Text>
        ),
      },
      {
        title: 'Продано услуг',
        dataIndex: 'appointmentCount',
        key: 'appointmentCount',
        width: 130,
        render: appointmentCount => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {appointmentCount}
          </Text>
        ),
      },
      {
        title: 'Скидка',
        dataIndex: 'discount',
        key: 'discount',
        width: 170,
        render: discount => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {discount ? `${discount} %` : ''}
          </Text>
        ),
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        width: 180,
        render: email => (
          <Text
            color='secondary.0'
            fontSize='12px'
            lineHeight={1}
            fontWeight='500'
          >
            {email}
          </Text>
        ),
      },
      {
        title: '',
        dataIndex: 'id',
        key: 'actions',
        width: 20,
        fixed: 'right',
        render: () => (
          <Box
            display='flex'
            alignItems='center'
            width='100%'
            height='100%'
            backgroundColor='white'
          >
            <Dropdown>
              <Dropdown.SimpleButton noCarret>
                <DotsIcon />
              </Dropdown.SimpleButton>
              <Dropdown.Menu direction='l'>
                <Dropdown.Item>{t('edit')}</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        ),
      },
    ],
    [checkedList.length, onChangeCbx],
  )

  return (
    <BaseTable
      data={data}
      columns={columns}
      onRow={(row: any) => ({
        onClick: () => handleUpdate(row.id),
      })}
    />
  )
}

export default withTranslation(['shop', 'common'])(Table)
