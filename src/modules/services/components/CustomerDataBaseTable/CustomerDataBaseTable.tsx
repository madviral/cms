import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { useTableCbx } from 'lib/hooks'

import { Table } from 'src/modules/services/components/CustomerDataBaseTable/components'
import { Box } from 'ui/layout'

import { CustomerDataBaseTableProps } from 'src/modules/services/types/interfaces'

import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { Text } from 'ui'
import { Pagination } from 'src/modules/common/components/Pagination'

const checkboxName = 'service_customers'

const CustomerDataBaseTable: FC<CustomerDataBaseTableProps> = ({
  data,
  places,
  pageCount,
  pageIndex,
  pageSize,
  onChangePageSize,
  onChangePageIndex,
  handleUpdate,
}) => {
  const [t] = useTranslation('services')
  const { cbxState, onChangeCbx, onCheckAllCbxChange } = useTableCbx(data)
  const disabledActions = cbxState.checkedList.length === 0

  return (
    <Box display='flex' flexDirection='column' height='100%'>
      <Box display='flex' alignItems='center' marginBottom='16px' pl='16px'>
        <Box flex='0 0 20px' mr='16px'>
          <FormCheckbox
            id='selectAllProducts'
            indeterminate={cbxState.indeterminate}
            name={checkboxName}
            value={checkboxName}
            checked={cbxState.checkAll}
            disabled={data.length === 0}
            onChange={onCheckAllCbxChange}
          />
        </Box>

        <Box width='200px' mr='32px'>
          <Dropdown width='100%' disabled={disabledActions}>
            <Dropdown.OutlinedButton disabled={disabledActions}>
              {t('shop:chooseAnAction')}
            </Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item>Удалить</Dropdown.Item>
              <Dropdown.Item>Архивировать</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Box>
        <Box mr='8px'>
          <Text fontSize='14px' color='secondary.0'>
            {t('filter')}:
          </Text>
        </Box>
        <Box width='200px'>
          <Dropdown width='100%' disabled={places.length === 0}>
            <Dropdown.OutlinedButton disabled={places.length === 0}>
              Все филиалы
            </Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item>Все филиалы</Dropdown.Item>
              {places.map((place, idx) => (
                <Dropdown.Item key={idx}>{place.name}</Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </Box>

        <Box ml='auto'>
          <Pagination
            pageIndex={pageIndex}
            defaultPageSize={pageSize}
            setPageSize={onChangePageSize}
            goToPage={onChangePageIndex}
            pageCount={pageCount}
          />
        </Box>
      </Box>

      <Table
        items={data}
        checkboxName={checkboxName}
        checkedList={cbxState.checkedList}
        handleUpdate={handleUpdate}
        onChangeCbx={onChangeCbx}
      />
    </Box>
  )
}

export default CustomerDataBaseTable
