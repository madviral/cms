import React, { FC } from 'react'

import { JobPositionCreate } from 'src/modules/services/components'
import { ServicesDrawerProps } from 'src/modules/services/types/interfaces'

import { Drawer } from 'ui/drawer'

interface JobPositionDrawerProps {
  id: number
}

const JobPositionDrawer: FC<ServicesDrawerProps & JobPositionDrawerProps> = ({
  isOpen,
  onClose,
  id,
}) => (
  <Drawer
    width='632px'
    handler={false}
    open={isOpen}
    level={null}
    onClose={onClose}
    className='drawer1'
    placement='right'
  >
    <JobPositionCreate id={id} onClose={onClose} />
  </Drawer>
)

export default JobPositionDrawer
