import React, { FC, useEffect } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { RootState } from 'src/store/rootReducer'

import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Button, FormField, FormTextArea, Text } from 'ui'

import {
  change,
  createJobPosition,
  updateJobPosition,
  loadJobPositionDetails,
} from 'src/modules/services/actions/jobPositions'

type BodyType = {
  name: string
  description?: string
}

interface JobPositionCreateProps {
  id?: number
  name: string
  description: string
  loadingOfDetails: boolean
  onClose: VoidFunction
  onChange: (event: any) => void
  onLoadJobPositionDetails: (id: number) => void
  onCreateJobPosition: (body: BodyType, closeCb: VoidFunction) => void
  onUpdateJobPosition: (
    body: BodyType,
    id: number,
    closeCb: VoidFunction,
  ) => void
}

const JobPositionCreate: FC<JobPositionCreateProps> = ({
  onClose,
  id,
  onCreateJobPosition,
  onUpdateJobPosition,
  onLoadJobPositionDetails,
  loadingOfDetails,
  onChange,
  name,
  description,
}) => {
  const [t] = useTranslation()

  useEffect(() => {
    if (id) {
      onLoadJobPositionDetails(id)
    }
  }, [id])

  return (
    <Box
      px='16px'
      display='flex'
      flexDirection='column'
      height='100%'
      backgroundColor='#fff'
    >
      <Box flex='1 1 auto' pt='32px' pb='20px' overflowY='auto'>
        <Box
          mb='16px'
          pb='16px'
          borderBottom={`1px solid ${theme.colorsList.secondary[2]}`}
        >
          <Text
            fontSize='14px'
            lineHeight='18px'
            fontWeight='600'
            color='secondary.0'
          >
            {t('services:addJobPosition')}
          </Text>
        </Box>

        <Box>
          <Box mb='16px'>
            <FormField
              id='name'
              name='name'
              type='text'
              placeholder='Введите должность'
              label='Название *'
              value={name}
              onChange={onChange}
              disabled={loadingOfDetails}
              error={'Введите должность'}
            />
          </Box>

          <Box>
            <FormTextArea
              name='description'
              height='160px'
              label='Описание'
              placeholder='Специалист в области создания стиля человека с помощью прически и парика'
              value={description}
              disabled={loadingOfDetails}
              onChange={onChange}
            />
          </Box>
        </Box>
      </Box>

      <Box
        display='flex'
        alignItems='center'
        px='10px'
        py='18px'
        justifyContent='flex-end'
        borderTop={`1px solid ${theme.colorsList.secondary[2]}`}
      >
        <Box mr='16px'>
          <Button onClick={onClose} appearance='secondary' size='m'>
            {t('common:cancel')}
          </Button>
        </Box>
        <Box>
          <Button
            appearance='primary'
            disabled={loadingOfDetails || name.length === 0}
            size='m'
            onClick={() => {
              if (id) {
                onUpdateJobPosition({ name, description }, id, onClose)
              } else {
                onCreateJobPosition({ name, description }, onClose)
              }
            }}
          >
            {t('common:save')}
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

const mapStateToProps = (state: RootState) => ({
  name: state.services.jobPositions.name,
  description: state.services.jobPositions.description,
  loadingOfDetails: state.services.jobPositions.loadingOfDetails,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onChange: event => dispatch(change(event.target.name, event.target.value)),
  onLoadJobPositionDetails: id => dispatch(loadJobPositionDetails(id)),
  onCreateJobPosition: (body, closeCb) =>
    dispatch(createJobPosition(body, closeCb)),
  onUpdateJobPosition: (body, id, closeCb) =>
    dispatch(updateJobPosition(body, id, closeCb)),
})

export default connect(mapStateToProps, mapDispatchToProps)(JobPositionCreate)
