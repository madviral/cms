import { createReducer } from 'lib/utils'

import * as actions from 'src/modules/services/constants/categories'

const initialState = {
  list: [],
  loadingOfList: false,
  errorOfList: null,

  typeList: [],
  loadingOfTypeList: false,
  errorOfTypeList: null,
}

export default createReducer(initialState, {
  [actions.LOAD_CATEGORIES_REQUEST]: state => ({
    ...state,
    loadingOfList: true,
    errorOfList: null,
  }),
  [actions.LOAD_CATEGORIES_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    list: payload.list,
    loadingOfList: false,
    errorOfList: null,
  }),
  [actions.LOAD_CATEGORIES_FAILURE]: (state, { error }: any) => ({
    ...state,
    list: [],
    loadingOfList: false,
    errorOfList: error,
  }),
  [actions.LOAD_CATEGORY_TYPES_REQUEST]: state => ({
    ...state,
    loadingOfTypeList: true,
    errorOfTypeList: null,
  }),
  [actions.LOAD_CATEGORY_TYPES_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    typeList: payload.list,
    loadingOfTypeList: false,
    errorOfTypeList: null,
  }),
  [actions.LOAD_CATEGORY_TYPES_FAILURE]: (state, { error }: any) => ({
    ...state,
    typeList: [],
    loadingOfTypeList: false,
    errorOfTypeList: error,
  }),
})
