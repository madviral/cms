import { createReducer } from 'lib/utils'

import * as actions from 'src/modules/services/constants/customerDetail'

const initialState = {
  sexValue: null,
  categoryValue: null,
  placeValue: null,
  firstName: '',
  lastName: '',
  phone: '',
  email: '',
  discount: '',
  comment: '',
  dateOfBirth: '',
}

export default createReducer(initialState, {
  [actions.CLEAR]: () => initialState,
  [actions.CHANGE]: (state, { payload }: any) => ({
    ...state,
    [payload.field]: payload.value,
  }),
})
