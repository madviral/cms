import { createReducer } from 'lib/utils'

import * as actions from 'src/modules/services/constants/specialists'

import { SpecialistsState } from 'src/modules/services/types/interfaces'

const initialState: SpecialistsState = {
  list: [],
  loadingOfList: false,
  errorOfList: null,
}

export default createReducer(initialState, {
  [actions.LOAD_SPECIALISTS_REQUEST]: state => ({
    ...state,
    loadingOfList: true,
    errorOfList: null,
  }),
  [actions.LOAD_SPECIALISTS_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    list: payload.list,
    loadingOfList: false,
    errorOfList: null,
  }),
  [actions.LOAD_SPECIALISTS_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfList: false,
    errorOfList: error,
  }),
})
