import { createReducer } from 'lib/utils'
import { PlacesState } from 'src/modules/services/types/interfaces'

import * as actions from 'src/modules/services/constants/places'

const initialState: PlacesState = {
  activePlace: {
    id: null,
    name: '',
  },
  places: {
    list: [],
  },

  placeDetail: {},
  loadingOfPLaceDetail: false,
  errorOfPlaceDetail: null,

  loadingOfPlaces: false,
  errorOfPlaces: null,

  loadingOfCreating: false,
  errorOfCreating: null,

  loadingOfActivating: false,
  errorOfActivating: null,

  loadingOfDeactivating: false,
  errorOfDeactivating: null,

  loadingOfDeleting: false,
  errorOfDeleting: null,
}

export default createReducer(initialState, {
  [actions.CHANGE_PLACE]: (state, { payload }: any) => ({
    ...state,
    activePlace: {
      id: payload.id,
      name: payload.name,
    },
  }),
  [actions.LOAD_PLACES_REQUEST]: state => ({
    ...state,
    loadingOfPLaces: true,
    errorOfPlaces: null,
  }),
  [actions.LOAD_PLACES_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    places: payload,
    loadingOfPLaces: false,
    errorOfPlaces: null,
  }),
  [actions.LOAD_PLACES_FAILURE]: (state, { error }: any) => ({
    ...state,
    places: {
      list: [],
    },
    loadingOfPLaces: false,
    errorOfPlaces: error,
  }),
  [actions.LOAD_PLACE_DETAILS_REQUEST]: state => ({
    ...state,
    loadingOfPLaceDetail: true,
    errorOfPlaceDetail: null,
  }),
  [actions.LOAD_PLACE_DETAILS_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    placeDetail: payload,
    loadingOfPLaceDetail: false,
    errorOfPlaceDetail: null,
  }),
  [actions.LOAD_PLACE_DETAILS_FAILURE]: (state, { error }: any) => ({
    ...state,
    placeDetail: {},
    loadingOfPLaceDetail: false,
    errorOfPlaceDetail: error,
  }),
  [actions.CREATE_PLACES_REQUEST]: state => ({
    ...state,
    loadingOfCreating: true,
    errorOfCreating: null,
  }),
  [actions.CREATE_PLACES_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    places: {
      list: payload ? [] : state.places.list,
    },
    loadingOfCreating: false,
    errorOfCreating: null,
  }),
  [actions.CREATE_PLACES_FAILURE]: (state, { error }: any) => ({
    ...state,
    places: {
      list: [],
    },
    loadingOfCreating: false,
    errorOfCreating: error,
  }),
  [actions.ACTIVATE_PLACE_REQUEST]: state => ({
    ...state,
    loadingOfActivating: true,
    errorOfActivating: null,
  }),
  [actions.ACTIVATE_PLACE_SUCCESS]: state => ({
    ...state,
    places: {
      list: [],
    },
    loadingOfActivating: false,
    errorOfActivating: null,
  }),
  [actions.ACTIVATE_PLACE_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfActivating: false,
    errorOfActivating: error,
  }),
  [actions.DEACTIVATE_PLACE_REQUEST]: state => ({
    ...state,
    loadingOfDeactivating: true,
    errorOfDeactivating: null,
  }),
  [actions.DEACTIVATE_PLACE_SUCCESS]: state => ({
    ...state,
    places: {
      list: [],
    },
    loadingOfDeactivating: false,
    errorOfDeactivating: null,
  }),
  [actions.DEACTIVATE_PLACE_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfDeactivating: false,
    errorOfDeactivating: error,
  }),
  [actions.DELETE_PLACES_REQUEST]: state => ({
    ...state,
    loadingOfDeleting: true,
    errorOfDeleting: null,
  }),
  [actions.DELETE_PLACES_SUCCESS]: state => ({
    ...state,
    places: {
      list: [],
    },
    loadingOfDeleting: false,
    errorOfDeleting: null,
  }),
  [actions.DELETE_PLACES_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfDeleting: false,
    errorOfDeleting: error,
  }),
})
