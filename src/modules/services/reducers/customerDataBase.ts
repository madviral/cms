import { createReducer } from 'lib/utils'
import { CustomersState } from 'src/modules/services/types/interfaces'
import { pageSizes } from 'src/modules/common/components/Pagination/Pagination'

import * as actions from 'src/modules/services/constants/customerDataBase'

const initialState: CustomersState = {
  customerDetail: {},
  page: {
    size: pageSizes[0],
    index: 1,
    count: 1,
  },
  list: [],

  loadingOfCustomers: false,
  errorOfCustomers: null,

  loadingOfCreating: false,
  errorOfCreating: null,

  loadingOfDeleting: false,
  errorOfDeleting: null,

  loadingOfSearching: false,
  errorOfSearching: null,

  loadingOfDetail: false,
  errorOfDetail: null,

  loadingOfUpdating: false,
  errorOfUpdating: null,
}

export default createReducer(initialState, {
  [actions.CHANGE_PAGINATION]: (state, { payload }: any) => {
    const { field, value } = payload

    return {
      ...state,
      page: {
        ...state.page,
        [field]: value,
      },
    }
  },
  [actions.LOAD_CUSTOMERS_REQUEST]: state => ({
    ...state,
    loadingOfCustomers: true,
    errorOfCustomers: null,
  }),
  [actions.LOAD_CUSTOMERS_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    list: payload.list,
    page: {
      ...state.page,
      count: payload.totalPages,
    },
    loadingOfCustomers: false,
    errorOfCustomers: null,
  }),
  [actions.LOAD_CUSTOMERS_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfCustomers: false,
    errorOfCustomers: error,
  }),
  [actions.CREATE_CUSTOMER_REQUEST]: state => ({
    ...state,
    loadingOfCreating: true,
    errorOfCreating: null,
  }),
  [actions.CREATE_CUSTOMER_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    list: [],
    loadingOfCreating: false,
    errorOfCreating: null,
  }),
  [actions.CREATE_CUSTOMER_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfCreating: false,
    errorOfCreating: error,
  }),
  [actions.DELETE_CUSTOMER_REQUEST]: state => ({
    ...state,
    loadingOfDeleting: true,
    errorOfDeleting: null,
  }),
  [actions.DELETE_CUSTOMER_SUCCESS]: state => ({
    ...state,
    loadingOfDeleting: false,
    errorOfDeleting: null,
  }),
  [actions.DELETE_CUSTOMER_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfDeleting: false,
    errorOfDeleting: error,
  }),
  [actions.SEARCH_CUSTOMER_REQUEST]: state => ({
    ...state,
    loadingOfSearching: true,
    errorOfSearching: null,
  }),
  [actions.SEARCH_CUSTOMER_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    // customers: {
    //   list: payload,
    //   page: state.customers.page,
    //   pagesize: state.customers.pagesize,
    //   sort: state.customers.sort,
    // },
    loadingOfSearching: false,
    errorOfSearching: null,
  }),
  [actions.SEARCH_CUSTOMER_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfSearching: false,
    errorOfSearching: error,
  }),
  [actions.GET_CUSTOMER_DETAIL_REQUEST]: state => ({
    ...state,
    customerDetail: {},
    loadingOfDetail: true,
    errorOfDetail: null,
  }),
  [actions.GET_CUSTOMER_DETAIL_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    customerDetail: payload,
    loadingOfDetail: false,
    errorOfDetail: null,
  }),
  [actions.GET_CUSTOMER_DETAIL_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfDetail: false,
    errorOfDetail: error,
  }),
  [actions.UPDATE_CUSTOMER_DETAIL_REQUEST]: state => ({
    ...state,
    loadingOfUpdating: true,
    errorOfUpdating: null,
  }),
  [actions.UPDATE_CUSTOMER_DETAIL_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    customerDetail: payload,
    loadingOfUpdating: false,
    errorOfUpdating: null,
  }),
  [actions.UPDATE_CUSTOMER_DETAIL_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfUpdating: false,
    errorOfUpdating: error,
  }),
})
