import { createReducer } from 'lib/utils'
import { ServicesState } from 'src/modules/services/types/interfaces'

import * as actions from 'src/modules/services/constants/services'
import { pageSizes } from 'src/modules/common/components/Pagination/Pagination'

const initialState: ServicesState = {
  list: [],
  page: {
    size: pageSizes[0],
    index: 1,
    count: 1,
  },
  loadingOfList: false,
  errorOfList: null,
}

export default createReducer(initialState, {
  [actions.LOAD_SERVICE_REQUEST]: state => ({
    ...state,
    loadingOfList: true,
    errorOfList: null,
  }),
  [actions.LOAD_SERVICE_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    list: payload.list,
    loadingOfList: false,
    errorOfList: null,
  }),
  [actions.LOAD_SERVICE_SUCCESS]: (state, { error }: any) => ({
    ...state,
    list: [],
    loadingOfList: false,
    errorOfList: error,
  }),
})
