import { createReducer } from 'lib/utils'

import * as actions from 'src/modules/services/constants/placeDetail'

const initialState = {
  city: null,
  cityId: null,
  street: '',
  house: '',
  flat: '',
  office: '',
  placeName: '',
  placePhone: '',
  webSiteUrl: '',
  INSTAGRAM: '',
  FACEBOOK: '',
  WHATSAPP: '',
  VK: '',
  TIKTOK: '',
  businessAreaValue: null,
  businessTypeValue: null,

  mapState: {
    center: [43.238293, 76.945465],
    zoom: 10,
    controls: [],
  },
}

export default createReducer(initialState, {
  [actions.CLEAR]: () => initialState,
  [actions.CHANGE]: (state, { payload }: any) => ({
    ...state,
    [payload.field]: payload.value,
  }),
  [actions.SET_DETAIL_DATA]: (state, { payload }: any) => payload,
  [actions.CHANGE_CITY]: (state, { payload }: any) => {
    const { map } = payload

    return {
      ...state,
      mapState: {
        ...state.mapState,
        center: map ? [map.latitude, map.longitude] : state.mapState.center,
        zoom: 10,
      },
      city: payload,
      cityId: payload.id,
    }
  },
  [actions.SUGGEST]: (state, { payload }: any) => {
    const address = payload.address.reduce(
      (prev, item) => ({
        ...prev,
        [item.kind]: item.name,
      }),
      {},
    )

    return {
      ...state,
      mapState: {
        ...state.mapState,
        center: payload.coordinates,
        zoom: 17,
      },
      street: address.street || address.district || '',
      house: address.house || '',
    }
  },
})
