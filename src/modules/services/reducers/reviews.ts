import { createReducer } from 'lib/utils'

import { pageSizes } from 'src/modules/common/components/Pagination/Pagination'
import { ReviewsState } from 'src/modules/services/types/interfaces'

import * as actions from 'src/modules/services/constants/reviews'

const initialState: ReviewsState = {
  list: [],
  rating: 0,
  loadingOfList: false,
  errorOfList: null,
  page: {
    size: pageSizes[0],
    index: 1,
    count: 1,
  },
}

export default createReducer(initialState, {
  [actions.LOAD_REVIEWS_REQUEST]: state => ({
    ...state,
    loadingOfList: true,
    errorOfList: null,
  }),
  [actions.LOAD_REVIEWS_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    page: {
      ...state.page,
      count: payload.totalPages,
    },
    list: payload.reviews,
    rating: payload.rating,
    loadingOfList: false,
    errorOfList: null,
  }),
  [actions.LOAD_REVIEWS_FAILURE]: (state, { error }: any) => ({
    ...state,
    list: [],
    rating: 0,
    loadingOfList: false,
    errorOfList: error,
  }),
  [actions.CHANGE_PAGINATION]: (state, { payload }: any) => {
    const { field, value } = payload

    return {
      ...state,
      page: {
        ...state.page,
        [field]: value,
      },
    }
  },
})
