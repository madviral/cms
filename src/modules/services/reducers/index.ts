import { combineReducers } from 'redux'

import customers from 'src/modules/services/reducers/customerDataBase'
import jobPositions from 'src/modules/services/reducers/jobPositions'
import specialists from 'src/modules/services/reducers/specialists'
import placeDetail from 'src/modules/services/reducers/placeDetail'
import categories from 'src/modules/services/reducers/categories'
import servicesPage from 'src/modules/services/reducers/services'
import reviews from 'src/modules/services/reducers/reviews'
import places from 'src/modules/services/reducers/places'

export default combineReducers({
  customers,
  jobPositions,
  categories,
  specialists,
  placeDetail,
  servicesPage,
  reviews,
  places,
})
