import { createReducer } from 'lib/utils'

import { pageSizes } from 'src/modules/common/components/Pagination/Pagination'
import { JobPositionsState } from 'src/modules/services/types/interfaces'

import * as actions from 'src/modules/services/constants/jobPositions'

const initialState: JobPositionsState = {
  page: {
    size: pageSizes[0],
    index: 1,
    count: 1,
  },
  list: [],

  name: '',
  description: '',

  loadingOfDetails: false,
  errorOfDetails: null,

  loadingOfList: false,
  errorOfList: null,

  loadingOfCreating: false,
  errorOfCreating: null,

  loadingOfUpdating: false,
  errorOfUpdating: null,
}

export default createReducer(initialState, {
  [actions.CLEAR]: state => ({
    ...state,
    name: '',
    description: '',
  }),
  [actions.CHANGE]: (state, { payload }: any) => ({
    ...state,
    [payload.field]: payload.value,
  }),
  [actions.CHANGE_PAGINATION]: (state, { payload }: any) => {
    const { field, value } = payload

    return {
      ...state,
      page: {
        ...state.page,
        [field]: value,
      },
    }
  },
  [actions.LOAD_JOB_POSITIONS_REQUEST]: state => ({
    ...state,
    loadingOfList: true,
    errorOfList: null,
  }),
  [actions.LOAD_JOB_POSITIONS_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    page: {
      ...state.page,
      count: payload.totalPages,
    },
    list: payload.list,
    loadingOfList: false,
    errorOfList: null,
  }),
  [actions.LOAD_JOB_POSITIONS_FAILURE]: (state, { error }: any) => ({
    ...state,
    list: [],
    loadingOfList: false,
    errorOfList: error,
  }),
  [actions.LOAD_JOB_POSITION_DETAILS_REQUEST]: state => ({
    ...state,
    loadingOfDetails: true,
    errorOfDetails: null,
  }),
  [actions.LOAD_JOB_POSITION_DETAILS_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    name: payload.name,
    description: payload.description,
    loadingOfDetails: false,
    errorOfDetails: null,
  }),
  [actions.LOAD_JOB_POSITION_DETAILS_FAILURE]: (state, { error }: any) => ({
    ...state,
    details: {},
    loadingOfDetails: false,
    errorOfDetails: error,
  }),
  [actions.CREATE_JOB_POSITION_REQUEST]: state => ({
    ...state,
    loadingOfCreating: true,
    errorOfCreating: null,
  }),
  [actions.CREATE_JOB_POSITION_SUCCESS]: (state, { payload }: any) => ({
    ...state,
    list: [...state.list, payload],
    loadingOfCreating: false,
    errorOfCreating: null,
  }),
  [actions.CREATE_JOB_POSITION_FAILURE]: (state, { error }) => ({
    ...state,
    list: [],
    loadingOfCreating: false,
    errorOfCreating: error,
  }),
  [actions.UPDATE_JOB_POSITION_REQUEST]: state => ({
    ...state,
    loadingOfUpdating: true,
    errorOfUpdating: null,
  }),
  [actions.UPDATE_JOB_POSITION_SUCCESS]: state => ({
    ...state,
    list: [],
    loadingOfUpdating: false,
    errorOfUpdating: null,
  }),
  [actions.UPDATE_JOB_POSITION_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfUpdating: false,
    errorOfUpdating: error,
  }),
  [actions.DELETE_JOB_POSITION_REQUEST]: state => ({
    ...state,
    loadingOfUpdating: true,
    errorOfUpdating: null,
  }),
  [actions.DELETE_JOB_POSITION_SUCCESS]: state => ({
    ...state,
    list: [],
    loadingOfUpdating: false,
    errorOfUpdating: null,
  }),
  [actions.DELETE_JOB_POSITION_FAILURE]: (state, { error }: any) => ({
    ...state,
    loadingOfUpdating: false,
    errorOfUpdating: error,
  }),
})
