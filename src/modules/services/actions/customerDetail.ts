import { ActionCreator } from 'redux'
import { IChangeAction } from 'src/modules/services/types'

import * as actions from 'src/modules/services/constants/placeDetail'

export const change: ActionCreator<any> = (
  field: string,
  value: string,
): IChangeAction<typeof actions.CHANGE> => ({
  type: actions.CHANGE,
  payload: {
    field,
    value,
  },
})

export const clear: ActionCreator<any> = () => ({
  type: actions.CLEAR,
})
