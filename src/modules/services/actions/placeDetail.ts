import { ActionCreator } from 'redux'

import { IChangeAction } from 'src/modules/services/types'
import * as actions from 'src/modules/services/constants/placeDetail'

export const change: ActionCreator<any> = (
  field: string,
  value: string,
): IChangeAction<typeof actions.CHANGE> => ({
  type: actions.CHANGE,
  payload: {
    field,
    value,
  },
})

export const setDetailData: ActionCreator<any> = data => ({
  type: actions.SET_DETAIL_DATA,
  payload: data,
})

export const changeCity: ActionCreator<any> = value => ({
  type: actions.CHANGE_CITY,
  payload: value,
})

export const suggest: ActionCreator<any> = value => ({
  type: actions.SUGGEST,
  payload: value,
})

export const clear: ActionCreator<any> = () => ({
  type: actions.CLEAR,
})
