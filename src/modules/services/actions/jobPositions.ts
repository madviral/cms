import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'

import * as actions from 'src/modules/services/constants/jobPositions'

export const loadJobPositions: ActionCreator<any> = (): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_JOB_POSITIONS_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_JOB_POSITIONS_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_JOB_POSITIONS_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    const { index, size } = getState().services.jobPositions.page
    const { activePlace } = getState().services.places

    try {
      const { data, error } = await client.get('/service/core/v1/positions', {
        params: {
          placeId: activePlace.id,
          page: index > 1 ? index - 1 : 0,
          pageSize: size,
        },
      })

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const loadJobPositionDetails: ActionCreator<any> = (id): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_JOB_POSITION_DETAILS_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_JOB_POSITION_DETAILS_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_JOB_POSITION_DETAILS_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.get(
        `/service/core/v1/positions/${id}`,
      )

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const createJobPosition: ActionCreator<any> = (
  body,
  closeCb,
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.CREATE_JOB_POSITION_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.CREATE_JOB_POSITION_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.CREATE_JOB_POSITION_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    const { activePlace } = getState().services.places

    try {
      const { data, error } = await client.post('/service/core/v1/positions', {
        placeId: activePlace.id,
        description: body.description,
        name: body.name,
      })

      if (!error) {
        closeCb()
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const updateJobPosition: ActionCreator<any> = (body, id, closeCb) => {
  const request: ActionCreator<any> = () => ({
    type: [actions.UPDATE_JOB_POSITION_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.UPDATE_JOB_POSITION_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.UPDATE_JOB_POSITION_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())
    closeCb()

    const { activePlace } = getState().services.places

    try {
      const { data, error } = await client.put(
        `/service/core/v1/positions/${id}`,
        {
          placeId: activePlace.id,
          description: body.description,
          name: body.name,
        },
      )

      if (!error) {
        dispatch(success(data))
        dispatch(loadJobPositions())
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const deleteJobPosition: ActionCreator<any> = (
  ids: number[],
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.DELETE_JOB_POSITION_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.DELETE_JOB_POSITION_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.DELETE_JOB_POSITION_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.delete(
        `/service/core/v1/positions?ids=${ids.join(',')}`,
      )

      if (!error) {
        dispatch(success(data))
        dispatch(loadJobPositions())
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const changePagination: ActionCreator<any> = (
  field,
  value,
): AppThunk => dispatch => {
  dispatch({
    type: actions.CHANGE_PAGINATION,
    payload: {
      field,
      value,
    },
  })

  dispatch(loadJobPositions())
}

export const clear: ActionCreator<any> = () => ({
  type: actions.CLEAR,
})

export const change: ActionCreator<any> = (field, value) => ({
  type: actions.CHANGE,
  payload: {
    field,
    value,
  },
})
