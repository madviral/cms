import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'
import {
  DeleteCustomersParamsType,
  SearchCustomersParamsType,
  GetCustomerDetailType,
  CreateCustomerBodyType,
  UpdateCustomerDetailType,
} from 'src/modules/services/types'

import * as actions from 'src/modules/services/constants/customerDataBase'

export const loadCustomers: ActionCreator<any> = (): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_CUSTOMERS_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_CUSTOMERS_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_CUSTOMERS_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    const { id } = getState().common.user
    const { index, size } = getState().services.customers.page
    const { activePlace } = getState().services.places

    dispatch(request())

    try {
      const { data, error } = await client.get('/service/core/v1/customers', {
        params: {
          merchantId: id,
          placeId: activePlace.id,
          page: index > 1 ? index - 1 : 0,
          pageSize: size,
        },
      })

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const changePagination: ActionCreator<any> = (
  field,
  value,
): AppThunk => dispatch => {
  dispatch({
    type: actions.CHANGE_PAGINATION,
    payload: {
      field,
      value,
    },
  })

  dispatch(loadCustomers())
}

export const createCustomer: ActionCreator<any> = (
  body: CreateCustomerBodyType,
  onClearCb: VoidFunction,
  onClose: VoidFunction,
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.CREATE_CUSTOMER_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.CREATE_CUSTOMER_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.CREATE_CUSTOMER_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    const { id } = getState().common.user

    dispatch(request())
    try {
      const { data, error } = await client.post('/service/core/v1/customers', {
        ...body,
        merchantId: id,
      })

      if (!error) {
        dispatch(success(data))
        dispatch(loadCustomers())
        onClearCb()
        onClose()
      }
    } catch (err) {
      dispatch(failure(err.message))
    }
  }
}

export const deleteCustomers: ActionCreator<any> = (
  params: DeleteCustomersParamsType,
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.DELETE_CUSTOMER_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.DELETE_CUSTOMER_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.DELETE_CUSTOMER_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { error } = await client.delete(
        `/service/core/v1/customers?merchantId=${
          params.merchantId
        }&ids=${params.ids.join(',')}`,
      )

      if (!error) {
        dispatch(success(params.ids))
        dispatch(
          loadCustomers({
            merchantId: 4,
          }),
        )
      }
    } catch (err) {
      dispatch(failure(err.message))
    }
  }
}

export const searchCustomers: ActionCreator<any> = (
  params: SearchCustomersParamsType,
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.SEARCH_CUSTOMER_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.SEARCH_CUSTOMER_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.SEARCH_CUSTOMER_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const {
        data,
        error,
      } = await client.get('/service/core/v1/customers/search', { params })

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err.message))
    }
  }
}

export const getCustomerDetail: ActionCreator<any> = (
  params: GetCustomerDetailType,
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.GET_CUSTOMER_DETAIL_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.GET_CUSTOMER_DETAIL_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.GET_CUSTOMER_DETAIL_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.get(
        `/service/core/v1/customers/${params.id}`,
      )

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err.message))
    }
  }
}

export const updateCustomerDetail: ActionCreator<any> = (
  params: UpdateCustomerDetailType,
  closeCb: VoidFunction,
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.UPDATE_CUSTOMER_DETAIL_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.UPDATE_CUSTOMER_DETAIL_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.UPDATE_CUSTOMER_DETAIL_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.put(
        `/service/core/v1/customers/${params.id}`,
        params.body,
      )

      if (!error) {
        dispatch(success(data))
        dispatch(loadCustomers())
        closeCb()
      }
    } catch (err) {
      dispatch(failure(err.message))
    }
  }
}
