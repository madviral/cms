import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'

import * as actions from 'src/modules/services/constants/services'
import { loadCustomers } from './customerDataBase'

export const loadServices: ActionCreator<any> = (): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_SERVICE_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_SERVICE_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_SERVICE_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.get(
        '/service/catalog/v1/service-items/all',
      )

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const changePagination: ActionCreator<any> = (
  field,
  value,
): AppThunk => dispatch => {
  dispatch({
    type: actions.CHANGE_PAGINATION,
    payload: {
      field,
      value,
    },
  })

  dispatch(loadCustomers())
}
