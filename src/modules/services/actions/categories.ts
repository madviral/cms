import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'

import * as actions from 'src/modules/services/constants/categories'

export const loadCategories: ActionCreator<any> = (): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_CATEGORIES_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_CATEGORIES_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_CATEGORIES_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.get('/service/catalog/v1/categories')

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const loadCategoryTypes: ActionCreator<any> = (catId): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_CATEGORY_TYPES_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_CATEGORY_TYPES_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_CATEGORY_TYPES_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.get(
        `/service/catalog/v1/categories/type?catId=${catId}`,
      )

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}
