import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'

import * as actions from 'src/modules/services/constants/reviews'

export const loadReviews: ActionCreator<any> = (): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_REVIEWS_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_REVIEWS_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_REVIEWS_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    const { index, size } = getState().services.reviews.page
    const { activePlace } = getState().services.places

    dispatch(request())

    try {
      const { data, error } = await client.get(
        '/review/review-backend/v1/reviews-cms',
        {
          params: {
            itemId: activePlace.id,
            itemType: 2,
            page: index > 1 ? index - 1 : 0,
            pageSize: size,
            dateFrom: '',
            dateTo: '',
            rating: '',
            sort: '',
          },
        },
      )

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const changePagination: ActionCreator<any> = (
  field,
  value,
): AppThunk => dispatch => {
  dispatch({
    type: actions.CHANGE_PAGINATION,
    payload: {
      field,
      value,
    },
  })

  dispatch(loadReviews())
}
