import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'

import * as actions from 'src/modules/services/constants/specialists'

export const loadSpecialists: ActionCreator<any> = (): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_SPECIALISTS_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_SPECIALISTS_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_SPECIALISTS_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    const { activePlace } = getState().services.places

    try {
      const { data, error } = await client.get('/service/core/v1/specialists', {
        params: {
          placeId: activePlace.id,
        },
      })

      if (!error) {
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}
