import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'

import * as actions from 'src/modules/services/constants/places'

import { setDetailData } from 'src/modules/services/actions/placeDetail'
import { PlaceType } from 'src/modules/services/types'
import { format } from 'date-fns'

export const changePlace: ActionCreator<any> = (place: PlaceType) => ({
  type: actions.CHANGE_PLACE,
  payload: place,
})

export const loadPlaces: ActionCreator<any> = (): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_PLACES_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_PLACES_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_PLACES_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    const { activePlace } = getState().services.places
    const { id } = getState().common.user

    dispatch(request())

    try {
      const { data, error } = await client.get(
        `/service/core/v1/places?merchantId=${id}`,
      )

      if (!error) {
        dispatch(success(data))

        if (!activePlace.id) {
          dispatch(changePlace(data.list[0]))
        }
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const loadPlaceDetails: ActionCreator<any> = (id: number) => {
  const request: ActionCreator<any> = () => ({
    type: [actions.LOAD_PLACE_DETAILS_REQUEST],
  })
  const success: ActionCreator<any> = data => ({
    type: [actions.LOAD_PLACE_DETAILS_SUCCESS],
    payload: data,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.LOAD_PLACE_DETAILS_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    const { cities } = getState().common.address

    dispatch(request())

    try {
      const { data, error } = await client.get(`/service/core/v1/places/${id}`)

      if (!error) {
        dispatch(
          setDetailData({
            placeName: data.name,
            placePhone: data.phoneNumbers[0],
            city: {
              ...cities.find(item => item.id === data.address.cityId),
              label: cities.find(item => item.id === data.address.cityId).name,
            },
            cityId: data.address.cityId,
            street: data.address.street || '',
            house: data.address.house || '',
            flat: data.address.flat || '',
            office: data.address.office || '',
            webSiteUrl: data.webSiteUrl || '',
            INSTAGRAM: '',
            FACEBOOK: '',
            WHATSAPP: '',
            VK: '',
            TIKTOK: '',
            businessAreaValue: data.category.id,
            businessTypeValue: data.type.id,

            mapState: {
              center: [data.latitude, data.longitude],
              zoom: 17,
              controls: [],
            },
          }),
        )
        dispatch(success(data))
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const createPlace: ActionCreator<any> = (
  closeCb,
  updateId,
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.CREATE_PLACES_REQUEST],
  })
  const success: ActionCreator<any> = id => ({
    type: [actions.CREATE_PLACES_SUCCESS],
    payload: id,
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.CREATE_PLACES_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    const { id } = getState().common.user
    const schedule = getState().common.workSchedule

    const {
      cityId,
      street,
      house,
      office,
      flat,
      businessAreaValue,
      businessTypeValue,
      placeName,
      placePhone,
      city,
      INSTAGRAM,
      webSiteUrl,
      FACEBOOK,
      WHATSAPP,
      TIKTOK,
      VK,
    } = getState().services.placeDetail

    const requestParams = {
      address: {
        city: city.name,
        cityId,
        office,
        flat,
        street,
        house,
      },
      latitude: city.map.latitude,
      longitude: city.map.longitude,
      categoryId: businessAreaValue,
      typeId: businessTypeValue,
      merchantId: id,
      name: placeName,
      websiteUrl: webSiteUrl,
      images: [],
      logoImagePath: null,
      phoneNumbers: [placePhone],
      socialNetworks: [
        {
          code: 'INSTAGRAM',
          phoneNumber: '',
          url: INSTAGRAM,
        },
        {
          code: 'FACEBOOK',
          phoneNumber: '',
          url: FACEBOOK,
        },
        {
          code: 'WHATSAPP',
          phoneNumber: WHATSAPP,
          url: '',
        },
        {
          code: 'VK',
          phoneNumber: '',
          url: VK,
        },
        {
          code: 'TIKTOK',
          phoneNumber: '',
          url: TIKTOK,
        },
      ],
      schedule: Object.keys(schedule).map(day => ({
        day,
        fromTime: format(schedule[day].fromTime, 'HH:mm'),
        toTime: format(schedule[day].toTime, 'HH:mm'),
        isDayOff: schedule[day].isDayOff,
      })),
    }

    try {
      const { error } = updateId
        ? await client.put(`/service/core/v1/places/${updateId}`, requestParams)
        : await client.post('/service/core/v1/places', requestParams)

      if (!error) {
        dispatch(success(updateId))
        dispatch(loadPlaces())
        closeCb()
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const activatePlace: ActionCreator<any> = (
  idList: number[],
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.ACTIVATE_PLACE_REQUEST],
  })
  const success: ActionCreator<any> = () => ({
    type: [actions.ACTIVATE_PLACE_SUCCESS],
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.ACTIVATE_PLACE_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.post(
        '/service/core/v1/places/activate',
        {
          list: idList,
        },
      )

      if (!error && data.status === 'OK') {
        dispatch(success())
        dispatch(loadPlaces())
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const deactivatePlace: ActionCreator<any> = (
  idList: number[],
): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.DEACTIVATE_PLACE_REQUEST],
  })
  const success: ActionCreator<any> = () => ({
    type: [actions.DEACTIVATE_PLACE_SUCCESS],
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.DEACTIVATE_PLACE_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.post(
        '/service/core/v1/places/deactivate',
        {
          list: idList,
        },
      )

      if (!error && data.status === 'OK') {
        dispatch(success())
        dispatch(loadPlaces())
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}

export const deletePlace: ActionCreator<any> = (ids: number[]): AppThunk => {
  const request: ActionCreator<any> = () => ({
    type: [actions.DELETE_PLACES_REQUEST],
  })
  const success: ActionCreator<any> = () => ({
    type: [actions.DELETE_PLACES_SUCCESS],
  })
  const failure: ActionCreator<any> = error => ({
    type: [actions.DELETE_PLACES_FAILURE],
    error,
  })

  return async (dispatch, getState, client) => {
    dispatch(request())

    try {
      const { data, error } = await client.delete(
        `/service/core/v1/places?ids=${ids.join(',')}`,
      )

      if (!error && data.status === 'OK') {
        dispatch(success())
        dispatch(loadPlaces())
      }
    } catch (err) {
      dispatch(failure(err))
    }
  }
}
