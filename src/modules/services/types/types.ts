export type CustomerType = {
  avatar?: string
  birthday?: string
  category?: string
  comment?: string
  discount?: number
  email: string
  merchantId?: number
  name?: string
  firstName?: string
  lastName?: string
  phoneNumber: string
  customerPlaces: Array<{
    categoryCode: string
    discount: number
    placeId: number
  }>
  secondaryPhone?: string
  sex: string
  id?: number
  sold?: number
  visits?: string
}

export type PlaceType = {
  id: number
  name: string
  status: 'INACTIVE' | 'ACTIVE'
  workingTime?: any[]
  address: {
    city?: string
    cityId: number
    country: string
    street: string
    house: string
    flat?: string
  }
}

export type ServiceSpecialistType = {
  id: number
  name: string
  photoUrl: string
  jobPosition: string
}

export type ServiceType = {
  id: number
  name: string
  minPrice: {
    value: number
    currency: string
  }
  maxPrice: {
    value: number
    currency: string
  }
  specialists: ServiceSpecialistType[]
  status: 'ONLINE' | 'OFFLINE'
}

export type ReviewType = {
  clientName: string
  createdDate: string
  stars: number
  text: string
}

export type CreateCustomerBodyType = CustomerType

export type DeleteCustomersParamsType = {
  merchantId: number
  ids: any
}

export type SearchCustomersParamsType = {
  merchantId: number
  query: string
}

export type GetCustomerDetailType = {
  id: number
}

export type UpdateCustomerDetailType = {
  id: number
  body: CustomerType
}

export type IChangeAction<ActionType> = {
  type: ActionType
  payload: {
    field: string
    value: string
  }
}
