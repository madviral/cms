import { Dispatch, SetStateAction } from 'react'
import {
  DeleteCustomersParamsType,
  SearchCustomersParamsType,
  ServiceType,
  ReviewType,
  PlaceType,
} from 'src/modules/services/types'
import { ISuggestArgs } from 'src/modules/common/components/YMaps/types'
import { DayOfWeekUnion, ICity, IWeekSchedule } from 'src/modules/common/types'

export interface CustomerDataBaseTableProps {
  data: any[]
  places: PlaceType[]
  pageCount: number
  pageIndex: number
  pageSize: number
  onChangePageSize?: (size: number) => void
  onChangePageIndex?: (index: number) => void
  handleUpdate: (id: number) => void
}

export interface CustomerDataBaseProps {
  places: {
    list: PlaceType[]
  }
  customers: any[]
  pageCount: number
  pageIndex: number
  pageSize: number
  loadingOfPLaces: boolean
  loadingOfDeleting: boolean
  loadingOfCustomers: boolean
  loadPlaces: VoidFunction
  onChangePageSize?: (size: number) => void
  onChangePageIndex?: (index: number) => void
  loadCustomers: () => void
  deleteCustomers: (params: DeleteCustomersParamsType) => void
  searchCustomers: (params: SearchCustomersParamsType) => void
  changePlace: (place: PlaceType) => void
  activePlace: {
    id: number
    name: string
  }
}

export interface CustomersState {
  customerDetail: any
  list: any[]
  page: {
    size: number
    index: number
    count: number
  }
  loadingOfCustomers: boolean
  errorOfCustomers: string | object
  loadingOfCreating: boolean
  errorOfCreating: string | object
  loadingOfDeleting: boolean
  errorOfDeleting: string | object
  loadingOfSearching: boolean
  errorOfSearching: string | object
  loadingOfDetail: boolean
  errorOfDetail: string | object
  loadingOfUpdating: boolean
  errorOfUpdating: string | object
}

export interface IPlacesData {
  activatePlace: (idList: number[]) => void
  deactivatePlace: (idList: number[]) => void
  deletePlace: (idList: number[]) => void
  places: {
    list: PlaceType[]
  }
  loadingOfPlaces: boolean
  errorOfPlaces: string | object
  setDrawerStatus: Dispatch<SetStateAction<{ open: boolean; id: number }>>
}

export interface SpecialistsState {
  list: []
  loadingOfList: boolean
  errorOfList: string | object
}

export interface PlacesState {
  activePlace: {
    id: number
    name: string
  }
  placeDetail: PlaceType | any
  places: {
    list: PlaceType[]
  }
  loadingOfPLaceDetail: boolean
  errorOfPlaceDetail: string | object
  loadingOfPlaces: boolean
  errorOfPlaces: string | object
  loadingOfCreating: boolean
  errorOfCreating: string | object
  loadingOfActivating: boolean
  errorOfActivating: string | object
  loadingOfDeactivating: boolean
  errorOfDeactivating: string | object
  loadingOfDeleting: boolean
  errorOfDeleting: string | object
}

export interface PlacesTableProps {
  checkboxName?: string
  checkedList: string[]
  onChangeCbx: (id: string | number) => void
  items: PlaceType[]
  setDrawerStatus: Dispatch<SetStateAction<{ open: boolean; id: number }>>
}

export interface CustomersTableProps {
  items: any[]
  checkboxName?: string
  checkedList: string[]
  onChangeCbx: (id: string | number) => void
  handleUpdate: (id: number) => void
}

export interface SpecialistsTableColumnsProps {
  checkboxName?: string
  checkedList: string[]
  onChangeCbx: (id: string | number) => void
  items: any[]
}

export interface ScheduleIconProps {
  rotate?: string
}

export interface ContactsTabProps {
  onSuggest: (value: ISuggestArgs) => void
  schedule?: IWeekSchedule
  onChangeDayFromTime: (day: DayOfWeekUnion, value: Date) => void
  onChangeDayToTime: (day: DayOfWeekUnion, value: Date) => void
  onChangeIsDayOff: (day: DayOfWeekUnion, value: boolean) => void
}

export interface MainTabProps {
  list: any[]
  loadingOfList: boolean
  loadingOfPLaceDetail: boolean
  errorOfList: string | object
  typeList: any[]
  loadingOfTypeList: boolean
  errorOfTypeList: string | object
  onLoadCategories: VoidFunction
  onLoadCategoryTypes: (n: number) => void
  businessAreaValue: string
  businessTypeValue: string
  INSTAGRAM: string
  FACEBOOK: string
  WHATSAPP: string
  VK: string
  TIKTOK: string
  placeName: string
  placePhone: string
  webSiteUrl: string
  onChange: (event) => void
  onSelectChange: (value, value1: any) => void
}

export interface ServicesDrawerProps {
  id?: number
  isOpen: boolean
  places?: PlaceType[]
  onClose: any
}

export interface PlacesProps {
  loadPlaces: VoidFunction
  deletePlace: (idList: number[]) => void
  activatePlace: (idList: number[]) => void
  deactivatePlace: (idList: number[]) => void
  loadPlaceDetails: (id: number) => void
  cities: ICity[]
  onClear: VoidFunction
  onLoadCities: VoidFunction
}

export interface SpecialistsProps {
  places: {
    list: PlaceType[]
  }
  activePlace: {
    id: number
    name: string
  }
  list: any[]
  loadingOfList: boolean
  loadingOfPlaces: boolean
  loadPlaces: VoidFunction
  loadSpecialists: VoidFunction
  changePlace: (place: PlaceType) => void
}

export interface JobPositionsProps {
  places: {
    list: PlaceType[]
  }
  activePlace: {
    id: number
    name: string
  }
  list: any[]
  pageCount: number
  pageIndex: number
  pageSize: number
  loadingOfList: boolean
  onClear: VoidFunction
  loadPlaces: VoidFunction
  loadJobPositions: VoidFunction
  deleteJobPosition: (ids: number[]) => void
  onChangePageSize: (size: number) => void
  onChangePageIndex: (index: number) => void
  changePlace: (place: PlaceType) => void
}

export interface ReviewsProps {
  list: ReviewType[]
  places: {
    list: PlaceType[]
  }
  activePlace: {
    id: number
    name: string
  }
  loadingOfList: boolean
  rating: number
  pageCount: number
  pageIndex: number
  pageSize: number
  loadPlaces: VoidFunction
  loadReviews: VoidFunction
  changePlace: (place: PlaceType) => void
  onChangePageSize: (size: number) => void
  onChangePageIndex: (index: number) => void
}

export interface SpecialistsTableProps {
  list: any[]
}

export interface JobPositionsState {
  name: string
  description: string
  page: {
    size: number
    index: number
    count: number
  }
  list: any[]

  loadingOfDetails: boolean
  errorOfDetails: string | object

  loadingOfList: boolean
  errorOfList: string | object

  loadingOfCreating: boolean
  errorOfCreating: string | object

  loadingOfUpdating: boolean
  errorOfUpdating: string | object
}

export interface ReviewsState {
  rating: number
  list: ReviewType[]
  loadingOfList: boolean
  errorOfList: string | object
  page: {
    size: number
    index: number
    count: number
  }
}

export interface ServicesProps {
  list: ServiceType[]
  pageCount: number
  pageIndex: number
  pageSize: number
  onChangePageSize?: (size: number) => void
  onChangePageIndex?: (index: number) => void
  places: {
    list: PlaceType[]
  }
  activePlace: {
    id: number
    name: string
  }
  loadPlaces: VoidFunction
  changePlace: (place: PlaceType) => void
}

export interface ServicesState {
  list: ServiceType[]
  page: {
    size: number
    index: number
    count: number
  }
  loadingOfList: boolean
  errorOfList: object | string
}
