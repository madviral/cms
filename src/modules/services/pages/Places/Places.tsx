import React, { FC, useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import { Text } from 'ui'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Button } from 'ui/buttons'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'

import { PlacesProps, PlacesState } from 'src/modules/services/types/interfaces'
import { PlaceCreateDrawer, PlacesTable } from 'src/modules/services/components'

const Places: FC<PlacesProps & PlacesState> = ({
  places,
  loadingOfPlaces,
  loadPlaceDetails,
  errorOfPlaces,
  loadPlaces,
  activatePlace,
  deactivatePlace,
  deletePlace,
  onLoadCities,
  cities,
  onClear,
}) => {
  const [drawerStatus, setDrawerStatus] = useState({
    open: false,
    id: null,
  })
  const [t] = useTranslation('services')

  useEffect(() => {
    if (cities.length === 0) {
      onLoadCities()
    }

    loadPlaces()
  }, [])

  useEffect(() => {
    if (drawerStatus.id) {
      loadPlaceDetails(drawerStatus.id)
    }
  }, [drawerStatus.id])

  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 34px [content] 1fr'
      gridGap='16px 0'
      height='100%'
    >
      <PlaceCreateDrawer
        id={drawerStatus.id}
        isOpen={drawerStatus.open}
        onClose={() => {
          onClear()
          setDrawerStatus({
            open: false,
            id: null,
          })
        }}
      />

      <Box
        gridArea='title 1'
        display='flex'
        alignItems='center'
        justifyContent='space-between'
      >
        <Text
          fontSize={18}
          lineHeight={1}
          color={theme.color.secondary}
          fontWeight={500}
        >
          {t('places')}
        </Text>

        <Box ml='auto'>
          <GlobalSearch isDefaultOpen isAbleToHide={false} />
        </Box>
        <Box ml={20}>
          <Button
            appearance='primary'
            size='m'
            onClick={() =>
              setDrawerStatus({
                open: true,
                id: null,
              })
            }
          >
            + {t('createPlaces')}
          </Button>
        </Box>
      </Box>
      <Box gridArea='content 1' minHeight='fit-content'>
        <PlacesTable
          places={places}
          deletePlace={deletePlace}
          activatePlace={activatePlace}
          deactivatePlace={deactivatePlace}
          loadingOfPlaces={loadingOfPlaces}
          setDrawerStatus={setDrawerStatus}
          errorOfPlaces={errorOfPlaces}
        />
      </Box>
    </Box>
  )
}

export default Places
