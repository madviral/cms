import React, { useState } from 'react'

import { Button, PillTabs, Table, Text } from 'ui'
import { Box } from 'ui/layout'
import { ArrowLeftIcon } from 'ui/icons'
import { styled, theme } from 'ui/theme'

import { ScheduleIconProps } from 'src/modules/services/types/interfaces'

import DatePicker, {
  registerLocale,
} from 'react-datepicker/dist/react-datepicker'
import ru from 'date-fns/locale/ru'
import 'react-datepicker/dist/react-datepicker.css'
import { Appearance as TableAppearance } from 'ui/table/types/enum'

registerLocale('ru', ru)

const columns = [
  {
    title: 'Сотрудники',
    key: 'specialist',
    extraLabelKey: 'jobPosition',
    avatar: {
      isExist: true,
      rounded: true,
    },
  },
]

const data = [
  {
    key: '1',
    id: '1',
    specialist: 'Сотрудник 1',
    jobPosition: 'Специализация',
  },
  {
    key: '2',
    id: '2',
    specialist: 'Сотрудник 1',
    jobPosition: 'Специализация',
  },
  {
    key: '3',
    id: '3',
    specialist: 'Сотрудник 1',
    jobPosition: 'Специализация',
  },
  {
    key: '4',
    id: '4',
    specialist: 'Сотрудник 1',
    jobPosition: 'Специализация',
  },
  {
    key: '5',
    id: '5',
    specialist: 'Сотрудник 1',
    jobPosition: 'Специализация',
  },
  {
    key: '6',
    id: '6',
    specialist: 'Сотрудник 1',
    jobPosition: 'Специализация',
  },
]

const Icon = styled.button<ScheduleIconProps>`
  display: block;
  border: none;
  outline: none;
  background: none;
  margin-right: 40px;
  cursor: pointer;
  padding: 5px 15px;

  svg {
    path {
      transition: all 0.2s;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${props => props.theme.color.primary};
      }
    }
  }

  &:last-child {
    margin-right: 0;
  }

  ${props =>
    props.rotate &&
    `
    transform: rotate(${props.rotate}) translateY(12%);
  `}
`

const InfoBlock = styled.div`
  width: 330px;
  height: 100%;
  display: flex;
  flex-direction: column;

  .__employees {
    width: 100%;
    background: ${props => props.theme.color.white};
    border-radius: 20px;
    margin-top: 10px;
    height: 370px;
    overflow: auto;
  }
`

const monthNames = [
  'Января',
  'Февраля',
  'Марта',
  'Апреля',
  'Мая',
  'Июня',
  'Июля',
  'Августа',
  'Сентября',
  'Октября',
  'Ноября',
  'Декабря',
]

export const tabs = [
  {
    title: 'День',
    value: 'day',
  },
  {
    title: 'Неделя',
    value: 'week',
  },
]

const Schedule = () => {
  const [selectedRows, setSelectedRow] = useState([])
  const [, setActiveTab] = useState('day')
  const [activeDate, setActiveDate] = useState(new Date())
  const [currentDay] = useState(new Date())

  const rowSelection = {
    selectedRows,
    onChange: selectedRowKeys => setSelectedRow(selectedRowKeys),
  }

  const prevDay = () => {
    const day = new Date(activeDate)
    const prev = day.setDate(day.getDate() - 1)

    setActiveDate(new Date(prev))
  }

  const nextDay = () => {
    const day = new Date(activeDate)
    const next = day.setDate(day.getDate() + 1)

    setActiveDate(new Date(next))
  }

  const handleDatePicker = date => {
    setActiveDate(date)
  }

  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 18px 40px [buttons] 32px 20px [content] 1fr'
      height='100%'
    >
      <Box gridArea='title 1'>
        <Text
          fontSize={18}
          lineHeight={1}
          color={theme.color.secondary}
          fontWeight={500}
        >
          Расписание
        </Text>
      </Box>

      <Box
        gridArea='buttons 1'
        display='flex'
        alignItems='center'
        justiftContent='space-between'
      >
        <Box display='flex' alignItems='center' width='50%'>
          <Button
            onClick={() => setActiveDate(currentDay)}
            appearance='outline'
            size='s'
            disabled={currentDay.getTime() === activeDate.getTime()}
          >
            Сегодня
          </Button>

          <Box mx='40px' display='flex' alignItems='center'>
            <Icon onClick={prevDay}>
              <ArrowLeftIcon />
            </Icon>

            <Icon onClick={nextDay} rotate='180deg'>
              <ArrowLeftIcon />
            </Icon>
          </Box>

          <Box>
            <Text color={theme.color.secondary} fontSize={14}>
              {new Date(activeDate).getDate()}{' '}
              {monthNames[new Date(activeDate).getMonth()]}{' '}
              {new Date(activeDate).getFullYear()} г.
            </Text>
          </Box>
        </Box>

        <Box width='50%' display='flex' justifyContent='flex-end'>
          <Box width='160px'>
            <PillTabs
              tabs={tabs}
              onActiveTabChange={value => setActiveTab(value)}
            />
          </Box>
        </Box>
      </Box>

      <Box gridArea='content 1' overflow='hidden' mt='20px'>
        <InfoBlock>
          <DatePicker
            inline
            locale='ru'
            selected={activeDate}
            calendarClassName='viled-calendar'
            onChange={handleDatePicker}
          />

          <div className='__employees'>
            <Table
              rowSelection={rowSelection}
              appearance={TableAppearance.mini}
              columns={columns}
              data={data}
            />
          </div>
        </InfoBlock>
      </Box>
    </Box>
  )
}

export default Schedule
