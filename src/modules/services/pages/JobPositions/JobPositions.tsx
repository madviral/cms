import React, { FC, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'

import { Button, Text } from 'ui'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'

import { JobPositionsProps } from 'src/modules/services/types/interfaces'
import {
  JobPositionsTable,
  JobPositionCreateDrawer,
} from 'src/modules/services/components'

const JobPositions: FC<JobPositionsProps> = ({
  loadPlaces,
  places,
  activePlace,
  changePlace,
  onChangePageIndex,
  onChangePageSize,
  deleteJobPosition,
  loadJobPositions,
  list,
  pageCount,
  pageIndex,
  pageSize,
  onClear,
}) => {
  const [drawerStatus, setDrawerStatus] = useState({
    open: false,
    id: null,
  })

  const [t] = useTranslation()

  useEffect(() => {
    loadPlaces()
  }, [])

  useEffect(() => {
    if (activePlace.id) {
      loadJobPositions()
    }
  }, [activePlace])

  const handleUpdate = (id: number) => {
    setDrawerStatus({ open: true, id })
  }

  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 34px [content] 1fr'
      gridGap='16px 0'
      height='100%'
    >
      <JobPositionCreateDrawer
        id={drawerStatus.id}
        isOpen={drawerStatus.open}
        onClose={() => {
          onClear()
          setDrawerStatus({
            open: false,
            id: null,
          })
        }}
      />
      <Box
        gridArea='title 1'
        display='flex'
        alignItems='center'
        justifyContent='space-between'
      >
        <Box display='flex' alignItems='center'>
          <Text
            fontSize={18}
            lineHeight={1}
            color={theme.color.secondary}
            fontWeight={500}
          >
            {t('services:jobPositions')}
          </Text>

          <Box ml='16px' width={180}>
            <Dropdown width='100%'>
              <Dropdown.SimpleButton>
                <Text fontSize={12} fontWeight={'600'} color={'primary.0'}>
                  {activePlace.name || 'Выберите филилал'}
                </Text>
              </Dropdown.SimpleButton>
              <Dropdown.Menu width='100%'>
                {places.list.map((place, idx) => (
                  <Dropdown.Item key={idx} onClick={() => changePlace(place)}>
                    {place.name}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        </Box>

        <Box ml='auto'>
          <GlobalSearch isDefaultOpen isAbleToHide={false} />
        </Box>
        <Box ml={20}>
          <Button
            appearance='primary'
            size='m'
            onClick={() =>
              setDrawerStatus({
                open: true,
                id: null,
              })
            }
          >
            + {t('services:addJobPosition')}
          </Button>
        </Box>
      </Box>

      <Box gridArea='content 1' minHeight='fit-content'>
        <JobPositionsTable
          list={list}
          pageCount={pageCount}
          pageIndex={pageIndex}
          pageSize={pageSize}
          handleUpdate={handleUpdate}
          deleteJobPosition={deleteJobPosition}
          onChangePageIndex={onChangePageIndex}
          onChangePageSize={onChangePageSize}
        />
      </Box>
    </Box>
  )
}

export default JobPositions
