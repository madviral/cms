import React, { FC, useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import { Box } from 'ui/layout'
import { Text } from 'ui'
import { theme } from 'ui/theme'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'

import { ReviewsProps } from 'src/modules/services/types/interfaces'

import { ReviewsTable } from 'src/modules/services/components'

const Reviews: FC<ReviewsProps> = ({
  places,
  activePlace,
  pageCount,
  pageIndex,
  pageSize,
  loadPlaces,
  loadReviews,
  changePlace,
  onChangePageIndex,
  onChangePageSize,
  list,
  rating,
  loadingOfList,
}) => {
  const [t] = useTranslation()

  useEffect(() => {
    loadPlaces()
  }, [])

  useEffect(() => {
    if (activePlace.id) {
      loadReviews()
    }
  }, [activePlace])

  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 34px [content] 1fr'
      gridGap='16px 0'
      height='100%'
    >
      <Box
        gridArea='title 1'
        display='flex'
        alignItems='center'
        justifyContent='space-between'
      >
        <Box display='flex' alignItems='center'>
          <Text
            fontSize={18}
            lineHeight={1}
            color={theme.color.secondary}
            fontWeight={500}
          >
            {t('common:reviews')}
          </Text>

          <Box ml='16px' width={180}>
            <Dropdown width='100%'>
              <Dropdown.SimpleButton>
                <Text fontSize={12} fontWeight={'600'} color={'primary.0'}>
                  {activePlace.name || 'Выберите филилал'}
                </Text>
              </Dropdown.SimpleButton>
              <Dropdown.Menu width='100%'>
                {places.list.map((place, idx) => (
                  <Dropdown.Item key={idx} onClick={() => changePlace(place)}>
                    {place.name}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        </Box>

        <Box ml='auto'>
          <GlobalSearch isDefaultOpen isAbleToHide={false} />
        </Box>
      </Box>

      <Box gridArea='content 1' minHeight='fit-content'>
        <ReviewsTable
          list={list}
          rating={rating}
          pageCount={pageCount}
          pageIndex={pageIndex}
          pageSize={pageSize}
          loadingOfList={loadingOfList}
          onChangePageSize={onChangePageSize}
          onChangePageIndex={onChangePageIndex}
        />
      </Box>
    </Box>
  )
}

export default Reviews
