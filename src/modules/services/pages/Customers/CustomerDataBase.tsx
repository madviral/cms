import React, { FC, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'

import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Button, Text } from 'ui'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'

import { CustomerDataBaseProps } from 'src/modules/services/types/interfaces'
import {
  CustomerDataBaseTable,
  CustomerCreateDrawer,
  CustomerDetailDrawer,
} from 'src/modules/services/components'
import { Dropdown } from 'ui/dropdowns/Dropdown'

const CustomerDataBase: FC<CustomerDataBaseProps> = ({
  loadPlaces,
  loadCustomers,
  customers,
  places,
  pageCount,
  pageIndex,
  pageSize,
  onChangePageIndex,
  onChangePageSize,
  activePlace,
  changePlace,
}) => {
  const [isCreate, setIsCreate] = useState(false)
  const [isUpdate, setIsUpdate] = useState({
    open: false,
    id: null,
  })

  useEffect(() => {
    loadPlaces()
  }, [])

  useEffect(() => {
    if (places.list.length !== 0) {
      changePlace(places.list[0])
    }
  }, [places])

  useEffect(() => {
    if (activePlace.id) {
      loadCustomers()
    }
  }, [activePlace])

  const handleUpdate = (id: number) => {
    setIsUpdate({ open: true, id })
  }

  const [t] = useTranslation()

  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 34px [content] 1fr'
      gridGap='16px 0'
      height='100%'
    >
      <CustomerCreateDrawer
        places={places.list}
        isOpen={isCreate}
        onClose={() => setIsCreate(false)}
      />

      <CustomerDetailDrawer
        places={places.list}
        id={isUpdate.id}
        isOpen={isUpdate.open}
        onClose={() =>
          setIsUpdate({
            open: false,
            id: null,
          })
        }
      />

      <Box
        gridArea='title 1'
        display='flex'
        alignItems='center'
        justifyContent='space-between'
      >
        <Box display='flex' alignItems='center'>
          <Text
            fontSize={18}
            lineHeight={1}
            color={theme.color.secondary}
            fontWeight={500}
          >
            {t('services:customerDataBase')}
          </Text>

          <Box ml='16px' width={180}>
            <Dropdown width='100%'>
              <Dropdown.SimpleButton>
                <Text fontSize={12} fontWeight={'600'} color={'primary.0'}>
                  {activePlace.name || 'Выберите филилал'}
                </Text>
              </Dropdown.SimpleButton>
              <Dropdown.Menu width='100%'>
                {places.list.map((place, idx) => (
                  <Dropdown.Item key={idx} onClick={() => changePlace(place)}>
                    {place.name}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        </Box>

        <Box ml='auto'>
          <GlobalSearch isDefaultOpen isAbleToHide={false} />
        </Box>
        <Box ml={20}>
          <Button
            onClick={() => setIsCreate(true)}
            appearance='primary'
            size='m'
          >
            + {t('services:createCustomer')}
          </Button>
        </Box>
      </Box>
      <Box gridArea='content 1' minHeight='fit-content'>
        <CustomerDataBaseTable
          data={customers}
          places={places.list}
          pageCount={pageCount}
          pageIndex={pageIndex}
          pageSize={pageSize}
          handleUpdate={handleUpdate}
          onChangePageIndex={onChangePageIndex}
          onChangePageSize={onChangePageSize}
        />
      </Box>
    </Box>
  )
}

export default CustomerDataBase
