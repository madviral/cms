import React, { FC, useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Button, Text } from 'ui'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'

import { ServicesProps } from 'src/modules/services/types/interfaces'
import { ServicesTable } from 'src/modules/services/components'
import { ServiceType } from 'src/modules/services/types'

const list: ServiceType[] = [
  {
    id: 1,
    name: 'Работа с головой',
    maxPrice: {
      currency: 'KZT',
      value: 2000,
    },
    minPrice: {
      currency: 'KZT',
      value: 10000,
    },
    status: 'ONLINE',
    specialists: [
      {
        id: 1,
        photoUrl: 'https://i.ibb.co/SdLPGzP/Rectangle.png',
        jobPosition: 'Барбер',
        name: 'Тестовое Имя',
      },
      {
        id: 2,
        photoUrl: 'https://i.ibb.co/SdLPGzP/Rectangle.png',
        jobPosition: 'Барбер',
        name: 'Тестовое Имя',
      },
      {
        id: 3,
        photoUrl: 'https://i.ibb.co/SdLPGzP/Rectangle.png',
        jobPosition: 'Барбер',
        name: 'Тестовое Имя',
      },
      {
        id: 4,
        photoUrl: 'https://i.ibb.co/SdLPGzP/Rectangle.png',
        jobPosition: 'Барбер',
        name: 'Тестовое Имя',
      },
    ],
  },
  {
    id: 2,
    name: 'Работа с головой',
    maxPrice: {
      currency: 'KZT',
      value: 2000,
    },
    minPrice: {
      currency: 'KZT',
      value: 10000,
    },
    status: 'OFFLINE',
    specialists: [
      {
        id: 1,
        photoUrl: 'https://i.ibb.co/SdLPGzP/Rectangle.png',
        jobPosition: 'Барбер',
        name: 'Тестовое Имя',
      },
    ],
  },
  {
    id: 3,
    name: 'Работа с головой',
    maxPrice: {
      currency: 'KZT',
      value: 2000,
    },
    minPrice: {
      currency: 'KZT',
      value: 10000,
    },
    status: 'ONLINE',
    specialists: [
      {
        id: 1,
        photoUrl: 'https://i.ibb.co/SdLPGzP/Rectangle.png',
        jobPosition: 'Барбер',
        name: 'Тестовое Имя',
      },
    ],
  },
  {
    id: 4,
    name: 'Работа с головой',
    maxPrice: {
      currency: 'KZT',
      value: 2000,
    },
    minPrice: {
      currency: 'KZT',
      value: 10000,
    },
    status: 'ONLINE',
    specialists: [
      {
        id: 1,
        photoUrl: 'https://i.ibb.co/SdLPGzP/Rectangle.png',
        jobPosition: 'Барбер',
        name: 'Тестовое Имя',
      },
    ],
  },
]

const Services: FC<ServicesProps> = ({
  places,
  activePlace,
  changePlace,
  loadPlaces,
  // list,
  pageCount,
  pageIndex,
  pageSize,
  onChangePageSize,
  onChangePageIndex,
}) => {
  const [t] = useTranslation('')

  useEffect(() => {
    loadPlaces()
  }, [])

  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 34px [content] 1fr'
      gridGap='16px 0'
      height='100%'
    >
      <Box
        gridArea='title 1'
        display='flex'
        alignItems='center'
        justifyContent='space-between'
      >
        <Box display='flex' alignItems='center'>
          <Text
            fontSize={18}
            lineHeight={1}
            color={theme.color.secondary}
            fontWeight={500}
          >
            {t('common:services')}
          </Text>

          <Box ml='16px' width={180}>
            <Dropdown width='100%'>
              <Dropdown.SimpleButton>
                <Text fontSize={12} fontWeight={'600'} color={'primary.0'}>
                  {activePlace.name || 'Выберите филилал'}
                </Text>
              </Dropdown.SimpleButton>
              <Dropdown.Menu width='100%'>
                {places.list.map((place, idx) => (
                  <Dropdown.Item key={idx} onClick={() => changePlace(place)}>
                    {place.name}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        </Box>

        <Box ml='auto'>
          <GlobalSearch isDefaultOpen isAbleToHide={false} />
        </Box>
        <Box ml={20}>
          <Button appearance='primary' size='m'>
            + {t('services:createCustomer')}
          </Button>
        </Box>
      </Box>

      <Box gridArea='content 1' minHeight='fit-content'>
        <ServicesTable
          data={list}
          pageCount={pageCount}
          pageIndex={pageIndex}
          pageSize={pageSize}
          onChangePageIndex={onChangePageIndex}
          onChangePageSize={onChangePageSize}
        />
      </Box>
    </Box>
  )
}

export default Services
