import React, { FC, useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Button, Text } from 'ui'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'
import { Dropdown } from 'ui/dropdowns/Dropdown'

import { SpecialistsTable } from 'src/modules/services/components'
import { SpecialistsProps } from 'src/modules/services/types/interfaces'

const Specialists: FC<SpecialistsProps> = ({
  places,
  list,
  loadSpecialists,
  activePlace,
  changePlace,
  loadPlaces,
}) => {
  const [t] = useTranslation()

  useEffect(() => {
    loadPlaces()
  }, [])

  useEffect(() => {
    if (activePlace.id) {
      loadSpecialists()
    }
  }, [activePlace])

  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 34px [content] 1fr'
      gridGap='16px 0'
      height='100%'
    >
      <Box
        gridArea='title 1'
        display='flex'
        alignItems='center'
        justifyContent='space-between'
      >
        <Box display='flex' alignItems='center'>
          <Text
            fontSize={18}
            lineHeight={1}
            color={theme.color.secondary}
            fontWeight={500}
          >
            {t('common:specialists')}
          </Text>

          <Box ml='16px' width={180}>
            <Dropdown width='100%'>
              <Dropdown.SimpleButton>
                <Text fontSize={12} fontWeight={'600'} color={'primary.0'}>
                  {activePlace.name || 'Выберите филилал'}
                </Text>
              </Dropdown.SimpleButton>
              <Dropdown.Menu width='100%'>
                {places.list.map((place, idx) => (
                  <Dropdown.Item key={idx} onClick={() => changePlace(place)}>
                    {place.name}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </Box>
        </Box>

        <Box ml='auto'>
          <GlobalSearch isDefaultOpen isAbleToHide={false} />
        </Box>
        <Box ml={20}>
          <Button appearance='primary' size='m'>
            + {t('services:addSpecialist')}
          </Button>
        </Box>
      </Box>

      <Box gridArea='content 1' minHeight='fit-content'>
        <SpecialistsTable list={list} />
      </Box>
    </Box>
  )
}

export default Specialists
