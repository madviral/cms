import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { RootState } from 'src/store/rootReducer'

import { changePlace, loadPlaces } from 'src/modules/services/actions/places'
import {
  loadCustomers,
  deleteCustomers,
  searchCustomers,
  changePagination,
} from 'src/modules/services/actions/customerDataBase'

import { CustomerDataBase } from 'src/modules/services/pages'
import { PlaceType } from 'src/modules/services/types'

const mapStateToProps = (state: RootState) => ({
  places: state.services.places.places,
  activePlace: state.services.places.activePlace,
  loadingOfPlaces: state.services.places.loadingOfPlaces,

  customers: state.services.customers.list,
  pageCount: state.services.customers.page.count,
  pageIndex: state.services.customers.page.index,
  pageSize: state.services.customers.page.size,

  loadingOfCustomers: state.services.customers.loadingOfCustomers,
  loadingOfDeleting: state.services.customers.loadingOfDeleting,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  loadCustomers: () => dispatch(loadCustomers()),
  loadPlaces: () => dispatch(loadPlaces()),
  deleteCustomers: (params: object) => dispatch(deleteCustomers(params)),
  searchCustomers: (params: object) => dispatch(searchCustomers(params)),
  onChangePageIndex: size => dispatch(changePagination('index', size)),
  onChangePageSize: size => dispatch(changePagination('size', size)),
  changePlace: (place: PlaceType) => dispatch(changePlace(place)),
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerDataBase)
