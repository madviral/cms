import { connect } from 'react-redux'

import { Schedule } from 'src/modules/services/pages'

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Schedule)
