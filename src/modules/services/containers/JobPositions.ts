import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { RootState } from 'src/store/rootReducer'

import { JobPositions } from 'src/modules/services/pages'
import { changePlace, loadPlaces } from 'src/modules/services/actions/places'
import {
  loadJobPositions,
  deleteJobPosition,
  changePagination,
  clear,
} from 'src/modules/services/actions/jobPositions'

import { PlaceType } from 'src/modules/services/types'

const mapStateToProps = (state: RootState) => ({
  places: state.services.places.places,
  activePlace: state.services.places.activePlace,
  loadingOfPlaces: state.services.places.loadingOfPlaces,

  list: state.services.jobPositions.list,
  loadingOfList: state.services.jobPositions.loadingOfList,

  pageCount: state.services.jobPositions.page.count,
  pageIndex: state.services.jobPositions.page.index,
  pageSize: state.services.jobPositions.page.size,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  loadPlaces: () => dispatch(loadPlaces()),
  loadJobPositions: () => dispatch(loadJobPositions()),
  deleteJobPosition: (ids: number[]) => dispatch(deleteJobPosition(ids)),
  changePlace: (place: PlaceType) => dispatch(changePlace(place)),
  onChangePageIndex: size => dispatch(changePagination('index', size)),
  onChangePageSize: size => dispatch(changePagination('size', size)),
  onClear: () => dispatch(clear()),
})

export default connect(mapStateToProps, mapDispatchToProps)(JobPositions)
