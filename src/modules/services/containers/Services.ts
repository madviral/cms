import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'

import { Services } from 'src/modules/services/pages'

import { loadPlaces, changePlace } from 'src/modules/services/actions/places'
import { changePagination } from 'src/modules/services/actions/services'

import { PlaceType } from 'src/modules/services/types'

const mapStateToProps = (state: RootState) => ({
  places: state.services.places.places,
  activePlace: state.services.places.activePlace,
  pageCount: state.services.customers.page.count,
  pageIndex: state.services.customers.page.index,
  pageSize: state.services.customers.page.size,
  list: state.services.servicesPage.list,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  loadPlaces: () => dispatch(loadPlaces()),
  changePlace: (place: PlaceType) => dispatch(changePlace(place)),
  onChangePageIndex: size => dispatch(changePagination('index', size)),
  onChangePageSize: size => dispatch(changePagination('size', size)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Services)
