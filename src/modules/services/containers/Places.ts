import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'

import {
  loadPlaces,
  deletePlace,
  activatePlace,
  deactivatePlace,
  loadPlaceDetails,
} from 'src/modules/services/actions/places'

import { Places } from 'src/modules/services/pages'
import { loadCities } from 'src/modules/common/actions/address'
import { clear } from 'src/modules/services/actions/placeDetail'

const mapStateToProps = (state: RootState) => ({
  cities: state.common.address.cities,
  places: state.services.places.places,
  loadingOfPlaces: state.services.places.loadingOfPlaces,
  errorOfPlaces: state.services.places.errorOfPlaces,
})

const mapDispatchToProps = dispatch => ({
  loadPlaces: () => dispatch(loadPlaces()),
  deletePlace: (idList: number[]) => dispatch(deletePlace(idList)),
  activatePlace: (idList: number[]) => dispatch(activatePlace(idList)),
  deactivatePlace: (idList: number[]) => dispatch(deactivatePlace(idList)),
  loadPlaceDetails: (id: number) => dispatch(loadPlaceDetails(id)),
  onLoadCities: () => dispatch(loadCities()),
  onClear: () => dispatch(clear()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Places)
