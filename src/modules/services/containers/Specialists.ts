import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'

import { Specialists } from 'src/modules/services/pages'

import { loadPlaces, changePlace } from 'src/modules/services/actions/places'
import { loadSpecialists } from 'src/modules/services/actions/specialists'
import { PlaceType } from 'src/modules/services/types'

const mapStateToProps = (state: RootState) => ({
  places: state.services.places.places,
  list: state.services.specialists.list,
  loadingOfList: state.services.specialists.loadingOfList,
  activePlace: state.services.places.activePlace,
  loadingOfPlaces: state.services.places.loadingOfPlaces,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  loadPlaces: () => dispatch(loadPlaces()),
  loadSpecialists: () => dispatch(loadSpecialists()),
  changePlace: (place: PlaceType) => dispatch(changePlace(place)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Specialists)
