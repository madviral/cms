import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'

import { Reviews } from 'src/modules/services/pages'

import { loadPlaces, changePlace } from 'src/modules/services/actions/places'
import {
  changePagination,
  loadReviews,
} from 'src/modules/services/actions/reviews'

import { PlaceType } from 'src/modules/services/types'

const mapStateToProps = (state: RootState) => ({
  places: state.services.places.places,
  activePlace: state.services.places.activePlace,

  list: state.services.reviews.list,
  rating: state.services.reviews.rating,
  loadingOfList: state.services.reviews.loadingOfList,

  pageCount: state.services.reviews.page.count,
  pageIndex: state.services.reviews.page.index,
  pageSize: state.services.reviews.page.size,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  loadPlaces: () => dispatch(loadPlaces()),
  changePlace: (place: PlaceType) => dispatch(changePlace(place)),
  onChangePageIndex: size => dispatch(changePagination('index', size)),
  onChangePageSize: size => dispatch(changePagination('size', size)),
  loadReviews: size => dispatch(loadReviews()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Reviews)
