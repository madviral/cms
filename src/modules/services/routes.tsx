import React from 'react'
import { Redirect } from 'react-router-dom'

import { getDashedStr } from 'lib/utils'
import { RenderRoutes } from 'src/modules/common/components/Routes'
import { ContentIcon, SettingsIcon, ReviewsIcon } from 'ui/icons'

import CustomerDataBase from 'src/modules/services/containers/CustomerDataBase'
import JobPositions from 'src/modules/services/containers/JobPositions'
import Specialists from 'src/modules/services/containers/Specialists'
import Services from 'src/modules/services/containers/Services'
import Schedule from 'src/modules/services/containers/Schedule'
import Reviews from 'src/modules/services/containers/Reviews'
import Places from 'src/modules/services/containers/Places'

enum KEYS {
  CUSTOMERS = 'CUSTOMERS',
  SPECIALISTS = 'SPECIALISTS',
  JOB_POSITIONS = 'JOB_POSITIONS',
  SETTINGS = 'SETTINGS',
  PLACES = 'PLACES',
  SCHEDULE = 'SCHEDULE',
  REDIRECT = 'REDIRECT',
  REVIEWS = 'REVIEWS',
}

export const getRoutes = rootKey => [
  {
    path: '/services',
    key: getDashedStr(rootKey, KEYS.REDIRECT),
    exact: true,
    skip: true,
    component: () => <Redirect to={'/services/customers'} />,
  },
  {
    path: '/services/customers',
    key: getDashedStr(rootKey, KEYS.CUSTOMERS),
    exact: false,
    component: RenderRoutes,
    nav: {
      name: 'common:content',
      icon: ContentIcon,
    },
    routes: [
      {
        path: '/services/customers',
        key: getDashedStr(rootKey, KEYS.CUSTOMERS),
        exact: true,
        component: CustomerDataBase,
        nav: {
          name: 'services:customerDataBase',
        },
      },
      {
        path: '/services/customers/places',
        key: getDashedStr(rootKey, KEYS.PLACES),
        exact: false,
        component: Places,
        nav: {
          name: 'services:places',
        },
      },
      {
        path: '/services/customers/schedule',
        key: getDashedStr(rootKey, KEYS.SCHEDULE),
        exact: false,
        component: Schedule,
        nav: {
          name: 'services:schedule',
        },
      },
    ],
  },
  {
    path: '/services/settings',
    key: getDashedStr(rootKey, KEYS.SETTINGS),
    exact: false,
    component: RenderRoutes,
    nav: {
      name: 'common:settings',
      icon: SettingsIcon,
    },
    routes: [
      {
        path: '/services/settings',
        key: getDashedStr(rootKey, KEYS.SETTINGS),
        exact: true,
        component: Services,
        nav: {
          name: 'common:services',
        },
      },
      {
        path: '/services/settings/specialists',
        key: getDashedStr(rootKey, KEYS.SPECIALISTS),
        exact: false,
        component: Specialists,
        nav: {
          name: 'common:specialists',
        },
      },
      {
        path: '/services/settings/job-positions',
        key: getDashedStr(rootKey, KEYS.JOB_POSITIONS),
        exact: false,
        component: JobPositions,
        nav: {
          name: 'services:jobPositions',
        },
      },
    ],
  },
  {
    path: '/services/reviews',
    key: getDashedStr(rootKey, KEYS.REVIEWS),
    exact: false,
    component: Reviews,
    nav: {
      name: 'common:reviews',
      icon: ReviewsIcon,
    },
  },
]
