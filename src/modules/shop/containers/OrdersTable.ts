import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { OrdersTable } from '../components/OrdersTable'
import { changePagination, load } from '../actions/orders'

export default connect(
  (state: RootState) => ({
    items: state.shop.orders.list,
    pageCount: state.shop.orders.page.count,
    pageIndex: state.shop.orders.page.index,
    pageSize: state.shop.orders.page.size,
  }),
  dispatch => ({
    onLoad: () => dispatch(load()),
    onChangePageIndex: size => dispatch(changePagination('index', size)),
    onChangePageSize: size => dispatch(changePagination('size', size)),
  }),
)(OrdersTable)
