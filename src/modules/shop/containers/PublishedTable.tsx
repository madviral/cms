import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { PublishedTable } from '../components'

const mapStateToProps = (state: RootState) => state.shop.published
export default connect(mapStateToProps)(PublishedTable)
