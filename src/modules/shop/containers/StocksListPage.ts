import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { StocksListPage } from '../components/StocksListPage'
import { triggerEditStock } from '../actions/editStock'

export default connect(
  (state: RootState) => ({
    list: state.shop.stock.list,
  }),
  { triggerEditStock },
)(StocksListPage)
