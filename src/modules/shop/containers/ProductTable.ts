import { connect } from 'react-redux'
import { ProductTable } from '../components/ProductTable'
import { changePagination, load } from '../actions/catalog'

export default connect(
  (state: any) => ({
    items: state.shop.catalog.list,
    pageCount: state.shop.catalog.page.count,
    pageIndex: state.shop.catalog.page.index,
    pageSize: state.shop.catalog.page.size,
  }),
  dispatch => ({
    // @ts-ignore
    onLoad: () => dispatch(load()),
    onChangePageIndex: size => dispatch(changePagination('index', size)),
    onChangePageSize: size => dispatch(changePagination('size', size)),
  }),
)(ProductTable)
