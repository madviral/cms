import { connect } from 'react-redux'
import { triggerModal } from 'src/modules/common/actions/modal'
import {
  updateDraftItemAttributes,
  createItemFromDraftItem,
} from 'src/modules/shop/actions/draftProduct'
import Catalog from '../pages/Shop/Catalog'
import { RootState } from 'src/store/rootReducer'

const mapStateToProps = (state: RootState) => ({
  draftProductInfo: state.shop.draftProduct,
})

const mapDispatchToProps = {
  triggerModal,
  update: updateDraftItemAttributes,
  create: createItemFromDraftItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(Catalog)
