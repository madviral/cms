import { connect } from 'react-redux'
import { Import } from '../pages/Import'
import { RootState } from 'src/store/rootReducer'
import { loadImports } from '../actions/imports'

const mapState = (state: RootState) => ({
  imports: state.shop.imports,
})

const mapDispatch = {
  load: loadImports,
}
export default connect(mapState, mapDispatch)(Import)
