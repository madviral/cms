import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { NewProductDrawer } from '../components'
import { triggerNewProductDrawer } from 'src/modules/common/actions/newProductDrawer'
import { triggerModal } from 'src/modules/common/actions/modal'

const mapStateToProps = (state: RootState) => ({
  isOpenNewProductDrawer: state.common.newProductDrawer.isOpenNewProductDrawer,
})

const mapDispatchToProps = {
  triggerNewProductDrawer,
  triggerModal,
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProductDrawer)
