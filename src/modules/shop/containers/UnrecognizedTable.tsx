import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { UnrecognizedTable } from '../components'

const mapStateToProps = (state: RootState) => state.shop.imports.unrecognized
export default connect(mapStateToProps)(UnrecognizedTable)
