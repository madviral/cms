import { connect } from 'react-redux'
import { loadCities } from 'src/modules/common/actions/address'
import { change as changeDay } from 'src/modules/common/actions/workSchedule'
import { triggerEditStock } from '../actions/editStock'
import { update } from '../actions/stock'
import { RootState } from 'src/store/rootReducer'
import {
  change,
  clear,
  create,
  suggest,
  changeCity,
} from '../actions/stockDetails'
import { EditStock } from '../components/EditStock'

export default connect(
  (state: RootState) => ({
    cities: state.common.address.cities,
    city: state.shop.stockDetails.city,
    name: state.shop.stockDetails.name,
    street: state.shop.stockDetails.street,
    house: state.shop.stockDetails.house,
    flat: state.shop.stockDetails.flat,
    schedule: state.common.workSchedule,
    mapState: state.shop.stockDetails.mapState,
  }),
  {
    updateStock: update,
    triggerEditStock,
    onLoadCities: loadCities,
    onChangeCity: value => changeCity(value),
    onChangeDayFromTime: (day, value) => changeDay('fromTime', day, value),
    onChangeDayToTime: (day, value) => changeDay('toTime', day, value),
    onChangeIsDayOff: (day, value) => changeDay('isDayOff', day, value),
    onChangeIsAllDay: (day, value) => changeDay('isAllDay', day, value),
    onChangeDay: (day, value) => changeDay('day', day, value),
    onChangeName: e => change('name', e.target.value),
    onChangeStreet: value => change('street', value),
    onChangeHouse: value => change('house', value),
    onChangeFlat: value => change('flat', value),
    onClear: clear,
    onCreateStock: (schedule, closeCb) => create(schedule, closeCb),
    onSuggest: value => suggest(value),
  },
)(EditStock)
