import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { AcceptOrder } from '../components/AcceptOrder'

export default connect(
  (state: RootState) => ({}),
  () => ({}),
)(AcceptOrder)
