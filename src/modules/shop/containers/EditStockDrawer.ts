import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import EditStockDrawer from '../components/StocksListPage/EditStockDrawer'
import { triggerEditStock } from '../actions/editStock'

const mapStateToProps = (state: RootState) => ({
  isOpenEditStock: state.shop.editStock.isOpenEditStock,
  isEdited: state.shop.stock.isEdited,
})

const mapDispatchToProps = {
  triggerEditStock,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditStockDrawer)
