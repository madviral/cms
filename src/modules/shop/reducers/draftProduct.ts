import { createReducer } from 'lib/utils'
import * as actions from '../constants/draftProduct'

const initialState = {
  data: null,
  loading: false,
  error: null,
}

export default createReducer(initialState, {
  [actions.CREATE_DRAFT_LOADING]: state => ({
    ...state,
    loading: true,
    error: null,
  }),
  [actions.CREATE_DRAFT_FINISH]: (state, { payload }: any) => ({
    data: payload.data,
    loading: false,
    error: null,
  }),
  [actions.CREATE_DRAFT_ERROR]: (state, { payload }: any) => ({
    data: null,
    loading: false,
    error: payload.error,
  }),
  [actions.LOAD_ATTRIBUTES_LOADING]: state => ({
    ...state,
    loading: true,
    error: null,
  }),
  [actions.LOAD_ATTRIBUTES_FINISH]: (state, { payload }: any) => ({
    data: {
      ...state.data,
      attributes: payload.attributes,
    },
    loading: false,
    error: false,
  }),
  [actions.LOAD_ATTRIBUTES_ERROR]: (state, { payload }: any) => ({
    ...state,
    loading: false,
    error: payload.error,
  }),
  [actions.UPDATE_DRAFT_ITEM_ATTRIBUTES_FINISH]: state => ({
    ...state,
    attributesUpdated: true,
  }),
  [actions.CREATE_ITEM_FROM_DRAFT_ITEM_SUCCESS]: state => ({
    ...state,
    itemCreated: true,
  }),
  [actions.DRAFT_ITEM_UPDATE]: state => ({
    ...state,
    draftItemUpdated: true,
  }),
})
