import { combineReducers } from 'redux'
import catalog from './catalog'
import orders from './orders'
import newProduct from './newProduct'
import draftProduct from './draftProduct'
import stock from './stock'
import stockDetails from './stockDetails'
import imports from './imports'
import subscribe from './subscribe'
import editStock from './editStock'
import published from './published'

export default combineReducers({
  catalog,
  orders,
  newProduct,
  draftProduct,
  stock,
  stockDetails,
  imports,
  subscribe,
  editStock,
  published,
})
