import { createReducer } from 'lib/utils'
import { pageSizes } from 'src/modules/common/components/Pagination/Pagination'
import * as actions from '../constants/orders'

const initialState = {
  page: {
    size: pageSizes[0],
    index: 1,
    count: 1,
  },
  list: [],
}

export default createReducer(initialState, {
  [actions.CHANGE_PAGINATION]: (state, { payload }: any) => {
    const { field, value } = payload

    return {
      ...state,
      page: {
        ...state.page,
        [field]: value,
      },
    }
  },
  [actions.LOAD]: (state, payload) => ({
    ...state,
    list: (payload as any).list,
  }),
})
