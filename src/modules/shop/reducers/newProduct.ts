import { createReducer } from 'lib/utils'
import * as actions from '../constants/newProduct'

const initialState = {
  data: null,
}

export default createReducer(initialState, {
  [actions.SUGGESTION_LOAD]: (state, { payload }: any) => ({
    ...state,
    ...payload,
  }),
  [actions.SUGGESTION_CLEAR]: (state, { payload }: any) => {
    const newState = {
      ...state,
      ...payload,
    }
    return newState
  },
})
