import { ImportsState, ImportsActionTypes } from '../types/imports'
import * as actions from '../constants/imports'

const initialState: ImportsState = {
  recognized: {
    loading: false,
    error: false,
    data: null,
  },
  unrecognized: {
    loading: false,
    error: false,
    data: null,
  },
}

export default (
  state = initialState,
  action: ImportsActionTypes,
): ImportsState => {
  switch (action.type) {
    case actions.IMPORTS_RECOGNIZED_LOAD_START:
      return {
        ...state,
        recognized: {
          ...state.recognized,
          loading: true,
          error: false,
        },
      }
    case actions.IMPORTS_UNRECOGNIZED_LOAD_START:
      return {
        ...state,
        unrecognized: {
          ...state.unrecognized,
          loading: true,
          error: false,
        },
      }
    case actions.IMPORTS_RECOGNIZED_LOAD_FINISH:
      return {
        ...state,
        recognized: {
          data: action.payload.data,
          loading: false,
          error: false,
        },
      }
    case actions.IMPORTS_UNRECOGNIZED_LOAD_FINISH:
      return {
        ...state,
        unrecognized: {
          data: action.payload.data,
          loading: false,
          error: false,
        },
      }
    case actions.IMPORTS_RECOGNIZED_LOAD_ERROR:
      return {
        ...state,
        recognized: {
          loading: false,
          error: true,
          data: null,
        },
      }
    case actions.IMPORTS_UNRECOGNIZED_LOAD_ERROR:
      return {
        ...state,
        unrecognized: {
          loading: false,
          error: true,
          data: null,
        },
      }
    default:
      return state
  }
}
