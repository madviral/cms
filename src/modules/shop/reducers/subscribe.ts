import { createReducer } from 'lib/utils'
import * as actions from '../constants/subscribe'

const initialState = {
  success: false,
  loading: false,
  error: null,
}

export default createReducer(initialState, {
  [actions.SUBSCRIBE_LOADING]: () => ({
    success: false,
    loading: true,
    error: null,
  }),
  [actions.SUBSCRIBE_SUCCESS]: () => ({
    success: true,
    loading: false,
    error: null,
  }),
  [actions.SUBSCRIBE_ERROR]: (state, { payload }: any) => ({
    success: false,
    loading: false,
    error: payload,
  }),
})
