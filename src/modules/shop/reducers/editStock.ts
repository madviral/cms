import { createReducer } from 'lib/utils'
import * as actions from '../constants/triggerEditStock'

const initialState = {
  isOpenEditStock: null,
}

export default createReducer(initialState, {
  [actions.TRIGGER_EDIT_STOCK]: state => ({
    isOpenEditStock: !state.isOpenEditStock,
  }),
})
