import { createReducer } from 'lib/utils'
import * as actions from '../constants/stock'

const initialState = {
  list: [],
  loading: false,
  errors: {},
  isEdited: false,
}

export default createReducer(initialState, {
  [actions.LOADING]: state => ({
    ...state,
    loading: true,
    errors: false,
  }),
  [actions.LOAD]: (state, { payload }: any) => ({
    ...state,
    loading: false,
    list: payload,
  }),
  [actions.UPDATED]: (state, { payload }: any) => ({
    ...state,
    loading: false,
    isEdited: true,
  }),
  [actions.CLEAR]: () => initialState,
})
