import { createReducer } from 'lib/utils'
import * as actions from '../constants/stockDetails'

const initialState = {
  name: '',
  city: null,
  street: '',
  house: '',
  flat: '',
  loading: false,
  mapState: {
    center: [43.238293, 76.945465],
    zoom: 10,
    controls: [],
  },
  errors: {},
}

export default createReducer(initialState, {
  [actions.LOADING]: state => ({
    ...state,
    loading: true,
    errors: false,
  }),
  [actions.CHANGE]: (state, { payload }: any) => ({
    ...state,
    [payload.field]: payload.value,
  }),
  [actions.LOAD]: (state, { payload }: any) => ({
    ...state,
    loading: false,
    list: payload,
  }),
  [actions.CHANGE_CITY]: (state, { payload }: any) => {
    const { map } = payload

    return {
      ...state,
      mapState: {
        ...state.mapState,
        center: map ? [map.latitude, map.longitude] : state.mapState.center,
        zoom: 10,
      },
      city: payload,
    }
  },
  [actions.SUGGEST]: (state, { payload }: any) => {
    const address = payload.address.reduce(
      (prev, item) => ({
        ...prev,
        [item.kind]: item.name,
      }),
      {},
    )

    return {
      ...state,
      mapState: {
        ...state.mapState,
        center: payload.coordinates,
        zoom: 17,
      },
      street: address.street || address.district || '',
      house: address.house || '',
    }
  },
  [actions.CLEAR]: () => initialState,
})
