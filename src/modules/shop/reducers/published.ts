import * as actions from '../constants/published'
import { createReducer } from 'lib/utils'

const initialState = {
  loading: false,
  error: false,
  data: null,
}

export default createReducer(initialState, {
  [actions.PUBLISHED_LOAD_START]: state => ({
    ...state,
    loading: true,
    error: false,
  }),
  [actions.PUBLISHED_LOAD_FINISH]: (state, { payload }: any) => ({
    data: payload.data,
    loading: false,
    error: false,
  }),
  [actions.PUBLISHED_LOAD_ERROR]: () => ({
    data: null,
    loading: false,
    error: true,
  }),
})
