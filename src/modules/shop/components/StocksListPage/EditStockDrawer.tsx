import React, { useEffect } from 'react'
import { Drawer } from 'ui/drawer'
import EditStock from '../../containers/EditStock'

const EditStockDrawer = ({
  triggerEditStock,
  isOpenEditStock,
  stock,
  isEdited,
}) => {
  useEffect(() => {
    if (isEdited) {
      window.location.reload()
    }
  }, [isEdited])

  return (
    <Drawer
      width='620px'
      handler={false}
      open={isOpenEditStock}
      level={null}
      onClose={triggerEditStock}
      className='drawer1'
      placement='right'
    >
      <EditStock stock={stock} />
    </Drawer>
  )
}

export default EditStockDrawer
