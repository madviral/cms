import React from 'react'
import { Drawer } from 'ui/drawer'
import CreateStock from '../../containers/CreateStock'

const CreateStockDrawer = ({ isOpen, onClose }) => {
  return (
    <Drawer
      width='620px'
      handler={false}
      open={isOpen}
      level={null}
      onClose={onClose}
      className='drawer1'
      placement='right'
    >
      <CreateStock onClose={onClose} />
    </Drawer>
  )
}

export default CreateStockDrawer
