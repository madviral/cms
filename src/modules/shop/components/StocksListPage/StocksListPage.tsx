import React, { useMemo } from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import { useTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { Space, Text } from 'ui/text'
import { Button } from 'ui/buttons/Button'
import { IStock } from 'src/modules/common/types'
import { DotsIcon } from 'ui/icons'
import Table, { ColumnType } from 'ui/table/controlled/Table'
import CreateStockDrawer from './CreateStockDrawer'
import EditStockDrawer from '../../containers/EditStockDrawer'

type StocksListPageProps = {
  list: IStock[]
  triggerEditStock?: () => void
}

interface RecordType {
  name?: string
  address?: string
  goods?: number
  schedule?: string
  id?: number
}

const StocksListPage = ({
  list,
  match,
  location,
  history,
  triggerEditStock,
}: StocksListPageProps & RouteComponentProps) => {
  const [isCreate, setIsCreate] = React.useState(false)
  const [stockToEdit, setStockToEdit] = React.useState({})

  const [t] = useTranslation('shop')
  const data = useMemo(
    () =>
      list.map(item => ({
        ...item,
        key: item.id,
        goods: 0,
        address: `г. ${item.address.city}, ${item.address.street} ${item.address.house}`,
        schedule: 'schedule',
      })),
    [],
  )

  const columns = useMemo(
    (): Array<ColumnType<RecordType>> => [
      {
        title: 'Название',
        dataIndex: 'name',
        key: 'name',
        width: 116,
      },
      {
        title: 'Адрес',
        dataIndex: 'address',
        key: 'address',
        width: 516,
      },
      {
        title: 'Товары',
        dataIndex: 'goods',
        key: 'goods',
        width: 76,
      },
      {
        title: 'Время работы',
        dataIndex: 'schedule',
        key: 'schedule',
        width: 356,
      },
      {
        title: '',
        dataIndex: 'id',
        key: 'actions',
        width: 20,
        fixed: 'right',
        render: id => {
          return (
            <Box
              display='flex'
              alignItems='center'
              width='100%'
              height='100%'
              backgroundColor='white'
              onClick={() => {
                triggerEditStock()
                setStockToEdit(list.filter(item => item.id === id))
              }}
            >
              <div>
                <DotsIcon />
              </div>
            </Box>
          )
        },
      },
    ],
    [],
  )

  return (
    <Box display='flex' flexDirection='column' height='100%'>
      <CreateStockDrawer isOpen={isCreate} onClose={() => setIsCreate(false)} />
      <EditStockDrawer stock={stockToEdit[0]} />
      <Box display='flex' alignItems='center' mb='32px'>
        <Text
          fontSize='18px'
          lineHeight={1}
          fontWeight={500}
          color='secondary.0'
        >
          {t('stocks')}
        </Text>

        <Box ml='auto'>
          <Button
            appearance='primary'
            size='m'
            onClick={() => setIsCreate(true)}
          >
            +
            <Space />
            {t('addStock')}
          </Button>
        </Box>
      </Box>
      {/* TODO: Add box add ref to that box */}
      <Table columns={columns} data={data} />
    </Box>
  )
}

export default withRouter(StocksListPage)
