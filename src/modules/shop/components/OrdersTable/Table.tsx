import React, { FC, useCallback, useMemo } from 'react'
import BaseTable, { ColumnType } from 'ui/table/controlled/Table'
import { withTranslation, WithTranslation } from 'react-i18next'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import queryString from 'query-string'
import { Space, Text, Truncate } from 'ui/text'
import { format } from 'date-fns'
import { Box } from 'ui/layout'
import { Condition } from 'ui/condition'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { ShadowedTooltip } from 'ui/tooltip'
import { Image } from 'ui/image'
import { IPrice } from 'src/modules/common/types'
import { ProductTableItemProps } from '../ProductTableItem/ProductTableItem'
import { Queries } from '../../types/enums'

type ItemPriceStock = {
  id: number
  name: string
  smallImage?: string
}

type ItemPriceStockItem = {
  itemPriceStock: ItemPriceStock
}

export type Order = {
  id: number
  status?: {
    value: string
  }
  dateCreated?: string
  itemPriceStocks?: ItemPriceStockItem[]
  totalPrice: IPrice
}

type ProductTable = {
  checkboxName?: string
  checkedList: string[]
  onChangeCbx: (id: string | number) => void
  items: Order[]
}

const onRowClick = (record, history) => {
  history.push(`?${Queries.SELECTED_ORDER}=${record.id}`)
}

const Table: FC<WithTranslation & ProductTable & RouteComponentProps> = ({
  checkedList,
  checkboxName,
  items,
  history,
  t,
  onChangeCbx,
}) => {
  const data = useMemo(
    () =>
      items.map(item => ({
        ...item,
        key: item.id,
        buyer: 'Темный Властелин',
        deliveryType: '-',
        paymentOption: '-',
        signing: '-',
      })),
    [items.length],
  )

  const columns = useMemo(
    (): Array<ColumnType<ProductTableItemProps>> => [
      {
        title: '',
        dataIndex: 'id',
        key: 'checkbox',
        width: 52,
        render: id => {
          const onChangeCbxCached = useCallback(() => {
            onChangeCbx(id)
          }, [id])
          return (
            <Box width='36px' pl='16px'>
              <FormCheckbox
                id={id}
                name={checkboxName}
                value={id}
                checked={checkedList.some(itemId => itemId === id)}
                onChange={onChangeCbxCached}
              />
            </Box>
          )
        },
      },
      {
        title: `${t('order')} №`,
        dataIndex: 'id',
        key: 'id',
        width: 96,
        ellipsis: true,
        render: id => `№ ${id}`,
      },
      {
        title: t('orderDate'),
        dataIndex: 'dateCreated',
        key: 'dateCreated',
        width: 116,
        ellipsis: true,
        render: dateStr => {
          const date = new Date(dateStr.slice(0, -5))
          return format(date, 'dd.MM.yy')
        },
      },
      {
        title: t('buyer'),
        dataIndex: 'buyer',
        key: 'buyer',
        width: 116,
      },
      {
        title: t('productName'),
        dataIndex: 'orderItems',
        key: 'orderItems',
        width: 216,
        ellipsis: true,
        render: ([priceStock]) => {
          const { itemPriceStock } = priceStock
          const imgUrl = itemPriceStock.smallImage
          const imageId = `productImage${itemPriceStock.id}`

          return (
            <Box display='flex' alignItems='center'>
              <Box flex='0 0 40px' pr='8px'>
                <Box data-tip data-for={imageId}>
                  <Image src={imgUrl} alt={name} width={32} height={32} />
                </Box>
                <Condition match={!!imgUrl}>
                  <ShadowedTooltip id={imageId} effect='solid' notFade>
                    <Image src={imgUrl} alt={name} width={200} height={200} />
                  </ShadowedTooltip>
                </Condition>
              </Box>

              <Box flex='1' pr='16px'>
                <Truncate limit={53}>{itemPriceStock.name}</Truncate>
              </Box>
            </Box>
          )
        },
      },
      {
        title: t('price'),
        dataIndex: 'totalPrice',
        key: 'totalPrice',
        width: 86,
        render: price => {
          return (
            <Text
              color='secondary.0'
              fontSize='12px'
              lineHeight={1}
              fontWeight='500'
            >
              {price.value.toLocaleString()}
              <Space />
              {price.currency}
            </Text>
          )
        },
      },
      {
        title: t('deliveryAddress'),
        dataIndex: 'delivery',
        key: 'delivery',
        width: 196,
        render: delivery => {
          const { detailsCourier, detailsPickup } = delivery
          if (delivery.type === 'PICKUP') {
            return detailsPickup.address
          }

          const { address = {} } = detailsCourier
          return `г. ${address.city}${
            address.street ? `, ${address.street} ` : ''
          } ${address.house || ''}`
        },
      },
      {
        title: t('delivery'),
        dataIndex: 'deliveryType',
        key: 'deliveryType',
        width: 106,
      },
      {
        title: t('payment'),
        dataIndex: 'paymentOption',
        key: 'paymentOption',
        width: 86,
      },
      {
        title: t('signing'),
        dataIndex: 'signing',
        key: 'signing',
        width: 132,
      },
    ],
    [checkedList.length, onChangeCbx],
  )

  const queryParams = queryString.parse(location.search)
  return (
    <BaseTable
      data={data}
      columns={columns}
      clickable
      onRow={record => ({
        onClick: onRowClick.bind(null, record, history),
        className:
          parseInt(queryParams[Queries.SELECTED_ORDER] as string, 10) ===
            (record as any).id && 'active',
      })}
    />
  )
}

export default withTranslation(['shop', 'common'])(withRouter(Table))
