import React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import { Box } from 'ui/layout'
import { Text, Link, Button } from 'ui'
import { useTranslation } from 'react-i18next'
import LearningItem from './LearningItem'

type FirstStockProps = {
  onRepeat: VoidFunction
}

const FirstStock = ({
  onRepeat,
  match,
}: FirstStockProps & RouteComponentProps) => {
  const [t] = useTranslation('shop')

  return (
    <Box
      display='flex'
      flexDirection='column'
      alignItems='center'
      justifyContent='center'
      height='100%'
    >
      <Box
        display='flex'
        alignItems='center'
        justifyContent='center'
        minWidth='0'
        mb='20px'
      >
        <LearningItem
          title={t('stepOne')}
          description={'Добавьте свой первый склад на сайт'}
          imgUrl={'/images/stocks/learning.1.jpg'}
          bigOne
        >
          <Box pt='16px' width='100%'>
            <Box mb='10px'>
              <Button
                fullWidth
                appearance='secondary'
                size='l'
                onClick={onRepeat}
              >
                {t('takeTraining')}
              </Button>
            </Box>
            <Box mb='10px'>
              <Link to={{ pathname: match.url, state: { isCreate: true } }}>
                <Button fullWidth size='l' appearance='primary'>
                  {t('addNewStock')}
                </Button>
              </Link>
            </Box>
          </Box>
        </LearningItem>
      </Box>
      <Text as='p' color='secondary.0' fontSize='12px' lineHeight='1'>
        {t('haveQuestions')}?<Link to='#'> {t('usevlifeGuide')}</Link>
      </Text>
    </Box>
  )
}

export default withRouter(FirstStock)
