import React from 'react'
import { Box } from 'ui/layout'
import { Text, Link } from 'ui'
import { useTranslation } from 'react-i18next'
import LearningItem from './LearningItem'

type OrdersLearningProps = {
  onLearn: VoidFunction
}

const course = [
  {
    title: 'stepOne',
    description: 'learningStepOneDescription',
    imgUrl: '/images/stocks/learning.1.jpg',
  },
  {
    title: 'stepTwo',
    description: 'learningStepTwoDescription',
    imgUrl: '/images/stocks/learning.2.jpg',
  },
  {
    title: 'stepThree',
    description: 'learningStepThreeDescription',
    imgUrl: '/images/stocks/learning.3.jpg',
  },
]
const StocksLearning = ({ onLearn }: OrdersLearningProps) => {
  const [t] = useTranslation('shop')

  return (
    <Box
      display='flex'
      flexDirection='column'
      alignItems='center'
      justifyContent='center'
      height='100%'
    >
      <Box
        display='flex'
        alignItems='center'
        justifyContent='center'
        minWidth='0'
        mb='20px'
        onClick={onLearn}
      >
        {course.map(step => (
          <Box px='15px' key={step.title}>
            <LearningItem
              title={step.title}
              description={step.description}
              imgUrl={step.imgUrl}
            />
          </Box>
        ))}
      </Box>
      <Text as='p' color='secondary.0' fontSize='12px' lineHeight='1'>
        {t('haveQuestions')}?<Link to='#'> {t('usevlifeGuide')}</Link>
      </Text>
    </Box>
  )
}

export default StocksLearning
