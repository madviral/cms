import React, { ReactNode } from 'react'
import { Box } from 'ui/layout'
import { styled } from 'ui/theme'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { Text } from 'ui'
import { useTranslation } from 'react-i18next'
import { ifProp } from 'styled-tools'

type LearningItemrops = {
  title: string
  description: string
  imgUrl?: string
  bigOne?: boolean
  children?: ReactNode
}

const StyledWrapper = styled(Box)`
  padding-top: 60px;
  padding-bottom: 32px;
  padding-left: 30px;
  padding-right: 30px;
  border-radius: ${({ theme }) => theme.borderRadius.l};
  background-color: #fff;
  min-width: 0;

  ${ifProp(
    'bigOne',
    `
      width: 400px;
      max-height: 606px;

      img {
        width: 100%;
        height: 260px;
      }
    `,
    `
      width: 300px;
      height: 395px;
      cursor: pointer;

      img {
        max-width: 250px;
        max-height: 187px;
      }
    `,
  )}

  .__title {
    position: relative;
    display: block;
    padding-bottom: 12px;
    &::before {
      content: '';
      position: absolute;
      left: 50%;
      bottom: 0;
      transform: translateX(-50%);
      display: block;
      background: ${({ theme }) => theme.colorsList.primary[1]};
      margin: 5px auto 0;
      width: 22px;
      height: 2px;
    }
  }
`

const LearningItem = ({
  bigOne,
  description,
  imgUrl,
  title,
  children,
}: LearningItemrops) => {
  const [t] = useTranslation('shop')

  return (
    <StyledWrapper
      display='flex'
      flexDirection='column'
      alignItems='center'
      justifyContent='flex-start'
      height='100%'
      bigOne={bigOne}
    >
      <Box mb='10px'>
        <Text
          className='__title'
          as='span'
          color='primary.1'
          fontWeight='500'
          fontSize='24px'
          lineHeight='1'
        >
          {t(title)}
        </Text>
      </Box>
      <Box mb='40px'>
        <Text
          as='p'
          color='secondary.0'
          fontWeight='500'
          fontSize='14px'
          lineHeight='1.5'
        >
          {t(description)}
        </Text>
      </Box>
      <Condition match={imgUrl}>
        <Box display='flex' justifyContent='center' width='100%' mt='auto'>
          <img src={imgUrl} />
        </Box>
      </Condition>
      {children}
    </StyledWrapper>
  )
}

export default LearningItem
