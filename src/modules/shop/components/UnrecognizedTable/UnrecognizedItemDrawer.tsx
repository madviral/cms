import React, { FC, useState, useEffect } from 'react'
import { Drawer as UIDrawer } from 'ui/drawer'
import { Table as Item } from 'src/modules/shop/types/imports'
import { FormField, Description, Caption, Button, Treeselect } from 'ui'
import styled from 'ui/theme/styled'
import { Box } from 'ui/layout'
import { useTranslation } from 'react-i18next'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import { similarItemSubscribe } from 'src/modules/shop/actions/imports'
import { triggerNewProductDrawer } from 'src/modules/common/actions/newProductDrawer'
import { RootState } from 'src/store/rootReducer'
import { load as loadCategories } from 'src/modules/common/actions/productCategories'
import Drawer from '../../pages/Shop/Drawer'
import { createItemFromDraftItem } from 'src/modules/shop/actions/draftProduct'

type Attribute = {
  id: number
  idName: string
  value: string
  valueName: string
}

type Image = {
  id: number
  large: string
  medium: string
  origin: string
  small: string
  sort: number
}

type Price = {
  value: number
  currency: string
}
type IDetails = {
  name: string
  attributes: Attribute[]
  categories: number[]
  description: string
  images: Image[]
  price: Price
  rating: number
  ratingCount: number
  smallImage: string
  vendorCode: string
} | null

interface IGridBox {
  fullWidth: boolean
}

const GridBox = styled(Box)<IGridBox>`
  ${props => props.fullWidth && `grid-column-start: 1; grid-column-end: 3;`}
`

interface UnrecognizedItemDrawerProps {
  isOpen: boolean
  onClose: () => void
  item: Item
}

interface FormProps {
  item: Item
  onClose: () => void
}

const formInputs = [
  'merchantProductId',
  'vendorCode',
  'barCode',
  'brand',
  'merchantProductName',
]

interface SimilarItemsProps {
  item: Item
  onClose: () => void
  recordId: number
}

const serialize = (obj: Item) => {
  const str = []
  const properties = ['brand', 'categoryId']
  properties.forEach(p => {
    if (obj[p]) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
    }
  })
  return str.join('&')
}

const EmptyItems = () => {
  const [t] = useTranslation('shop')
  const dispatch = useDispatch()
  return (
    <Box
      display='flex'
      alignItems='center'
      justifyContent='center'
      flexDirection='column'
    >
      <Caption>{t('nothingFound')}</Caption>
      <Box mt='16px'>
        <Button
          appearance='outline'
          size='l'
          onClick={() => dispatch(triggerNewProductDrawer())}
        >
          {t('hintDoesntMatch')}
        </Button>
      </Box>
    </Box>
  )
}

type SimilarItem = IDetails & {
  id: number
}

interface RenderListProps {
  items: SimilarItem[]
  onClose: () => void
  recordId: number
}

const SimilarItemWrap = styled(Box)`
  &:not(:first-of-type) {
    margin-top: 8px;
  }
  display: flex;
  flex-direction: row;
  cursor: pointer;
`

const SimilarItemSelectWrap = styled(Box)`
  height: 104px;
  display: flex;
  align-items: center;
  width: 36px;
  flex-shrink: 0;
`

interface ISimilarItemSelect {
  active: boolean
}

const SimilarItemSelect = styled(Box)<ISimilarItemSelect>`
  height: 20px;
  width: 20px;
  border-radius: 10px;
  border: ${props =>
    props.active
      ? `6px solid ${props.theme.colorsList.primary[0]}`
      : `1px solid ${props.theme.colorsList.secondary[1]}`};
`

const SimilarItemDescription = styled(Box)`
  height: 104px;
  border-radius: 8px;
  background: ${props => props.theme.colorsList.secondary[3]};
  padding: 16px;
  display: flex;
  flex-direction: row;
  flex: 1;
`

interface IImage {
  url: string
}

const SimilarItemImage = styled(Box)<IImage>`
  height: 70px;
  width: 70px;
  border-radius: 8px;
  background: url(${props => props.url});
  background-color: white;
  background-size: contain;
`

const Footer = styled(Box)`
  height: 48px;
  border-top: 1px solid ${props => props.theme.colorsList.secondary[2]};
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: space-between;
  margin-top: 16px;
`

const RenderList: FC<RenderListProps> = ({ items, onClose, recordId }) => {
  const [selected, setSelected] = useState(0)
  const dispatch = useDispatch()
  const [t] = useTranslation()
  const sign = () => {
    const data = {
      recordId,
      itemId: items[selected].id,
    }
    dispatch(similarItemSubscribe(data, onClose))
  }
  return (
    <Box>
      {items.map((item, idx) => (
        <SimilarItemWrap onClick={() => setSelected(idx)}>
          <SimilarItemSelectWrap>
            <SimilarItemSelect active={selected === idx} />
          </SimilarItemSelectWrap>
          <SimilarItemDescription key={item.id}>
            <Box mr='16px' flexShrink='0'>
              <SimilarItemImage url={item.smallImage} />
            </Box>
            <Box>
              <Caption style={{ fontSize: 10 }}>#{item.vendorCode}</Caption>
              <Caption style={{ fontSize: 14, fontWeight: 600 }}>
                {item.name}
              </Caption>
            </Box>
          </SimilarItemDescription>
        </SimilarItemWrap>
      ))}
      <Footer>
        <Button
          appearance='outline'
          size='m'
          onClick={() => dispatch(triggerNewProductDrawer())}
        >
          {t('shop:hintDoesntMatch')}
        </Button>
        <Box display='flex'>
          <Box mr='16px'>
            <Button appearance='secondary' size='m' onClick={onClose}>
              {t('cancel')}
            </Button>
          </Box>
          <Button appearance='primary' size='m' onClick={sign}>
            {t('sign')}
          </Button>
        </Box>
      </Footer>
    </Box>
  )
}

interface ListCategories {
  callback: (categoryId: number) => void
}

const ListCategories: FC<ListCategories> = ({ callback }) => {
  const { data } = useSelector(
    (state: RootState) => state.common.general.productCategories,
  )
  const dispatch = useDispatch()
  useEffect(() => {
    if (!data) {
      dispatch(loadCategories())
    }
  }, [])
  const [t] = useTranslation('shop')
  if (!data) {
    return null
  }
  return (
    <Description title={t('category')}>
      <Treeselect
        callback={item => callback(item.id)}
        options={data}
        placeholder={t('shop:chooseCategory')}
      />
    </Description>
  )
}

const SimilarItems: FC<SimilarItemsProps> = ({ item, onClose, recordId }) => {
  const [t] = useTranslation()
  const [similarItems, setSimilarItems] = useState(null)
  const loadSimilarItems = (categoryId: number | null = null) => {
    const serializeItem = {
      ...item,
      categoryId,
    }
    const query = serialize(serializeItem)
    axios
      .get(
        `https://api-dev.vlife.kz/market/catalog/v1/items/similarlist?${query}`,
        {
          headers: {
            'Accept-Language': 'ru',
          },
        },
      )
      .then(res => setSimilarItems(res.data))
  }
  useEffect(() => {
    loadSimilarItems()
  }, [item.recordId])
  if (!item) {
    return null
  }
  return (
    <Box mt='16px'>
      <Box my='16px'>
        <ListCategories callback={loadSimilarItems} />
      </Box>
      <Description title={t('shop:similarItems')}>
        {similarItems ? (
          similarItems.empty ? (
            <EmptyItems />
          ) : (
            <RenderList
              items={similarItems.content}
              onClose={onClose}
              recordId={recordId}
            />
          )
        ) : (
          <Box display='flex' justifyContent='center' alignItems='center'>
            <Caption>{t('loading')}</Caption>
          </Box>
        )}
      </Description>
    </Box>
  )
}

const Form: FC<FormProps> = ({ item, onClose }) => {
  const [t] = useTranslation('shop')
  const [form, setForm] = useState(item)
  return (
    <Box display='flex' flexDirection='column'>
      <Box flex='1'>
        <Description title={t('yourUnrecognizedItem')}>
          <Box
            borderWidth='1px'
            borderStyle='solid'
            borderColor='alert.0'
            borderRadius='8px'
            p='16px'
            display='grid'
            gridTemplateColumns='1fr 1fr'
            gridGap='16px'
          >
            {formInputs.map((input, idx, arr) => (
              <GridBox key={input} fullWidth={idx + 1 === arr.length}>
                <FormField
                  value={form[input]}
                  label={t(input)}
                  error='empty'
                  onChange={e => setForm({ ...form, [input]: e.target.value })}
                />
              </GridBox>
            ))}
          </Box>
        </Description>
      </Box>
      <SimilarItems item={item} onClose={onClose} recordId={item.recordId} />
    </Box>
  )
}

const UnrecognizedItemDrawer: FC<UnrecognizedItemDrawerProps> = ({
  isOpen,
  onClose,
  item,
}) => {
  const { draftProduct } = useSelector((state: RootState) => state.shop)
  return (
    <>
      <UIDrawer
        width='632px'
        handler={false}
        open={isOpen}
        level={null}
        onClose={onClose}
        placement='right'
      >
        <Box p='16px'>
          {item ? <Form item={item} onClose={onClose} /> : null}
        </Box>
      </UIDrawer>
      <Drawer
        draftProductInfo={draftProduct}
        shouldTriggerModal={false}
        create={createItemFromDraftItem}
      />
    </>
  )
}

export default UnrecognizedItemDrawer
