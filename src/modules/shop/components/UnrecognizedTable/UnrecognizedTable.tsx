import React, { FC, useState, useEffect } from 'react'
import Table from '../Table'
import { UnrecognizedState } from '../../types/imports'
import Drawer from './UnrecognizedItemDrawer'
import { ColumnsType } from 'rc-table/lib/interface'
import { Table as TableType } from 'src/modules/shop/types/imports'
import order from './order'
import { Box } from 'ui/layout'
import { useTranslation } from 'react-i18next'
import { loadItems } from '../../actions/imports'
import { useDispatch } from 'react-redux'

interface UnrecognizedTableProps extends UnrecognizedState {}

const UnrecognizedTable: FC<UnrecognizedTableProps> = ({ data }) => {
  const dispatch = useDispatch()
  useEffect(() => {
    if (!data) {
      dispatch(loadItems('unrecognized', 0, 25))
    }
  }, [])
  const [isOpen, setIsOpen] = useState(false)
  const [item, setItem] = useState(null)
  const [t] = useTranslation(['shop', 'common'])
  const onClose = () => {
    setIsOpen(false)
  }
  if (!data) {
    return null
  }
  const columns: ColumnsType<any> = [
    ...order.map(col => ({
      title: t(col),
      dataIndex: col,
      key: col,
      width: 180,
      render: (val: string, row: TableType) => {
        if (val) {
          return <div>{val}</div>
        }
        if (row.details[col]) {
          return (
            <Box color='alert.0'>
              <p style={{ fontWeight: 500 }}>{row.details[col]}</p>
            </Box>
          )
        }
      },
    })),
    {
      title: t('price'),
      dataIndex: 'price',
      key: 'price',
      width: 200,
    },
    ...data.stocks.map(stock => ({
      title: stock || 'Stock',
      dataIndex: '',
      key: 'stock',
      width: 90,
      render: (val: string, row: TableType, idx: number) => {
        return <Box>{row.stocks[stock] ? t('yes') : t('no')}</Box>
      },
    })),
  ]
  const onRow = (row: TableType, i: number) => {
    const onRowProps = {
      onClick: () => {
        setItem(row)
        setIsOpen(true)
      },
    }
    if (Object.keys(row.details).length) {
      return {
        className: 'error',
        ...onRowProps,
      }
    }
    return onRowProps
  }
  const paginateTo = (page: number) => {
    dispatch(loadItems('unrecognized', page - 1, 25))
  }
  return (
    <>
      <Drawer isOpen={isOpen} onClose={onClose} item={item} />
      <Table
        tableData={data}
        paginateTo={paginateTo}
        columns={columns}
        onRow={onRow}
      />
    </>
  )
}

export default UnrecognizedTable
