import { Main, Attributes } from './components'

export default [
  {
    id: 0,
    name: 'shop:main',
    component: Main,
    color: 'primary',
  },
  {
    id: 1,
    name: 'shop:features',
    component: Attributes,
    color: 'primary',
  },
]
