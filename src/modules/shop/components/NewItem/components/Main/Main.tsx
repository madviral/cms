import React, { FC, useEffect, useState } from 'react'
import { Footer, Wrap, Body } from '../shared'
import { FormField, Description, Treeselect } from 'ui'
import { Box } from 'ui/layout'
import { useDispatch, useSelector } from 'react-redux'
import { load } from 'src/modules/common/actions/productCategories'
import { createOrUpdateDraft as createDraftAction } from 'src/modules/shop/actions/draftProduct'
import { RootState } from 'src/store/rootReducer'
import { useTranslation } from 'react-i18next'

const Main: FC<MainProps> = ({ next }) => {
  const dispatch = useDispatch()
  const options = useSelector(
    (state: RootState) => state.common.general.productCategories.data,
  )
  useEffect(() => {
    if (!options) {
      dispatch(load())
    }
  }, [])
  const [t] = useTranslation('shop')
  const [serialNumber, setSerialNumber] = useState('')
  const [itemCodeNumber, setItemCodeNumber] = useState('')
  const [itemName, setItemName] = useState('')
  const [cat, setCat] = useState(null)
  const disabled = [serialNumber, itemCodeNumber, itemName, cat].some(v => !v)
  const createDraft = () => {
    const data = {
      name: itemName,
      vendorCode: itemCodeNumber,
      productCode: serialNumber,
      categoryId: cat,
    }
    dispatch(createDraftAction(data, next))
  }
  return (
    <Wrap>
      <Body>
        <Box
          display='grid'
          gridTemplateColumns='1fr 1fr'
          gridGap='0 30px'
          mb={30}
        >
          <FormField
            type='text'
            value={serialNumber}
            onChange={e => setSerialNumber(e.target.value)}
            label={t('serialNumber')}
          />
          <FormField
            type='text'
            value={itemCodeNumber}
            onChange={e => setItemCodeNumber(e.target.value)}
            label={t('itemCodeNumber')}
          />
        </Box>
        <Box mb={30}>
          <Description title='shop:category'>
            <Treeselect
              callback={item => setCat(item.id)}
              options={options || []}
              placeholder={'hello world'}
              isAbsolute
            />
          </Description>
        </Box>
        <Box>
          <FormField
            type='text'
            label={t('itemName')}
            value={itemName}
            onChange={e => setItemName(e.target.value)}
          />
        </Box>
      </Body>
      <Footer text={'continue'} disabled={disabled} onClick={createDraft} />
    </Wrap>
  )
}

export default Main
