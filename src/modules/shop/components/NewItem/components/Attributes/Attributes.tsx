import React, { FC, useEffect, useState } from 'react'
import { connect } from 'react-redux'
import {
  loadDraftItemAttributes,
  updateDraftItemAttributes,
} from 'src/modules/shop/actions/draftProduct'
import { RootState } from 'src/store/rootReducer'
import { Box } from 'ui/layout'
import { Wrap, Body, Footer } from '../shared'
import { Expander, Description } from 'ui'

const Attributes: FC<any> = ({
  load,
  data,
  loading,
  next,
  update,
  noFooter,
  callback,
  noTitle,
  setIsEmptyAttributes,
}) => {
  const [shouldDisplayFooter] = useState(noFooter)
  const [selectedAttributes, setSelectedAttributes] = useState({})

  useEffect(() => {
    if (data) {
      load(data.id)
    }
  }, [data?.id])
  setIsEmptyAttributes(data?.attributes?.length === 0)
  if (loading || !data?.attributes) {
    return null
  }

  const disabled =
    data.attributes.reduce((cur, n) => {
      return cur + n.attributes.length
    }, 0) === Object.keys(selectedAttributes).length
  const setAttributes = (attr: any) => {
    setSelectedAttributes({
      ...selectedAttributes,
      [attr.attributeId]: attr,
    })
    callback(selectedAttributes)
  }

  const updateAttributes = () => {
    update(Object.values(selectedAttributes))
  }

  return (
    <Wrap>
      <Body noTitle={noTitle}>
        <Description title={noTitle ? '' : 'shop:features'}>
          {data?.attributes.map(a => (
            <Box key={a.id} mb={10}>
              <Expander {...a} callback={setAttributes} />
            </Box>
          ))}
        </Description>
      </Body>
      {!shouldDisplayFooter && (
        <Footer
          onClick={updateAttributes}
          text={'continue'}
          disabled={!disabled}
        />
      )}
    </Wrap>
  )
}

const mapState = (state: RootState) => state.shop.draftProduct
const mapDispatch = {
  load: loadDraftItemAttributes,
  update: updateDraftItemAttributes,
}
export default connect(mapState, mapDispatch)(Attributes)
