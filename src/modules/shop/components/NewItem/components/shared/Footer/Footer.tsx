import React, { FC } from 'react'
import styled from 'ui/theme/styled'
import { Button } from 'ui'
import { useTranslation } from 'react-i18next'

const Wrap = styled.div`
  width: 100%;
  height: 56px;
  border-top: 1px solid ${props => props.theme.colorsList.secondary[2]};
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

interface FooterProps {
  text: string
  disabled?: boolean
  onClick: () => void
}

const Footer: FC<FooterProps> = ({ text, disabled, onClick }) => {
  const [t] = useTranslation()
  return (
    <Wrap>
      <Button
        appearance='outline'
        size='s'
        disabled={disabled}
        onClick={onClick}
      >
        {t(text)}
      </Button>
    </Wrap>
  )
}
Footer.defaultProps = {
  disabled: false,
}
export default Footer
