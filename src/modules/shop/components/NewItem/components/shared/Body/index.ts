import styled from 'ui/theme/styled'

interface NoTitleProps {
  noTitle?: boolean
}

export default styled.div<NoTitleProps>`
  flex: 1;
  padding: ${props => !props.noTitle && '30px 0'};
`
