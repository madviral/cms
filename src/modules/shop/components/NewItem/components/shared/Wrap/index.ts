import styled from 'ui/theme/styled'

export default styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`
