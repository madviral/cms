import React, { FC } from 'react'
import tabs from './tabs'
import styled from 'ui/theme/styled'
import { Tabs } from 'ui'

const Wrap = styled.div`
  background: ${props => props.theme.color.white};
  border-radius: 20px;
  box-sizing: border-box;
  height: 100%;
  display: flex;
  flex-direction: column;
`

const NewItem: FC = () => (
  <Wrap>
    <Tabs tabs={tabs} />
  </Wrap>
)

export default NewItem
