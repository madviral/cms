import React, { FC, useCallback } from 'react'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Space, Text, Truncate } from 'ui/text'
import { IImage, IPrice, IStock } from 'src/modules/common/types'
import { Box } from 'ui/layout'
import { format } from 'date-fns'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { Image } from 'ui/image'
import { ShadowedTooltip } from 'ui/tooltip'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { DotsIcon } from 'ui/icons'

export interface ProductTableItemProps {
  id?: string | number
  checkboxName?: string
  createdDate?: string
  name?: string
  vendorCode?: string
  price?: IPrice
  images?: IImage[]
  status?: {
    type: string
    updatedDate: string
  }
  queryKey: string
  stocks: IStock[]
  checkedList: string[]
  onChangeCbx: (id: string | number) => void
}

const ProductTableItem: FC<WithTranslation & ProductTableItemProps> = ({
  checkboxName,
  checkedList,
  images,
  name,
  id,
  vendorCode,
  price,
  status,
  stocks,
  onChangeCbx,
  t,
}) => {
  const statusDate = new Date(status.updatedDate.slice(0, -5))
  const imgUrl = images[0]
  const imageId = `productImage${id}`
  const onChangeCbxCached = useCallback(() => {
    onChangeCbx(id)
  }, [id])

  return (
    <Box display='flex' alignItems='center' width='100%' px='16px' minWidth='0'>
      <Box
        display='flex'
        alignItems='center'
        width='24%'
        minWidth='318px'
        px='16px'
        overflow='hidden'
      >
        <Box flex='0 0 36px' pr='16px'>
          <FormCheckbox
            id={id}
            name={checkboxName}
            value={id}
            checked={checkedList.some(itemId => itemId === id)}
            onChange={onChangeCbxCached}
          />
        </Box>

        <Box flex='0 0 40px' pr='8px'>
          <Box data-tip data-for={imageId}>
            <Image src={imgUrl?.small} alt={name} width={32} height={32} />
          </Box>
          <Condition match={!!imgUrl}>
            <ShadowedTooltip id={imageId} effect='solid' notFade>
              <Image src={imgUrl?.medium} alt={name} width={200} height={200} />
            </ShadowedTooltip>
          </Condition>
        </Box>

        <Box flex='1' pr='16px'>
          <Truncate
            color='secondary.0'
            fontSize='12px'
            lineHeight={1.25}
            fontWeight='500'
            limit={53}
          >
            {name}
          </Truncate>
        </Box>
      </Box>

      <Box width='16%' minWidth='216px' pr='16px'>
        <Text
          color='secondary.0'
          fontSize='12px'
          lineHeight={1.25}
          fontWeight='500'
        >
          {vendorCode}
        </Text>
      </Box>

      <Box width='8%' minWidth='106px' pr='16px'>
        <Text
          color='secondary.0'
          fontSize='12px'
          lineHeight={1}
          fontWeight='500'
        >
          {price.value.toLocaleString()}
          <Space />
          {price.currency}
        </Text>
      </Box>

      <Box width='12%' minWidth='146px' pr='16px'>
        <Text
          color='secondary.0'
          fontSize='12px'
          lineHeight={1.25}
          fontWeight='500'
        >
          -
        </Text>
      </Box>

      <Box width='16%' minWidth='146px' pr='16px'>
        <Truncate
          color='secondary.0'
          fontSize='12px'
          lineHeight={1.25}
          fontWeight='500'
          limit={55}
        >
          {stocks.map(stock => stock.name).join(', ')}
        </Truncate>
      </Box>

      <Condition match={status.type === 'PUBLISHED'}>
        <Box width='24%' minWidth='254px' pr='16px'>
          <Text
            color='secondary.0'
            fontWeight='500'
            fontSize='12px'
            lineHeight={1}
          >
            {t('publishedFrom')}
            <Space />
            {format(statusDate, 'dd.MM.yy')}
          </Text>
        </Box>
      </Condition>

      <Box
        position='absolute'
        right='0'
        height='100%'
        backgroundColor='white'
        display='flex'
        alignItems='center'
        pr='16px'
      >
        <Dropdown>
          <Dropdown.SimpleButton noCarret>
            <DotsIcon />
          </Dropdown.SimpleButton>
          <Dropdown.Menu direction='l' width='216px'>
            <Dropdown.Item>{t('shop:edit')}</Dropdown.Item>
            <Dropdown.Item>{t('shop:removeProduct')}</Dropdown.Item>
            <Dropdown.Item>{t('shop:removeFromSale')}</Dropdown.Item>
            <Dropdown.Item>{t('shop:archive')}</Dropdown.Item>
            <Dropdown.Item>{t('shop:submitForModeration')}</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Box>
    </Box>
  )
}

export default withTranslation(['common', 'shop'])(ProductTableItem)
