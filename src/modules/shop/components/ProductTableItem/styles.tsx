import styled from 'ui/theme/styled'
import { ifProp } from 'styled-tools'

export interface PanelProps {
  active?: boolean
  isTile?: boolean
}

export const Panel = styled('div', {
  shouldForwardProp: prop => !['isTile'].includes(prop),
})<PanelProps>`
  padding: 20px;
  border-radius: ${props => props.theme.borderRadius.s};
  background-color: ${props => props.theme.colorsList.secondary[5]};
  border: 2px solid;
  border-color: ${props => props.theme.colorsList.secondary[5]};
  cursor: pointer;
  transition: border-color ease 150ms;
  box-sizing: border-box;
  text-decoration: none;

  :hover {
    border-color: ${props => props.theme.color.primary};
  }

  :not(:last-child) {
    margin-bottom: 10px;
  }

  ${ifProp(
    'isTile',
    `
    display: inline-block;
    width: calc(50% - 5px);
    :nth-child(odd) {
      margin-right: 10px;
    }
  `,
    `
    display: grid;
    grid-template-columns: 70px 1fr;
    grid-template-rows: 14px 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 10px;
    .__img-wrapper {
      grid-column-start: 1;
      grid-column-end: 2;
    }
    .__controls {
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 1;
      grid-row-end: 2;
    }
    .__content {
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 2;
      grid-row-end: 3;
    }
    .__content-footer {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
  `,
  )}
`
