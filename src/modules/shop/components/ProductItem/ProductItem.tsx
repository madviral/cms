import React from 'react'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'

const ProductItem = () => (
  <Box
    borderRadius={theme.borderRadius.m}
    backgroundColor={theme.color.white}
    p={30}
    height='100%'
    boxSizing='border-box'
  >
    ProductItem
  </Box>
)

export default ProductItem
