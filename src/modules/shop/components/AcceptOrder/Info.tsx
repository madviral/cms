import React, { FC } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Divider } from 'ui/divider'
import { Text } from 'ui/text'
// import { ControledSelect } from 'ui/dropdowns/ControledSelect'
import { FileTextIcon } from 'ui/icons'
import Row from './Row'

const Info: FC<{} & WithTranslation> = ({ t }) => {
  return (
    <Box
      p='16px'
      borderRadius='8px'
      border={`1px solid ${theme.colorsList.secondary[1]}`}
    >
      <Box display='flex' alignItems='center' mb='16px'>
        <Box mr='16px'>
          <FileTextIcon />
        </Box>
        <Text
          fontSize='14px'
          lineHeight='1.25'
          fontWeight='600'
          color='secondary.0'
        >
          {t('orderInfo')}
        </Text>
      </Box>
      <Divider />
      {/* <Row title='Статус заказа'>
        <Text fontSize='12px' lineHeight='1.5' fontWeight='600' color='secondary.0'>
          Новый (Время на обработку - 9 ч. 57 мин.)
        </Text>
      </Row>
      <Row title='Резерв до'>
        <Box width='200px'>
          <ControledSelect 
            options={[{ value: '1', label: '1 день (28.06.2020)' }]}
            placeholder="Выберите склад"
            withoutPortal
            width='100%'
            onChange={console.info}
          />
        </Box>
      </Row> */}
      <Row title={t('orderDate')}>
        <Text
          fontSize='12px'
          lineHeight='1.5'
          fontWeight='600'
          color='secondary.0'
        >
          24.06.2020 22:00 {/* STUB */}
        </Text>
      </Row>
      <Box display='flex' justifyContent='space-between' alignItems='center'>
        <Box width='392px'>
          <Row title={t('amount')}>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='600'
              color='secondary.0'
            >
              80 000 {/* STUB */}
            </Text>
          </Row>
          <Row title={t('deliveryPrice')}>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='600'
              color='secondary.0'
            >
              Бесплатно {/* STUB */}
            </Text>
          </Row>
        </Box>
        <Box
          width='1px'
          height='84px'
          backgroundColor={`${theme.colorsList.secondary[2]}`}
        />
        <Box width='392px'>
          <Row title={t('paymentOption')}>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='600'
              color='secondary.0'
            >
              Оплата в кредит {/* STUB */}
            </Text>
          </Row>
          <Row title={t('deliveryOption')}>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='600'
              color='secondary.0'
            >
              vlife доставка {/* STUB */}
            </Text>
          </Row>
        </Box>
      </Box>
      <Row title={t('orderDeliveryDate')}>
        <Text
          fontSize='12px'
          lineHeight='1.5'
          fontWeight='600'
          color='secondary.0'
        >
          03.07.2020 {/* STUB */}
        </Text>
      </Row>
    </Box>
  )
}

export default withTranslation('shop')(Info)
