import React, { FC } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Divider } from 'ui/divider'
import { Text } from 'ui/text'
import { ClockIcon } from 'ui/icons'
import { Condition } from 'ui/condition'

type RowProps = {
  border?: boolean
}
const Row: FC<RowProps> = ({ border, children }) => {
  return (
    <>
      <Box display='flex' alignItems='center' height='48px'>
        {children}
      </Box>
      <Condition match={border}>
        <Box flexShrink='0'>
          <Divider />
        </Box>
      </Condition>
    </>
  )
}

const History: FC<{} & WithTranslation> = ({ t }) => {
  return (
    <Box
      p='16px'
      borderRadius='8px'
      border={`1px solid ${theme.colorsList.secondary[1]}`}
    >
      <Box display='flex' alignItems='center' mb='16px'>
        <Box mr='16px'>
          <ClockIcon color={theme.colorsList.secondary[1]} />
        </Box>
        <Text
          fontSize='14px'
          lineHeight='1.25'
          fontWeight='600'
          color='secondary.0'
        >
          {t('changeHistory')}
        </Text>
      </Box>
      <Divider />

      <Box
        display='flex'
        flexDirection='column'
        overflowY='auto'
        maxHeight='144px'
      >
        <Row border>
          <Box flex='1 1 250px'>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='600'
              color='secondary.0'
            >
              {t('date')}
            </Text>
          </Box>
          <Box flex='1 1 250px'>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='600'
              color='secondary.0'
            >
              {t('common:status')}
            </Text>
          </Box>
          <Box flex='1 1 250px'>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='600'
              color='secondary.0'
            >
              {t('user')}
            </Text>
          </Box>
          <Box flex='1 1 250px'>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='600'
              color='secondary.0'
            >
              {t('contacts')}
            </Text>
          </Box>
        </Row>
        <Row border>
          <Box flex='1 1 250px'>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='500'
              color='secondary.0'
            >
              24.06.2020
            </Text>
          </Box>
          <Box flex='1 1 250px'>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='500'
              color='secondary.0'
            >
              Оформлен новый заказ
            </Text>
          </Box>
          <Box flex='1 1 250px'>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='500'
              color='secondary.0'
            >
              Vlife
            </Text>
          </Box>
          <Box flex='1 1 250px'>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='500'
              color='secondary.0'
            >
              +7 705 740 78 1 7
            </Text>
            <Text
              fontSize='12px'
              lineHeight='1.5'
              fontWeight='500'
              color='secondary.0'
            >
              example@vlife.kz
            </Text>
          </Box>
        </Row>
      </Box>
    </Box>
  )
}

export default withTranslation()(History)
