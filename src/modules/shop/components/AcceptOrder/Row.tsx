import React, { FC } from 'react'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Divider } from 'ui/divider'
import { Text } from 'ui/text'
import { Condition } from 'ui/condition'

const defaultProps = {
  border: true,
}

type RowProps = {
  title: string
} & Partial<typeof defaultProps>

const Row: FC<RowProps> = ({ border, children, title }) => {
  return (
    <Box display='flex' flexDirection='column'>
      <Box display='flex' alignItems='center' height='48px'>
        <Box
          display='flex'
          alignItems='center'
          width='176px'
          height='32px'
          pr='12px'
          borderRight={`1px solid ${theme.colorsList.secondary[2]}`}
        >
          <Text
            fontSize='12px'
            lineHeight='1.5'
            fontWeight='500'
            color='secondary.0'
          >
            {title}
          </Text>
        </Box>
        <Box display='flex' alignItems='center' pl='16px'>
          {children}
        </Box>
      </Box>
      <Condition match={border}>
        <Divider />
      </Condition>
    </Box>
  )
}

Row.defaultProps = defaultProps

export default Row
