import React, { FC } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Divider } from 'ui/divider'
import { Space, Text } from 'ui/text'
import { MarkerIcon } from 'ui/icons'
import { FormField } from 'ui/inputs'

const Delivery: FC<WithTranslation> = ({ t }) => {
  return (
    <Box
      p='16px'
      borderRadius='8px'
      border={`1px solid ${theme.colorsList.secondary[1]}`}
    >
      <Box display='flex' alignItems='center' mb='16px'>
        <Box mr='10px'>
          <MarkerIcon />
        </Box>
        <Text
          fontSize='14px'
          lineHeight='1'
          fontWeight='600'
          color='secondary.0'
        >
          {t('deliveryAddress')}
        </Text>
      </Box>
      <Box mb='16px'>
        <Divider />
      </Box>
      <Box mb='16px'>
        <Box mb='2px'>
          <Text
            fontSize='12px'
            lineHeight='1'
            fontWeight='500'
            color='secondary.0'
          >
            {t('city')}
          </Text>
        </Box>
        <FormField value={'Усть - Каменогорск'} readOnly />
      </Box>

      <Box display='flex' justifyContent='space-between'>
        <Box width='256px' mr='8px'>
          <Box mb='2px'>
            <Text
              fontSize='12px'
              lineHeight='1'
              fontWeight='500'
              color='secondary.0'
            >
              {t('street')}
              <Space />
              {'/'}
              <Space />
              {t('district')}
            </Text>
          </Box>
          <FormField value={'ул. Нана'} readOnly />
        </Box>
        <Box flex='1 1 56px' mr='8px'>
          <Box mb='2px'>
            <Text
              fontSize='12px'
              lineHeight='1'
              fontWeight='500'
              color='secondary.0'
            >
              {t('house')}
            </Text>
          </Box>
          <FormField value={'278'} readOnly />
        </Box>
        <Box flex='1 1 56px'>
          <Box mb='2px'>
            <Text
              fontSize='12px'
              lineHeight='1'
              fontWeight='500'
              color='secondary.0'
            >
              {t('app.')}
            </Text>
          </Box>
          <FormField value={'44'} readOnly />
        </Box>
      </Box>
    </Box>
  )
}

export default withTranslation('shop')(Delivery)
