import React, { FC } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { NativeLink } from 'ui/links'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Divider } from 'ui/divider'
import { Text } from 'ui/text'
import { CalendarIcon, MailPlaneIcon, PhonePlaneIcon, VanIcon } from 'ui/icons'
import { Image } from 'ui/image'

const Courier: FC<WithTranslation> = ({ t }) => {
  return (
    <Box
      width='100%'
      p='16px'
      borderRadius='8px'
      border={`1px solid ${theme.colorsList.secondary[1]}`}
    >
      <Box display='flex' alignItems='center' mb='16px'>
        <Box mr='10px'>
          <VanIcon />
        </Box>
        <Text
          fontSize='14px'
          lineHeight='1.25'
          fontWeight='600'
          color='secondary.0'
        >
          {t('courierService')}
        </Text>
      </Box>
      <Box mb='16px'>
        <Divider />
      </Box>
      <Box display='flex' alignItems='center' mb='16px'>
        <Box width='32px' mr='12px'>
          <Image src='' alt='Dhl' width={32} height={32} borderRadius='r' />
        </Box>
        <Text
          fontSize='12px'
          lineHeight='1.5'
          fontWeight='500'
          color='secondary.0'
        >
          DHL
        </Text>
      </Box>
      <Box
        display='flex'
        alignItems='center'
        justifyContent='space-between'
        mb='16px'
      >
        <Box width='50%'>
          <NativeLink href='tel:+77057407817'>
            <Box display='flex' alignItems='center'>
              <Box width='20px' mr='8px' pt='4px'>
                <PhonePlaneIcon />
              </Box>
              <Text fontSize='12px'>+7 705 740 78 17</Text>
            </Box>
          </NativeLink>
        </Box>
        <Box width='50%'>
          <NativeLink href='mailto:example@vlife.kz'>
            <Box display='flex' alignItems='center'>
              <Box width='20px' mr='8px' pt='4px'>
                <MailPlaneIcon />
              </Box>
              <Text fontSize='12px'>example@vlife.kz</Text>
            </Box>
          </NativeLink>
        </Box>
      </Box>
      <Box display='flex' alignItems='center' mb='16px'>
        <Box display='flex' alignItems='center'>
          <Box width='20px' mr='8px' pt='4px'>
            <CalendarIcon />
          </Box>
          <Text fontSize='12px' lineHeight='1.5' color='secondary.0'>
            Пн-Сб 9:00-19:00 Вс выходной
          </Text>
        </Box>
      </Box>
    </Box>
  )
}

export default withTranslation('shop')(Courier)
