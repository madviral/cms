import React, { FC } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Divider } from 'ui/divider'
import { Text } from 'ui/text'
import { PackageIcon } from 'ui/icons'
import { Image } from 'ui/image'

const Product: FC<WithTranslation> = ({ t }) => {
  return (
    <Box
      width='100%'
      p='16px'
      borderRadius='8px'
      border={`1px solid ${theme.colorsList.secondary[1]}`}
      backgroundColor={theme.colorsList.primary[4]}
    >
      <Box display='flex' alignItems='center' mb='16px'>
        <Box mr='10px'>
          <PackageIcon />
        </Box>
        <Text
          fontSize='14px'
          lineHeight='1.25'
          fontWeight='600'
          color='secondary.0'
        >
          {t('order')}
        </Text>
      </Box>
      <Box mb='16px'>
        <Divider />
      </Box>
      <Box display='flex' mb='16px'>
        <Box width='70px' mr='16px'>
          <Image
            src=''
            alt='Apple iPhone 11 Pro Max 512 Гб, Темно-зеленый'
            width={70}
            height={70}
          />
        </Box>
        <Box display='flex' flexDirection='column' width='200px'>
          <Box display='flex' mb='4px'>
            <Text fontSize='10px' lineHeight='1' color='secondary.1'>
              #1000945454E
            </Text>
          </Box>
          <Box display='flex' mb='4px' maxHeight='36px' overflow='hidden'>
            <Text
              fontSize='14px'
              lineHeight='18px'
              fontWeight='600'
              color='secondary.0'
            >
              Apple iPhone 11 Pro Max 512 Гб, Темно-зеленый
            </Text>
          </Box>
          <Box display='flex' alignItems='center'>
            <Box display='flex' alignItems='center' width='90px' mr='16px'>
              <Text fontSize='12px' lineHeight='1.5' color='secondary.0'>
                700 000 KZT
              </Text>
            </Box>
            <Text fontSize='12px' lineHeight='1.5' color='secondary.0'>
              1 шт
            </Text>
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default withTranslation('shop')(Product)
