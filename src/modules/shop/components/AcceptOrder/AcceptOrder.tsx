import React, { FC } from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { theme } from 'ui/theme'
import { Button } from 'ui/buttons'
import Info from './Info'
import Buyer from './Buyer'
import Product from './Product'
import Delivery from './Delivery'
import Courier from './Courier'
import History from './History'

type AcceptOrder = {
  onClose: VoidFunction
} & WithTranslation

const AcceptOrder: FC<AcceptOrder> = ({ t, onClose }) => {
  return (
    <Box
      display='flex'
      flexDirection='column'
      height='100%'
      backgroundColor='#fff'
      p='1px'
    >
      <Box flex='1 1 auto' p='16px' overflowY='auto'>
        <Box mb='16px'>
          <Info />
        </Box>
        <Box display='flex' justifyContent='space-between' mb='16px'>
          <Box display='flex' width='416px'>
            <Buyer />
          </Box>

          <Box display='flex' width='416px'>
            <Product />
          </Box>
        </Box>
        <Box
          display='flex'
          justifyContent='space-between'
          alignItems='stretch'
          mb='16px'
        >
          <Box display='flex' width='416px'>
            <Delivery />
          </Box>

          <Box display='flex' width='416px'>
            <Courier />
          </Box>
        </Box>
        <Box>
          <History />
        </Box>
      </Box>
      <Box
        flex='0 0 66px'
        display='flex'
        alignItems='center'
        px='10px'
        mx='16px'
        justifyContent='flex-end'
        borderTop={`1px solid ${theme.colorsList.secondary[2]}`}
      >
        <Box mr='16px'>
          <Button onClick={onClose} appearance='secondary' size='m'>
            {t('back')}
          </Button>
        </Box>
        <Box mr='16px' ml='auto'>
          <Button onClick={onClose} appearance='outline' size='m'>
            {t('cancelOrder')}
          </Button>
        </Box>
        <Box>
          <Button appearance='primary' size='m' disabled>
            {t('acceptOrder')}
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

export default withTranslation('shop')(AcceptOrder)
