import { styled } from 'ui/theme'

interface IconWrapProps {
  isDisabledNewStock: boolean
}

const IconWrap = styled.div<IconWrapProps>`
  float: left;
  marginright: 8px;
  cursor: ${props => (props.isDisabledNewStock ? 'not-allowed' : 'pointer')};
`

export default IconWrap
