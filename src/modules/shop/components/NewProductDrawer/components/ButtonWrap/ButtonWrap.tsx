import { styled } from 'ui/theme'

const ButtonWrap = styled.div`
  margin-left: 16px;
  display: inline-block;
`

export default ButtonWrap
