import React, { FC } from 'react'
import { styled } from 'ui/theme'
import { FormRadioButton } from 'src/ui/inputs'

const Wrap = styled.li`
  max-height: 102px;
  display: grid;
  grid-template-columns: 36px auto;
  line-height: 102px;
  margin-bottom: 8px;
  padding: 0 0 0 16px;
  font-size: 10px;
  color: ${props => props.theme.colorsList.secondary[0]};
`

// TODO: change second auto to 199px
const InfoWrap = styled.div`
  height: 100%;
  border-radius: ${props => props.theme.borderRadius.xs};
  background: ${props => props.theme.colorsList.secondary[4]};
  padding: 16px;
  display: grid;
  grid-template-columns: 70px auto auto;

  grid-gap: 16px;
  box-sizing: border-box;
  max-width: 548px;
  & > div {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: hidden;
    & > p,
    b {
      float: left;
    }
  }
`

const ImageWrap = styled.div`
  max-width: 70px;
  max-height: 70px;
  text-align: center;
  & > img {
    max-height: 70px;
    max-width: 70px;
    border-radius: ${props => props.theme.borderRadius.xs};
  }
`

interface HintProps {
  key: any
  id: any
  smallImage: string
  vendorCode: string
  name: string
  categories?: any
  callback: (id: number) => void
  checked: string
}

const Hint: FC<HintProps> = ({
  id,
  smallImage,
  vendorCode,
  name,
  categories,
  callback,
  checked,
}) => {
  const handleRadioButtonChange = (e: any) => {
    callback(id)
  }

  // TODO: for the categories
  //  useEffect(() => {
  //    categories.map(item => {
  //      loadDraftItemAttributes(id)
  //    })
  //  }, [])

  return (
    <Wrap>
      <FormRadioButton
        id={name}
        name={name}
        onChange={handleRadioButtonChange}
        value={name}
        checked={name === checked}
      />
      <InfoWrap>
        <ImageWrap>
          <img src={smallImage} alt={name} />
        </ImageWrap>
        <div style={{ lineHeight: '10px', textAlign: 'left' }}>
          <p style={{ width: '100%' }}>{vendorCode}</p>
          <b style={{ fontSize: '14px', marginTop: '4px' }}>{name}</b>
          <div>{}</div>
        </div>
        <div style={{ background: '' }}></div>
      </InfoWrap>
    </Wrap>
  )
}

export default Hint
