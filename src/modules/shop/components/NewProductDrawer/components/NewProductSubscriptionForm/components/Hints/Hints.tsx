import React, { FC, useState, useEffect } from 'react'
import { styled, theme } from 'ui/theme'
import { Caption } from 'src/ui/caption'
import { PackageIcon } from 'src/ui/icons'
import { useTranslation } from 'react-i18next'
import { Hint } from './components'
import { Box } from 'ui/layout'

interface Expandable {
  isExpanded: boolean
}

interface HintsProps {
  vendorCode: string
  selectedSimilarItems: any[]
  setSelectedSimilarItems: (value: any[]) => void
  data: any
}

const Wrap = styled.div`
  font-size: 12px;
  min-height: 100%;
`

const HintsPlaceholder = styled.div<Expandable>`
  text-align: center;
  max-height: ${props => (props.isExpanded ? '239px' : '279px')};
  width: 70%;
  position: relative;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -75%);
`

const Hints: FC<Expandable & HintsProps> = ({
  isExpanded,
  selectedSimilarItems,
  setSelectedSimilarItems,
  data,
  vendorCode,
}) => {
  const [t] = useTranslation('shop')
  const [similarItems, setSimilarItems] = useState([])

  useEffect(() => {
    setSimilarItems([])
    if (data !== null) {
      setSimilarItems([data].flat())
    }
    //    if (!vendorCode) {
    //      setSimilarItems([])
    //    }
  }, [data])

  return (
    <Wrap>
      <Caption fontSize={theme.fontSize.s}>{t('similarItems') + ':'}</Caption>
      {similarItems.length === 0 ? (
        <HintsPlaceholder isExpanded={isExpanded}>
          <PackageIcon height={40} width={40} />
          <Caption
            color={theme.colorsList.secondary[0]}
            fontSize={theme.fontSize.l}
          >
            Заполните минимум один из основных пунктов что бы получить список
            похожих товаров
          </Caption>
        </HintsPlaceholder>
      ) : (
        <Box
          overflow='auto'
          overflowX='hidden'
          height='calc(100% - 16px)'
          boxSizing='border-box'
          padding='4px 0'
        >
          <ul>
            {similarItems.map(item => (
              <Hint
                key={item.id}
                id={item.id}
                smallImage={item.smallImage}
                vendorCode={item.vendorCode}
                name={item.name}
                categories={item.categories}
                checked={selectedSimilarItems[0]?.name}
                callback={id =>
                  setSelectedSimilarItems(
                    similarItems.filter(similarItem => similarItem.id === id),
                  )
                }
              />
            ))}
          </ul>
        </Box>
      )}
    </Wrap>
  )
}

export default Hints
