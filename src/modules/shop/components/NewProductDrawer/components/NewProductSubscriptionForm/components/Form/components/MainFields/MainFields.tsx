import React, { useRef, FC } from 'react'
import { theme } from 'ui/theme'
import { FormField } from 'src/ui/inputs'
import { useTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { DebounceInput } from 'react-debounce-input'

interface FormProps {
  formData: {
    productCode: string
    vendorCode: string
    barcode: string
    productName: string
    category: {}
    brand: string
    price: string
    stock: any[]
  }
  callback: any
}

const MainFields: FC<FormProps> = ({ formData, callback }) => {
  const [t] = useTranslation('shop')
  const [e] = useTranslation('errorMessages')
  const inputRef = useRef()

  return (
    <Box
      display='grid'
      gridTemplateRows='60px 60px 60px'
      gridTemplateColumns='276px 276px'
      gridGap='16px'
      border={'1px solid ' + theme.colorsList.secondary[0]}
      borderRadius={theme.borderRadius.xs}
      padding='16px'
    >
      <FormField
        value={formData.productCode}
        label={t('itemCodeNumber') + ' *'}
        placeholder={t('enterItemCodeNumber')}
        labelFontSize={12}
        onChange={event =>
          callback({ ...formData, productCode: event.target.value })
        }
        error={e('productCodeError')}
      />

      <DebounceInput
        element={FormField as any}
        autoComplete='off'
        autoCorrect='off'
        value={formData.vendorCode}
        label={t('vendorCode') + ' *'}
        placeholder={t('enterVendorCode')}
        inputRef={inputRef}
        labelFontSize={12}
        onChange={event =>
          callback({ ...formData, vendorCode: event.target.value })
        }
        debounceTimeout={300}
        error={e('vendorCodeError')}
      />

      <FormField
        value={formData.barcode}
        label={t('barCode')}
        placeholder={t('barCode13')}
        labelFontSize={12}
        onChange={event =>
          callback({ ...formData, barcode: event.target.value })
        }
      />
      <FormField
        value={formData.brand}
        label={t('brand') + ' *'}
        placeholder={t('productBrand')}
        labelFontSize={12}
        onChange={event => callback({ ...formData, brand: event.target.value })}
        error={e('brandError')}
      />
      <div style={{ gridColumn: '1/-1' }}>
        <FormField
          value={formData.productName}
          label={t('productName') + ' *'}
          placeholder={t('enterMerchantProductName')}
          labelFontSize={12}
          onChange={event =>
            callback({ ...formData, productName: event.target.value })
          }
          error={e('productNameError')}
        />
      </div>
    </Box>
  )
}

export default MainFields
