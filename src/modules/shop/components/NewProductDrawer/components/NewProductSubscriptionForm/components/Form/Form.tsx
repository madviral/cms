import React, { FC, useEffect, useState } from 'react'
import { theme } from 'ui/theme'
import { Treeselect } from 'src/ui/dropdowns/Treeselect'
import { MainFields } from './components'
import { FormField } from 'src/ui/inputs'
import { useTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { Caption } from 'src/ui/caption'
import { AddStock, AdditionalStock } from '../../../'
import { TreeselectChangeOptions } from '../../../../constants/treeselectChangeOptions'

interface FormProps {
  isExpanded: boolean
  formData: any
  setFormData: (value: any) => void
  setStockSelectorArray: (value: any[]) => void
  stockSelectorArray: any[]
  loadStocks: () => void
  loadCategories: () => void
  stocks: any
  categories: any
  test: any
}

const Form: FC<FormProps> = ({
  isExpanded,
  formData,
  setFormData,
  setStockSelectorArray,
  stockSelectorArray,
  loadStocks,
  loadCategories,
  stocks,
  categories,
  test,
}) => {
  const [t] = useTranslation('shop')
  const [a] = useTranslation('common')
  const [e] = useTranslation('errorMessages')
  const [stocksArray, setStocksArray] = useState([])
  const [is500, setIs500] = useState(false)

  useEffect(() => {
    setIs500(test.error?.message.includes('500'))
    setFormData({ ...formData, price: '', stock: [] })
  }, [test.error?.message])

  useEffect(() => {
    setIs500(false)
  }, [formData.price, formData.stock])

  useEffect(() => {
    if (!categories) {
      loadCategories()
    }
    if (stocks.length === 0) {
      loadStocks()
    }
  }, [])

  useEffect(() => {
    setStocksArray(
      stocks.map(item => {
        return {
          id: item.id,
          name: Object.values(item.address).join(', '),
        }
      }),
    )
  }, [stocks])

  const deleteStock = (index: number) => {
    const buffer = [...stockSelectorArray]
    buffer.splice(index, 1)
    setStockSelectorArray(buffer)
    setFormData({
      ...formData,
      stock: formData.stock.filter((item, i) => i !== index + 1),
    })
  }

  const treeselectChange = (value: any, flag: string) => {
    switch (flag) {
      case TreeselectChangeOptions.Category:
        setFormData({ ...formData, category: value })
        break
      case TreeselectChangeOptions.DeleteCategory:
        setFormData({
          ...formData,
          category: {
            children: [],
            description: '',
            id: 0,
            image: {},
            isAccent: null,
            isTabbedChildren: null,
            name: '',
          },
        })
        break
      case TreeselectChangeOptions.Stock:
        const buff = [...formData.stock]
        buff[0] = value
        setFormData({ ...formData, stock: buff })
        break
      case TreeselectChangeOptions.AdditionalStock:
        setFormData({
          ...formData,
          stock: [...new Set([...formData.stock, value])],
        })
        break
      case TreeselectChangeOptions.DeleteItem:
        const newArray = [...formData.stock]
        setFormData({
          ...formData,
          stock: newArray.filter(item => item.id !== value[0]),
        })
        break
      default:
        break
    }
  }

  return (
    <Box
      display='grid'
      gridTemplateRows={'auto 60px 60px ' + (isExpanded ? '156px' : '116px')}
      gridGap='16px'
    >
      <MainFields formData={formData} callback={setFormData} />
      <Box>
        <Box height='16px' marginBottom='4px'>
          <Caption fontSize={theme.fontSize.s}>{t('category') + ' *'}</Caption>
        </Box>
        <Treeselect
          placeholder={t('chooseCategory')}
          options={categories}
          isSingleItemSelector
          callback={(value, flag?) =>
            treeselectChange(value, flag ? 'deleteCategory' : 'category')
          }
          error={e('chooseCategoryError')}
          isAbsolute
        />
      </Box>
      <Box fontSize='12px'>
        <FormField
          id='1'
          type={'text'}
          value={formData.price}
          labelFontSize={12}
          label={a('price') + ' *'}
          placeholder={t('enterThePrice')}
          shouldDisplayError={is500}
          onChange={event =>
            setFormData({ ...formData, price: event.target.value })
          }
          error={is500 ? e('error500price') : e('priceError')}
        />
      </Box>
      <Box>
        <Box marginBottom='4px'>
          <Caption fontSize={theme.fontSize.s}>{t('stock') + ' 1 *'}</Caption>
        </Box>
        <Treeselect
          placeholder={t('chooseStock')}
          options={stocksArray}
          isSingleItemSelector
          isAbsolute
          shouldDisplayError={is500}
          callback={(value, flag?) => treeselectChange(value, flag || 'stock')}
          error={is500 ? e('error500stock') : e('chooseStockError')}
        />
        <Box
          maxHeight={isExpanded ? '80px' : '40px'}
          overflow='auto'
          overflowX='hidden'
          margin='16px 0 16px 0'
        >
          <ul>
            {stockSelectorArray.map((stock, i) => {
              return (
                <AdditionalStock
                  key={i}
                  i={i}
                  stocksArray={stocksArray}
                  treeselectChange={treeselectChange}
                  deleteStock={deleteStock}
                  isInitialDrawer
                />
              )
            })}
            <AddStock
              isExpanded={isExpanded}
              setStockSelectorArray={setStockSelectorArray}
              stockSelectorArray={stockSelectorArray}
              stock={formData.stock}
            />
          </ul>
        </Box>
      </Box>
    </Box>
  )
}

export default Form
