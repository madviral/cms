import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { load as loadCategories } from 'src/modules/common/actions/productCategories'
import { load as loadStocks } from 'src/modules/shop/actions/stock'
import { Form } from '../../components'

const mapStateToProps = (state: RootState) => ({
  categories: state.common.general.productCategories.data,
  stocks: state.shop.stock.list,
  items: state.shop.catalog.list,
  test: state.shop.subscribe,
})

const mapDispatchToProps = {
  loadStocks,
  loadCategories,
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)
