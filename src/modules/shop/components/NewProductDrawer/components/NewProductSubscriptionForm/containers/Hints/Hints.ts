import { connect } from 'react-redux'
import { Hints } from '../../components'
import { RootState } from 'src/store/rootReducer'

const mapStateToProps = (state: RootState) => ({
  data: state.shop.newProduct.data,
})

export default connect(mapStateToProps)(Hints)
