import React, { FC, useState, useEffect, useMemo } from 'react'
import { theme } from 'ui/theme'
import { Caption } from 'src/ui/caption'
import { useTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { Hints, Form } from './containers'
import { schema } from '../../constants/schema'

interface NewProductDrawerProps {
  triggerNewProductDrawer: () => void
  loadSuggestions: (value: any) => void
  subscribeStatus: any
  passShouldCreateDraftItem: (value: boolean) => void
  passFormData: (formData: any) => void
  passSelectedSimilarItems: (value: any[]) => void
  data: any
}

const NewProductSubscriptionForm: FC<NewProductDrawerProps> = ({
  triggerNewProductDrawer,
  passShouldCreateDraftItem,
  subscribeStatus,
  data,
  passSelectedSimilarItems,
  passFormData,
  loadSuggestions,
}) => {
  const [t] = useTranslation('shop')
  const [stockSelectorArray, setStockSelectorArray] = useState([])
  const [selectedSimilarItems, setSelectedSimilarItems] = useState([])
  const [isExpanded, setIsExpanded] = useState(false)
  const [formData, setFormData] = useState({
    productCode: '',
    vendorCode: '',
    barcode: '',
    productName: '',
    category: {
      children: [],
      description: '',
      id: 0,
      image: {},
      isAccent: null,
      isTabbedChildren: null,
      name: '',
    },
    brand: '',
    price: '',
    stock: [],
  })

  useMemo(() => {
    if (!data) {
      setSelectedSimilarItems([])
    }
  }, [data])

  useEffect(() => {
    setIsExpanded(stockSelectorArray.length > 0)
  }, [stockSelectorArray])

  useEffect(() => {
    loadSuggestions(formData.vendorCode)
  }, [formData.vendorCode])

  useEffect(() => {
    passFormData(formData)
    schema.isValid(formData).then(valid => {
      passShouldCreateDraftItem(!valid)
      passSelectedSimilarItems(selectedSimilarItems)
    })
  }, [formData, selectedSimilarItems])

  return (
    <Box
      maxWidth='100%'
      height='100%'
      padding='16px 16px 66px 16px'
      boxSizing='border-box'
      display='grid'
      gridTemplateRows={'23px ' + (isExpanded ? '568px' : '528px') + ' auto'}
      overflow='hidden'
    >
      <Box marginBottom='7px'>
        <Caption fontSize={theme.fontSize.s}>{t('userNewProduct')}</Caption>
      </Box>
      <Form
        isExpanded={isExpanded}
        formData={formData}
        setFormData={setFormData}
        setStockSelectorArray={setStockSelectorArray}
        stockSelectorArray={stockSelectorArray}
      />
      <Hints
        vendorCode={formData.vendorCode}
        isExpanded={isExpanded}
        selectedSimilarItems={selectedSimilarItems}
        setSelectedSimilarItems={setSelectedSimilarItems}
      />
    </Box>
  )
}

export default NewProductSubscriptionForm
