import React, { FC, useState, useEffect } from 'react'
import { FormField } from 'src/ui/inputs/FormField'
import { Treeselect } from 'src/ui/dropdowns/Treeselect'
import { Box } from 'ui/layout'
import { Caption } from 'src/ui/caption'
import { useTranslation } from 'react-i18next'
import { theme } from 'ui/theme'
import { AddStock, AdditionalStock } from '../../'
import { schema } from '../../../constants/schema'
import { TreeselectChangeOptions } from '../../../constants/treeselectChangeOptions'

interface NewProductFormProps {
  draftProductInfo: any
  loadDraftItemAttributes: (id: number) => void
  loadStocks: () => void
  loadCategories: () => void
  formData: any
  setFormData: (value: any) => void
  categories: any
  stocks: any
  activeTabIndex: number
}

interface DrawerChildProps {
  callback: (value: boolean) => void
}

const NewProductForm: FC<NewProductFormProps & DrawerChildProps> = ({
  draftProductInfo,
  loadDraftItemAttributes,
  callback,
  formData,
  setFormData,
  loadStocks,
  loadCategories,
  categories,
  stocks,
  activeTabIndex,
}) => {
  const [stockSelectorArray, setStockSelectorArray] = useState(
    formData.stock.slice(1),
  )
  const [stocksArray, setStocksArray] = useState([])
  const [t] = useTranslation(['shop', 'common', 'errorMessages'])
  const [innerFormData, setInnerFormData] = useState(formData)

  const treeselectChange = (value: any, flag: string) => {
    switch (flag) {
      case TreeselectChangeOptions.Category:
        setInnerFormData({ ...innerFormData, category: value })
        break
      case TreeselectChangeOptions.DeleteCategory:
        setInnerFormData({ ...innerFormData, category: {} })
        break
      case TreeselectChangeOptions.Stock:
        const buff = [...innerFormData.stock]
        buff[0] = value
        setInnerFormData({ ...innerFormData, stock: buff })
        break
      case TreeselectChangeOptions.AdditionalStock:
        setInnerFormData({
          ...innerFormData,
          stock: [...new Set([...innerFormData.stock, value])],
        })
        break
      case TreeselectChangeOptions.DeleteItem:
        const newArray = [...formData.stock]
        setInnerFormData({
          ...innerFormData,
          stock: newArray.filter(item => item.id !== value[0]),
        })
        break
      default:
        break
    }
  }

  useEffect(() => {
    if (!categories) {
      loadCategories()
    }
    if (stocks.length === 0) {
      loadStocks()
    }
    if (draftProductInfo.data) {
      loadDraftItemAttributes(draftProductInfo.data.id)
    }
  }, [])

  useEffect(() => {
    if (draftProductInfo.attributesUpdated) {
      setFormData(innerFormData)
    }
  }, [draftProductInfo.attributesUpdated])

  useEffect(() => {
    schema.isValid(innerFormData).then(valid => {
      callback(!valid)
    })
  }, [innerFormData])

  useEffect(() => {
    setStocksArray(
      stocks.map(item => {
        return {
          id: item.id,
          name: Object.values(item.address).join(', '),
        }
      }),
    )
  }, [stocks])

  const deleteStock = (index: number) => {
    const buffer = [...stockSelectorArray]
    buffer.splice(index, 1)
    setStockSelectorArray(buffer)
    setInnerFormData({
      ...innerFormData,
      stock: innerFormData.stock.filter((item, i) => i !== index + 1),
    })
  }

  return (
    <Box
      padding='16px 16px 0 16px'
      display='grid'
      gridTemplateRows='60px 60px 60px 60px 60px auto'
      gridGap='16px'
      height='calc(100% - 114px)'
      boxSizing='border-box'
      position='absolute'
    >
      <Box
        display='grid'
        gridTemplateColumns='160px 240px 168px'
        gridGap='16px'
      >
        <FormField
          label={t('itemCodeNumber') + ' *'}
          value={innerFormData.productCode}
          labelFontSize={12}
          placeholder={t('enterItemCodeNumber')}
          onChange={event =>
            setInnerFormData({
              ...innerFormData,
              productCode: event.target.value,
            })
          }
          error={t('productCodeError')}
        />
        <FormField
          label={t('itemNumber') + ' *'}
          value={innerFormData.vendorCode}
          labelFontSize={12}
          placeholder={t('enterVendorCode')}
          onChange={event =>
            setInnerFormData({
              ...innerFormData,
              vendorCode: event.target.value,
            })
          }
          error={t('vendorCodeError')}
        />
        <FormField
          label={t('barCode')}
          value={innerFormData.barcode}
          labelFontSize={12}
          placeholder={t('barCode13')}
          onChange={event =>
            setInnerFormData({ ...innerFormData, barcode: event.target.value })
          }
        />
      </Box>
      <Box>
        <FormField
          label={t('productName') + ' *'}
          value={innerFormData.productName}
          labelFontSize={12}
          placeholder={t('enterMerchantProductName')}
          onChange={event => {
            setInnerFormData({
              ...innerFormData,
              productName: event.target.value,
            })
          }}
          error={t('productNameError')}
        />
      </Box>
      <Box>
        <Box width='100%' marginBottom='8px'>
          <Caption fontSize={theme.fontSize.s}>{t('category') + ' *'}</Caption>
        </Box>
        <Treeselect
          placeholder={t('chooseCategory')}
          options={categories}
          value={innerFormData.category?.name}
          callback={(value, flag?) =>
            treeselectChange(value, flag ? 'deleteCategory' : 'category')
          }
          isAbsolute
          error={t('chooseCategoryError')}
        />
      </Box>
      <Box display='grid' gridTemplateColumns='292px 292px' gridGap='16px'>
        <FormField
          label={t('brand') + ' *'}
          value={innerFormData.brand}
          labelFontSize={12}
          placeholder={t('productBrand')}
          onChange={event =>
            setInnerFormData({ ...innerFormData, brand: event.target.value })
          }
          error={t('brandError')}
        />
        <FormField
          label={t('price') + ' *'}
          value={innerFormData.price}
          labelFontSize={12}
          placeholder={t('enterPrice')}
          onChange={event =>
            setInnerFormData({ ...innerFormData, price: event.target.value })
          }
          error={t('priceError')}
        />
      </Box>
      <Box>
        <Box marginBottom='8px' display='inline-block' gridColumn='1/-1'>
          <Caption fontSize={theme.fontSize.s}>{t('stock') + ' 1 *'}</Caption>
        </Box>
        <Treeselect
          placeholder={t('chooseStock')}
          options={stocksArray}
          callback={(value, flag?) => treeselectChange(value, flag || 'stock')}
          isAbsolute
          value={
            innerFormData.stock?.length > 0 ? innerFormData.stock[0].name : ''
          }
          error={t('chooseStockError')}
        />
      </Box>
      <Box overflow='auto' overflowX='hidden' width='calc(100% + 16px)'>
        <ul>
          {stockSelectorArray.map((stock, i) => {
            return (
              <AdditionalStock
                key={i}
                i={i}
                stocksArray={stocksArray}
                value={stock}
                treeselectChange={treeselectChange}
                deleteStock={deleteStock}
              />
            )
          })}
          <AddStock
            setStockSelectorArray={setStockSelectorArray}
            stockSelectorArray={stockSelectorArray}
            stock={formData.stock}
            isExpanded
          />
        </ul>
      </Box>
    </Box>
  )
}

export default NewProductForm
