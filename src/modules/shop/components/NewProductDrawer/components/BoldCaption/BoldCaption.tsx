import React, { FC } from 'react'
import { styled, theme } from 'ui/theme'
import { Box } from 'ui/layout'

interface BoldCaptionProps {
  isDisabledNewStock: boolean
}

const BoldCaptionWrapper = styled.div`
  display: block;
  margin: auto;
`

const BoldCaption: FC<BoldCaptionProps> = ({
  isDisabledNewStock,
  children,
  ...props
}) => {
  return (
    <BoldCaptionWrapper>
      <Box
        color={theme.colorsList.primary[0]}
        cursor={isDisabledNewStock ? 'not-allowed' : 'pointer'}
      >
        <b style={{ fontSize: '12px' }}>{children}</b>
      </Box>
    </BoldCaptionWrapper>
  )
}

export default BoldCaption
