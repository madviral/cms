import React, { FC, useState, useEffect } from 'react'
import { styled, theme } from 'ui/theme'
import { PlusIcon } from 'src/ui/icons'
import { useTranslation } from 'react-i18next'
import { BoldCaption, IconWrap } from '../'

interface AddStockWrapperProps {
  isDisabledNewStock?: boolean
  isExpanded?: boolean
}

interface AddStockProps {
  setStockSelectorArray: (value: any[]) => void
  stockSelectorArray: any[]
  stock: any[]
}

interface Clickable {
  onClick: () => void
}

const Wrap = styled.div<AddStockWrapperProps & Clickable>`
  height: 20px;
  max-width: 200px;
  cursor: ${props => (props.isDisabledNewStock ? 'not-allowed' : 'pointer')};
  margin-bottom: ${props => props.isExpanded && '16px'};
`

const AddStock: FC<AddStockProps & AddStockWrapperProps> = ({
  stock,
  isExpanded,
  setStockSelectorArray,
  stockSelectorArray,
}) => {
  const [isDisabledNewStock, setIsDisabledNewStock] = useState(true)

  useEffect(() => {
    setIsDisabledNewStock(stock.length === 0)
  }, [stock])

  const [t] = useTranslation('shop')
  return (
    <Wrap
      onClick={() =>
        !isDisabledNewStock && setStockSelectorArray([...stockSelectorArray, 1])
      }
      isDisabledNewStock={isDisabledNewStock}
      isExpanded={isExpanded}
    >
      <IconWrap isDisabledNewStock={isDisabledNewStock}>
        <PlusIcon color={theme.colorsList.primary[0]} />
      </IconWrap>
      <BoldCaption isDisabledNewStock={isDisabledNewStock}>
        {t('addStock')}
      </BoldCaption>
    </Wrap>
  )
}

export default AddStock
