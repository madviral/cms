import React, { FC } from 'react'
import { Trash } from 'src/ui/icons'
import { Treeselect } from 'src/ui/dropdowns/Treeselect'
import { Box } from 'ui/layout'
import { Caption } from 'ui/caption'
import { useTranslation } from 'react-i18next'
import { theme } from 'ui/theme'

interface AdditionalStockProps {
  i: number
  stocksArray: any[]
  treeselectChange: (value: string, flag?: string) => void
  deleteStock: (id: number) => void
  isInitialDrawer?: boolean
  value?: any
}

const AdditionalStock: FC<AdditionalStockProps> = ({
  i,
  stocksArray,
  treeselectChange,
  deleteStock,
  isInitialDrawer,
  value,
}) => {
  const [t] = useTranslation('shop')
  return (
    <li key={i}>
      <Box
        height='60px'
        display='grid'
        gridTemplateColumns={isInitialDrawer ? '530px 60px' : '564px auto'}
        gridTemplateRows='20px 40px'
        marginBottom='16px'
      >
        <Box
          fontSize='14px'
          marginBottom='8px'
          color={theme.colorsList.secondary[0]}
          display='inline-block'
          gridColumn='1/-1'
        >
          <Caption fontSize={'12px'}>{t('stock') + ' ' + (2 + i)}</Caption>
        </Box>
        <Treeselect
          options={stocksArray}
          placeholder={t('chooseStock')}
          value={value?.name || ''}
          callback={(item, flag?) =>
            treeselectChange(item, flag || 'additionalStock')
          }
          isAbsolute
        />
        <Box
          height='40px'
          lineHeight='45px'
          textAlign='center'
          position='relative'
          marginLeft='calc(50% - 10px)'
          marginTop={isInitialDrawer && 'calc(50% - 20px)'}
          top={!isInitialDrawer && '25%'}
          onClick={() => deleteStock(i)}
        >
          <Trash />
        </Box>
      </Box>
    </li>
  )
}

export default AdditionalStock
