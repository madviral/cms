import React, { FC, useEffect } from 'react'
import { styled } from 'ui/theme'
import { ButtonWrap } from '../'
import { Button } from 'ui/buttons'
import { useTranslation } from 'react-i18next'

interface FooterProps {
  draftProductInfo: any
  update: (value: any[]) => void
  isInitialDrawer: boolean
  isDisabled: boolean
  selectedAttributes: any
  createOrUpdateDraft: (postData: any, callback: () => void) => void
  triggerModal: (value: string) => void
  setIsInitialDrawer: (value: boolean) => void
  subscribe: (formData: any, id: number) => void
  formData: any
  selectedSimilarItems: any[]
  triggerNewProductDrawer: () => void
  passFormData: (formData: any) => void
  isEmptyAttributes: boolean
  shouldTriggerModal?: boolean
  subscribeStatus?: boolean
}

const Wrap = styled.div`
  position: absolute;
  bottom: 0;
  background: white;
  right: 16px;
  width: calc(100% - 32px);
  height: 66px;
  box-sizing: border-box;
  border-top: 1px solid ${props => props.theme.colorsList.secondary[2]};
  padding: 16px 0 16px 0;
  text-align: right;
`

const Footer: FC<FooterProps> = ({
  draftProductInfo,
  update,
  isInitialDrawer,
  setIsInitialDrawer,
  isDisabled,
  selectedAttributes,
  createOrUpdateDraft,
  triggerModal,
  subscribe,
  subscribeStatus,
  formData,
  selectedSimilarItems,
  triggerNewProductDrawer,
  passFormData,
  isEmptyAttributes,
  shouldTriggerModal,
}) => {
  const [t] = useTranslation(['shop', 'common'])

  useEffect(() => {
    if (subscribeStatus) {
      triggerNewProductDrawer()
    }
  }, [subscribeStatus])

  useEffect(() => {
    if (draftProductInfo.attributesUpdated) {
      createOrUpdateDraft(
        {
          id: draftProductInfo.data.id,
          name: formData.productName,
          vendorCode: formData.productCode,
          productCode: formData.vendorCode,
          price: formData.price,
          groupId: 0,
          categoryId: formData.category.id,
          stockIds: formData.stock.map(item => item.id),
        },
        () => null,
      )
    }
  }, [draftProductInfo.attributesUpdated, formData])

  return (
    <Wrap>
      {isInitialDrawer ? (
        <>
          <Button
            size='m'
            appearance='outline'
            style={{ float: 'left' }}
            onClick={() => {
              createOrUpdateDraft(
                {
                  name: formData.productName,
                  vendorCode: formData.productCode,
                  productCode: formData.vendorCode,
                  price: formData.price,
                  groupId: 0,
                  categoryId: formData.category.id,
                  stockIds: formData.stock.map(item => item.id),
                },
                () => passFormData(formData),
              )
              setIsInitialDrawer(!isInitialDrawer)
            }}
            disabled={isDisabled}
          >
            {t('hintDoesntMatch')}
          </Button>
          <Button
            size='m'
            appearance='secondary'
            style={{ marginRight: '16px' }}
            onClick={() => {
              triggerNewProductDrawer()
              if (shouldTriggerModal) {
                triggerModal('newItem')
              }
            }}
          >
            {t('cancel')}
          </Button>
          <Button
            size='m'
            appearance='primary'
            disabled={isDisabled || selectedSimilarItems.length === 0}
            onClick={() => {
              subscribe(
                {
                  merchantVendorCode: formData.vendorCode,
                  price: formData.price,
                  productCode: formData.productCode,
                  // HARDCODED
                  quantity: 1,
                  stockId: formData.stock[0].id,
                },
                selectedSimilarItems[0].id,
              )
            }}
          >
            {t('sign')}
          </Button>
        </>
      ) : (
        <>
          <ButtonWrap>
            <Button
              size='m'
              appearance='secondary'
              onClick={() => setIsInitialDrawer(!isInitialDrawer)}
            >
              {t('back')}
            </Button>
          </ButtonWrap>
          <ButtonWrap>
            <Button
              size='m'
              appearance='primary'
              disabled={
                (!selectedAttributes && !isEmptyAttributes) || isDisabled
              }
              onClick={() => {
                selectedAttributes
                  ? update(Object.values(selectedAttributes))
                  : update([])
              }}
            >
              {t('submitForModeration')}
            </Button>
          </ButtonWrap>
        </>
      )}
    </Wrap>
  )
}

Footer.defaultProps = {
  shouldTriggerModal: true,
}
export default Footer
