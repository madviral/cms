import React, { FC } from 'react'
import { PhotoUpload } from 'src/ui/uploader/PhotoUpload'
import { Attributes } from 'src/modules/shop/components/NewItem/components'
import { Box } from 'ui/layout'

interface NewProductCharacteristicsProps {
  attributes: any[]
  callback: (value: any) => void
  setIsEmptyAttributes: (value: boolean) => void
}

const NewProductCharacteristics: FC<NewProductCharacteristicsProps> = ({
  attributes,
  callback,
  setIsEmptyAttributes,
}) => {
  return (
    <Box
      boxSizing='border-box'
      height='calc(100% - 114px)'
      background='white'
      zIndex='101'
      padding='16px 16px 0 16px'
      display='grid'
      gridTemplateRows='264px auto'
      gridGap='8px'
      width='100%'
      position='absolute'
    >
      <PhotoUpload />
      <Box maxHeight='100%' overflow='auto' overflowX='hidden'>
        <Attributes
          data={attributes}
          setIsEmptyAttributes={setIsEmptyAttributes}
          noFooter
          callback={callback}
          noTitle
        />
      </Box>
    </Box>
  )
}

export default NewProductCharacteristics
