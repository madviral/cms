import React, { FC } from 'react'
import { Drawer } from 'src/ui/drawer'

interface NewProductDrawerProps {
  isOpenNewProductDrawer: boolean
  children: React.ReactNode
  triggerNewProductDrawer: () => void
  triggerModal: (value: string) => void
  shouldTriggerModal?: boolean
}

const NewProductDrawer: FC<NewProductDrawerProps> = ({
  isOpenNewProductDrawer,
  children,
  triggerNewProductDrawer,
  triggerModal,
  shouldTriggerModal,
}) => {
  return (
    <Drawer
      width='632px'
      handler={false}
      open={isOpenNewProductDrawer}
      level={null}
      className='drawer1'
      placement='right'
      onClose={() => {
        triggerNewProductDrawer()
        if (shouldTriggerModal) {
          triggerModal('newItem')
        }
      }}
      maskStyle={{
        background: '#000000',
        opacity: '0.15',
        boxSizing: 'border-box',
        overflowY: 'hidden',
      }}
    >
      {children}
    </Drawer>
  )
}

NewProductDrawer.defaultProps = {
  shouldTriggerModal: true,
}
export default NewProductDrawer
