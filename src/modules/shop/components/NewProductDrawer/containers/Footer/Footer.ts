import { connect } from 'react-redux'
import {
  updateDraftItemAttributes,
  createOrUpdateDraft,
} from 'src/modules/shop/actions/draftProduct'
import { triggerModal } from 'src/modules/common/actions/modal'
import { RootState } from 'src/store/rootReducer'
import { subscribe } from 'src/modules/shop/actions/subscribe'
import { triggerNewProductDrawer } from 'src/modules/common/actions/newProductDrawer'
import { Footer } from '../../components'

const mapStateToProps = (state: RootState) => ({
  draftProductInfo: state.shop.draftProduct,
  subscribeStatus: state.shop.subscribe.success,
})

const mapDispatchToProps = {
  update: updateDraftItemAttributes,
  createOrUpdateDraft,
  triggerModal,
  subscribe,
  triggerNewProductDrawer,
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer)
