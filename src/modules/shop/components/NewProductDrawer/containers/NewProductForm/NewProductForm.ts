import { connect } from 'react-redux'
import { NewProductForm } from '../../components'
import { RootState } from 'src/store/rootReducer'
import { loadDraftItemAttributes } from 'src/modules/shop/actions/draftProduct'
import { load as loadStocks } from 'src/modules/shop/actions/stock'
import { load as loadCategories } from 'src/modules/common/actions/productCategories'

const mapStateToProps = (state: RootState) => ({
  draftProductInfo: state.shop.draftProduct,
  categories: state.common.general.productCategories.data,
  stocks: state.shop.stock.list,
})

const mapDispatchToProps = {
  loadDraftItemAttributes,
  loadStocks,
  loadCategories,
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProductForm)
