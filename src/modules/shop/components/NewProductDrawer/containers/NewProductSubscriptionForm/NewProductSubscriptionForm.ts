import { connect } from 'react-redux'
import { triggerModal } from 'src/modules/common/actions/modal'
import { subscribe } from 'src/modules/shop/actions/subscribe'
import { triggerNewProductDrawer } from 'src/modules/common/actions/newProductDrawer'
import { createOrUpdateDraft } from 'src/modules/shop/actions/draftProduct'
import { load as loadSuggestions } from 'src/modules/shop/actions/newProduct'
import { NewProductSubscriptionForm } from '../../components'
import { RootState } from 'src/store/rootReducer'

const mapStateToProps = (state: RootState) => ({
  data: state.shop.newProduct.data,
  subscribeStatus: state.shop.subscribe,
})

const mapDispatchToProps = {
  triggerModal,
  triggerNewProductDrawer,
  subscribe,
  createOrUpdateDraft,
  loadSuggestions,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewProductSubscriptionForm)
