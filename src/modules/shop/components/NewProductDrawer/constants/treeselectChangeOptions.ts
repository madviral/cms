export enum TreeselectChangeOptions {
  Category = 'category',
  DeleteCategory = 'deleteCategory',
  Stock = 'stock',
  AdditionalStock = 'additionalStock',
  DeleteItem = 'deleteItem',
}
