import * as yup from 'yup'

export const schema = yup.object().shape({
  productCode: yup.string().required(),
  vendorCode: yup.string().required(),
  productName: yup.string().required(),
  category: yup.object().required(),
  brand: yup.string().required(),
  price: yup
    .number()
    .positive()
    .integer()
    .defined(),
  stock: yup
    .array()
    .required()
    .min(1),
})
