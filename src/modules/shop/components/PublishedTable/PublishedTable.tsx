import React, { FC, useEffect } from 'react'
import Table from '../Table'
import { ColumnsType } from 'rc-table/lib/interface'
import order from './order'
import { useTranslation } from 'react-i18next'
import { loadPublished } from '../../actions/published'
import { useDispatch } from 'react-redux'

interface Stock {
  address: string
  id: number
  map: string
  name: string
  schedule: string
  storeName: string
}

interface Status {
  name: string
  type: string
  updatedDate: string
}

interface Price {
  currency: string
  value: number
}

interface Image {
  id: number
  medium: string
  origin: string
  small: string
  sort: number
  status: number
}

interface Content {
  createdDate: string
  id: number
  images: Image[]
  name: string
  price: Price
  status: Status
  stocks: Stock[]
  vendorCode: number
}

interface PublishedData {
  pageNumber: number
  total: number
  totalPages: number
  content: Content[]
}

interface PublishedTableProps {
  data: null | PublishedData
}

const PublishedTable: FC<PublishedTableProps> = ({ data }) => {
  const dispatch = useDispatch()
  useEffect(() => {
    if (!data) {
      dispatch(loadPublished(0, 25))
    }
  }, [])
  const [t] = useTranslation(['shop', 'common'])
  if (!data) {
    return null
  }
  const columns: ColumnsType<any> = [
    ...order.map(col => ({
      title: t(col),
      dataIndex: col,
      key: col,
      width: 180,
    })),
    {
      title: t('price'),
      dataIndex: 'price',
      key: 'price',
      width: 200,
      render: (val: string, record: Content) => (
        <div key={record.id}>{record.price.value}</div>
      ),
    },
    {
      title: t('stocks'),
      dataIndex: 'stocks',
      key: 'stocks',
      width: 200,
      render: (stocks: Stock[]) => stocks.map(stock => stock.name).join(', '),
    },
  ]
  const paginateTo = (page: number) => {
    dispatch(loadPublished(page - 1, 25))
  }
  return <Table tableData={data} paginateTo={paginateTo} columns={columns} />
}

export default PublishedTable
