import React, { FC } from 'react'
import { HeaderTooltipWrap, HeaderTooltipFooter } from './ui'
import { useTranslation } from 'react-i18next'
import { Tooltip, Button } from 'ui'

const StatusHeader: FC = () => {
  const [t] = useTranslation('')
  return (
    <div style={{ cursor: 'pointer' }}>
      <div data-tip data-for='status_tooltip' data-event='click'>
        <p>{t('status')}</p>
      </div>
      <Tooltip effect='solid' id='status_tooltip' place='bottom' clickable>
        <HeaderTooltipWrap>
          {/* <RadioSort />
          <Range /> */}
          <HeaderTooltipFooter>
            <Button appearance='primary' size='l'>
              {t('apply')}
            </Button>
          </HeaderTooltipFooter>
        </HeaderTooltipWrap>
      </Tooltip>
    </div>
  )
}

export default StatusHeader
