import React, { FC, useState } from 'react'
import {
  Button,
  FormRadioButton,
  FormSwitch,
  FormField,
  Tooltip,
  Chevron,
} from 'ui'
import { Box } from 'ui/layout'
import { useTranslation } from 'react-i18next'
import {
  HeaderTooltipWrap,
  HeaderTooltipFooter,
  HeaderTooltipItem,
  RangeFieldWrap,
} from './ui'
import { useDispatch } from 'react-redux'
import { load } from 'src/modules/shop/actions/catalog'

interface PriceHeader {
  currency: string
}

const Range = ({ setChecked, checked, from, setFrom, to, setTo }) => {
  const [t] = useTranslation()
  const onChange = (check: boolean) => {
    setChecked(check)
  }

  return (
    <>
      <HeaderTooltipItem>
        <FormSwitch onChange={onChange} checked={checked} label={t('range')} />
      </HeaderTooltipItem>
      <Box display={checked ? 'block' : 'none'}>
        <HeaderTooltipItem>
          <RangeFieldWrap>
            <p>{t('from')}</p>
            <Box ml='4px'>
              <FormField
                value={from}
                onChange={e => setFrom(Number(e.target.value))}
                type='number'
              />
            </Box>
          </RangeFieldWrap>
          <RangeFieldWrap>
            <p>{t('to')}</p>
            <Box ml='4px'>
              <FormField
                value={to}
                onChange={e => setTo(Number(e.target.value))}
                type='number'
              />
            </Box>
          </RangeFieldWrap>
        </HeaderTooltipItem>
      </Box>
    </>
  )
}

const radioSorting = [
  {
    id: 'asc',
    name: 'price_sort',
    label: 'ascending',
    value: 3,
  },
  {
    id: 'desc',
    name: 'price_sort',
    label: 'descending',
    value: 4,
  },
]

const RadioSort = ({ sort, setSort }) => {
  const [t] = useTranslation('')

  return (
    <>
      {radioSorting.map(sr => (
        <HeaderTooltipItem>
          <FormRadioButton
            id={sr.id}
            onChange={() => setSort(sr.value)}
            name={sr.name}
            label={t(sr.label)}
            value={sr.value}
            checked={sort === sr.value}
          />
        </HeaderTooltipItem>
      ))}
    </>
  )
}

const PriceHeader: FC<PriceHeader> = ({ currency }) => {
  const [t] = useTranslation('')
  const [sort, setSort] = useState(radioSorting[0].value)
  const [from, setFrom] = useState(0)
  const [to, setTo] = useState(0)
  const [checked, setChecked] = useState(false)
  const dispatch = useDispatch()
  const loadFiltering = () => {
    const minMax = checked
      ? {
          min: from,
          max: to,
        }
      : {}
    dispatch(
      load({
        ...minMax,
        sort,
      }),
    )
  }
  return (
    <div style={{ cursor: 'pointer' }}>
      <Box
        data-tip
        data-for='price_tooltip'
        data-event='click'
        display='flex'
        flexDirection='row'
        alignItems='center'
      >
        <p>
          {t('shop:price')} {currency}
        </p>
        <Chevron fill='#9FB3C8' />
      </Box>
      <Tooltip effect='solid' id='price_tooltip' place='bottom' clickable>
        <HeaderTooltipWrap>
          <RadioSort setSort={setSort} sort={sort} />
          <Range
            from={from}
            to={to}
            setFrom={setFrom}
            setTo={setTo}
            checked={checked}
            setChecked={setChecked}
          />
          <HeaderTooltipFooter>
            <Button appearance='primary' size='l' onClick={loadFiltering}>
              {t('apply')}
            </Button>
          </HeaderTooltipFooter>
        </HeaderTooltipWrap>
      </Tooltip>
    </div>
  )
}

export default PriceHeader
