import React, { useEffect, FC } from 'react'
import { Box } from 'ui/layout'
import { Text } from 'ui/text'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Pagination } from 'src/modules/common/components/Pagination'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { useTableCbx } from 'lib/hooks'
import { ProductTableItemProps } from '../ProductTableItem/ProductTableItem'
import Table from './Table'
// import { Queries } from '../../types/enums'

type ProductListProps = {
  productID?: string
  items: ProductTableItemProps[]
  onLoad: VoidFunction
  pageCount?: number
  pageIndex?: number
  pageSize?: number
  onChangePageIndex?: (index: number) => void
  onChangePageSize?: (size: number) => void
}

const checkboxName = 'shop_catalog'
const ProductTable: FC<WithTranslation &
  Omit<ProductListProps, 'queryLink'>> = ({
  pageCount,
  pageIndex,
  items,
  pageSize,
  t,
  onChangePageIndex,
  onChangePageSize,
  onLoad,
}) => {
  const { cbxState, onChangeCbx, onCheckAllCbxChange } = useTableCbx(items)

  const disabledActions = cbxState.checkedList.length === 0

  useEffect(() => {
    onLoad()
  }, [])

  return (
    <Box display='flex' flexDirection='column' height='100%'>
      <Box display='flex' alignItems='center' marginBottom='16px' pl='32px'>
        <Box flex='0 0 20px' mr='16px'>
          <FormCheckbox
            id='selectAllProducts'
            indeterminate={cbxState.indeterminate}
            name={checkboxName}
            value={checkboxName}
            checked={cbxState.checkAll}
            disabled={items.length === 0}
            onChange={onCheckAllCbxChange}
          />
        </Box>

        <Box width='200px' mr='32px'>
          <Dropdown width='100%' disabled={disabledActions}>
            <Dropdown.OutlinedButton disabled={disabledActions}>
              {t('shop:chooseAnAction')}
            </Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item>{t('shop:removeFromSale')}</Dropdown.Item>
              <Dropdown.Item>{t('shop:changePrice')}</Dropdown.Item>
              <Dropdown.Item>{t('shop:archive')}</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Box>
        <Box mr='8px'>
          <Text fontSize='14px' color='secondary.0'>
            {t('filter')}:
          </Text>
        </Box>
        <Box width='200px'>
          <Dropdown width='100%'>
            <Dropdown.OutlinedButton>
              {t('shop:published')}
            </Dropdown.OutlinedButton>
            <Dropdown.Menu width='100%'>
              <Dropdown.Item>{t('shop:published')}</Dropdown.Item>
              <Dropdown.Item>{t('shop:notPublished')}</Dropdown.Item>
              <Dropdown.Item>{t('shop:archived')}</Dropdown.Item>
              <Dropdown.Item>{t('shop:withSales')}</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Box>

        <Box ml='auto'>
          <Pagination
            pageIndex={pageIndex}
            defaultPageSize={pageSize}
            setPageSize={onChangePageSize}
            goToPage={onChangePageIndex}
            pageCount={pageCount}
          />
        </Box>
      </Box>
      <Table
        checkboxName={checkboxName}
        checkedList={cbxState.checkedList}
        items={items}
        onChangeCbx={onChangeCbx}
      />
    </Box>
  )
}

export default withTranslation(['common', 'shop'])(ProductTable)
