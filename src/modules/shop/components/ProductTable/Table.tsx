import React, { FC, useCallback, useMemo } from 'react'
import BaseTable, { ColumnType } from 'ui/table/controlled/Table'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Space, Text, Truncate } from 'ui/text'
import { format } from 'date-fns'
import { Box } from 'ui/layout'
import { Condition } from 'ui/condition'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { DotsIcon } from 'ui/icons'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { ShadowedTooltip } from 'ui/tooltip'
import { Image } from 'ui/image'
import { ProductTableItemProps } from '../ProductTableItem/ProductTableItem'
import PriceHeader from './PriceHeader'

type ProductTable = {
  checkboxName?: string
  checkedList: string[]
  onChangeCbx: (id: string | number) => void
  items: ProductTableItemProps[]
}

const Table: FC<WithTranslation & ProductTable> = ({
  checkedList,
  checkboxName,
  items,
  t,
  onChangeCbx,
}) => {
  const data = useMemo(
    () =>
      items.map(item => ({
        ...item,
        key: item.id,
        sale: '-',
        stocks: item.stocks.map(stock => stock.name).join(', '),
      })),
    [items.length],
  )

  const columns = useMemo(
    (): Array<ColumnType<ProductTableItemProps>> => [
      {
        title: '',
        dataIndex: 'id',
        key: 'checkbox',
        width: 52,
        render: id => {
          const onChangeCbxCached = useCallback(() => {
            onChangeCbx(id)
          }, [id])
          return (
            <Box pl='16px'>
              <FormCheckbox
                id={id}
                name={checkboxName}
                value={id}
                checked={checkedList.some(itemId => itemId === id)}
                onChange={onChangeCbxCached}
              />
            </Box>
          )
        },
      },
      {
        title: t('product'),
        dataIndex: 'name',
        key: 'name',
        width: 266,
        ellipsis: true,
        render: (name, row) => {
          const imgUrl = row.images[0]
          const imageId = `productImage${row.id}`

          return (
            <Box display='flex' alignItems='center'>
              <Box flex='0 0 40px' pr='8px'>
                <Box data-tip data-for={imageId}>
                  <Image
                    src={imgUrl?.small}
                    alt={name}
                    width={32}
                    height={32}
                  />
                </Box>
                <Condition match={!!imgUrl}>
                  <ShadowedTooltip id={imageId} effect='solid' notFade>
                    <Image
                      src={imgUrl?.medium}
                      alt={name}
                      width={200}
                      height={200}
                    />
                  </ShadowedTooltip>
                </Condition>
              </Box>

              <Box flex='1' pr='16px'>
                <Truncate limit={53}>{name}</Truncate>
              </Box>
            </Box>
          )
        },
      },
      {
        title: t('vendorCode'),
        dataIndex: 'vendorCode',
        key: 'vendorCode',
        width: 216,
      },
      {
        title: <PriceHeader currency={data[0]?.price.currency || 'KZT'} />,
        dataIndex: 'price',
        key: 'price',
        width: 106,
        render: price => {
          return (
            <Text
              color='secondary.0'
              fontSize='12px'
              lineHeight={1}
              fontWeight='500'
            >
              {price.value.toLocaleString()}
            </Text>
          )
        },
      },
      {
        title: t('common:sale'),
        dataIndex: 'sale',
        key: 'sale',
        width: 146,
      },
      {
        title: t('stock'),
        dataIndex: 'stocks',
        key: 'stocks',
        width: 146,
        ellipsis: true,
      },
      {
        title: t('status'),
        dataIndex: 'status',
        key: 'status',
        width: 254,
        ellipsis: true,
        render: status => {
          const statusDate = new Date(status.updatedDate.slice(0, -5))
          return (
            <Condition match={status.type === 'PUBLISHED'}>
              <Box width='24%' minWidth='254px' pr='16px'>
                <Text
                  color='secondary.0'
                  fontWeight='500'
                  fontSize='12px'
                  lineHeight={1}
                >
                  {t('publishedFrom')}
                  <Space />
                  {format(statusDate, 'dd.MM.yy')}
                </Text>
              </Box>
            </Condition>
          )
        },
      },
      {
        title: '',
        dataIndex: 'id',
        key: 'actions',
        width: 20,
        fixed: 'right',
        render: () => {
          return (
            <Box
              display='flex'
              alignItems='center'
              width='100%'
              height='100%'
              backgroundColor='white'
            >
              <Dropdown>
                <Dropdown.SimpleButton noCarret>
                  <DotsIcon />
                </Dropdown.SimpleButton>
                <Dropdown.Menu direction='l' width='216px'>
                  <Dropdown.Item>{t('edit')}</Dropdown.Item>
                  <Dropdown.Item>{t('removeProduct')}</Dropdown.Item>
                  <Dropdown.Item>{t('removeFromSale')}</Dropdown.Item>
                  <Dropdown.Item>{t('archive')}</Dropdown.Item>
                  <Dropdown.Item>{t('submitForModeration')}</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Box>
          )
        },
      },
    ],
    [checkedList.length, onChangeCbx],
  )

  return <BaseTable data={data} columns={columns} />
}

export default withTranslation(['shop', 'common'])(Table)

// const fromOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//   const newFrom = Number(e.target.value)
//   if (newFrom < to && newFrom >= 0) {
//     setFrom(newFrom)
//   }
// }
// const toOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//   const newTo = Number(e.target.value)
//   if (newTo > from && newTo >= 0) {
//     setTo(newTo)
//   }
// }
