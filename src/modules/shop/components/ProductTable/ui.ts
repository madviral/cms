import styled from 'ui/theme/styled'
import { Box } from 'ui/layout'

export const HeaderTooltipWrap = styled(Box)`
  width: 256px;
  height: fit-content;
  border: 1px solid ${props => props.theme.colorsList.primary[0]};
  padding: 16px;
  box-shadow: 0px 0px 8px #d9e2ec;
  background: #fff;
  opacity: 1 !important;
  border-radius: 8px;
  cursor: initial;
`

export const HeaderTooltipFooter = styled(Box)`
  border-top: 1px solid ${props => props.theme.colorsList.secondary[2]};
  padding-top: 16px;
  display: flex;
  justify-content: flex-end;
  margin-top: 16px;
`

export const HeaderTooltipItem = styled(Box)`
  height: 40px;
  display: flex;
  flex-direction: row;
`

export const RangeFieldWrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  font-weight: 500;
  font-size: 14px;
  line-height: 18px;
  color: ${props => props.theme.colorsList.secondary[0]};
  &:nth-of-type(2) {
    margin-left: 16px;
  }
`
