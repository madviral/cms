import { Recognized, Unrecognized } from '../../types/imports'

export interface TableProps {
  tableData: Recognized | Unrecognized | any
  paginateTo?: (page: number) => void
}
