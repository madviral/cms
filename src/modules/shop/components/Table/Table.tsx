import React, { FC, useRef, useState, useEffect } from 'react'
import Styles from './Styles'
import { TableProps } from './types'
import RCTable from 'rc-table'
import { TableProps as RcTableProps } from 'rc-table/lib/Table'
import { Pagination } from 'src/modules/common/components/Pagination'
import { Box } from 'ui/layout'

const Table: FC<TableProps & RcTableProps> = ({
  tableData,
  paginateTo,
  ...props
}) => {
  const ref = useRef(null)
  const goToPage = (page: number) => {
    paginateTo(page)
  }
  const { totalPages } = tableData
  const [h, sh] = useState(0)
  useEffect(() => {
    sh(ref.current.clientHeight)
  }, [ref.current])
  return (
    <Styles ref={ref}>
      <Box
        display='flex'
        flexDirection='row'
        justifyContent='flex-end'
        mb='16px'
      >
        <Pagination pageCount={totalPages} goToPage={goToPage} pageIndex={1} />
      </Box>
      <Box bg='white' borderRadius='16px' px='16px' mb='16px'>
        <RCTable data={tableData.content} {...props} scroll={{ y: h - 96 }} />
      </Box>
    </Styles>
  )
}

export default Table
