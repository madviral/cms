import styled from 'ui/theme/styled'

export default styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  table {
    border-collapse: collapse;
    width: 100%;
    text-align: left;
    min-width: 1000px;
  }
  th {
    border-bottom: 1px solid ${props => props.theme.colorsList.primary[2]};
    height: 44px;
    font-weight: 600;
    font-size: 12px;
    color: ${props => props.theme.colorsList.secondary[0]};
    background: #fff;
  }
  tr:hover {
    td {
      background: ${props => props.theme.colorsList.secondary[5]};
    }
  }
  td {
    border-bottom: 1px solid ${props => props.theme.colorsList.primary[3]};
    font-size: 12px;
    height: 44px;
    color: ${props => props.theme.colorsList.secondary[0]};
    background: #fff;
    padding-right: 4px;
    cursor: pointer;
  }
  .error {
    & > td {
      background: ${props => props.theme.colorsList.alert[4]};
    }
  }
`
