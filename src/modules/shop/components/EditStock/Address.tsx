import React, { memo } from 'react'
import { Box } from 'ui/layout'
import { FormField, Text, Caption } from 'ui'
import { useTranslation } from 'react-i18next'
import { ICity, IStringCb } from 'src/modules/common/types'
import { ControledSelect } from 'ui/dropdowns/ControledSelect'
import { theme } from 'ui/theme'
import { MapState } from 'react-yandex-maps'
import { ISuggestArgs } from 'src/modules/common/components/YMaps/types'

export type AddressProps = {
  name?: string
  cities?: ICity[]
  city?: ICity
  street?: string
  house?: string
  mapState?: MapState
  flat?: string
  onChangeCity?: (city: ICity) => void
  onChangeStreet?: IStringCb
  onChangeHouse?: IStringCb
  onChangeFlat?: IStringCb
  onSuggest?: (value: ISuggestArgs) => void
}

const Address = ({
  name,
  cities,
  city,
  house,
  flat,
  mapState,
  street,
  onChangeStreet,
  onChangeHouse,
  onChangeFlat,
  onChangeCity,
  onSuggest,
}: AddressProps) => {
  const [t] = useTranslation(['shop', 'errorMessages'])
  const mapedCities = cities.map(cityItem => ({
    ...cityItem,
    label: cityItem.name,
    value: String(cityItem.id),
  }))

  return (
    <Box>
      <Box mb='16px'>
        <Box mb='4px'>
          <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
            {t('address')}
          </Text>
        </Box>
        <Box mb='16px'>
          <Box height='16px' marginBottom='4px'>
            <Caption fontSize={theme.fontSize.s}>{t('city') + ' *'}</Caption>
          </Box>

          <ControledSelect<ICity>
            style={{ width: '100%' }}
            showSearch
            placeholder={city || t('city')}
            options={mapedCities}
            withoutPortal
            onChange={value => onChangeCity(value)}
          />
        </Box>
        <Box mb='16px'>
          <FormField
            label={t('street') + ' *'}
            labelFontSize={12}
            value={street}
            placeholder={t('street')}
            onChange={e => onChangeStreet(e.target.value)}
            disabled={!city}
            error={t('errorMessages:requiredField')}
          />
        </Box>
        <Box display='grid' gridTemplateColumns='1fr 1fr' gridColumnGap='16px'>
          <FormField
            label={t('house') + ' *'}
            labelFontSize={12}
            value={house}
            placeholder={t('house')}
            onChange={e => onChangeHouse(e.target.value)}
            disabled={!city}
            error={t('errorMessages:requiredField')}
          />
          <FormField
            label={t('office')}
            labelFontSize={12}
            value={flat}
            onChange={e => onChangeFlat(e.target.value)}
            placeholder={t('office')}
            disabled={!city}
          />
        </Box>
      </Box>
    </Box>
  )
}

export default memo(Address)

//        <Map
//          width='100%'
//          height={300}
//          state={mapState}
//          onLoad={ymaps => ((window as any).ymaps = ymaps)}
//          modules={['geocode', 'suggest']}
//        />

//          <Suggest
//            id='stockSuggest'
//            value={street}
//            prefix={`Казахстан, ${city ? city.label : ''}`}
//            placeholder={t('street')}
//            appearance={FormFieldAppearance.default}
//            onChange={value => onChangeStreet(value)}
//            disabled={!city}
//            onSuggest={onSuggest}
//            required
//          />
