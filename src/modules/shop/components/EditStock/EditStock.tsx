import React, { useEffect, useState, useMemo } from 'react'
import { Box } from 'ui/layout'
import { Button, Text, FormField } from 'ui'
import { format } from 'date-fns'
import { useTranslation } from 'react-i18next'
import {
  IWeekSchedule,
  DayOfWeekUnion,
  DayOfWeekArray,
} from 'src/modules/common/types'
import { MapState } from 'react-yandex-maps'
import WorkSchedule from 'src/modules/common/containers/WorkSchedule'
import { ISuggestArgs } from 'src/modules/common/components/YMaps/types'
import { theme } from 'ui/theme'
import Address, { AddressProps } from './Address'

type EditStockProps = {
  updateStock: (value: any) => void
  stock: any
  triggerEditStock: () => void
  name?: string
  mapState?: MapState
  schedule?: IWeekSchedule
  onCreateStock?: (schedule: IWeekSchedule, closeCb: VoidFunction) => void
  onLoadCities?: VoidFunction
  onChangeDayFromTime?: (day: DayOfWeekUnion, value: Date) => void
  onChangeDayToTime?: (day: DayOfWeekUnion, value: Date) => void
  onChangeIsDayOff?: (day: DayOfWeekUnion, value: boolean) => void
  onChangeIsAllDay?: (day: DayOfWeekUnion, value: boolean) => void
  onChangeDay?: (day: DayOfWeekUnion, value: string) => void
  onClear?: VoidFunction
  onClose?: VoidFunction
  onChangeName?: (e: React.FormEvent<HTMLInputElement>) => void
  onSuggest?: (value: ISuggestArgs) => void
}

const EditStock = ({
  updateStock,
  triggerEditStock,
  stock,
  cities,
  city,
  schedule,
  house,
  flat,
  mapState,
  street,
  onCreateStock,
  onChangeIsDayOff,
  onChangeIsAllDay,
  onChangeDay,
  onChangeDayFromTime,
  onChangeDayToTime,
  onChangeStreet,
  onChangeHouse,
  onChangeFlat,
  onChangeCity,
  onClear,
  onClose,
  onSuggest,
  onLoadCities,
}: EditStockProps & AddressProps) => {
  const [t] = useTranslation(['shop', 'errorMessages'])
  const [formData, setFormData] = useState(stock)
  const [address, setAddress] = useState(stock?.address)

  useEffect(() => {
    if (cities?.length === 0) {
      onLoadCities()
    }
    onClear()
  }, [])

  useEffect(() => {
    setFormData({ ...formData, address })
  }, [address])

  useEffect(() => {
    setFormData(stock)

    stock.schedule.map((weekDayInfo: any) => {
      if (weekDayInfo.fromTime instanceof String) {
        onChangeIsDayOff(weekDayInfo.day, weekDayInfo.isDayOff)
        onChangeIsAllDay(weekDayInfo.day, weekDayInfo.isAllDay)
        onChangeDay(weekDayInfo.day, weekDayInfo.day)
        const newDate = new Date()
        const newDate2 = new Date()

        let time = weekDayInfo.fromTime.split(':')
        newDate.setHours(time[0])
        newDate.setMinutes(time[1])
        onChangeDayFromTime(weekDayInfo.day, newDate)

        time = weekDayInfo.toTime.split(':')
        newDate2.setHours(time[0])
        newDate2.setMinutes(time[1])
        onChangeDayToTime(weekDayInfo.day, newDate2)
      }
    })
  }, [stock])

  useMemo(() => {
    const test = Object.values(schedule).map((item: any) => {
      return {
        day: item.day,
        fromTime: format(item.fromTime, 'HH:mm'),
        isAllDay: item.isAllDay,
        isDayOff: item.isDayOff,
        toTime: format(item.toTime, 'HH:mm'),
      }
    })
    setFormData({ ...formData, schedule: test })
  }, [schedule])

  return (
    <Box
      px='16px'
      display='flex'
      flexDirection='column'
      height='100%'
      backgroundColor='#fff'
    >
      <Box flex='1 1 auto' pt='32px' pb='20px' overflowY='auto'>
        <Box mb='16px'>
          <Address
            cities={cities}
            name={formData.name}
            city={formData.address.city}
            house={formData.address.house}
            mapState={formData.address.mapState}
            street={formData.address.street}
            flat={formData.address.flat}
            onChangeCity={value => {
              setAddress({ ...address, city: value })
            }}
            onChangeStreet={value => {
              setAddress({ ...address, street: value })
            }}
            onChangeHouse={value => {
              setAddress({ ...address, house: value })
            }}
            onChangeFlat={value => {
              setAddress({ ...address, flat: value })
            }}
            onSuggest={onSuggest}
          />
        </Box>
        <FormField
          label={t('storeName') + ' *'}
          labelFontSize={12}
          value={formData.storeName || ''}
          onChange={e =>
            setFormData({ ...formData, storeName: e.target.value })
          }
          placeholder={t('storeName')}
          error={t('errorMessages:requiredField')}
        />

        <Box mb='4px'>
          <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
            {t('workingHours')}
          </Text>
        </Box>
        <WorkSchedule
          week={Object.keys(schedule) as DayOfWeekArray}
          onChangeIsDayOff={onChangeIsDayOff}
          onChangeDayFromTime={onChangeDayFromTime}
          onChangeDayToTime={onChangeDayToTime}
          schedule={schedule}
        />
      </Box>

      <Box
        flex='1 0 66px'
        display='flex'
        alignItems='center'
        px='10px'
        justifyContent='flex-end'
        borderTop={`1px solid ${theme.colorsList.secondary[2]}`}
        maxHeight={'66px'}
      >
        <Box mr='16px'>
          <Button
            onClick={() => triggerEditStock()}
            appearance='secondary'
            size='m'
          >
            {t('common:cancel')}
          </Button>
        </Box>
        <Box>
          <Button
            disabled={
              !formData.address.city ||
              !formData.address.street ||
              !formData.address.house ||
              !formData.storeName
            }
            onClick={() => {
              updateStock(formData)
              triggerEditStock()
            }}
            appearance='primary'
            size='m'
          >
            {t('updateStock')}
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

export default EditStock
