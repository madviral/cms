import React, { memo } from 'react'
import { Box } from 'ui/layout'
import { FormField, Text } from 'ui'
import { Appearance as FormFieldAppearance } from 'ui/inputs/FormField/types/interfaces'
import { useTranslation } from 'react-i18next'
import { ICity, IStringCb } from 'src/modules/common/types'
import { ControledSelect } from 'ui/dropdowns/ControledSelect'
import { theme } from 'ui/theme'
import { Map, MapState } from 'react-yandex-maps'
import { YMaps, Suggest } from 'src/modules/common/components/YMaps'
import { ISuggestArgs } from 'src/modules/common/components/YMaps/types'

export type AddressProps = {
  cities: ICity[]
  city?: ICity
  street?: string
  house?: string
  mapState?: MapState
  flat?: string
  onChangeCity: (city: ICity) => void
  onChangeStreet: IStringCb
  onChangeHouse: IStringCb
  onChangeFlat: IStringCb
  onSuggest: (value: ISuggestArgs) => void
}

const Address = ({
  cities,
  city,
  house,
  flat,
  mapState,
  street,
  onChangeStreet,
  onChangeHouse,
  onChangeFlat,
  onChangeCity,
  onSuggest,
}: AddressProps) => {
  const [t] = useTranslation('shop')
  const mapedCities = cities.map(cityItem => ({
    ...cityItem,
    label: cityItem.name,
    value: String(cityItem.id),
  }))

  return (
    <YMaps>
      <Box mb='16px'>
        <Box mb='4px'>
          <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
            {t('address')}
          </Text>
        </Box>
        <Box mb='16px'>
          <ControledSelect<ICity>
            style={{ width: '100%' }}
            showSearch
            placeholder={t('city')}
            options={mapedCities}
            withoutPortal
            onChange={value => onChangeCity(value)}
          />
        </Box>
        <Box mb='16px'>
          <Suggest
            id='stockSuggest'
            value={street}
            prefix={`Казахстан, ${city ? city.label : ''}`}
            placeholder={t('street')}
            appearance={FormFieldAppearance.default}
            onChange={value => onChangeStreet(value)}
            disabled={!city}
            onSuggest={onSuggest}
            required
          />
        </Box>
        <Box display='grid' gridTemplateColumns='1fr 1fr' gridColumnGap='16px'>
          <FormField
            value={house}
            placeholder={t('house')}
            onChange={e => onChangeHouse(e.target.value)}
            disabled={!city}
            required
          />
          <FormField
            value={flat}
            onChange={e => onChangeFlat(e.target.value)}
            placeholder={t('office')}
            disabled={!city}
          />
        </Box>
      </Box>
      <Box mb='4px'>
        <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
          {t('location')}
        </Text>
      </Box>

      <Box
        borderRadius='8px'
        border='1px solid'
        borderColor={theme.colorsList.secondary[2]}
        overflow='hidden'
      >
        <Map
          width='100%'
          height={300}
          state={mapState}
          onLoad={ymaps => ((window as any).ymaps = ymaps)}
          modules={['geocode', 'suggest']}
        />
      </Box>
    </YMaps>
  )
}

export default memo(Address)
