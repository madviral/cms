import React, { useEffect } from 'react'
import { Box } from 'ui/layout'
import { Button, Text } from 'ui'
import { useTranslation } from 'react-i18next'
import {
  IWeekSchedule,
  DayOfWeekUnion,
  DayOfWeekArray,
} from 'src/modules/common/types'
import { MapState } from 'react-yandex-maps'
import WorkSchedule from 'src/modules/common/containers/WorkSchedule'
import { ISuggestArgs } from 'src/modules/common/components/YMaps/types'
import { theme } from 'ui/theme'
import Address, { AddressProps } from './Address'

type CreateStockProps = {
  name?: string
  mapState?: MapState
  schedule?: IWeekSchedule
  onCreateStock: (schedule: IWeekSchedule, closeCb: VoidFunction) => void
  onLoadCities: VoidFunction
  onChangeDayFromTime: (day: DayOfWeekUnion, value: Date) => void
  onChangeDayToTime: (day: DayOfWeekUnion, value: Date) => void
  onChangeIsDayOff: (day: DayOfWeekUnion, value: boolean) => void
  onClear: VoidFunction
  onClose: VoidFunction
  onChangeName: (e: React.FormEvent<HTMLInputElement>) => void
  onSuggest: (value: ISuggestArgs) => void
}

const CreateStock = ({
  cities,
  city,
  house,
  flat,
  mapState,
  schedule,
  street,
  onCreateStock,
  onChangeIsDayOff,
  onChangeDayFromTime,
  onChangeDayToTime,
  onChangeStreet,
  onChangeHouse,
  onChangeFlat,
  onChangeCity,
  onClear,
  onClose,
  onSuggest,
  onLoadCities,
}: CreateStockProps & AddressProps) => {
  const [t] = useTranslation('shop')

  useEffect(() => {
    if (cities.length === 0) {
      onLoadCities()
    }
    onClear()
  }, [])

  return (
    <Box
      px='16px'
      display='flex'
      flexDirection='column'
      height='100%'
      backgroundColor='#fff'
    >
      <Box flex='1 1 auto' pt='32px' pb='20px' overflowY='auto'>
        <Box mb='16px'>
          <Address
            cities={cities}
            city={city}
            house={house}
            mapState={mapState}
            street={street}
            flat={flat}
            onChangeCity={onChangeCity}
            onChangeStreet={onChangeStreet}
            onChangeHouse={onChangeHouse}
            onChangeFlat={onChangeFlat}
            onSuggest={onSuggest}
          />
        </Box>

        <Box mb='4px'>
          <Text fontSize='12px' lineHeight='1.25' color='secondary.0'>
            {t('workingHours')}
          </Text>
        </Box>
        <WorkSchedule
          week={Object.keys(schedule) as DayOfWeekArray}
          onChangeIsDayOff={onChangeIsDayOff}
          onChangeDayFromTime={onChangeDayFromTime}
          onChangeDayToTime={onChangeDayToTime}
          schedule={schedule}
        />
      </Box>

      <Box
        flex='1 0 66px'
        display='flex'
        alignItems='center'
        px='10px'
        justifyContent='flex-end'
        borderTop={`1px solid ${theme.colorsList.secondary[2]}`}
      >
        <Box mr='16px'>
          <Button onClick={onClose} appearance='secondary' size='m'>
            {t('common:cancel')}
          </Button>
        </Box>
        <Box>
          <Button
            disabled={!city || !street || !house}
            onClick={() => {
              onCreateStock(schedule, onClose)
            }}
            appearance='primary'
            size='m'
          >
            {t('createStock')}
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

export default CreateStock
