export const SUBSCRIBE_LOADING = 'shop/subscribe/SUBSCRIBE_LOADING'
export const SUBSCRIBE_SUCCESS = 'shop/subscribe/SUBSCRIBE_SUCCESS'
export const SUBSCRIBE_ERROR = 'shop/subscribe/SUBSCRIBE_ERROR'
