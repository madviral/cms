import { AppThunk } from 'src/store/rootReducer'
import * as action from '../constants/published'

type ILoadPublished = (page: number, pageSize: number) => AppThunk

export const loadPublished: ILoadPublished = (page, pageSize) => async (
  dispatch,
  getState,
  client,
) => {
  dispatch({
    type: action.PUBLISHED_LOAD_START,
  })
  const { id } = getState().common.user
  try {
    const { data } = await client.get(
      `market/core/v1/items/merchant-list?merchantId=${id}&page=${page}&pageSize=${pageSize}&sort=1&status=PUBLISHED`,
    )
    dispatch({
      type: action.PUBLISHED_LOAD_FINISH,
      payload: {
        data,
      },
    })
  } catch (err) {
    dispatch({
      type: action.PUBLISHED_LOAD_ERROR,
    })
  }
}
