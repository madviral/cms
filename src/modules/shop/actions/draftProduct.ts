import { AppThunk } from 'src/store/rootReducer'
import * as actions from '../constants/draftProduct'

type UpdateAttribute = {
  attributeId: number
  attributeValueId?: number
  value: string
}

type PostData = {
  id?: number
  name: string
  categoryId: number
  vendorCode: string
  productCode: string
  groupId?: number
  price?: number
}

export const createOrUpdateDraft = (
  postData: PostData,
  callback: () => void,
): AppThunk => async (dispatch, getState, client) => {
  dispatch({
    type: actions.CREATE_DRAFT_LOADING,
  })
  const { data, error } = await client.post(
    'market/core/v1/drafts/items',
    postData,
  )
  if (!error) {
    dispatch({
      type: postData.id
        ? actions.DRAFT_ITEM_UPDATE
        : actions.CREATE_DRAFT_FINISH,
      payload: {
        data,
      },
    })
    callback()
  } else {
    dispatch({
      type: actions.CREATE_DRAFT_ERROR,
      payload: {
        error,
      },
    })
  }
}

export const loadDraftItemAttributes = (id: number): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  dispatch({
    type: actions.LOAD_ATTRIBUTES_LOADING,
  })
  const { error, data } = await client.get(
    `market/core/v1/drafts/items/${id}/attributes`,
  )
  if (!error) {
    dispatch({
      type: actions.LOAD_ATTRIBUTES_FINISH,
      payload: {
        attributes: data,
      },
    })
  } else {
    alert('error')
    dispatch({
      type: actions.LOAD_ATTRIBUTES_ERROR,
      payload: {
        error,
      },
    })
  }
}

export const updateDraftItemAttributes = (
  attrs: UpdateAttribute[],
): AppThunk => (dispatch, getState, client) => {
  const { shop } = getState()
  const { id } = shop.draftProduct.data
  client
    .post(`market/core/v1/drafts/items/${id}/attributes`, attrs)
    .then(response =>
      dispatch({
        type: actions.UPDATE_DRAFT_ITEM_ATTRIBUTES_FINISH,
      }),
    )
    .catch()
}

export const createItemFromDraftItem = (id: number): AppThunk => (
  dispatch,
  getState,
  client,
) => {
  client
    .post(`market/core/v1/drafts/items/create?draftItemId=${id}`)
    .then(() =>
      dispatch({
        type: actions.CREATE_ITEM_FROM_DRAFT_ITEM_SUCCESS,
      }),
    )
    .catch()
}
