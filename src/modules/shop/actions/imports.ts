import { AppThunk } from 'src/store/rootReducer'

type LoadImportsType = (
  type: 'recognized' | 'unrecognized',
  page: number,
  pageSize: number,
) => AppThunk

export const loadItems: LoadImportsType = (type, page, pageSize) => async (
  dispatch,
  getState,
  client,
) => {
  const upperCaseType = type.toUpperCase()
  dispatch({
    type: `IMPORTS_${upperCaseType}_LOAD_START`,
  })
  try {
    const { data } = await client.get(
      `market/core/v1/imports/${type}?page=${page}&pageSize=${pageSize}`,
    )
    dispatch({
      type: `IMPORTS_${upperCaseType}_LOAD_FINISH`,
      payload: {
        data,
      },
    })
  } catch (err) {
    dispatch({
      type: `IMPORTS_${upperCaseType}_LOAD_ERROR`,
    })
  }
}

export const loadImports = (): AppThunk => async dispatch => {
  dispatch(loadItems('recognized', 0, 25))
  dispatch(loadItems('unrecognized', 0, 25))
}

type Data = {
  itemId: number
  recordId: number
}

export const similarItemSubscribe = (
  postData: Data,
  callback: () => void,
): AppThunk => async (dispatch, getState, client) => {
  try {
    await client.post('market/core/v1/imports/subscribe', postData)
    dispatch(loadImports())
    callback()
    // console.log(data)
  } catch (err) {
    // console.log(err)
  }
}

export const downloadExcelTemplate = (): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  await client.post(
    'market/core/v1/imports/download/pricesNStocksTemplate',
    { filename: 'xlsx/templates/clothes-import-template.xlsm' },
    { responseType: 'blob' },
  )
  // console.log(data)
}
