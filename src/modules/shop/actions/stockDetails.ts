import { IChangeAction } from 'src/modules/common/types'
import { format } from 'date-fns'
import { load } from './stock'
import * as actions from '../constants/stockDetails'

export const change = (
  field: string,
  value: string,
): IChangeAction<typeof actions.CHANGE> => ({
  type: actions.CHANGE,
  payload: { field, value },
})

export const changeCity = value => ({
  type: actions.CHANGE_CITY,
  payload: value,
})

export const suggest = value => ({
  type: actions.SUGGEST,
  payload: value,
})

export const create = (schedule, closeCb) => async (
  dispatch,
  getState,
  client,
  history,
) => {
  closeCb()

  // const { id } = getState().common.user

  const { name, city, street, house, flat } = getState().shop.stockDetails
  const { error } = await client.post(`market/core/v1/stocks`, {
    address: {
      cityId: parseInt(city.value, 10),
      street,
      flat,
      house,
    },
    name,
    schedule: Object.keys(schedule).map(day => ({
      day,
      fromTime: format(schedule[day].fromTime, 'HH:mm'),
      toTime: format(schedule[day].toTime, 'HH:mm'),
      isDayOff: schedule[day].isDayOff,
    })),
  })
  if (!error) {
    dispatch({ type: actions.CLEAR })
    history.push({
      pathname: '/shop/stocks',
      hash: '',
      state: { isCreate: false },
    })
    dispatch(load())
  } else {
    dispatch({
      type: actions.ERROR,
      payload: error,
    })
  }
}

export const clear = () => ({ type: actions.CLEAR })
