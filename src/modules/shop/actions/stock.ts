import { AppThunk } from 'src/store/rootReducer'
import * as actions from '../constants/stock'

export const load = (): AppThunk => async (dispatch, getState, client) => {
  dispatch({ type: actions.LOADING })
  const { id } = getState().common.user

  const { data, error } = await client.get('market/core/v1/stocks', {
    params: {
      merchantId: id,
      page: 0,
      pageSize: 50,
    },
  })

  if (!error) {
    dispatch({
      type: actions.LOAD,
      payload: data.content,
    })
  } else {
    dispatch({
      type: actions.ERROR,
      payload: error,
    })
  }
}

export const update = (formData: any): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  dispatch({ type: actions.LOADING })

  const { data, error } = await client.put(
    `market/core/v1/stocks/${formData.id}`,
    {
      address: formData.address,
      map: formData.map,
      schedule: formData.schedule,
      storeName: formData.storeName,
    },
  )

  if (!error) {
    dispatch({
      type: actions.UPDATED,
      payload: data,
    })
  } else {
    dispatch({
      type: actions.ERROR,
      payload: error,
    })
  }
}

export const clear = () => ({ type: actions.CLEAR })
