import { AppThunk } from 'src/store/rootReducer'
import * as actions from '../constants/triggerEditStock'

export const triggerEditStock = (): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  dispatch({
    type: actions.TRIGGER_EDIT_STOCK,
  })
}
