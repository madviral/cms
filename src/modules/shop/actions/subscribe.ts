import { AppThunk } from 'src/store/rootReducer'
import * as actions from '../constants/subscribe'

type SubscribeData = {
  merchantVendorCode: string
  price: number
  productCode: string
  quantity: number
  stockId: number
}

export const subscribe = (
  formData: SubscribeData,
  itemId: number,
): AppThunk => async (dispatch, getState, client) => {
  dispatch({
    type: actions.SUBSCRIBE_LOADING,
  })

  client
    .post(`market/core/v1/items/${itemId}/subscribe`, formData)
    .then(response =>
      dispatch({
        type: actions.SUBSCRIBE_SUCCESS,
      }),
    )
    .catch(error =>
      dispatch({
        type: actions.SUBSCRIBE_ERROR,
        payload: error,
      }),
    )
}
