import { AppThunk } from 'src/store/rootReducer'
import * as actions from '../constants/newProduct'

export const load = (vendorCode: string): AppThunk => (
  dispatch,
  getState,
  client,
) => {
  client
    .get(`market/core/v1/items?vendorCode=${vendorCode}`)
    .then(resp => {
      dispatch({
        type: actions.SUGGESTION_LOAD,
        payload: {
          data: resp.data,
        },
      })
    })
    .catch(er => {
      dispatch({
        type: actions.SUGGESTION_CLEAR,
        payload: {
          data: null,
          er,
        },
      })
    })
}
