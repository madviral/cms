import { ActionCreator } from 'redux'
import * as actions from '../constants/catalog'
import { AppThunk } from 'src/store/rootReducer'

export const load = (filterParams: any = { sort: 1 }): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  //  const { id } = getState().common.user
  const { index, size } = getState().shop.catalog.page
  const { data, error } = await client.get(
    '/market/core/v1/items/merchant-list',
    {
      params: {
        //      merchantId: id,
        page: index > 1 ? index - 1 : 0,
        pageSize: size,
        ...filterParams,
      },
    },
  )

  if (!error) {
    dispatch({
      type: actions.LOAD,
      payload: data,
    })
  }
}

export const changePagination: ActionCreator<any> = (
  field,
  value,
) => dispatch => {
  dispatch({
    type: actions.CHANGE_PAGINATION,
    payload: {
      field,
      value,
    },
  })

  dispatch(load())
}
