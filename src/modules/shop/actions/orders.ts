import { ActionCreator } from 'redux'
import * as actions from '../constants/orders'

export const load: ActionCreator<any> = () => async (
  dispatch,
  getState,
  client,
) => {
  const { id } = getState().common.user
  const { index, size } = getState().shop.orders.page

  const { data, error } = await client.get('market/core/v1/orders/list', {
    params: {
      merchantId: id,
      page: index > 1 ? index - 1 : 0,
      pageSize: size,
      status: 'PAID',
    },
  })

  if (!error) {
    dispatch({
      type: actions.LOAD,
      list: data.content,
    })
  }
}

export const changePagination: ActionCreator<any> = (
  field,
  value,
) => dispatch => {
  dispatch({
    type: actions.CHANGE_PAGINATION,
    payload: {
      field,
      value,
    },
  })

  dispatch(load())
}
