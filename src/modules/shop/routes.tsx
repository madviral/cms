import React from 'react'
import { Redirect } from 'react-router-dom'
import { ContentIcon, ShoppingIcon, PackageIcon } from 'ui/icons'
import { getDashedStr } from 'lib/utils'
import { RenderRoutes } from 'src/modules/common/components/Routes'
import Catalog from './containers/Catalog'
import Import from './containers/Import'
import Orders from './pages/Shop/Orders'
import Stocks from './pages/Stocks/Stocks'
import { Unrecognized, Published } from './pages'

enum KEYS {
  NOT_PUBLISHED = 'NOT_PUBLISHED',
  UNRECOGNIZED = 'UNRECOGNIZED',
  CATALOG = 'CATALOG',
  STOCKS = 'STOCKS',
  ORDERS = 'ORDERS',
  REDIRECT = 'REDIRECT',
  IMPORT = 'IMPORT',
  PUBLIHSED = 'PUBLIHSED',
}

export const getRoutes = rootKey => [
  {
    path: '/shop',
    key: getDashedStr(rootKey, KEYS.REDIRECT),
    exact: true,
    skip: true,
    component: () => <Redirect to={'/shop/catalog'} />,
  },
  {
    path: '/shop/catalog',
    key: getDashedStr(rootKey, KEYS.CATALOG),
    exact: false,
    component: RenderRoutes,
    nav: {
      name: 'common:content',
      icon: ContentIcon,
    },
    routes: [
      {
        path: '/shop/catalog',
        key: getDashedStr(rootKey, KEYS.CATALOG, KEYS.NOT_PUBLISHED),
        exact: true,
        component: Catalog,
        nav: {
          name: 'shop:goodsCatalog',
        },
      },
      {
        path: '/shop/catalog/published',
        key: getDashedStr(rootKey, KEYS.CATALOG, KEYS.PUBLIHSED),
        exact: true,
        component: Published,
        nav: {
          name: 'shop:published',
        },
      },
      {
        path: '/shop/catalog/unrecognized',
        key: getDashedStr(rootKey, KEYS.CATALOG, KEYS.UNRECOGNIZED),
        exact: true,
        component: Unrecognized,
        nav: {
          name: 'shop:unrecognizedTable',
        },
      },
    ],
  },
  {
    path: '/shop/stocks',
    key: getDashedStr(rootKey, KEYS.STOCKS),
    exact: false,
    component: Stocks,
    nav: {
      name: 'shop:stocks',
      icon: PackageIcon,
    },
  },
  {
    path: '/shop/orders',
    key: getDashedStr(rootKey, KEYS.ORDERS),
    exact: false,
    component: Orders,
    nav: {
      name: 'shop:orders',
      icon: ShoppingIcon,
    },
  },
  {
    path: '/shop/import',
    key: getDashedStr(rootKey, KEYS.IMPORT),
    exact: true,
    component: Import,
    skip: true,
  },
]
