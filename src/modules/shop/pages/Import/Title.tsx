import React, { FC } from 'react'
import { Box } from 'ui/layout'
import { useTranslation } from 'react-i18next'
import { Caption, Button } from 'ui'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'
import { useSelector } from 'react-redux'
import { RootState } from 'src/store/rootReducer'

const Title: FC = () => {
  const [t] = useTranslation('shop')
  const { recognized, unrecognized } = useSelector(
    (state: RootState) => state.shop.imports,
  )
  const recognizedCount = recognized.data.recognized || 0
  const unrecognizedCount = unrecognized.data.unrecognized || 0
  return (
    <Box display='flex' flexDirection='row' justifyContent='space-between'>
      <Caption>{t('importTitle')}</Caption>
      <Box display='flex' flexDirection='row'>
        <GlobalSearch isDefaultOpen isAbleToHide={false} />
        <Box ml='16px'>
          <Button size='m' appearance='primary'>
            {t('publish')} ({recognizedCount + unrecognizedCount})
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

export default Title
