import React, { FC, createContext, useState, useEffect } from 'react'
import { Box } from 'ui/layout'
import Title from './Title'
import Tabs from './Tabs'
import Filters from './Filters'
import { Table } from './Table'
import { ImportsState } from '../../types/imports'

export const Context = createContext(null)

interface ImportProps {
  load: () => void
  imports: ImportsState
}

const Import: FC<ImportProps> = ({ load, imports }) => {
  useEffect(() => {
    load()
  }, [])
  const tabs = useState(1)
  if (!imports.recognized.data || !imports.unrecognized.data) {
    return null // temp
  }
  return (
    <Box
      display='grid'
      height='100%'
      gridTemplateRows='[title] 34px [tabs] 34px [filters] 34px [content] 1fr'
      gridGap='16px 0'
    >
      <Context.Provider value={tabs}>
        <Title />
        <Tabs
          recognizedCount={imports.recognized.data.recognized}
          unrecognizedCount={imports.unrecognized.data.unrecognized}
        />
        <Filters />
        <Table />
      </Context.Provider>
    </Box>
  )
}

export default Import
