import React, { FC, useContext } from 'react'
import { Box } from 'ui/layout'
import { Context } from './Import'
import styled from 'ui/theme/styled'
import { useTranslation } from 'react-i18next'

interface IStyled {
  color: string
  active: boolean
}

const Tab = styled.div<IStyled>`
  &:not(:first-of-type) {
    margin-left: 32px;
  }
  color: ${props =>
    props.active
      ? props.theme.color[props.color]
      : props.theme.color.secondary};
  cursor: pointer;
  display: flex;
  flex-direction: row;
  position: relative;
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;
`

const Count = styled.div<IStyled>`
  border-radius: 12px;
  background: ${props =>
    props.theme.colorsList[props.active ? props.color : 'secondary'][2]};
  color: ${props =>
    props.theme.colorsList[props.active ? props.color : 'secondary'][0]};
  padding: 0 10px;
  height: 20px;
  margin-left: 4px;
  font-weight: 500;
  font-size: 10px;
  line-height: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`

const Underline = styled.div<IStyled>`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  height: 2px;
  border-radius: 12px;
  opacity: ${props => Number(props.active)};
  background: ${props => props.theme.colorsList[props.color][0]};
`

interface TabsProps {
  recognizedCount: number
  unrecognizedCount: number
}

const Tabs: FC<TabsProps> = ({ recognizedCount, unrecognizedCount }) => {
  const [t] = useTranslation('shop')
  const [currentTab, setCurrentTab] = useContext(Context)
  const tabs = [
    {
      color: 'primary',
      text: 'recognizedTable',
      count: recognizedCount,
      id: 1,
    },
    {
      color: 'alert',
      text: 'unrecognizedTable',
      count: unrecognizedCount,
      id: 2,
    },
  ]
  return (
    <Box display='flex' flexDirection='row'>
      {tabs.map(tab => (
        <Tab
          color={tab.color}
          key={tab.id}
          active={tab.id === currentTab}
          onClick={() => setCurrentTab(tab.id)}
        >
          {t(tab.text)}
          <Count color={tab.color} active={tab.id === currentTab}>
            {tab.count}
          </Count>
          <Underline color={tab.color} active={tab.id === currentTab} />
        </Tab>
      ))}
    </Box>
  )
}

export default Tabs
