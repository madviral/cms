import React, { FC, useContext, useState } from 'react'
import { Box } from 'ui/layout'
// import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { Context } from './Import'
import styled from 'ui/theme/styled'
// import { Dropdown } from 'ui/dropdowns/Dropdown'
// import { useTranslation } from 'react-i18next'
import { Pagination } from 'src/modules/common/components/Pagination'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { loadItems } from 'src/modules/shop/actions/imports'

const Wrap = styled(Box)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 0 16px;
`

const LeftWrap = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;
`

const RecognizedFilter = () => {
  // const [t] = useTranslation()
  const { totalPages } = useSelector(
    (state: RootState) => state.shop.imports.recognized.data,
  )
  const dispatch = useDispatch()
  const goToPage = (page: number) => {
    dispatch(loadItems('recognized', page - 1, 25))
  }
  const [pageIndex] = useState(1)

  return (
    <Wrap>
      <LeftWrap>
        {/* <Box mr='16px'>
          <FormCheckbox name='recognizedCheckName' id='recognizedCheckId' />
        </Box>
        <Dropdown width='100%'>
          <Dropdown.OutlinedButton>
            {t('shop:chooseAnAction')}
          </Dropdown.OutlinedButton>
          <Dropdown.Menu width='100%'>
            <Dropdown.Item>{t('common:accept')}</Dropdown.Item>
            <Dropdown.Item>{t('common:cancel')}</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown> */}
      </LeftWrap>
      <Pagination
        pageCount={totalPages}
        goToPage={goToPage}
        pageIndex={pageIndex}
      />
    </Wrap>
  )
}

const UnrecognizedFilter = () => {
  // const [t] = useTranslation()
  const { totalPages } = useSelector(
    (state: RootState) => state.shop.imports.unrecognized.data,
  )
  const dispatch = useDispatch()
  const goToPage = (page: number) => {
    dispatch(loadItems('unrecognized', page - 1, 25))
  }
  const [pageIndex] = useState(1)
  return (
    <Wrap>
      <LeftWrap>
        {/* <Box mr='16px'>
          <FormCheckbox name='unrecognizedCheckName' id='unrecognizedCheckId' />
        </Box>
        <Dropdown width='100%'>
          <Dropdown.OutlinedButton>
            {t('shop:chooseAnAction')}
          </Dropdown.OutlinedButton>
          <Dropdown.Menu width='100%'>
            <Dropdown.Item>{t('common:accept')}</Dropdown.Item>
            <Dropdown.Item>{t('common:cancel')}</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown> */}
      </LeftWrap>
      <Pagination
        pageCount={totalPages}
        goToPage={goToPage}
        pageIndex={pageIndex}
      />
    </Wrap>
  )
}

const filters = [
  {
    id: 1,
    component: RecognizedFilter,
  },
  {
    id: 2,
    component: UnrecognizedFilter,
  },
]

const Filters: FC = () => {
  const [currentFilter] = useContext(Context)
  return (
    <div>
      {filters.map(filter => (
        <Box display={filter.id === currentFilter ? 'block' : 'none'}>
          {<filter.component />}
        </Box>
      ))}
    </div>
  )
}

export default Filters
