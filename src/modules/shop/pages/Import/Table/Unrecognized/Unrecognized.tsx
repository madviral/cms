import React, { FC, useContext, useState } from 'react'
import HeightContext from '../context'
import { Box } from 'ui/layout'
import { TableProps } from '../types'
import Table from 'rc-table'
import { useTranslation } from 'react-i18next'
// import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import colOrder from '../colOrder'
import { ColumnsType } from 'rc-table/lib/interface'
import { Table as TableType } from 'src/modules/shop/types/imports'
import UnrecognizedItemDrawer from './UnrecognizedItemDrawer'

const Unrecognized: FC<TableProps> = ({ table }) => {
  const [t] = useTranslation('shop')
  const height = useContext(HeightContext)
  const [isOpen, setIsOpen] = useState(false)
  const [item, setItem] = useState(null)
  const columns: ColumnsType<any> = [
    {
      key: 'checkbox',
      fixed: 'left',
      render: (val: string, row: TableType) => (
        <div style={{ paddingLeft: 16 }}>
          {/* <FormCheckbox id={row.recordId} name={String(row.recordId)} /> */}
        </div>
      ),
      width: 52,
    },
    ...colOrder.map(col => ({
      title: t(col),
      dataIndex: col,
      key: col,
      render: (val: string, row: TableType) => {
        if (val) {
          return <div>{val}</div>
        }
        if (row.details[col]) {
          return (
            <Box color='alert.0'>
              <p style={{ fontWeight: 500 }}>{row.details[col]}</p>
            </Box>
          )
        }
      },
    })),
    {
      title: t('price'),
      dataIndex: 'price',
      key: 'price',
    },
  ]
  return (
    <>
      <UnrecognizedItemDrawer
        isOpen={isOpen}
        onClose={() => {
          setItem(null)
          setIsOpen(false)
        }}
        item={item}
      />
      <Table
        onRow={(row: TableType, i: number) => {
          const onRowProps = {
            onClick: () => {
              setItem(row)
              setIsOpen(true)
            },
          }
          if (Object.keys(row.details).length) {
            return {
              className: 'error',
              ...onRowProps,
            }
          }
          return onRowProps
        }}
        data={table}
        columns={columns}
        scroll={{ y: height - 48 }}
      />
    </>
  )
}

export default Unrecognized
