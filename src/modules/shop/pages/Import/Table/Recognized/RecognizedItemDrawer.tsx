import React, { FC, useState, useEffect } from 'react'
import { Drawer } from 'ui/drawer'
import { Table as Item } from 'src/modules/shop/types/imports'
import axios from 'axios'
import { Box } from 'ui/layout'
import { Caption, Button } from 'ui'
import { styled } from 'ui/theme'
import { useTranslation } from 'react-i18next'

interface RecognizedItemDrawerProps {
  isOpen: boolean
  onClose: () => void
  item: Item
}

interface BodyProps {
  item: Item
  onClose: () => void
}

type Attribute = {
  id: number
  idName: string
  value: string
  valueName: string
}

type Image = {
  id: number
  large: string
  medium: string
  origin: string
  small: string
  sort: number
}

type Price = {
  value: number
  currency: string
}

export type IDetails = {
  name: string
  attributes: Attribute[]
  categories: number[]
  description: string
  images: Image[]
  price: Price
  rating: number
  ratingCount: number
  smallImage: string
  vendorCode: string
} | null

interface IImage {
  url: string
}
const Image = styled(Box)<IImage>`
  background: url(${props => props.url});
  height: 100px;
  width: 100px;
  border-radius: 8px;
  flex-shrink: 0;
  background-size: contain;
  &:not(:first-of-type) {
    margin-left: 8px;
  }
`

const Footer = styled(Box)`
  height: 64px;
  border-top: 1px solid ${props => props.theme.colorsList.secondary[2]};
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`

const Body: FC<BodyProps> = ({ item, onClose }) => {
  const [details, setDetails] = useState<IDetails>(null)
  const [t] = useTranslation()
  useEffect(() => {
    axios
      .get(`https://api-dev.vlife.kz/market/catalog/v1/items/${item.itemId}`, {
        headers: {
          'Accept-Language': 'ru',
        },
      })
      .then(res => setDetails(res.data.item))
    // .catch(er => {})
  }, [item.recordId])
  if (!details) {
    return (
      <Box
        display='flex'
        justifyContent='center'
        alignItems='center'
        height='100%'
        width='100%'
      >
        <Caption style={{ fontSize: 16 }}>{t('loading')}</Caption>
      </Box>
    )
  }
  return (
    <Box
      p='16px'
      height='100%'
      width='100%'
      display='flex'
      flexDirection='column'
    >
      <Box height='100%'>
        <Box mb='32px'>
          <Caption style={{ fontSize: 20 }}>{details.name}</Caption>
        </Box>
        <Box display='flex' flexDirection='row' overflow='scroll'>
          {details.images.map(img => (
            <Image url={img.large} key={img.id} />
          ))}
        </Box>
        <Box mt='32px'>
          {details.attributes.map(attr => (
            <Box
              key={attr.id}
              display='flex'
              flexDirection='row'
              justifyContent='space-between'
            >
              <Caption>{attr.idName}</Caption>
              <Caption style={{ fontWeight: 'bold' }}>{attr.valueName}</Caption>
            </Box>
          ))}
        </Box>
      </Box>
      <Footer>
        <Button size='l' appearance='secondary' onClick={onClose}>
          {t('close')}
        </Button>
      </Footer>
    </Box>
  )
}

const RecognizedItemDrawer: FC<RecognizedItemDrawerProps> = ({
  isOpen,
  onClose,
  item,
}) => {
  return (
    <Drawer
      width='632px'
      handler={false}
      open={isOpen}
      level={null}
      onClose={onClose}
      placement='right'
    >
      {item ? <Body item={item} onClose={onClose} /> : null}
    </Drawer>
  )
}

export default RecognizedItemDrawer
