import React, { FC, useContext, useState } from 'react'
import HeightContext from '../context'
import { Box } from 'ui/layout'
import { TableProps } from '../types'
import Table from 'rc-table'
import { useTranslation } from 'react-i18next'
import styled from 'ui/theme/styled'
// import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { DotsIcon } from 'ui'
import colOrder from '../colOrder'
import { ColumnsType } from 'rc-table/lib/interface'
import { Table as TableType } from 'src/modules/shop/types/imports'
import RecognizedItemDrawer from './RecognizedItemDrawer'

const Styles = styled.div`
  td:nth-of-type(-n + 2) {
    border-bottom: 0;
  }
  th:nth-of-type(-n + 2) {
    border-bottom: 0;
  }
`

const ItemDescriptionWrap = styled(Box)`
  height: 40px;
  background: ${props => props.theme.colorsList.secondary[3]};
  border-radius: 8px;
  padding: 0 4px;
  display: flex;
  flex-direction: row;
  align-items: center;
  font-weight: 500;
  font-size: 12px;
  line-height: 16px;
  margin-right: 24px;
`

interface IPhoto {
  src: string
}

const Photo = styled.div<IPhoto>`
  background: white;
  background-image: url(${props => props.src});
  background-size: contain;
  height: 32px;
  width: 32px;
  border-radius: 6px;
  flex-shrink: 0;
  margin-right: 8px;
`

const Recognized: FC<TableProps> = ({ table }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [item, setItem] = useState(null)
  const [t] = useTranslation('shop')
  const height = useContext(HeightContext)
  const columns: ColumnsType<any> = [
    {
      key: 'checkbox',
      fixed: 'left',
      render: (val: string, row: TableType) => (
        <div style={{ paddingLeft: 16 }}>
          {/* <FormCheckbox id={row.recordId} name={String(row.recordId)} /> */}
        </div>
      ),
      width: 52,
    },
    {
      title: t('vlifeItem'),
      dataIndex: 'itemName',
      key: 'itemName',
      render: (val: string, row: TableType) => (
        <ItemDescriptionWrap>
          <Photo src={row.itemImagePath} />
          {val}
        </ItemDescriptionWrap>
      ),
      width: 274,
    },
    ...colOrder.map(col => ({
      title: t(col),
      dataIndex: col,
      key: col,
    })),
    {
      key: 'tripleDot',
      width: 36,
      fixed: 'right',
      render: () => <DotsIcon />,
    },
  ]
  return (
    <>
      <Styles>
        <Table
          onRow={(row: TableType) => {
            return {
              onClick: () => {
                setIsOpen(true)
                setItem(row)
              },
            }
          }}
          data={table}
          columns={columns}
          scroll={{ y: height - 48 }}
        />
      </Styles>
      <RecognizedItemDrawer
        item={item}
        isOpen={isOpen}
        onClose={() => {
          setItem(null)
          setIsOpen(false)
        }}
      />
    </>
  )
}

export default Recognized
