import React, { FC, useContext, useRef, useState, useEffect } from 'react'
import { Box } from 'ui/layout'
import { Context } from '../Import'
import { useSelector } from 'react-redux'
import Recognized from './Recognized'
import Unrecognized from './Unrecognized'
import { RootState } from 'src/store/rootReducer'
import Styles from './Styles'
import HeightContext from './context'

const Table: FC = () => {
  const [recognized, unrecognized] = useSelector((state: RootState) => [
    state.shop.imports.recognized.data.content,
    state.shop.imports.unrecognized.data.content,
  ])
  const [activeTableId] = useContext(Context)
  const tables = [
    {
      id: 1,
      component: <Recognized table={recognized} />,
    },
    {
      id: 2,
      table: unrecognized,
      component: <Unrecognized table={unrecognized} />,
    },
  ]
  const ref = useRef(null)
  const [h, sh] = useState(0)
  useEffect(() => {
    sh(ref.current.clientHeight)
  }, [ref.current])
  return (
    <Box display='flex' overflow='hidden' ref={ref}>
      {tables.map(table => (
        <Box
          key={table.id}
          display={table.id === activeTableId ? 'block' : 'none'}
          bg='white'
          height='100%'
          borderRadius='16px'
          flex='1'
          overflow='auto'
        >
          <HeightContext.Provider value={h}>
            <Styles>{table.component}</Styles>
          </HeightContext.Provider>
        </Box>
      ))}
    </Box>
  )
}

export default Table
