import { Table } from '../../../types/imports'

export interface TableProps {
  table: Table[]
}
