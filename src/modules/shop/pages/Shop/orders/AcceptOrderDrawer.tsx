import React from 'react'
import { Drawer } from 'ui/drawer'
import AcceptOrder from '../../../containers/AcceptOrder'

const AcceptOrderDrawer = ({ isOpen, onClose }) => {
  return (
    <Drawer
      width='880px'
      handler={false}
      open={isOpen}
      level={null}
      onClose={onClose}
      className='drawer1'
      placement='right'
    >
      <AcceptOrder onClose={onClose} />
    </Drawer>
  )
}

export default AcceptOrderDrawer
