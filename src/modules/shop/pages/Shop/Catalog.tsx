import React, { FC } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { Box } from 'ui/layout'
import { Text } from 'ui'
import { theme } from 'ui/theme'
import { Button } from 'ui/buttons'
import { useTranslation } from 'react-i18next'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'
import ProductTable from '../../containers/ProductTable'
import { Queries } from '../../types/enums'
import Drawer from './Drawer'

interface CatalogProps {
  triggerModal: (name: string) => void
  draftProductInfo: {
    data: { id: number; attributes: any[] }
    loading: boolean
    error: boolean
    attributesUpdated?: boolean
    itemCreated?: boolean
  }
  update: (value: any) => any
  create: (value: number) => void
}

const Catalog: FC<CatalogProps & RouteComponentProps> = ({
  triggerModal,
  location,
  draftProductInfo,
  update,
  create,
}) => {
  const [t] = useTranslation('shop')
  const queries = new URLSearchParams(location.search)
  const productID = queries.get(Queries.SELECTED_PRODUCT)
  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 34px [content] 1fr'
      gridGap='16px 0'
      height='100%'
    >
      <Box
        gridArea='title 1'
        display='flex'
        alignItems='center'
        justifyContent='space-between'
      >
        <Text
          fontSize={18}
          lineHeight={1}
          color={theme.color.secondary}
          fontWeight={500}
        >
          {t('goodsCatalog')}
        </Text>

        <Box ml='auto'>
          <GlobalSearch isDefaultOpen isAbleToHide={false} />
        </Box>
        <Box ml={20}>
          <Button
            appearance='primary'
            size='m'
            onClick={() => triggerModal('newItem')}
          >
            + {t('createProduct')}
          </Button>
          <Drawer draftProductInfo={draftProductInfo} create={create} />
        </Box>
      </Box>
      <Box gridArea='content 1' minHeight='fit-content'>
        <ProductTable productID={productID} />
      </Box>
    </Box>
  )
}

export default Catalog
