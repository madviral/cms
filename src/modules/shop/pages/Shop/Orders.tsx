import React, { memo } from 'react'
import { withRouter } from 'react-router-dom'
import { Box } from 'ui/layout'
import { Text } from 'ui'
import { theme } from 'ui/theme'
import { useTranslation } from 'react-i18next'
import queryString from 'query-string'
import AcceptOrderDrawer from './orders/AcceptOrderDrawer'
import OrdersTable from '../../containers/OrdersTable'
import { Queries } from '../../types/enums'

const Orders = ({ history }) => {
  const [t] = useTranslation('shop')
  const queryParams = queryString.parse(location.search)
  const isAcceptDrawer =
    parseInt(queryParams[Queries.SELECTED_ORDER] as string, 10) > 0

  return (
    <>
      <AcceptOrderDrawer isOpen={isAcceptDrawer} onClose={history.goBack} />
      <Box
        display='grid'
        gridTemplateRows='[title] 34px [content] 1fr'
        gridGap='16px 0'
        height='100%'
      >
        <Box gridArea='title 1'>
          <Text
            fontSize={18}
            lineHeight={1}
            color={theme.color.secondary}
            fontWeight={500}
          >
            {t('orders')}
          </Text>
        </Box>
        <Box gridArea='content 1' overflow='hidden'>
          <OrdersTable />
        </Box>
      </Box>
    </>
  )
}

export default withRouter(memo(Orders))
