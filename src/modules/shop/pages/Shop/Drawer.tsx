import React, { FC, useState, useEffect } from 'react'
import {
  Footer,
  NewProductSubscriptionForm,
  NewProductForm,
} from '../../components/NewProductDrawer/containers'
import NewProductDrawer from '../../containers/NewProductDrawer'
import { useTranslation } from 'react-i18next'
import { NewProductCharacteristics } from '../../components/NewProductDrawer/components'
import { Tabs } from 'ui'
import { triggerNewProductDrawer } from 'src/modules/common/actions/newProductDrawer'

interface DrawerProps {
  draftProductInfo: {
    data: { id: number; attributes: any[] }
    loading: boolean
    error: boolean
    attributesUpdated?: boolean
    itemCreated?: boolean
    draftItemUpdated?: boolean
  }
  create: (n: number) => void
  shouldTriggerModal?: boolean
}

const Drawer: FC<DrawerProps> = ({
  draftProductInfo,
  create,
  shouldTriggerModal,
}) => {
  const [t] = useTranslation('shop')
  const [isDisabled, setIsDisabled] = useState(true)
  const [isInitialDrawer, setIsInitialDrawer] = useState(true)
  const [selectedSimilarItems, setSelectedSimilarItems] = useState([])
  const [formData, setFormData] = useState({
    productCode: '',
    vendorCode: '',
    barcode: '',
    productName: '',
    category: {
      children: [],
      description: '',
      id: 0,
      image: {},
      isAccent: null,
      isTabbedChildren: null,
      name: '',
    },
    brand: '',
    price: '',
    stock: [],
  })
  const [selectedAttributes, setSelectedAttributes] = useState(null)
  const [isEmptyAttributes, setIsEmptyAttributes] = useState(true)
  const [activeTabIndex, setActiveTabIndex] = useState(0)
  const [tabs, setTabs] = useState([
    {
      id: 1,
      name: '1. ' + t('main'),
      component: () => (
        <NewProductForm
          callback={value => setIsDisabled(value)}
          formData={formData}
          setFormData={setFormData}
          activeTabIndex={activeTabIndex}
        />
      ),
      color: 'primary',
    },
    {
      id: 2,
      name: '2. ' + t('features'),
      component: () => (
        <NewProductCharacteristics
          attributes={draftProductInfo.data && draftProductInfo.data.attributes}
          callback={setSelectedAttributes}
          setIsEmptyAttributes={setIsEmptyAttributes}
        />
      ),
      color: 'primary',
    },
  ])

  useEffect(() => {
    if (draftProductInfo.itemCreated) {
      triggerNewProductDrawer()
      return
    }
    if (
      draftProductInfo.attributesUpdated &&
      draftProductInfo.draftItemUpdated
    ) {
      create(draftProductInfo.data.id)
      triggerNewProductDrawer()
      window.location.reload()
    }
  }, [
    draftProductInfo.itemCreated,
    draftProductInfo.attributesUpdated,
    draftProductInfo.draftItemUpdated,
  ])

  useEffect(() => {
    const buff = [...tabs]
    buff[0] = {
      id: 1,
      name: '1. ' + t('main'),
      component: () => (
        <NewProductForm
          callback={value => setIsDisabled(value)}
          formData={formData}
          setFormData={setFormData}
          activeTabIndex={activeTabIndex}
        />
      ),
      color: 'primary',
    }
    setTabs(buff)
  }, [formData])
  return (
    <NewProductDrawer shouldTriggerModal={shouldTriggerModal}>
      {isInitialDrawer ? (
        <NewProductSubscriptionForm
          passShouldCreateDraftItem={setIsDisabled}
          passSelectedSimilarItems={setSelectedSimilarItems}
          passFormData={setFormData}
        />
      ) : (
        <Tabs tabs={tabs} passActiveTabIndex={setActiveTabIndex} />
      )}
      <Footer
        isInitialDrawer={isInitialDrawer}
        setIsInitialDrawer={setIsInitialDrawer}
        isDisabled={isDisabled}
        selectedAttributes={selectedAttributes}
        isEmptyAttributes={isEmptyAttributes}
        formData={formData}
        selectedSimilarItems={selectedSimilarItems}
        passFormData={setFormData}
        shouldTriggerModal={shouldTriggerModal}
      />
    </NewProductDrawer>
  )
}

Drawer.defaultProps = {
  shouldTriggerModal: true,
}
export default Drawer
