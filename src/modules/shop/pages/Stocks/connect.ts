import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { load } from '../../actions/stock'

export default connect(
  (state: RootState) => ({
    list: state.shop.stock.list,
    loading: state.shop.stock.loading,
  }),
  {
    onLoad: load,
  },
)
