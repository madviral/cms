import React, { /*useCallback, */ useEffect } from 'react'
// import { useCookies } from 'react-cookie'
// import { StocksLearning } from '../../components/StocksLearning'
// import FirstStock from '../../components/StocksLearning/FirstStock'
import StocksListPage from '../../containers/StocksListPage'
import connect from './connect'

// const learnedStocksKey = 'learnedStocks'
const Stocks = ({ /*list, location,*/ loading, onLoad }) => {
  //  const [cookies, setCookie] = useCookies([learnedStocksKey])
  //  const learnedStocks = JSON.parse(cookies[learnedStocksKey] || false)

  useEffect(() => {
    onLoad()
  }, [])

  // const onLearn = useCallback(() => {
  //   setCookie(learnedStocksKey, true)
  // }, [])
  // const onRepeat = useCallback(() => {
  //   setCookie(learnedStocksKey, false)
  // }, [])

  if (loading) {
    return null
  }

  // if (location.state && location.state.isCreate) {
  //   return <CreateStock />
  // }
  //  if (list.length > 0) {
  return <StocksListPage />
  //  }

  //  if (!learnedStocks) {
  //    return <StocksLearning onLearn={onLearn} />
  //  }

  //  return <FirstStock onRepeat={onRepeat} />
}

export default connect(Stocks)
