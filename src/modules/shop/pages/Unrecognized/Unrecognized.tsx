import React, { FC } from 'react'
import { Box } from 'ui/layout'
import { Text } from 'ui'
import { useTranslation } from 'react-i18next'
import UnrecognizedTable from '../../containers/UnrecognizedTable'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'

const Unrecognized: FC = () => {
  const [t] = useTranslation('shop')
  return (
    <Box
      display='grid'
      gridTemplateRows='32px 1fr'
      gridRowGap='16px'
      height='100%'
    >
      <Box
        display='flex'
        flexDirection='row'
        alignItems='center'
        justifyContent='space-between'
      >
        <Text color='secondary.0' fontSize='18px'>
          {t('unrecognizedTable')}
        </Text>
        <GlobalSearch isDefaultOpen />
      </Box>
      <Box minHeight='fit-content'>
        <UnrecognizedTable />
      </Box>
    </Box>
  )
}

export default Unrecognized
