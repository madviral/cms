import React from 'react'
import { Switch, Route } from 'react-router-dom'

const Shop = ({ childrenRoutes }) => {
  return (
    <Switch>
      {childrenRoutes.map(route => (
        <Route
          key={route.path}
          path={route.path}
          exact={route.exact}
          component={route.component}
        />
      ))}
    </Switch>
  )
}

export default Shop
