import * as actions from '../constants/imports'

export type Details = {
  [key: string]: string
}

export type Table = {
  itemId: number | null
  itemImagePath: string | null
  itemName: string | null
  merchantProductId: string | null
  vendorCode: string | null
  barCode: string | null
  merchantProductName: string | null
  brand: string | null
  details: Details | null
  recordId: number
  stocks: {
    [key: string]: boolean
  }
}

export type Items = {
  content: Table[]
  pageNumber: number
  totalPages: number
  recognized: number
  unrecognized: number
  stocks: string[]
}

export interface Recognized extends Items {
  type: 'recognized'
}

export interface Unrecognized extends Items {
  type: 'unrecognized'
}

interface DefaultState {
  loading: boolean
  error: boolean
}

export interface RecognizedState extends DefaultState {
  data: Recognized | null
}

export interface UnrecognizedState extends DefaultState {
  data: Unrecognized | null
}

export interface ImportsState {
  recognized: RecognizedState
  unrecognized: UnrecognizedState
}

export type ImportsRecognizedLoadStartAction = {
  type: typeof actions.IMPORTS_RECOGNIZED_LOAD_START
}

export type ImportsRecognizedLoadErrorAction = {
  type: typeof actions.IMPORTS_RECOGNIZED_LOAD_ERROR
}

export type ImportsRecognizedLoadFinishAction = {
  type: typeof actions.IMPORTS_RECOGNIZED_LOAD_FINISH
  payload: {
    data: Recognized
  }
}

export type ImportsUnrecognizedLoadStartAction = {
  type: typeof actions.IMPORTS_UNRECOGNIZED_LOAD_START
}

export type ImportsUnrecognizedLoadErrorAction = {
  type: typeof actions.IMPORTS_UNRECOGNIZED_LOAD_ERROR
}

export type ImportsUnrecognizedLoadFinishAction = {
  type: typeof actions.IMPORTS_UNRECOGNIZED_LOAD_FINISH
  payload: {
    data: Unrecognized
  }
}

export type ImportsActionTypes =
  | ImportsRecognizedLoadStartAction
  | ImportsRecognizedLoadErrorAction
  | ImportsRecognizedLoadFinishAction
  | ImportsUnrecognizedLoadStartAction
  | ImportsUnrecognizedLoadErrorAction
  | ImportsUnrecognizedLoadFinishAction
