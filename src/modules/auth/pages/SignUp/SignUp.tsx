import React, { FC, useState, useEffect } from 'react'
import { useLocation, Link } from 'react-router-dom'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import queryString from 'query-string'
import { Container } from 'src/components'
import { ArrowLeftIcon, Text, FormField, Button } from 'ui'
import { theme } from 'ui/theme'
import { Box } from 'ui/layout'
import { merchantSignUp, getSiteKey } from '../../api'
import { loadReCaptcha, ReCaptcha } from 'react-recaptcha-v3'

const SignUp: FC = () => {
  const location = useLocation()
  const queryParams = queryString.parse(location.search)

  const [fullName, setFullName] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [email, setEmail] = useState('')
  const [recaptcha, setRecaptcha] = useState('')
  const [error, setError] = useState(null)
  const [siteKey, setSiteKey] = useState('')
  const [isVerified, setIsVerified] = useState(false)

  useEffect(() => {
    getSiteKey().then(response => {
      if (response.success) {
        loadReCaptcha(response.data.key)
        setSiteKey(response.data.key)
        setIsVerified(true)
      }
    })
  }, [])

  const verifyCallback = recaptchaToken => {
    if (isVerified) {
      setRecaptcha(recaptchaToken)
    }
  }

  const handlSubmit = async event => {
    event.preventDefault()
    try {
      const response = await merchantSignUp(
        fullName,
        email,
        recaptcha,
        phoneNumber,
      )
      if (response?.success) {
        // tslint:disable-next-line: no-console
        console.log('success')
      } else {
        if (response?.data?.message) {
          setError(response?.data?.message)
        }
      }
    } catch (error) {
      // tslint:disable-next-line: no-console
      console.log('something get error')
    }
  }

  return (
    <Container>
      {queryParams.locationBack === 'true' && (
        <Link
          to='/auth/signin'
          style={{
            display: 'flex',
            alignItems: 'center',
            marginBottom: '20px',
            textDecoration: 'none',
          }}
        >
          <ArrowLeftIcon />
          <Text
            ml={20}
            color={theme.color.secondary}
            fontWeight={theme.fontWeight.medium}
            fontSize={12}
          >
            на стартовую
          </Text>
        </Link>
      )}
      <Box
        width={1}
        style={{ boxSizing: 'border-box' }}
        maxWidth={400}
        mx='auto'
        my='auto'
      >
        <Box
          pt={60}
          px={30}
          pb={30}
          backgroundColor={theme.color.white}
          borderRadius={theme.borderRadius.m}
        >
          <Box width={100} mb={40} mx='auto'>
            <img
              style={{ display: 'block', width: '100%' }}
              src='/images/logo.svg'
              alt='logo'
            />
          </Box>
          <Text
            as='p'
            mb={20}
            color={theme.color.secondary}
            fontSize={16}
            lineHeight={theme.lineHeight.s}
          >
            Контактные данные
          </Text>
          <form onSubmit={handlSubmit}>
            <Box mb={20}>
              <FormField
                id='1'
                type='text'
                value={fullName}
                placeholder='ФИО'
                onChange={e => setFullName(e.target.value)}
              />
            </Box>
            <Box mb={20}>
              <FormField
                id='2'
                type='tel'
                value={phoneNumber}
                placeholder='Телефон'
                onChange={e => setPhoneNumber(e.target.value)}
              />
            </Box>
            <Box mb={20}>
              <FormField
                id='3'
                type='email'
                value={email}
                placeholder='Электронная почта'
                onChange={e => setEmail(e.target.value)}
              />
            </Box>
            <Text as='p' color={theme.color.softRed} fontSize={12} mb={16}>
              {error}
            </Text>
            <Condition match={siteKey}>
              <ReCaptcha
                sitekey={siteKey}
                action='signup'
                verifyCallback={verifyCallback}
              />
            </Condition>
            <Button fullWidth size='l' appearance='primary'>
              Войти
            </Button>
          </form>
        </Box>
      </Box>
    </Container>
  )
}

export default SignUp
