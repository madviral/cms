import React, { FC, useState, useEffect } from 'react'
import { useLocation, Link } from 'react-router-dom'
import queryString from 'query-string'
import { Container } from 'src/components'
import { Text, ArrowLeftIcon, FormField, Button } from 'ui'
import { theme } from 'ui/theme'
import { Box } from 'ui/layout'
import { SignUpApplyActions } from '../../actions/interfaces'
import { Appearance } from 'src/ui/inputs/FormField/types/interfaces'
import { Multiselect } from 'ui/dropdowns'

const SignUpApply: FC<SignUpApplyActions> = ({
  bankList,
  companyList,
  getBankList,
  getCompanyTypeList,
  signUpApplyRequest,
}) => {
  const location = useLocation()
  const queryParams = queryString.parse(location.search)

  const [state, setState] = useState({
    companyType: null,
    companyName: '',
    companyIinBin: '',
    bankId: null,
    bankCode: '',
    bankAccount: '',
    legalAddress: '',
    actualAddress: '',
    phoneNumber: '',
    email: '',
    fullName: '',
    jobPosition: '',
    error: null,
  })

  useEffect(() => {
    getCompanyTypeList()
    getBankList()
  }, [])

  const handlSubmit = async event => {
    event.preventDefault()
    const companyType = parseInt(state.companyType, 10)
    const body = {
      address: state.actualAddress,
      bankAccount: state.bankAccount,
      bankId: state.bankId,
      companyName: state.companyName,
      companyType,
      email: state.email,
      fio: state.fullName,
      iinBin: state.companyIinBin,
      jobPosition: state.jobPosition,
      jurAddress: state.legalAddress,
      phoneNumber: state.phoneNumber,
    }
    signUpApplyRequest(body)
  }

  return (
    <Container>
      {queryParams.locationBack === 'true' && (
        <Link
          to='/auth/signin'
          style={{
            display: 'flex',
            alignItems: 'center',
            marginBottom: '20px',
            textDecoration: 'none',
          }}
        >
          <ArrowLeftIcon />
          <Text
            ml={20}
            color={theme.color.secondary}
            fontWeight={theme.fontWeight.medium}
            fontSize={12}
          >
            на стартовую
          </Text>
        </Link>
      )}
      <Box
        display='grid'
        gridTemplateColumns='minmax(230px, 400px) 1fr'
        gridGap='0 20px'
        height='100%'
      >
        <Box
          display='flex'
          flexDirection='column'
          py={30}
          px={20}
          backgroundColor={theme.color.lightBlue}
          borderRadius={theme.borderRadius.m}
        >
          <Box
            display='flex'
            alignItems='center'
            justifyContent='center'
            flex={1}
            mb={20}
          >
            <img src='/images/logoWhite.svg' alt='logo' />
          </Box>
          <Box>
            <Text
              as='p'
              color={theme.color.white}
              fontSize={12}
              textAlign='center'
            >
              Уже есть аккаунт на vLife?
            </Text>
            <Link to='/auth/signin' style={{ textDecoration: 'none' }}>
              <Text
                as='p'
                color={theme.color.white}
                fontSize={12}
                fontWeight={theme.fontWeight.bold}
                textAlign='center'
              >
                Войдите в свой аккаунт
              </Text>
            </Link>
          </Box>
        </Box>
        <Box
          p={30}
          backgroundColor={theme.color.white}
          borderRadius={theme.borderRadius.m}
        >
          <form onSubmit={handlSubmit}>
            <Box
              mb={20}
              borderBottom='1px solid'
              borderColor={theme.color.lightGrayishBlue}
            >
              <Box mb={30}>
                <Text
                  as='p'
                  mb={20}
                  color={theme.color.secondary}
                  fontWeight={theme.fontWeight.medium}
                  fontSize={16}
                  lineHeight='16px'
                >
                  Реквизиты компании
                </Text>
                <Box
                  display='grid'
                  gridTemplateColumns='100px auto 200px'
                  gridGap='0 20px'
                >
                  <Multiselect
                    isAbsolute
                    defaultFirst
                    options={companyList.list}
                    onChange={data =>
                      setState({
                        ...state,
                        companyType: data.value,
                      })
                    }
                  />
                  <FormField
                    id='1'
                    type='text'
                    value={state.companyName}
                    placeholder='Наименование'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        companyName: e.target.value,
                      })
                    }
                  />
                  <FormField
                    id='2'
                    type='number'
                    value={state.companyIinBin}
                    placeholder='ИИН'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        companyIinBin: e.target.value,
                      })
                    }
                  />
                </Box>
              </Box>
              <Box mb={30}>
                <Text
                  as='p'
                  mb={20}
                  color={theme.color.secondary}
                  fontWeight={theme.fontWeight.medium}
                  fontSize={16}
                  lineHeight='16px'
                >
                  Реквизиты компании
                </Text>
                <Box
                  display='grid'
                  gridTemplateColumns='auto 120px 200px'
                  gridGap='0 20px'
                >
                  <Multiselect
                    isAbsolute
                    options={bankList.list}
                    placeholder='Банк'
                    onChange={item => {
                      return setState(prevState => ({
                        ...prevState,
                        bankId: item.value,
                        bankCode: item.data,
                      }))
                    }}
                  />
                  <FormField
                    id='3'
                    type='text'
                    value={state.bankCode}
                    placeholder='БИК'
                    appearance={Appearance.large}
                    disabled
                  />
                  <FormField
                    id='4'
                    type='text'
                    value={state.bankAccount}
                    placeholder='Банковский счет'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        bankAccount: e.target.value,
                      })
                    }
                  />
                </Box>
              </Box>
              <Box mb={30}>
                <Text
                  as='p'
                  mb={20}
                  color={theme.color.secondary}
                  fontWeight={theme.fontWeight.medium}
                  fontSize={16}
                  lineHeight='16px'
                >
                  Адрес
                </Text>
                <Box
                  display='grid'
                  gridTemplateColumns='1fr 1fr'
                  gridGap='0 20px'
                >
                  <FormField
                    id='5'
                    type='text'
                    value={state.legalAddress}
                    placeholder='Юридический адрес'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        legalAddress: e.target.value,
                      })
                    }
                  />
                  <FormField
                    id='4'
                    type='text'
                    value={state.actualAddress}
                    placeholder='Фактический адрес'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        actualAddress: e.target.value,
                      })
                    }
                  />
                </Box>
              </Box>
              <Box mb={30}>
                <Text
                  as='p'
                  mb={20}
                  color={theme.color.secondary}
                  fontWeight={theme.fontWeight.medium}
                  fontSize={16}
                  lineHeight='16px'
                >
                  Контакты
                </Text>
                <Box
                  display='grid'
                  gridTemplateColumns='1fr 1fr'
                  gridGap='0 20px'
                >
                  <FormField
                    id='7'
                    type='tel'
                    value={state.phoneNumber}
                    placeholder='Телефон'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        phoneNumber: e.target.value,
                      })
                    }
                  />
                  <FormField
                    id='8'
                    type='email'
                    value={state.email}
                    placeholder='Электронная почта'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        email: e.target.value,
                      })
                    }
                  />
                </Box>
              </Box>
              <Box mb={30}>
                <Text
                  as='p'
                  mb={20}
                  color={theme.color.secondary}
                  fontWeight={theme.fontWeight.medium}
                  fontSize={16}
                  lineHeight='16px'
                >
                  Контактное лицо
                </Text>
                <Box
                  display='grid'
                  gridTemplateColumns='1fr 1fr'
                  gridGap='0 20px'
                >
                  <FormField
                    id='9'
                    type='text'
                    value={state.fullName}
                    placeholder='Ф.И.О'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        fullName: e.target.value,
                      })
                    }
                  />
                  <FormField
                    id='10'
                    type='text'
                    value={state.jobPosition}
                    placeholder='Должность'
                    appearance={Appearance.large}
                    required
                    onChange={e =>
                      setState({
                        ...state,
                        jobPosition: e.target.value,
                      })
                    }
                  />
                </Box>
              </Box>
            </Box>
            <Text as='p' color={theme.color.softRed} fontSize={12} mb={16}>
              {state.error}
            </Text>
            <Button appearance='outline' size='s'>
              Отправить заявку
            </Button>
          </form>
        </Box>
      </Box>
    </Container>
  )
}

export default SignUpApply
