import React, { useState, useEffect, FC } from 'react'
import { Link, useHistory } from 'react-router-dom'
import {
  FormField,
  Button,
  ReferenceBox,
  NoteIcon,
  EyeIcon,
  MailIcon,
} from 'ui'
import { Box } from 'ui/layout'
import { Text } from 'ui/text'
import { theme } from 'ui/theme'

import { merchantSignIn } from '../../api'
import { SignInActions } from '../../actions/interfaces'
import { Container } from 'src/components'

const SignIn: FC<SignInActions> = ({ accessToken, onAuth }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState({
    value: '',
    type: 'password',
    iconHide: false,
  })
  const [error, setError] = useState(null)
  const router = useHistory()

  useEffect(() => {
    if (accessToken) {
      router.push('/')
    }
  }, [])

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value)
  }

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target
    setPassword({ ...password, value })
  }

  const handleIconClick = () => {
    const hide = !password.iconHide
    const newType = hide ? 'text' : 'password'
    setPassword(prevState => {
      return { ...prevState, type: newType, iconHide: hide }
    })
  }

  const handlSubmit = async event => {
    event.preventDefault()
    try {
      const response = await merchantSignIn(email, password.value)
      if (response?.success) {
        onAuth(response.data)
        router.push('/')
      } else if (response?.data?.message) {
        setError(response.data.message)
      }
    } catch (error) {
      // tslint:disable-next-line: no-console
      console.log('something get error')
    }
  }

  return (
    <Container>
      <Box
        width={1}
        style={{ boxSizing: 'border-box' }}
        maxWidth={400}
        mx='auto'
        my='auto'
      >
        <Box
          pt={60}
          px={30}
          pb={30}
          mb={20}
          backgroundColor={theme.color.white}
          borderRadius={theme.borderRadius.m}
        >
          <Box width={100} mb={30} mx='auto'>
            <img
              style={{ display: 'block', width: '100%' }}
              src='/images/logo.svg'
              alt='logo'
            />
          </Box>
          <Box
            mb={30}
            pb={30}
            borderBottom='1px solid'
            borderColor={theme.color.lightGrayishBlue}
          >
            <form onSubmit={handlSubmit}>
              <Box mb={20}>
                <FormField
                  id='1'
                  type='text'
                  value={email}
                  label='E-mail адрес'
                  placeholder='name@name.kz'
                  onChange={handleEmailChange}
                />
              </Box>
              <Box mb={30}>
                <Box display='flex' justifyContent='space-between' mb='8px'>
                  <Text fontSize='14px' color='secondary.0'>
                    <label htmlFor='2'>Пароль</label>
                  </Text>

                  <Text
                    as='p'
                    color={theme.color.primary}
                    fontSize={12}
                    lineHeight='14px'
                    fontWeight={theme.fontWeight.medium}
                  >
                    Забыли пароль?
                  </Text>
                </Box>
                <FormField
                  id='2'
                  type={password.type}
                  value={password.value}
                  placeholder='***********'
                  icon={<EyeIcon width={20} height={20} />}
                  onChange={handlePasswordChange}
                  onIconClick={handleIconClick}
                />
              </Box>
              <Text as='p' color={theme.color.softRed} fontSize={12} mb={16}>
                {error}
              </Text>
              <Button fullWidth appearance='primary' size='l'>
                Войти
              </Button>
            </form>
          </Box>
          <Box
            display='flex'
            alignItems='stretch'
            justifyContent='space-between'
          >
            <ReferenceBox
              text='Создать аккаунт'
              link='/auth/signup?locationBack=true'
            >
              <NoteIcon width={60} className='icon' />
            </ReferenceBox>
            <ReferenceBox
              text='Заявка на регистрацию'
              link='/auth/signup-request?locationBack=true'
            >
              <MailIcon width={60} className='icon' />
            </ReferenceBox>
          </Box>
        </Box>
        <Link to='/' style={{ textDecoration: 'none' }}>
          <Text
            as='p'
            color={theme.color.primary}
            fontSize={12}
            textAlign='center'
          >
            Узнать об условиях сотрудничества vLife
          </Text>
        </Link>
      </Box>
    </Container>
  )
}

export default SignIn
