import { ActionCreator } from 'redux'
import { signUpApplyConstants } from '../constants'
import {
  fetchCompanyTypeList,
  fetchBankList,
  merchantSignUpApply,
} from '../api'
import { SignUpApply } from './interfaces'

export const getBankList = () => {
  return dispatch => {
    return fetchBankList().then(response => {
      if (response.success) {
        const data = response.data.map(element => {
          return {
            value: element.id,
            label: element.name,
            data: element.bic,
          }
        })
        return dispatch(bankListSucceeded(data))
      }
    })
  }
}

export const bankListSucceeded: ActionCreator<any> = (payload: []) => ({
  type: signUpApplyConstants.GET_BANKLIST_SUCCESS,
  payload,
})

export const getCompanyTypeList = () => {
  return dispatch => {
    return fetchCompanyTypeList().then(response => {
      if (response.success) {
        const data = response.data.map(element => {
          return {
            value: element.code,
            label: element.value,
          }
        })
        return dispatch(companyTypeListSucceeded(data))
      }
    })
  }
}

export const companyTypeListSucceeded: ActionCreator<any> = (payload: []) => ({
  type: signUpApplyConstants.GET_COMPANYTYPELIST_SUCCESS,
  payload,
})

export const signUpApplyRequest = (body: SignUpApply) => {
  return dispatch => {
    return merchantSignUpApply(body).then(response => {
      if (response.success) {
        alert(response.statussText)
      }
    })
  }
}
