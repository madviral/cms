import { ActionCreator } from 'redux'
import { signUpConstants } from '../constants'

export const signUpRequest: ActionCreator<any> = () => ({
  type: signUpConstants.SIGNUP_REQUEST,
})

export const signUpSuccess: ActionCreator<any> = (payload: any) => ({
  type: signUpConstants.SIGNUP_REQUEST,
  payload,
})

export const signUpFailure: ActionCreator<any> = error => ({
  type: signUpConstants.SIGNUP_FAILURE,
  error,
})
