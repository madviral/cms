import { ActionCreator } from 'redux'
import { signInConstants } from '../constants'

export const signInRequest: ActionCreator<any> = () => ({
  type: signInConstants.SIGNIN_REQUEST,
})

export const signInSuccess: ActionCreator<any> = (token: string) => ({
  type: signInConstants.SIGNIN_SUCCESS,
  token,
})

export const signInFailure: ActionCreator<any> = error => ({
  type: signInConstants.SIGNIN_FAILURE,
  error,
})
