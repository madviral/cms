import { IToken } from 'src/modules/common/types'

export interface SignInActions {
  accessToken?: string
  onAuth: (token: IToken) => void
}

export interface SignUpApplyActions {
  bankList?: { loaded: boolean; list: [] }
  companyList?: { loaded: boolean; list: [] }
  getBankList: () => void
  getCompanyTypeList: () => void
  signUpApplyRequest: (body: SignUpApply) => void
}

export interface SignUpApply {
  address: string
  bankAccount: string
  bankId: number
  companyName: string
  companyType: number
  email: string
  fio: string
  iinBin: string
  jobPosition: string
  jurAddress: string
  phoneNumber: string
}
