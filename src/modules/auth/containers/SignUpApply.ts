import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { SignUpApply } from '../pages'
import * as signUpApplyActions from '../actions/signUpApply'

const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators(signUpApplyActions, dispatch),
})

const mapStateToProps = state => ({
  bankList: state.bankList,
  companyList: state.companyTypeList,
})

export default connect(mapStateToProps, mapDispatchToProps)(SignUpApply)
