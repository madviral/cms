import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { SignUp } from '../pages'
import * as signUpActions from '../actions/signUp'

const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators(signUpActions, dispatch),
})

export default connect(null, mapDispatchToProps)(SignUp)
