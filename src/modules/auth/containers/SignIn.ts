import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { auth } from 'src/modules/common/actions/credentials'
import { SignIn } from '../pages'
import * as signInActions from '../actions/signIn'

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onAuth: token => dispatch(auth(token)),
  ...bindActionCreators(signInActions, dispatch),
})

export default connect(
  state => ({
    accessToken: (state as any).common.credentials.accessToken,
  }),
  mapDispatchToProps,
)(SignIn)
