import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { routes } from './routes'

const Auth = () => {
  return (
    <Switch>
      {routes.map(route => (
        <Route
          key={route.key}
          path={route.path}
          exact={route.exact}
          component={route.component}
        />
      ))}
    </Switch>
  )
}

export default Auth
