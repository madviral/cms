import { POST_REQUEST, GET_REQUEST } from 'lib/api'
import { SignUpApply } from '../actions/interfaces'

const merchantSignIn = (email: string, password: string): Promise<any> =>
  POST_REQUEST('auth/merchant/login', {
    email,
    password,
  })

const merchantSignUp = (
  fio: string,
  email: string,
  gRecaptchaResponse: string,
  phoneNumber: string,
): Promise<any> =>
  POST_REQUEST('user/v1/merchant/register-request', {
    fio,
    gRecaptchaResponse,
    email,
    phoneNumber,
  })

const merchantSignUpApply = (body: SignUpApply): Promise<any> =>
  POST_REQUEST('user/v1/merchant/full-register-request', body)

const fetchCompanyTypeList = (): Promise<any> =>
  GET_REQUEST('user/v1/dict/company-types')
const fetchBankList = (): Promise<any> => GET_REQUEST('user/v1/dict/banks')
const getSiteKey = (): Promise<any> =>
  GET_REQUEST('user/v1/config/captcha-site-key')

export {
  merchantSignIn,
  merchantSignUp,
  merchantSignUpApply,
  fetchBankList,
  fetchCompanyTypeList,
  getSiteKey,
}
