import { IMapRoute } from 'src/modules/common/types'
import SignIn from './containers/SignIn'
import SignUp from './containers/SignUp'
import SignUpApply from './containers/SignUpApply'

export const routes: IMapRoute[] = [
  {
    path: '/auth/signin',
    key: 'signin',
    component: SignIn,
  },
  {
    path: '/auth/signup',
    key: 'signup',
    component: SignUp,
  },
  {
    path: '/auth/signup-request',
    key: 'signup-request',
    component: SignUpApply,
  },
]
