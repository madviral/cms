export interface CompanyItem {
  code: string
  value: string
}

export interface BankItem {
  id: number
  name: string
  bic: string
}
