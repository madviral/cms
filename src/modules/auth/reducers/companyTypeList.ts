import { signUpApplyConstants } from '../constants'
import { CompanyItem } from './interfaces'

export interface CompanyTypesState {
  loaded: boolean
  list: CompanyItem[]
}

const initialState: CompanyTypesState = {
  loaded: false,
  list: [],
}

export function companyTypeList(
  state = initialState,
  action,
): CompanyTypesState {
  switch (action.type) {
    case signUpApplyConstants.GET_COMPANYTYPELIST_SUCCESS:
      return {
        ...state,
        loaded: true,
        list: action.payload,
      }

    default:
      return state
  }
}
