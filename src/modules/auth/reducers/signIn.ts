import { signInConstants } from '../constants'

export interface SignInState {
  isAuthenticated: boolean
  token?: string
}

const initialState: SignInState = {
  isAuthenticated: false,
  token: '',
}

export function signIn(state = initialState, action): SignInState {
  switch (action.type) {
    case signInConstants.SIGNIN_REQUEST:
      return {
        ...state,
      }

    case signInConstants.SIGNIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
      }

    default:
      return state
  }
}
