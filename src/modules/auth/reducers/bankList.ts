import { signUpApplyConstants } from '../constants'
import { BankItem } from './interfaces'

export interface BankListState {
  loaded: boolean
  list: BankItem[]
}

const initialState: BankListState = {
  loaded: false,
  list: [],
}

export function bankList(state = initialState, action): BankListState {
  switch (action.type) {
    case signUpApplyConstants.GET_BANKLIST_SUCCESS:
      return {
        ...state,
        loaded: true,
        list: action.payload,
      }

    default:
      return state
  }
}
