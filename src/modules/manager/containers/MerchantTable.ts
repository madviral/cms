import { connect } from 'react-redux'
import { MerchantTable } from '../components'
import { getAllMerchants } from 'src/modules/common/actions/user'
import { RootState } from 'src/store/rootReducer'

const mapStateToProps = (state: RootState) => ({
  merchants: state.common.user.merchants,
  items: state.shop.catalog.list,
  pageCount: 1,
  pageIndex: 1,
  pageSize: 'Все',
})

const mapDispatchToProps = {
  getAllMerchants,
}

export default connect(mapStateToProps, mapDispatchToProps)(MerchantTable)
