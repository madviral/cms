import { connect } from 'react-redux'
import { Partners } from '../pages'
import { triggerNewMerchantDrawer } from '../actions/newMerchantDrawer'

const mapDispatchToProps = {
  triggerNewMerchantDrawer,
}

const mapStateToProps = state => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Partners)
