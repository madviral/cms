import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { NewMerchantDrawer } from '../components'
import { triggerNewMerchantDrawer } from 'src/modules/manager/actions/newMerchantDrawer'

const mapStateToProps = (state: RootState) => ({
  isOpenNewMerchantDrawer: state.manager.isOpenNewMerchantDrawer,
})

const mapDispatchToProps = {
  triggerNewMerchantDrawer,
}

export default connect(mapStateToProps, mapDispatchToProps)(NewMerchantDrawer)
