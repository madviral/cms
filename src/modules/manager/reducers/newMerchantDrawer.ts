import { createReducer } from 'lib/utils'
import * as actions from '../constants/newMerchantDrawer'

const initialState = {
  isOpenNewMerchantDrawer: false,
}

export default createReducer(initialState, {
  [actions.TRIGGER]: state => ({
    isOpenNewMerchantDrawer: !state.isOpenNewMerchantDrawer,
  }),
})
