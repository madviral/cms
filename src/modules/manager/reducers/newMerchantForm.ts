import { createReducer } from 'lib/utils'
import * as actions from '../constants/newMerchantForm'

const initialState = {
  loading: false,
  error: null,
  success: null,
}

export default createReducer(initialState, {
  [actions.LOADING]: (state, { payload }: any) => ({
    ...state,
    loading: true,
    error: false,
  }),
  [actions.FINISH]: (state, { payload }: any) => ({
    loading: false,
    error: null,
    success: true,
  }),
  [actions.ERROR]: (state, { payload }: any) => ({
    loading: false,
    error: payload.data,
    success: false,
  }),
})
