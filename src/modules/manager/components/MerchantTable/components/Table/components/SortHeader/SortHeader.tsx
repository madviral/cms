import React, { FC } from 'react'
import { Chevron, ExpanderIcon } from 'ui'
import { Box } from 'ui/layout'
// import { useTranslation } from 'react-i18next'
// import { HeaderTooltipItem, RangeFieldWrap } from './ui'

interface PriceHeader {
  value?: string
  isSelect?: boolean
}

// const Range = ({ setChecked, checked, from, setFrom, to, setTo }) => {
//  const [t] = useTranslation()
//  const onChange = (check: boolean) => {
//    setChecked(check)
//  }
//
//  return (
//    <>
//      <HeaderTooltipItem>
//        <FormSwitch onChange={onChange} checked={checked} label={t('range')} />
//      </HeaderTooltipItem>
//      <Box display={checked ? 'block' : 'none'}>
//        <HeaderTooltipItem>
//          <RangeFieldWrap>
//            <p>{t('from')}</p>
//            <Box ml='4px'>
//              <FormField
//                value={from}
//                onChange={e => setFrom(Number(e.target.value))}
//                type='number'
//              />
//            </Box>
//          </RangeFieldWrap>
//          <RangeFieldWrap>
//            <p>{t('to')}</p>
//            <Box ml='4px'>
//              <FormField
//                value={to}
//                onChange={e => setTo(Number(e.target.value))}
//                type='number'
//              />
//            </Box>
//          </RangeFieldWrap>
//        </HeaderTooltipItem>
//      </Box>
//    </>
//  )
// }

// const radioSorting = [
//  {
//    id: 'asc',
//    name: 'price_sort',
//    label: 'ascending',
//    value: 3,
//  },
//  {
//    id: 'desc',
//    name: 'price_sort',
//    label: 'descending',
//    value: 4,
//  },
// ]

// const RadioSort = ({ sort, setSort }) => {
//  const [t] = useTranslation('')
//
//  return (
//    <>
//      {radioSorting.map(sr => (
//        <HeaderTooltipItem>
//          <FormRadioButton
//            id={sr.id}
//            onChange={() => setSort(sr.value)}
//            name={sr.name}
//            label={t(sr.label)}
//            value={sr.value}
//            checked={sort === sr.value}
//          />
//        </HeaderTooltipItem>
//      ))}
//    </>
//  )
// }

const SortHeader: FC<PriceHeader> = ({ value, isSelect }) => {
  return (
    <div style={{ cursor: 'pointer' }}>
      <Box
        data-tip
        data-for={isSelect && 'price_tooltip'}
        data-event={isSelect && 'click'}
        display='flex'
        flexDirection='row'
        alignItems='center'
      >
        <p>{value}</p>
        {isSelect ? (
          <Chevron fill='#9FB3C8' />
        ) : (
          <ExpanderIcon fill='#9FB3C8' width={20} height={15} />
        )}
      </Box>
    </div>
  )
}

export default SortHeader
