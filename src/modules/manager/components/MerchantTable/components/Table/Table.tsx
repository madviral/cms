import React, { FC, useCallback, useMemo, useEffect } from 'react'
import BaseTable, { ColumnType } from 'ui/table/controlled/Table'
import { useTranslation } from 'react-i18next'
import { Box } from 'ui/layout'
import { Dropdown } from 'ui/dropdowns/Dropdown'
import { Caption } from 'ui/caption'
import { DotsIcon } from 'ui/icons'
import { GearsIcon } from 'ui/icons/Gears'
import { FormCheckbox } from 'ui/inputs/FormCheckbox'
import { SortHeader } from './components'
import { theme } from 'ui/theme'

type MerchantTable = {
  checkboxName?: string
  checkedList: string[]
  onChangeCbx: (id: string | number) => void
  items: any[]
}

const Table: FC<MerchantTable> = ({
  checkedList,
  checkboxName,
  items,
  onChangeCbx,
}) => {
  const [t] = useTranslation('manager')
  const statuses = {
    TERMINATED: { color: theme.color.alert, value: 'Расторгнут' },
    PROCESSED: { color: theme.color.warning, value: 'В обработке' },
    SIGNED: { color: theme.color.darkerSuccess, value: 'Подписан' },
  }

  useEffect(() => {
    items.reverse()
  }, [])

  const data = useMemo(
    () =>
      items?.reverse().map(item => ({
        ...item,
        key: item.id,
        vendor: item.formJur?.value + ' ' + item.jurName,
        vendorSign: item.jurName,
        status: (
          <Caption
            color={
              item.status ? statuses[item.status].color : theme.color.warning
            }
          >
            {item.status ? statuses[item.status].value : 'В обработке'}
          </Caption>
        ),
        direction: item.companyDirection.value,
        contactName: item.fio,
        inCharge: item.manager,
      })),
    [items?.length],
  )

  const columns = useMemo(
    (): Array<ColumnType<any>> => [
      {
        title: (
          <Box paddingLeft={'15px'}>
            <GearsIcon />
          </Box>
        ),
        dataIndex: 'id',
        key: 'checkbox',
        width: 52,
        render: id => {
          const onChangeCbxCached = useCallback(() => {
            onChangeCbx(id)
          }, [id])
          return (
            <Box pl='16px'>
              <FormCheckbox
                id={id}
                name={checkboxName}
                value={id}
                checked={checkedList?.some(itemId => itemId === id)}
                onChange={onChangeCbxCached}
              />
            </Box>
          )
        },
      },
      {
        title: <SortHeader value={t('vendor')} />,
        dataIndex: 'vendor',
        key: 'vendor',
        width: 156,
        ellipsis: true,
      },
      {
        title: <SortHeader value={t('vendorSign')} />,
        dataIndex: 'vendorSign',
        key: 'vendorSign',
        width: 156,
        ellipsis: true,
      },
      {
        title: <SortHeader value={t('status')} isSelect />,
        dataIndex: 'status',
        key: 'status',
        width: 106,
      },
      {
        title: <SortHeader value={t('direction')} isSelect />,
        dataIndex: 'direction',
        key: 'direction',
        width: 126,
      },
      {
        title: <SortHeader value={t('contactName')} />,
        dataIndex: 'contactName',
        key: 'contactName',
        width: 196,
        ellipsis: true,
      },
      {
        title: <SortHeader value={t('inCharge')} />,
        dataIndex: 'inCharge',
        key: 'inCharge',
        width: 162,
      },
      {
        title: '',
        dataIndex: 'but',
        key: 'but',
        width: 20,
        fixed: 'right',
        render: () => {
          return (
            <Box
              display='flex'
              alignItems='center'
              width='100%'
              height='100%'
              backgroundColor='white'
            >
              <Dropdown>
                <Dropdown.SimpleButton noCarret>
                  <DotsIcon />
                </Dropdown.SimpleButton>
                <Dropdown.Menu direction='l' width='216px'></Dropdown.Menu>
              </Dropdown>
            </Box>
          )
        },
      },
    ],
    [checkedList?.length, onChangeCbx],
  )

  return (
    <BaseTable
      data={data}
      columns={columns}
      isPlaceholder={items.length === 0}
      placeholderText={t('noMerchants')}
    />
  )
}

export default Table
