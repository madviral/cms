import React, { FC, useState, useEffect } from 'react'
import { Box } from 'ui/layout'
import { theme, styled } from 'ui/theme'
import { FormField, Treeselect, Caption, Button } from 'ui'
import { useTranslation } from 'react-i18next'
import * as yup from 'yup'

const ButtonWrap = styled.div`
  margin-left: 16px;
  display: inline-block;
  float: right;
`

const NewMerchantForm: FC<any> = ({
  triggerNewMerchantDrawer,
  createMerchant,
  newMerchantStatus,
}) => {
  const [t] = useTranslation(['manager', 'common'])
  const [e] = useTranslation('errorMessages')
  const [isDisabled, setIsDisabled] = useState(true)
  const [formData, setFormData] = useState({
    companyDirection: null,
    companyName: '',
    formJur: '',
    jurName: '',
    iinBin: '',
    kbe: null,
    bankAccount: '',
    bankId: '',
    jurAddress: '',
    factAddress: '',
    phoneNumber: '',
    email: '',
    fio: '',
    jobPosition: '',
    status: null,
    responsibleManager: '',
  })
  const [formJurDict] = useState([
    {
      id: 1,
      name: 'ИП',
    },
    {
      id: 2,
      name: 'ТОО',
    },
  ])
  const [companyDirectionDict] = useState([
    {
      id: 1,
      name: 'Маркет',
    },
    {
      id: 2,
      name: 'Услуги',
    },
    {
      id: 3,
      name: 'Здоровье',
    },
  ])
  const [statusDict] = useState([
    {
      id: 0,
      name: 'В обработке',
    },
    {
      id: 1,
      name: 'Подписан',
    },
    {
      id: 2,
      name: 'Расторгнут',
    },
  ])
  const [bankDict] = useState([
    {
      id: 'bbc55432-16bf-4e7c-b017-e50f2058dcae',
      name: 'АО Народный Банк Казахстана',
      code: 'HSBKKZKX',
    },
    {
      id: 'a295ff71-74fd-41a3-ba10-9f01b3b886ee',
      name: 'АО KASPI BANK',
      code: 'CASPKZKA',
    },
    {
      id: '107372d8-3974-444b-b514-2d60a8202324',
      name: 'АО АЛЬФА-БАНК',
      code: 'ALFAKZKA',
    },
    {
      id: '4f586603-375f-4900-af1f-2dc1f8e7b2fa',
      name: 'АО ForteBank',
      code: 'IRTYKZKA',
    },
    {
      id: 'b5ab98f6-5d06-4dc2-921a-c131fa22271e',
      name: 'First Heartland Jýsan Bank',
      code: 'TSESKZKA',
    },
    {
      id: '533400e4-ccf3-11ea-87d0-0242ac130003',
      name: 'Банк ЦентрКредит',
      code: 'KCJBKZKX',
    },
    {
      id: '53340300-ccf3-11ea-87d0-0242ac130003',
      name: 'ДБ АО «Сбербанк России»',
      code: 'SABRKZKA',
    },
    {
      id: 'bd2c3f54-cd8c-11ea-87d0-0242ac130003',
      name: 'Евразийский Банк',
      code: 'EURIKZKA',
    },
    {
      id: '3d6df862-ce62-11ea-87d0-0242ac130003',
      name: 'АТФБанк',
      code: 'ALMNKZKA',
    },
    {
      id: 'td6df862-te62-t1ea-t7d0-t242ac13tttt',
      name: 'test',
      code: 'ASDQEVAS',
    },
  ])

  const schema = yup.object().shape({
    companyDirection: yup.number().required(),
    companyName: yup.string().required(),
    formJur: yup.number().required(),
    jurName: yup.string().required(),
    iinBin: yup.string().required(),
    kbe: yup.number().required(),
    bankAccount: yup.string().required(),
    bankId: yup.string().required(),
    jurAddress: yup.string().required(),
    factAddress: yup.string().required(),
    phoneNumber: yup.string().required(),
    email: yup
      .string()
      .email()
      .required(),
    fio: yup.string().required(),
    //  jobPosition: yup.string().required(),
    status: yup.number().required(),
  })

  useEffect(() => {
    setIsDisabled(true)
    if (newMerchantStatus.success) {
      window.location.reload()
      triggerNewMerchantDrawer()
    } else if (newMerchantStatus.error) {
      setIsDisabled(false)
      alert('an error occured')
    }
  }, [newMerchantStatus.success, newMerchantStatus.error])

  useEffect(() => {
    schema.isValid(formData).then((valid: boolean) => setIsDisabled(!valid))
  }, [formData])

  return (
    <Box
      display='grid'
      gridTemplateRows='60px 60px auto 64px'
      height='100%'
      padding='32px 16px 0 16px'
      gridGap='16px'
    >
      <Box
        display='grid'
        gridTemplateColumns='calc(50% - 8px) calc(50% - 8px)'
        gridGap='16px'
      >
        <FormField
          value={formData.companyName}
          label={t('shopName') + ' *'}
          placeholder={t('enterShopName')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, companyName: event.target.value })
          }
          error={e('requiredField')}
        />
        <Box>
          <Box height='16px' marginBottom='4px'>
            <Caption fontSize={theme.fontSize.s}>
              {t('direction') + ' *'}
            </Caption>
          </Box>
          <Treeselect
            placeholder={t('selectFromList')}
            options={companyDirectionDict}
            isSingleItemSelector
            callback={(value, flag?) =>
              flag
                ? setFormData({ ...formData, companyDirection: null })
                : setFormData({ ...formData, companyDirection: value.id })
            }
            error={e('didNotSelect')}
            isAbsolute
          />
        </Box>
      </Box>
      <Box
        display='grid'
        gridTemplateColumns='140px 181px 181px 50px'
        gridGap='16px'
      >
        <Box>
          <Box height='16px' marginBottom='4px'>
            <Caption fontSize={theme.fontSize.s}>
              {t('merchantForm') + ' *'}
            </Caption>
          </Box>
          <Treeselect
            placeholder={''}
            options={formJurDict}
            isSingleItemSelector
            callback={(value, flag?) =>
              flag
                ? setFormData({ ...formData, formJur: null })
                : setFormData({ ...formData, formJur: value.id })
            }
            error={e('didNotSelect')}
            isAbsolute
          />
        </Box>
        <FormField
          value={formData.jurName}
          label={t('merchantName') + ' *'}
          placeholder={t('enterName')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, jurName: event.target.value })
          }
          error={e('requiredField')}
        />
        <FormField
          value={formData.iinBin}
          label={t('BIN') + ' *'}
          placeholder={t('enterBIN')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, iinBin: event.target.value })
          }
          error={e('requiredField')}
        />
        <FormField
          value={formData.kbe}
          label={t('KBE') + ' *'}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, kbe: Number(event.target.value) })
          }
          error={e('requiredField')}
        />
      </Box>
      <Box
        display='grid'
        gridTemplateRows='60px 60px 60px 60px auto'
        gridTemplateColumns='calc(50% - 8px) calc(50% - 8px)'
        gridGap='16px'
      >
        <Box>
          <Box height='16px' marginBottom='4px'>
            <Caption fontSize={theme.fontSize.s}>{t('bank') + ' *'}</Caption>
          </Box>
          <Treeselect
            placeholder={t('selectFromList')}
            options={bankDict}
            isSingleItemSelector
            callback={(value, flag?) =>
              flag
                ? setFormData({ ...formData, bankId: null })
                : setFormData({ ...formData, bankId: value.id })
            }
            error={e('didNotSelect')}
            isAbsolute
          />
        </Box>
        <FormField
          value={formData.bankAccount}
          label={t('bankCheck') + ' *'}
          placeholder={t('enterBankCheck')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, bankAccount: event.target.value })
          }
          error={e('requiredField')}
        />
        <FormField
          value={formData.jurAddress}
          label={t('merchantAddress') + ' *'}
          placeholder={t('enterAddress')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, jurAddress: event.target.value })
          }
          error={e('requiredField')}
        />
        <FormField
          value={formData.factAddress}
          label={t('factAddress') + ' *'}
          placeholder={t('enterAddress')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, factAddress: event.target.value })
          }
          error={e('requiredField')}
        />
        <FormField
          value={formData.phoneNumber}
          label={t('phone') + ' *'}
          placeholder={t('enterPhone')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, phoneNumber: event.target.value })
          }
          error={e('requiredField')}
        />
        <FormField
          value={formData.email}
          label={t('email') + ' *'}
          placeholder={t('emailExample')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, email: event.target.value })
          }
          error={e('requiredField')}
        />
        <FormField
          value={formData.fio}
          label={t('contactPerson') + ' *'}
          placeholder={t('FIO')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, fio: event.target.value })
          }
          error={e('requiredField')}
        />
        <FormField
          value={formData.jobPosition}
          label={t('position') + ' *'}
          placeholder={t('enterPosition')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, jobPosition: event.target.value })
          }
          error={e('requiredField')}
        />
        <Box>
          <Box height='16px' marginBottom='4px'>
            <Caption fontSize={theme.fontSize.s}>
              {t('contractStatus') + ' *'}
            </Caption>
          </Box>
          <Treeselect
            placeholder={t('selectFromList')}
            options={statusDict}
            isSingleItemSelector
            callback={(value, flag?) => {
              flag
                ? setFormData({ ...formData, status: null })
                : setFormData({ ...formData, status: value.id })
            }}
            error={e('didNotSelect')}
            isAbsolute
          />
        </Box>

        <FormField
          value={formData.responsibleManager}
          label={t('responsibleManager')}
          placeholder={t('FIO')}
          labelFontSize={12}
          onChange={event =>
            setFormData({ ...formData, responsibleManager: event.target.value })
          }
        />
      </Box>
      <Box
        borderTop={'1px solid ' + theme.colorsList.secondary[2]}
        padding='16px 0'
        float='right'
      >
        <ButtonWrap>
          <Button
            size='m'
            appearance='primary'
            disabled={isDisabled}
            onClick={() => createMerchant(formData)}
          >
            {t('createMerchant')}
          </Button>
        </ButtonWrap>

        <ButtonWrap>
          <Button
            size='m'
            appearance='secondary'
            onClick={() => triggerNewMerchantDrawer()}
          >
            {t('back')}
          </Button>
        </ButtonWrap>
      </Box>
    </Box>
  )
}

export default NewMerchantForm
