import { connect } from 'react-redux'
import { RootState } from 'src/store/rootReducer'
import { NewMerchantForm } from '../components'
import { triggerNewMerchantDrawer } from 'src/modules/manager/actions/newMerchantDrawer'
import { createMerchant } from 'src/modules/manager/actions/newMerchantForm'

const mapStateToProps = (state: RootState) => ({
  newMerchantStatus: state.newMerchant,
})

const mapDispatchToProps = {
  triggerNewMerchantDrawer,
  createMerchant,
}

export default connect(mapStateToProps, mapDispatchToProps)(NewMerchantForm)
