import React, { FC } from 'react'
import { Drawer } from 'src/ui/drawer'
import NewMerchantForm from './containers/NewMerchantForm'

interface NewMerchantDrawerProps {
  isOpenNewMerchantDrawer: boolean
  triggerNewMerchantDrawer: () => void
}

const NewMerchantDrawer: FC<NewMerchantDrawerProps> = ({
  isOpenNewMerchantDrawer,
  triggerNewMerchantDrawer,
}) => {
  return (
    <Drawer
      width='632px'
      handler={false}
      open={isOpenNewMerchantDrawer}
      level={null}
      className='drawer1'
      placement='right'
      onClose={() => triggerNewMerchantDrawer()}
      maskStyle={{
        background: '#000000',
        opacity: '0.15',
        boxSizing: 'border-box',
        overflowY: 'hidden',
      }}
    >
      <NewMerchantForm />
    </Drawer>
  )
}

export default NewMerchantDrawer
