import React, { FC } from 'react'
import { Box } from 'ui/layout'
import { Text } from 'ui'
import { theme } from 'ui/theme'
import { GlobalSearch } from 'ui/inputs/GlobalSearch'
import { Button } from 'ui/buttons'
import { useTranslation } from 'react-i18next'
import { Queries } from '../../types/enums'
import MerchantTable from '../../containers/MerchantTable'
import NewMerchantDrawer from '../../containers/NewMerchantDrawer'

interface PartnersProps {
  triggerNewMerchantDrawer: () => void
}

const Partners: FC<PartnersProps> = ({ triggerNewMerchantDrawer }) => {
  const [t] = useTranslation('manager')
  const queries = new URLSearchParams(location.search)
  const merchantID = queries.get(Queries.SELECTED_MERCHANT)

  return (
    <Box
      display='grid'
      gridTemplateRows='[title] 34px [content] 1fr'
      gridGap='16px 0'
      height='100%'
    >
      <Box
        gridArea='title 1'
        display='flex'
        alignItems='center'
        justifyContent='space-between'
      >
        <Text
          fontSize={18}
          lineHeight={1}
          color={theme.color.secondary}
          fontWeight={500}
        >
          {t('partners')}
        </Text>

        <Box ml='auto'>
          <GlobalSearch isDefaultOpen isAbleToHide={false} />
        </Box>
        <Box ml={20}>
          <Button
            appearance='primary'
            size='m'
            onClick={() => triggerNewMerchantDrawer()}
          >
            + {t('addPartner')}
          </Button>
          <NewMerchantDrawer />
        </Box>
      </Box>
      <Box gridArea='content 1' minHeight='fit-content'>
        <MerchantTable merchantID={merchantID} />
      </Box>
    </Box>
  )
}

export default Partners
