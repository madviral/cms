import * as actions from '../constants/newMerchantDrawer'
import { AppThunk } from 'src/store/rootReducer'
import { ActionCreator } from 'redux'

export const triggerNewMerchantDrawer: ActionCreator<any> = (): AppThunk => async dispatch => {
  dispatch({
    type: actions.TRIGGER,
  })
}
