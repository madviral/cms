import * as actions from '../constants/newMerchantForm'

export const createMerchant = (formData: any) => async (
  dispatch,
  getState,
  client,
) => {
  dispatch({
    type: [actions.LOADING],
  })
  const { data, error } = await client.post('user/v1/profiles', formData)

  if (!error) {
    dispatch({
      type: [actions.FINISH],
      payload: data,
    })
  } else {
    dispatch({
      type: [actions.ERROR],
      payload: error,
    })
  }
}
