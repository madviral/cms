import React from 'react'
import { UsersIcon } from 'ui/icons'
import { Redirect } from 'react-router-dom'
import { RenderRoutes } from 'src/modules/common/components/Routes'
import { getDashedStr } from 'lib/utils'
import Partners from './containers/Partners'

enum KEYS {
  PARTNERS = 'PARTNERS',
  REDIRECT = 'REDIRECT',
}

export const getRoutes = rootKey => [
  {
    path: '/manager',
    key: getDashedStr(rootKey, KEYS.REDIRECT),
    exact: true,
    skip: true,
    component: () => <Redirect to={'/manager/partners'} />,
  },
  {
    path: '/manager/partners',
    key: getDashedStr(rootKey, KEYS.PARTNERS),
    exact: true,
    component: RenderRoutes,
    nav: {
      name: 'manager:partners',
      icon: UsersIcon, // TODO: icon
    },
    routes: [
      {
        path: '/manager/partners',
        key: getDashedStr(rootKey, KEYS.PARTNERS),
        exact: true,
        component: Partners,
        nav: {
          name: 'manager:partners',
        },
      },
    ],
  },
]
