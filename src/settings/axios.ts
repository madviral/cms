import axios, { AxiosInstance } from 'axios'

const axiosConfig: AxiosInstance = axios.create({
  baseURL: process.env.BASE_URL,
})

export default axiosConfig
