import { RouteMarketIcon, RouteServicesIcon, RouteManager } from 'ui'
import { getRoutes as getManagerRoutes } from './modules/manager/routes'
import { getRoutes as getMarketRoutes } from './modules/shop/routes'
import { getRoutes as getServicesRoutes } from './modules/services/routes'
import { getRoutes as getFeedRoutes } from './modules/feed/routes'
import { routes as authRoutes } from './modules/auth/routes'
import RenderMainRoutes from './containers/RenderMainRoutes'
import { RenderRoutes } from './modules/common/components/Routes'
import { Access } from './modules/common/types/enums'

enum KEYS {
  AUTH = 'AUTH',
  INDEX = 'INDEX',
  MARKET = 'MARKET',
  SERVICES = 'SERVICES',
  FEED = 'FEED',
  MANAGER = 'MANAGER',
}

export const mainRoutes = [
  {
    path: '/shop',
    key: KEYS.MARKET,
    exact: false,
    routes: getMarketRoutes(KEYS.MARKET),
    component: RenderRoutes,
    nav: {
      name: 'common:market',
      icon: RouteMarketIcon,
      childs: [],
    },
    access: Access.MERCHANT,
  },
  {
    path: '/services',
    key: KEYS.SERVICES,
    exact: false,
    routes: getServicesRoutes(KEYS.SERVICES),
    component: RenderRoutes,
    nav: {
      name: 'common:services',
      icon: RouteServicesIcon,
      childs: [],
    },
    // TODO Rewrite to ACCESS.SERVICES
    access: Access.MERCHANT,
  },
  {
    path: '/feed',
    key: KEYS.FEED,
    exact: false,
    routes: getFeedRoutes(KEYS.FEED),
    component: RenderRoutes,
    nav: {
      name: 'common:feed',
      icon: RouteServicesIcon,
      childs: [],
    },
    access: Access.FEED,
  },
  {
    path: '/manager',
    key: KEYS.MANAGER,
    exact: false,
    routes: getManagerRoutes(KEYS.MANAGER),
    component: RenderRoutes,
    nav: {
      name: 'manager:manager',
      icon: RouteManager,
      childs: [],
    },
    access: Access.MANAGER,
  },
]

export const routes = [
  {
    path: '/auth',
    key: KEYS.AUTH,
    component: RenderRoutes,
    routes: authRoutes,
  },
  {
    path: '/',
    key: KEYS.INDEX,
    component: RenderMainRoutes,
    routes: mainRoutes,
  },
]
