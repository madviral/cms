import { Component, ReactNode } from 'react'
import { connect } from 'react-redux'
import { History } from 'history'
import { RootState } from '../store/rootReducer'
import { load } from '../modules/common/actions/user'

type PreloadProps = {
  userId?: number
  children: ReactNode
  history: History
  token?: string
  onInit?: VoidFunction
  onAuth?: VoidFunction
  onMain?: VoidFunction
}

class Preload extends Component<PreloadProps> {
  componentDidMount() {
    const { token, onInit, onAuth, onMain } = this.props

    if (!!token) {
      // eslint-disable-line no-extra-boolean-cast
      onInit()
    } else if (!/^\/auth/.test(window.location.pathname)) {
      onAuth()
    }

    if (token && /^\/auth/.test(window.location.pathname)) {
      onMain()
    }
  }

  componentDidUpdate(prevProps) {
    const { token, onMain, onInit, onAuth } = this.props

    if (!prevProps.token && token) {
      onMain()
      onInit()
    }

    if (prevProps.token && !token) {
      onAuth()
    }
  }

  render() {
    const { children, token, userId } = this.props

    if (token && !userId) {
      return null
    }

    return children
  }
}

export default connect(
  (state: RootState) => ({
    userId: state.common.user.id,
    token: state.common.credentials.accessToken,
  }),
  (dispatch, { history }: PreloadProps) => ({
    onInit: () => dispatch(load()),
    onAuth: () => history.replace('/auth/signin'),
    onMain: () => history.replace('/'),
  }),
)(Preload)
