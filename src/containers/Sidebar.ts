import { connect } from 'react-redux'
import { Sidebar } from '../components/Sidebar'

export default connect((state: any) => ({
  fio: state.common.user.fio,
  jobPosition: state.common.user.jobPosition,
}))(Sidebar)
