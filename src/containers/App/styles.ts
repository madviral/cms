import { styled } from 'ui/theme'

export const StyledApp = styled.div`
  height: 100vh;
  width: 100%;
  display: grid;
  grid-template-areas: 'aside main';
  grid-template-rows: 1fr;
  grid-template-columns: auto 1fr;

  .__main {
    grid-area: main;
    display: flex;
    max-height: 100vh;
    overflow-y: auto;
    background: #f6f9ff;
    box-sizing: border-box;
  }

  .__routeWrapper {
    position: relative;
    width: 100%;
  }

  .__route {
    flex: 1 1 100%;
    padding: 40px 30px 16px;
    min-height: 800px;
    height: 100%;
    box-sizing: border-box;
  }
`
