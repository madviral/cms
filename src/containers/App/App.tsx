import React, { FC, useState } from 'react'
import { withRouter, RouteProps } from 'react-router-dom'
import SidebarContext from 'src/context/SidebarContext'
import { AppProps } from './types'
import { StyledApp } from './styles'
import Sidebar from '../Sidebar'
import Modal from '../Modal'

const App: FC<AppProps & RouteProps & any> = ({
  children,
  allRoutes,
  location,
}) => {
  const sidebarToggle = useState(false) // recommended: collapsed, setCollapsed
  const [currentRoute] = allRoutes.filter(route =>
    route.routes.some(
      navRoute =>
        location.pathname.split('/')[1] === navRoute.path.split('/')[1],
    ),
  )

  return (
    <StyledApp>
      <SidebarContext.Provider value={sidebarToggle}>
        <Sidebar
          mainRoutes={allRoutes.filter(route => !route.skip)}
          navigationRoutes={currentRoute ? currentRoute.routes : []}
        />
        <main className='__main'>
          <section className='__routeWrapper'>
            <Modal />
            <div className='__route'>{children}</div>
          </section>
        </main>
      </SidebarContext.Provider>
    </StyledApp>
  )
}

export default withRouter(App)
