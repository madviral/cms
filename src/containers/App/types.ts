import { IPageRoute } from 'src/modules/common/types'

export interface AppProps {
  allRoutes: IPageRoute[]
}
