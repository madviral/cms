import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { RootState } from '../store/rootReducer'
import { App } from './App'
import { RenderRoutes } from 'src/modules/common/components/Routes'

const RenderMainRoutes = ({ routes, roles }) => {
  const accessRoutes = routes.filter(route =>
    roles.some(role => role === route.access),
  )
  const allRoutes = [...accessRoutes]

  if (allRoutes.length > 0) {
    allRoutes.push({
      path: '/',
      key: 'stub',
      exact: true,
      skip: true,
      component: () => <Redirect to={accessRoutes[0].path} />,
      routes: [],
    })
  }

  return (
    <App allRoutes={allRoutes}>
      <RenderRoutes routes={allRoutes} />
    </App>
  )
}

export default connect((state: RootState) => ({
  roles: state.common.user.roles,
}))(RenderMainRoutes)
