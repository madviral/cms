import { connect } from 'react-redux'
import { Modal } from '../components/Modal'
import { RootState } from 'src/store/rootReducer'
import { triggerModal } from '../modules/common/actions/modal'

const mapStateToProps = (state: RootState) => ({
  isOpen: state.common.modal.isOpen,
  type: state.common.modal.type,
  id: state.common.modal.id,
  domNode: state.common.modal.domNode,
})
const mapDispatchToProps = {
  triggerModal,
}
export default connect(mapStateToProps, mapDispatchToProps)(Modal)
