export const stylesCondition = <T>(match: boolean, styles: T): T | {} =>
  match ? styles : {}
