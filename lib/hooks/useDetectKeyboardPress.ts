import { useEffect } from 'react'

interface IKeyboardPress {
  (key: KeyboardEvent['key'], action: () => void)
}

// React.Dispatch<React.setStateAction<any>>

const useDetectKeyboardPress: IKeyboardPress = (key, action) => {
  const handleClickOutside = (event: KeyboardEvent) => {
    if (event.key === key) {
      action()
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', handleClickOutside)
    return () => document.removeEventListener('keydown', handleClickOutside)
  })
}

export default useDetectKeyboardPress
