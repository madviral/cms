import { useEffect } from 'react'

interface IOutsideClick {
  (ref: React.MutableRefObject<HTMLElement>, setStateAction: () => void)
}

const useDetectOutsideClick: IOutsideClick = (ref, setStateAction) => {
  const handleClickOutside = (event: MouseEvent) => {
    if (ref.current && !ref.current.contains(event.target as Node)) {
      setStateAction()
    }
  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside)
    return () => document.removeEventListener('mousedown', handleClickOutside)
  })
}

export default useDetectOutsideClick
