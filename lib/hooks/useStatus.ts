import { useState } from 'react'

export enum StatusStates {
  loading = 'loading',
  empty = 'empty',
  error = 'error',
  success = 'success',
}

type StatesUnion =
  | StatusStates.loading
  | StatusStates.empty
  | StatusStates.error
  | StatusStates.success

export const useStatus = (initialState: StatesUnion) => {
  const [status, setStatus] = useState(initialState)

  const Status = props => {
    return props[status] || null
  }

  return { Status, setStatus }
}
