import { useCallback, useState } from 'react'

export const useTableCbx = items => {
  const [cbxState, setCbxState] = useState({
    checkedList: [],
    indeterminate: false,
    checkAll: false,
  })

  const onChangeCbx = useCallback(
    checkedId => {
      setCbxState(prevState => {
        const { checkedList } = prevState
        const newList = checkedList.some(item => item === checkedId)
          ? checkedList.filter(item => item !== checkedId)
          : [...checkedList, checkedId]

        return {
          checkedList: newList,
          indeterminate: newList.length > 0 && newList.length < items.length,
          checkAll: newList.length === items.length,
        }
      })
    },
    [items.length],
  )

  const onCheckAllCbxChange = useCallback(
    e => {
      setCbxState({
        checkedList: e.target.checked ? items.map(item => item.id) : [],
        indeterminate: false,
        checkAll: e.target.checked,
      })
    },
    [items.length],
  )

  return {
    cbxState,
    onChangeCbx,
    onCheckAllCbxChange,
    setCbxState,
  }
}
