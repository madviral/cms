import common from './common.json'
import shop from './shop.json'
import services from './services.json'
import feed from './feed.json'
import manager from './manager.json'
import errorMessages from './errorMessages.json'

export default {
  common,
  shop,
  services,
  feed,
  manager,
  errorMessages,
}
