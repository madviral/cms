import common from './common.json'
import shop from './shop.json'

export default {
  common,
  shop,
}
