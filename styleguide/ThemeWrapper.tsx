import React, { FC } from 'react'
import { ThemeProvider } from 'src/ui/theme'
import { StaticRouter } from 'react-router-dom'
import 'assets/main.css'
import 'src/settings/i18n'

const ThemeWrapper: FC = ({ children }) => (
  <StaticRouter>
    <ThemeProvider>{children}</ThemeProvider>
  </StaticRouter>
)

export default ThemeWrapper
